<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
    <meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
    <meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
    <meta property="og:site_name" content="e3rafny" />
    <meta name="twitter:card" value="summary" />
    <meta name="twitter:site" value="e3rafny" />
    <meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
    <meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
    <meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
    <meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
    <meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
    <meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
    <meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">
    <title><?php echo $user_info[0]->user_cv_name ; ?></title>

    <!-- Bootstrap -->
  
    <link href="<?php echo base_url("styles/css/web5/bootstrap_".$cv[0]->lang.".css") ?>" rel="stylesheet">
    
 <link href="<?php echo base_url(); ?>styles/css/web5/<?php echo $style_name."_".$cv[0]->lang.".css"; ?>" rel="stylesheet">
  </head>
  <body >
<div class="row header">
  <div class="container">
    <div class="col-md-4">
      <?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
      <img class="personal" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>" >
      <?php } ?>
    </div>
    <div class="col-md-4">
      <div class="row">
<h1 class="name"><?php echo ucwords($user_info[0]->user_cv_name) ; ?> </h1>
<h1 class="nam"><?php echo $user_info[0]->title ; ?> </h1>
</div>
<div class="row">
    <?php foreach ($social as $item) { ?>
  <div class="col-md-1">
    <ul>
       <li class="<?php if($item->name=='facebook'){echo 'social';}elseif($item->name=='twitter'){echo 'tweter';}elseif($item->name=='linkledin'){echo 'linked-in';}elseif($item->name=='google'){echo 'google';}elseif($item->name=='behance'){echo 'behance';} ?>"><a href="<?php echo $item->link ; ?>"></a></li>
    </ul>
  </div>
    <?php } ?>
  
  </div>
    </div>
    <div class="col-md-4">
        <?php if($user_info[0]->adress1 !=''){ ?>
      <div class="row">
        <div class="col-md-1 qqq">
        </div>
        <div class="col-md-10">
          <p class="text"><?php echo $user_info[0]->adress1; ?></p>
        </div>
      </div>
        <?php } ?>
        <?php if($user_info[0]->adress2 !=''){ ?>
      <div class="row">
        <div class="col-md-1 qqq">
        </div>
        <div class="col-md-10">
          <p class="text"><?php echo $user_info[0]->adress2; ?></p>
        </div>
      </div>
        <?php } ?>
      <div class="row">
          <?php foreach ($phones as $item) {?>
        <div class="col-md-1 www">
         
        </div>
        <div class="col-md-10">
          <p class="text"><?php echo $item->code.$item->number ; ?></p>
        </div>
          <?php } ?>   
      </div>
      <div class="row">
        <div class="col-md-1 eee">
         
        </div>
        <div class="col-md-10">
          <p class="text"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="clr">
</div>
<div class="row cv">
  <div class="container">
    <h1><?php echo lang("objective") ;?> </h1>
    <div class="col-md-4 col-md-offset-4">
    <p><?php echo $cv[0]->objective ;?></p>
</div>
  </div>
  </div>
<div class="clr">
</div>
<div class="tab-pics">
 
    <div class="col-md-1">
 <div class="ww2">
      </div>
   
  </div>
  <div class="col-md-10 y">
  <ul class="nav nav-tabs hidden-sm" id="myTab">
  <li class="activ" id="h"><a href="#home" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/obj.png"/></a></li>
  <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
  <li class="skill"><a href="#profile" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/skill.png"/></a></li>
  <?php } ?>
  <?php if( !(empty($education))){ ?>
  <li class="cer"><a href="#messages" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/nn.png"/></a></li>
  <?php } ?>
  <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
  <li class="ceer"><a href="#settings" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/cer.png"/></a></li>
  <?php } ?>
  <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>  
  <li class="exp"><a href="#lang" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/exp.png"/></a></li>
  <?php } ?>
  <?php if( !(empty($langs))){ ?> 
  <li class="lang"><a href="#per" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/lang.png"/></a></li>
  <?php } ?>
  <?php if( !(empty($references))){ ?>
  <li class="ref"><a href="#ref" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/ref.png"/></a></li>
  <?php } ?>
  <li class="rec"><a href="#rec" data-toggle="tab"><img class="n" src="<?php echo base_url(); ?>assets/images/web5/rec.png"/></a></li>

</ul>
  </div>
   <div class="col-md-1">
 <div class="ww">
      </div>
   
  </div>
  <div class="row d">
    <div class="container">
    <div class="col-md-2">
      <p class="p"><?php echo lang("personalinfo") ; ?></p>
    </div>
        <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
     <div class="col-md-1">
      <p class="mv"><?php echo lang("skils") ; ?> </p>
    </div>
        <?php } ?>
        <?php if( !(empty($education))){ ?>
    <div class="col-md-2">
      <p class="mm"><?php echo lang("education") ; ?> </p>
    </div>
        <?php } ?>  
        <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
     <div class="col-md-1">
      <p class="m"><?php if( !(empty($courses))) { echo lang("courses") ;} ?> </p>
    </div>
        <?php } ?> 
        <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>  
     <div class="col-md-1">
      <p class="kh"><?php if( !(empty($experience))) { echo lang("experience") ;} ?> </p>
    </div>
        <?php } ?>  
        <?php if( !(empty($langs))){ ?> 
     <div class="col-md-1">
      <p class="lo"><?php echo lang("langs") ; ?> </p>
    </div>
        <?php } ?>
        <?php if( !(empty($references))){ ?>
     <div class="col-md-1">
      <p class="ms"><?php echo lang("references") ; ?> </p>
    </div>
        <?php } ?>    
    <div class="col-md-1">
      <p class="tw"><?php echo lang('recommendation'); ?> </p>
    </div>
  </div>
</div>
  </div>

   
  <div class="clr">
  </div>
  <div class="row tabs-content">
    <div class="container">
      <div class="tab-content">
  <div class="tab-pane active" id="home">
    <div class="col-md-6">
    <?php if($user_info[0]->nationality !=''){ ?>
    <h1><?php echo lang("nationality");?> : <span><?php echo ucfirst($user_info[0]->nationality); ?></span></h1>
    <?php } ?>
    <?php if($user_info[0]->marital_status !='n'){ 
		if($user_info[0]->marital_status =='s'){
			$m_status=lang("single");
		}elseif($user_info[0]->marital_status =='m'){
			$m_status=lang("married");
		}elseif($user_info[0]->marital_status =='d'){
			$m_status=lang("divercoed");
		}
?>    
    <h1><?php echo lang("marital_rstatus"); ?> : <span><?php echo $m_status ; ?></span></h1>
    <?php } ?>
    <?php if($user_info[0]->license !='no'){ 
		 if($user_info[0]->license =='y'){
			$License=lang("yes");
		}elseif($user_info[0]->license =='n'){
			$License=lang("no");
		}
?>   
    <h1><?php echo lang("License"); ?> : <span><?php echo $License ; ?></span></h1>
<?php } ?>

  </div>
    <div class="col-md-6">
    <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?> 
    <h1><?php echo lang("birth_date") ; ?> : <span><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h1>
    <?php } ?>
    <?php if($user_info[0]->gender !='n'){ 
			if($user_info[0]->gender =='m'){
				$gender=lang("male");
			}elseif($user_info[0]->gender =='f'){
				$gender=lang("female");
			}
	?>      
        <h1><?php echo lang("gender"); ?> : <span><?php echo $gender ; ?></span></h1>
    <?php } ?> 
    <?php if($user_info[0]->military !='n'){ 
		if($user_info[0]->military =='e'){
			$military=lang("exemption");
		}elseif($user_info[0]->military =='c'){
			$military=lang("Complete_service");
		}elseif($user_info[0]->military =='p'){
			$military=lang("Postponed");
		}elseif($user_info[0]->military =='s'){
			$military=lang("Currently_serving");
		}elseif($user_info[0]->military =='d'){
			$military=lang("Doesnt_apply");
		}	 
	?>    
    <h1><?php echo lang("military"); ?> : <span><?php echo $military; ?></span></h1>
    <?php } ?>

  </div>
      </div>
          <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
       <div class="tab-pane" id="profile">
     <?php foreach ($skills as $item) { ?>
     <div class="row">
     <div class="col-md-1 ttt">
     </div>
     <div class="col-md-4">
      <h6 class="sk"><?php echo $item->name ; ?> .. </h6>
     </div>
     <div class="col-md-7 <?php if($item->grade=='i'){ echo "foor"; }elseif($item->grade=='g'){ echo "cir"; }elseif($item->grade=='w'){ echo "tww"; ?> <?php } ?>">
         
     </div>
     </div>
           <?php } ?>  
           
     <?php foreach ($qualifications as $item) { ?>
     <div class="row">
     <div class="col-md-1 ttt">
     </div>
     <div class="col-md-4">
      <h6 class="sk"><?php echo $item->name ; ?> .. </h6>
     </div>
     
     </div>
           <?php } ?>      
           
  </div>
   <?php } ?> 
   <?php if( !(empty($education))){ ?>       
   <div class="tab-pane" id="messages">
    <?php foreach ($education as $item) { ?>   
    <div class="row">
    <div class="col-md-2 col-md-offset-2 ooo">
    </div>
    <div class="col-md-4">
      <p class="q" style="margin-top:54px;">-<?php if( !(empty($item->type)))  echo $item->type ; ?></p>
            <p class="q" >-<?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ; ?>
          
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year." - ".lang($item->to_month) ; ?>
        
	<?php	} ?></p>
      <p class="q" >- <?php if( !(empty($item->name)))  echo $item->name ; ?></p>
      <p class="q" >-<?php if( !(empty($item->desc)))  echo $item->desc ; ?></p>
    </div>
</div>
    <?php } ?>
     </div>
   <?php } ?> 
    <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>        
     <div class="tab-pane" id="settings">
    <?php foreach ($courses as $item) { ?>     
    <div class="row">
    <div class="col-md-2 col-md-offset-2 ppp">
    </div>
    <div class="col-md-4">
      <?php if( !(empty($item->name))){ ?>
      <p class="q" style="margin-top:54px;">-<?php  echo $item->name ; ?></p>
    <?php } ?>  
    <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){ ?>
      <p class="q" >-<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ;  ?></p>
    <?php } ?>  
    <?php if( !(empty($item->institute))){ ?>  
      <p class="q" >-<?php echo $item->institute ; ?></p>
    <?php } ?>  

    </div>
</div>
    <?php } ?> 
    <?php foreach ($certifcates as $item) { ?>     
 <div class="row">
    <div class="col-md-2 col-md-offset-2 ppp">
    </div>
    <div class="col-md-4">
      <?php if( !(empty($item->name))){ ?>
      <p class="q" style="margin-top:54px;">-<?php  echo $item->name ; ?></p>
    <?php } ?>  
    <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){ ?>
      <p class="q" >-<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ;  ?></p>
    <?php } ?>  
    <?php if( !(empty($item->institute))){ ?>  
      <p class="q" >-<?php echo $item->institute ; ?></p>
    <?php } ?>  

    </div>
    <div class="col-md-4">
    </div>
</div>
    <?php } ?>       
     </div>
    <?php } ?> 
    <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>       
  <div class="tab-pane" id="lang">
   <?php if (!(empty($user_info[0]->exp_no))){ ?>
        <span><?php echo lang('no_exp'); ?> : <?php echo $user_info[0]->exp_no ?></span>
   <?php } ?>   
   <?php foreach ($experience as $item) { ?>       
    <div class="row">
    <div class="col-md-2 col-md-offset-2 aaa">
    </div>
    <div class="col-md-4">
      <p class="q" style="margin-top:54px;"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
       
			<?php } ?></p>
            <?php if( ( !(empty($item->name))) || ( !(empty($item->company)))){ ?>
            <p class="q" >< <?php echo $item->name  ; ?><?php if(!empty($item->name) && !empty($item->company)){ ?> .. <?php } ?> <?php echo $item->company ; ?> ></p>
            <?php } ?>
      <?php  if(!empty($item->role)){ ?>  
            <?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
            <p class="q" >< <?php echo $r_item ; ?> ></p>
            <?php } ?>
            <?php } ?>
             <?php if( !(empty($item->description))){ ?>    
      		<p class="q">< <?php echo $item->description ; ?> ></p>
            <?php } ?>

    </div>
</div>
   <?php } ?>  
   <?php foreach ($projects as $item) { ?>         
 <div class="row">
    <div class="col-md-2 col-md-offset-2 aaa">
    </div>
    <div class="col-md-4">
       <p class="q" style="margin-top:54px;"><?php if(!(empty($item->name))){ ?>< <?php echo $item->name  ; ?> ><?php } ?></p>
      <p class="q" ><?php if(!(empty($item->link))){ ?><a style="color:#FFF;font-size:18px;" href="<?php echo $item->link  ; ?>">< <?php echo $item->link  ; ?> ></a> <?php } ?></p>
      <p class="q"><?php if( !(empty($item->description))){ ?>  < <?php echo $item->description ; ?> ><?php } ?>
</p>>

    </div>
     <div class="col-md-4">
    </div>
</div>
   <?php } ?>     
     </div>
    <?php } ?>
    <?php if( !(empty($langs))){ ?>       
     <div class="tab-pane" id="per">
  <?php foreach ($langs as $item) {  ?>
 <div class="row">
    <div class="col-md-2 col-md-offset-2 sss">
    </div>
    <div class="col-md-4">
       <p class="q" style="margin-top:78px;">-<?php echo lang($item->name); ?></p>
           

    </div>
     <div class="col-md-4">
     
 <div class="row <?php if($item->grade=='p'){ echo "on"; }elseif($item->grade=='f'){ echo "tww"; }elseif($item->grade=='g'){ echo "foor"; }elseif($item->grade=='v'){ echo "t"; }elseif($item->grade=='t'){
	echo "cir"; }elseif($item->grade=='fl'){ echo "seven" ; } ?>">
      </div>
    </div>
</div>
  <?php } ?>        
     </div>  
    <?php } ?>
    <?php if( !(empty($references))){ ?>
     <div class="tab-pane" id="ref">
     <?php foreach ($references as $item) { ?>    
    <div class="row">
    <div class="col-md-2 col-md-offset-2 ggg">
    </div>
    <div class="col-md-4">
      <p class="q" style="margin-top:54px;"><?php if( !(empty($item->name))){ echo" - " .$item->name." .. "; }?></p>
      <p class="q" ><?php echo $item->phone ; ?> </p>
      <p class="q" ><?php echo $item->mail ; ?></p>

    </div>
</div>
     <?php } ?> 
</div>
    <?php } ?>
         
   

<div class="tab-pane" id="rec">
  <div id="all">
    <?php foreach ($recommends as $item) { ?>
     <div class="row">
      <div class="col-md-4 col-md-offset-1 f">
      <p class="qq">-<?php echo date("d/m/Y" ,$item->date); ?></p>
      <p class="q">-<?php echo $item->name; ?></p>
      <p class="q"><?php echo $item->job; ?></p>
      <p class="q"><?php echo $item->recommend; ?></p>

     </div>
    
   </div>
    <?php } ?> 
   </div>
 
   <div class="row lin">
    <div class="btn-wrapper">
                            <a class="btn" id="show-form"><?php echo strtoupper(lang('leave_recommendation')); ?></a>
                        </div>
   <div id="form-wrapper">
       <span class="form-success" id="form-success" style="display:none"></span>
       <form class="recom-form">
       <input name="cv_lang" type="hidden" value="<?php echo $cv[0]->lang ; ?>">
       <input name="cv" type="hidden" value="<?php echo $cv[0]->id ; ?>">
       <input name="user_id" type="hidden" value="<?php echo $user_id ; ?>">
       <span class="error_message" id="error_message_name" style="display:none"></span>
       <input type="text" placeholder="<?php echo lang('name') ?>" name="name" class="recom_form" />
       <span class="error_message" id="error_message_job" style="display:none"></span>
       <input type="text" placeholder="<?php echo lang('job') ?>" name="job" class="recom_form" />
       <span class="error_message" id="error_message_recommend" style="display:none"></span>
       <textarea rows="4" placeholder="<?php echo lang('recommend') ?>" name="recommend" class="recom_form" ></textarea>
       <input class="btn-submit" type="button" value="<?php echo lang('recommend') ?>" id="save_btn">
       </form>
  </div><!--form-wrapper-->
                        
    </div>   
      </div>
    </div>
  </div>


      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>styles/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url(); ?>styles/js/bootstrap-tabcollapse.js"></script>

<script>
            $(document).ready(function() {
                $('#show-form').click(function() {
                    $('#form-wrapper').slideToggle("fast");
                                        $('#all').slideToggle("fast");

                });
            });
            
            $('#save_btn').click(function () {
				
				dataString = $(".recom-form").serialize();
				
				var request2 = $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>profile/save",
                     data: dataString,
					 dataType : 'json',
                     success: function(result) {
					
						if(result=='<?php echo lang('done') ?>'){
							
							$('.error_message').hide();
							$('#form-success').show();
							$('#form-success').html("<?php echo lang('done') ?>");
							$('.recom_form').val("");
							//setTimeout(function() { $("#form-success").fadeOut(1500); }, 5000);
							//setTimeout(function() { $("#all").fadeIn(1500); }, 5000);
							 //$("#form-wrapper").slideToggle(1000);
							 //$("#all").slideToggle(1000);
							//$('#form-wrapper').slideToggle("slow");
                   			//$('#all').slideToggle("slow");
							
						}else{
							$('#form-success').hide();
							$('.error_message').hide();
							if(result['name']){
								$('#error_message_name').show();
								$('#error_message_name').html(result['name']);
							}
							if(result['job']){	
								$('#error_message_job').show();
								$('#error_message_job').html(result['job']);
							}
							if(result['recommend']){
								$('#error_message_recommend').show();
								$('#error_message_recommend').html(result['recommend']);
							}
							
						}
						
					},
											
				});
				
   			 });

        </script>
         <script>
        $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
        </script>
        <script>
        $('#myTab').tabCollapse();
        </script>
       

</body>
</html>
  