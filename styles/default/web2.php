<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
    <meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
    <meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
    <meta property="og:site_name" content="e3rafny" />
    <meta name="twitter:card" value="summary" />
    <meta name="twitter:site" value="e3rafny" />
    <meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
    <meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
    <meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
    <meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
    <meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
    <meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
    <meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">
    <title><?php echo $user_info[0]->user_cv_name ; ?></title>

    <!-- Bootstrap -->
   <link href="<?php echo base_url("styles/css/web2/bootstrap_".$cv[0]->lang.".css") ?>" rel="stylesheet">
   <link href="<?php echo base_url(); ?>styles/css/web2/<?php echo $style_name."_".$cv[0]->lang.".css"; ?>" rel="stylesheet">	
    
	
  </head>
  <body >
      <div class="row hh">
      <div class="container">
        <div class="row">

  <div class="col-md-4 border ">
    <div class="row"style="margin-top:-20px;">
      <h6 class="name"><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h6>
      <p class="e-mail"style="margin-top:-10px;padding-bottom:20px;"><i><?php echo $user_info[0]->title ; ?></i></p>
    </div>
<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
    <div class="pic">
      <img class="cam img-responsive" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>">
    </div>
<?php } ?>      
    <div class="row">
        
        <?php foreach ($social as $item) { ?>
      <div class="col-md-1 two">
        <a class="fb" href="<?php echo $item->link ; ?>"><img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'web2_fb.png';}elseif($item->name=='twitter'){echo 'web2_tweet.png';}elseif($item->name=='linkledin'){echo 'web2_in.png';}elseif($item->name=='google'){echo 'web2_google.png';}elseif($item->name=='behance'){echo 'web2_be.png';} ?>" alte="social icon"></a>
      </div>
     <?php } ?>
  
    </div>
    <div class="row">
      <h6 class="mail"><?php echo lang("emaill"); ?>:</h6>
      <p class="e-mail"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>
    </div>
    <div class="row">
       <h6 class="mail"><?php echo lang("phones"); ?>:</h6>
      <?php foreach ($phones as $item) {?>
      <p class="phone"><?php echo $item->code.$item->number ; ?></p>
      <?php } ?>

    </div>
      <?php if($user_info[0]->adress1 !=''){ ?>
  <div class="row">
       <h6 class="mail"><?php echo lang("address") ; ?>:</h6>
      <p class="add"> <?php echo $user_info[0]->adress1; ?>
 </p>
     </div>      
      <?php } ?>
      
      <?php if($user_info[0]->adress2 !=''){ ?>
  <div class="row">
       <h6 class="mail"><?php echo lang("another_address") ; ?>:</h6>
      <p class="add"> <?php echo $user_info[0]->adress2; ?>
 </p>
     </div>      
      <?php } ?>
    </div>
        </div>
      </div>
    </div>
      
<div class="row">
  <div class="container bg">
    <!-- Nav tabs -->
<ul class="nav nav-tabs hidden-sm" id="myTab">
  <li class="active"><a href="#info" data-toggle="tab"><?php echo lang("personalinfo") ; ?></a></li>
  <?php if( !(empty($cv[0]->objective))){ ?>  
  	<li><a href="#home" data-toggle="tab"><?php echo lang("objective") ;?></a></li>
  <?php } ?>
  <?php if( !(empty($education))){ ?>   
  	<li><a href="#profile" data-toggle="tab"><?php echo lang("education") ; ?></a></li>
  <?php } ?>  
  <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>  
  	<li><a href="#messages" data-toggle="tab"><?php if( !(empty($courses))) { echo lang("courses") ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo lang("certifcates") ; ?></a></li>
  <?php } ?>
  <?php if( !(empty($experience)) || !(empty($projects)) ){ ?> 
  	<li><a href="#settings" data-toggle="tab"><?php if( !(empty($experience))) { echo lang("experience") ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo lang("projects") ; ?></a></li>
  <?php } ?>  
  <?php if( !(empty($langs))){ ?> 
  	<li><a href="#lang" data-toggle="tab"><?php echo lang("langs") ; ?></a></li>
 <?php } ?> 
 <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?>   
  <li><a href="#per" data-toggle="tab"><?php echo lang("skils") ; ?></a></li>
 <?php } ?> 
 <?php if( !(empty($references))){ ?> 
  <li><a href="#ref" data-toggle="tab"><?php echo lang("references") ; ?></a></li>
 <?php } ?> 
  <li><a href="#rec" data-toggle="tab"><?php echo lang('recommendation'); ?></a></li>
   

</ul>
</div>
</div>
   

<div class="row">
  <div class="container">
 
  <div class="col-md-4 border">
    <div class="row"style="margin-top:-20px;">
      <h6 class="name"><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h6>
      <p class="e-mail"style="margin-top:-10px;padding-bottom:20px;"><i><?php echo $user_info[0]->title ; ?></i></p>
    </div>
	<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
    <div class="pic">
      <img class="cam img-responsive" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>">
    </div>
    <?php } ?>
    <div class="row">
    <?php foreach ($social as $item) { ?>
      <div class="col-md-1 two">
        <a class="fb" href="<?php echo $item->link ; ?>"><img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'web2_fb.png';}elseif($item->name=='twitter'){echo 'web2_tweet.png';}elseif($item->name=='linkledin'){echo 'web2_in.png';}elseif($item->name=='google'){echo 'web2_google.png';}elseif($item->name=='behance'){echo 'web2_be.png';} ?>" alte="social icon"></a>
      </div>
     <?php } ?>
       
    </div>
    <div class="row">
      <h6 class="mail"><?php echo lang("emaill"); ?> :</h6>
      <p class="e-mail"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>
    </div>
    <div class="row">
       <h6 class="mail"><?php echo lang("phones"); ?> :</h6>
      <?php foreach ($phones as $item) {?>
      <p class="phone"><?php echo $item->code.$item->number ; ?></p>
      <?php } ?>
    </div>
  <?php if($user_info[0]->adress1 !=''){ ?>
  <div class="row">
       <h6 class="mail"><?php echo lang("address") ; ?> :</h6>
      <p class="add"> <?php echo $user_info[0]->adress1; ?></p>
     </div>      
<?php } ?>
<?php if($user_info[0]->adress2 !=''){ ?>
  <div class="row">
       <h6 class="mail"><?php echo lang("another_address") ; ?> :</h6>
      <p class="add"> <?php echo $user_info[0]->adress2; ?></p>
     </div>      
<?php } ?>

    </div>
    <div class="col-md-8">
      <!-- Tab panes -->
<div class="tab-content">
<div class="tab-pane active" id="info">
     <h1 class="title"><?php echo strtoupper(lang("personalinfo")) ; ?></h1>
     <?php if($user_info[0]->nationality !=''){ ?>
    	<h6 class="nation" style="margin-top:36px;"><?php echo lang("nationality");?> : <span class="ans"><?php echo ucfirst($user_info[0]->nationality); ?></span></h6>
     <?php } ?> 
     <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>    
        <h6 class="nation" ><?php echo lang("birth_date") ; ?> : <span class="ans"><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h6>
     <?php } ?>   
     <?php if($user_info[0]->marital_status !='n'){ 
						if($user_info[0]->marital_status =='s'){
							$m_status=lang("single");
						}elseif($user_info[0]->marital_status =='m'){
							$m_status=lang("married");
						}elseif($user_info[0]->marital_status =='d'){
							$m_status=lang("divercoed");
						}
	?>    
    	<h6 class="nation" ><?php echo lang("marital_rstatus"); ?> : <span class="ans"><?php echo $m_status ; ?></span></h6>
    <?php } ?> 
    <?php if($user_info[0]->gender !='n'){ 
							if($user_info[0]->gender =='m'){
								$gender=lang("male");
							}elseif($user_info[0]->gender =='f'){
								$gender=lang("female");
							}
	?>       
    	<h6 class="nation" ><?php echo lang("gender"); ?> : <span class="ans"><?php echo $gender ; ?></span></h6>
    <?php } ?>
    <?php if($user_info[0]->license !='no'){ 
							 if($user_info[0]->license =='y'){
								$License=lang("yes");
							}elseif($user_info[0]->license =='n'){
								$License=lang("no");
							}
	?>   
    	<h6 class="nation" ><?php echo lang("License"); ?> : <span class="ans"><?php echo $License ; ?></span></h6>
    <?php } ?>
    <?php if($user_info[0]->military !='n'){ 
							if($user_info[0]->military =='e'){
								$military=lang("exemption");
							}elseif($user_info[0]->military =='c'){
								$military=lang("Complete_service");
							}elseif($user_info[0]->military =='p'){
								$military=lang("Postponed");
							}elseif($user_info[0]->military =='s'){
								$military=lang("Currently_serving");
							}elseif($user_info[0]->military =='d'){
								$military=lang("Doesnt_apply");
							}	 
	?>  
    	<h6 class="nation" ><?php echo lang("military"); ?> : <span class="ans"><?php echo $military; ?></span></h6>
    <?php } ?>    

  </div>
  
  <?php if( !(empty($cv[0]->objective))){ ?>  
  <div class="tab-pane" id="home">
    <h1 class="title"><?php echo strtoupper(lang("objective")); ?></h1>
    <p class="text"><?php echo $cv[0]->objective ;?></p>
  </div>
  <?php } ?>
  <?php if( !(empty($education))){ ?>   
  <div class="tab-pane" id="profile">
  <h1 class="title"><?php echo strtoupper(lang("education")); ?></h1>
  <?php foreach ($education as $item) { ?>
     <h6 class="ed"><?php if( !(empty($item->type)))  echo strtoupper($item->type) ; ?></h6>
     <span class="m"><?php if( !(empty($item->name)))  echo $item->name ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo "(". $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year .")" ; ?>
          
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo "(". $item->from_month."/".$item->from_year." - ".lang($item->to_month).")" ; ?>
        
	<?php	} ?></span>
    <p class="text2"><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>
  <?php } ?>  
    
  </div>
<?php } ?> 

 <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>  
  <div class="tab-pane" id="messages">
     <h1 class="title"><?php if (!(empty($courses))){ echo strtoupper(lang("courses"));  } ?><?php if (!(empty($courses)) && !(empty($certifcates))){ echo "&nbsp;".lang("and")."&nbsp;" ; } if (!(empty($certifcates))) echo strtoupper(lang("certifcates")); ?></h1>
     <?php foreach ($courses as $item) { ?>
     <h6 class="ed"><?php echo strtoupper($item->name) ; ?></h6>
     <span class="m"><?php if( !(empty($item->institute))) echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
             
				<?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?>
              
				<?php } ?></span>
     <?php } ?>
     
     <?php foreach ($certifcates as $item) { ?>
     <h6 class="ed"><?php echo strtoupper($item->name) ; ?></h6>
     <span class="m"><?php if( !(empty($item->institute))) echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
             
				<?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?>
              
				<?php } ?></span>
     <?php } ?>
    
  </div>
  <?php } ?>
  
  <?php if( !(empty($experience)) || !(empty($projects)) ){ ?> 
  <div class="tab-pane" id="settings">
    <h1 class="title"><?php if (!(empty($experience))){ echo strtoupper(lang("experience")); } ?><?php if (!(empty($experience)) && !(empty($projects))){  echo "&nbsp;".lang("and")."&nbsp;" ; } if (!(empty($projects))) echo strtoupper(lang("projects")); ?></h1>
    <?php if (!(empty($user_info[0]->exp_no))){ ?>
    <span class="exp"><?php echo "(".lang('no_exp')." : ".$user_info[0]->exp_no.")" ; ?></span>
    <?php } ?>
    <?php foreach ($experience as $item) { ?>   
     <h6 class="ed"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
       
			<?php } ?></h6>
     <span class="m"><?php if( !(empty($item->company)))  echo $item->company ; ?> <?php if( !(empty($item->name)))  echo "(".$item->name .")" ; ?></span>
     <?php  if(!empty($item->role)){ ?>  
     <ul style="list-style:dashed">
     <?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
     <li class="text2"><?php echo $r_item ; ?></li>
     <?php } ?>
</ul>
<?php } ?>
    <p class="text2"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
    <?php } ?>
    <?php foreach ($projects as $item) { ?>
     <h6 class="ed"><?php if( !(empty($item->name)))  echo $item->name  ; ?></h6>
     <?php if(!empty($item->link )){ ?>
     <span class="m"><a href="<?php echo $item->link ; ?>" style="color: #40A9E0;font-size: 18px;" ><?php echo $item->link ; ?></a> </span>
     <?php } ?>
    <p class="text2"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
    <?php } ?>
  
  </div>
  <?php } ?>
  <?php if( !(empty($langs))){ ?> 
  <div class="tab-pane" id="lang">
    <h1 class="title"><?php echo strtoupper(lang("langs")); ?></h1>
    <?php foreach ($langs as $item) {  ?>
     <h6 class="ed"><?php echo lang($item->name); ?></h6>
     <span class="m"><b><?php if($item->grade=='p') echo lang('poor');
										elseif($item->grade=='f') echo lang('fair');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='v') echo lang('very_good');
										elseif($item->grade=='fl') echo lang('fluent');
										elseif($item->grade=='t') echo lang('tongue'); ?></b> </span>
     <?php } ?>
    
  </div>
  <?php } ?>
  <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?>   
     <div class="tab-pane" id="per">
     <?php if( !(empty($qualifications))){ ?> 
    <h1 class="title"><?php echo strtoupper(lang("qualifications")); ?></h1>
    <?php foreach ($qualifications as $item) { ?>
     <h6 class="ed"><?php echo $item->name ; ?></h6>
    <?php } ?>
    <?php } ?>
    <?php if( !(empty($skills))){ ?> 
     <h1 class="title"><?php echo strtoupper(lang("skills")); ?></h1>
     <?php foreach ($skills as $item) { ?>
     <h6 class="ed"><?php echo $item->name ; ?></h6>
     <span class="m"><b><?php if($item->grade=='i') echo lang('interemdate');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='w') echo lang('weak'); ?></b> </span>
    <?php } ?>                                    
    <?php } ?>
  </div>
  <?php } ?>
  <?php if( !(empty($references))){ ?>
  <div class="tab-pane" id="ref">
    <h1 class="title"><?php echo strtoupper(lang("references")); ?></h1>
     <?php foreach ($references as $item) { ?>
     <h6 class="ed"><?php if( !(empty($item->name))) echo $item->name; ?></h6>
     <?php if( !(empty($item->mail))){ ?>
     <span class="m"><b><?php echo $item->mail ; ?> </b></span>
     <br>
     <?php } ?>
     <span class="m"><b><?php if( !(empty($item->phone))) echo $item->phone ; ?></b> </span>
    <?php } ?> 
      </div>
  <?php } ?>    
 <div class="tab-pane" id="rec">
  <div class="row">
    
    <h1 class="title" style="font-size:30px !important;font-weight:bold;"><?php echo strtoupper(lang('recommendation')); ?></h1>
  </div>
  <div class="row">
    <div class="btn-wrapper">
                            <a class="btn" style="margin-bottom:10px;" id="show-form"><?php echo lang('leave_recommendation'); ?> </a>
                        </div>
    <div id="form-wrapper">
                            <span id="form-success" class="form-success" style="display:none;"><?php echo lang('done') ?></span>
                            <form class="recom-form">
                                <input name="cv_lang" type="hidden" value="<?php echo $cv[0]->lang ; ?>">
                                <input name="cv" type="hidden" value="<?php echo $cv[0]->id ; ?>">
                                <input name="user_id" type="hidden" value="<?php echo $user_id ; ?>">
                                <span id="error_message_name" class="error_message" style="display:none" ></span>
                                <input type="text" placeholder="<?php echo lang('name') ?>" name="name" />
                                <span id="error_message_job" class="error_message" style="display:none" ></span>
                                <input type="text"  placeholder="<?php echo lang('job') ?>" name="job" />
                                <span id="error_message_recommend" class="error_message" style="display:none" ></span>
                                <textarea rows="4" placeholder="<?php echo lang('recommend') ?>" name="recommend" ></textarea>
                                <input class="btn-submit" id="save_btn" type="button" value="<?php echo lang('recommend') ?>">
                            </form>
                        </div><!--form-wrapper-->
                        
  </div>
  
    <div class="row">
    <?php foreach($recommends as $item){ ?> 
     <h6 class="ed"><?php echo date("d/m/Y" ,$item->date); ?></h6>
          <span class="m"><b><?php echo $item->name; ?> </b></span>
<br>
     <span class="m"><b><i><?php echo $item->job; ?></i> </b></span>
         <p class="text2"><?php echo $item->recommend; ?></p>
 	<?php } ?>
  </div>
   
      </div>
      
</div>
  </div>
</div>
    </div>
  </div>
</div>
<div class="row footer">
  <div class="col-md-4 col-md-offset-5">
    <p class="copy">copywrite@ e3rafny.com</p>
  </div>
</div>



  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>styles/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>styles/js/bootstrap-tabcollapse.js"></script>

   <script>
            $(document).ready(function() {
                $('#show-form').click(function() {
                    $('#form-wrapper').slideToggle("fast");
                });
            });
			

$('#save_btn').click(function () {
				
				dataString = $(".recom-form").serialize();
				
				var request2 = $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>profile/save",
                     data: dataString,
					 dataType : 'json',
                     success: function(result) {
					
						if(result=='<?php echo lang('done') ?>'){
							
							$('.error_message').hide();
							$('#form-success').show().html("<?php echo lang('done') ?>");
							//setTimeout(function() { $("#form-success").fadeOut(1500); }, 5000);
							
						}else{
							$('#form-success').hide();
							$('.error_message').hide();
							if(result['name'])
								$('#error_message_name').show().html(result['name']);
							if(result['job'])	
								$('#error_message_job').show().html(result['job']);
							if(result['recommend'])
								$('#error_message_recommend').show().html(result['recommend']);
							
						}
						
					},
											
				});
				
   			 });
        </script>
         <script>
        $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
        </script>
        <script>
        $('#myTab').tabCollapse();
        </script>
  </body>
 
  
   
   
  
 
  </html>