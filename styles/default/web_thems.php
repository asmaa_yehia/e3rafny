<br class="clr">
<h1 class="blue-title"><?php echo lang('choose_web');?></h1>

<br class="clr">
<form name="form1" action="<?php echo site_url(); ?>/thems/save_webthem" method="post" enctype="multipart/form-data">
    <div class="step8">
        <input type="hidden" name="cv_id" id="cv_id" value="<?php echo $cv->id; ?>"  />
        <?php
        $result = $this->session->flashdata('result');
        if (!empty($result)) {
            echo $result;
        }
        ?>
        <ul id="shapes" class="themes">
            <?php foreach ($query as $item) { ?>
                <li class="shape" id="shape<?php echo $item['sendlogs']->id; ?>" data-bind="<?php echo $item['sendlogs']->id; ?>" ><a href="#"><img src="<?php echo base_url(); ?>assets/images/<?php echo $item['sendlogs']->img; ?>" id="img<?php echo $item['sendlogs']->id; ?>"  class="img" /></a><p class="title"></p></li>
                <ul class="cv-colors">
                    <?php foreach ($item['web_thems_child'] as $item2) { ?>
                        <li>  
                            <div onclick="get_img(<?php echo $item2->id; ?>,<?php echo $item2->parent; ?>, '<?php echo $item2->img; ?>')" style="background-color: <?php echo $item2->color; ?>;width: 30px;height: 30px;"></div>
                        </li>
                    <?php } ?>
                </ul>
            <?php } ?>
        </ul>
        <input type="hidden" name="id" id="id" value="<?php
        if ($cv->style_them == 0 || $cv->style_them == "")
            echo $web_themsdefault[0]->id;
        else
            echo $cv->style_them;
        ?>"  />
    </div>
    <br class="clr">
    <!-- buttons start -->
    <div class="buttons">
        <input type="submit" name="button_prev" id="button_prev" class="previous-step" value="<?php echo lang('last'); ?>" />
     

        <div class="save-next">
            <input type="submit" value="<?php echo lang('save'); ?>" class="save"  name="button_save">
            <input type="submit" class="next-step" value="<?php echo lang('next'); ?>" name="button_next"></a>
        </div>
    </div>

</form>
<script>
    $(document).ready(function() {
        $('.cv-colors li div').click(function() {
            $('.cv-colors li div').removeClass('selected');
            $(this).addClass('selected');
        });
        var them = $('#id').val();
        //  alert(them);
        var cv_id = $('#cv_id').val();
        $('#shape' + them + ' .img').css({'border': '1px solid black'});


        var request = $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>thems/get_them",
            data: "data=" + them + "&cv_id=" + cv_id,
            success: function(result) {

                data = result.split("*");
                $("#img" + data[1]).attr('src', '<?php echo base_url(); ?>assets/images/' + data[2]);
                $('#shape' + data[1] + ' .img').css({'border': '1px solid black'});
                $("#shape" + data[1]).attr('data-bind', data[0]);
            },
        });





    });
//function get_id(style_id){
    $("#shapes .shape").click(function() {
        //alert($(this).attr('data-bind'));
        var style_id = $(this).attr('data-bind');
        $('#id').val(style_id);
        $("li .img").css("border", "0px");
        $('#shape' + style_id + ' .img').css({'border': '1px solid black'});

        //preview web them 
        var cv_id = $('#cv_id').val();
        var style_id = $('#id').val();
        if (style_id)
            window.open("<?php echo site_url(); ?>/thems/preview_webthem/" + cv_id + "/" + style_id);
        else
            window.open("<?php echo site_url(); ?>/thems/preview_webthem/" + cv_id);

    });

    function get_img(id, parent, img) {

        $("#img" + parent).attr('src', '<?php echo base_url(); ?>assets/images/' + img);
        $("#shape" + parent).attr('data-bind', id);
        $('#id').val(id);

    }

</script>