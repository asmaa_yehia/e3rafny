﻿<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>E3rfny</title>


        <!-- my styleshhets -->
        <link href="<?php echo base_url(); ?>styles/css/stylesheet.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>styles/fonts/stylesheet.css" rel="stylesheet">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="top-bar"></div>

        <div class="page-wrap">
            <div class="container">

                <!-- logo and navbar -->
                <div class="nav-logo-area login-top-area">

                    <div class="left-area">
                        <!-- lang -->
                        <div class="user-lang-area">

                            <ul>
                                <li><a href="<?php echo base_url(); ?>ar" class="ar"><?php echo lang('arabic'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>en" class="en"><?php echo lang('english'); ?></a></li>
                            </ul>
                        </div>
                        <!-- lang end -->

                    </div>
                </div>
                <!-- logo and navbar end-->

                <div class="clr"></div>

                <a href="#" class="login-logo"><img src="images/logo-large.png" alt=""></a>

                <!-- content -->
                <div class="steps-content">

                    <div class="register-login">
                        <form name="form1" action="<?php echo site_url(); ?>/auth/facebook_add" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="fb_id" value="<?php echo $user_profile['id']; ?>" >
                            <table class="register-form">
                                <tr>
                                    <td><input type="text" tabindex="1" name="first_name" value="<?php echo set_value('first_name',$user_profile['first_name']); ?>" placeholder="<?php echo lang('first_name'); ?>">
                                        <div class="form-msg-error-text"><?php echo form_error('first_name'); ?></div></td>

                                </tr>
                                <tr>
                                    <td><input type="text" tabindex="2" name="last_name" value="<?php echo set_value('last_name',$user_profile['last_name']); ?>" placeholder="<?php echo lang('last_name'); ?>">
                                        <div class="form-msg-error-text"><?php echo form_error('last_name'); ?></div></td>

                                </tr>
                                <tr>
                                    <td><input type="email" tabindex="3" name="email" value="<?php echo set_value('email',$user_profile['email']); ?>" placeholder="<?php echo lang('email'); ?>" readonly>
                                        <div class="form-msg-error-text"><?php echo form_error('email'); ?></div></td>

                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <input type="submit" name="button" id="button" value="<?php echo lang('register_us'); ?>" /></td>
                                </tr>
                            </table>
                        </form>
                    </div>

                </div>
                <!-- content end -->

            </div>
            <!-- container end -->
        </div>
        <!-- page wrap end -->


        <!-- footer wrapper start -->
        <div class="footer-wrapper">

        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="js/jquery-1.7.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery-ui.js"></script>

    </body>

    <script type="text/javascript">
        $(document).ready(function() {
            $('li.msg').click(function() {
                $('li.msg .panel').slideToggle();
            });

            $('li.ntf').click(function() {
                $('li.ntf .panel').slideToggle();
            });

            $('.panel').click(function(event) {
                event.stopPropagation();
            });
        });
    </script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $("#tabs").tabs({show: {effect: "fade", duration: 800}});
        });

    </script>

</html>
</body>
</html>   
