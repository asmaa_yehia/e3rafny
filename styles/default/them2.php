<!DOCTYPE>
<html>
<head>
	 
      <link href="<?php echo base_url("styles/css/style2_".$cv->lang.".css") ?>" rel="stylesheet">
  </head>
  <body>
  	

  	<div class="name">
  		<div class="container">
        <?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
        <div class="imgg">
         
          <img src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="square" width="100px" height="100px">
        
        </div><?php } ?> 
  			<h1><b><?php echo ucwords($user_info[0]->user_cv_name) ; ?></b></h1>
  			<h3><?php echo $user_info[0]->title ; ?></h3>
            
            <div class="clr">
            </div>
            <?php if( !(empty($phones))){ ?>
            <div class="phone-no">
        
          <h4 class="e"><b><?php echo lang("phones"); ?>:</b></h4>
          <?php
				foreach ($phones as $item) {
		  ?>
          <p><?php echo $item->code.$item->number ; ?></p>
          <?php } ?>
         
         
         </div>
         <?php } ?>
         <?php if($user_info[0]->adress2 !='' || $user_info[0]->adress1 !=''){ ?>
         <div class="adress">
        <?php if($user_info[0]->adress1 !=''){ ?>
          <h4 class="add"><b><?php echo lang("address") ; ?>:</b></h4>
        <p class="center"><?php echo $user_info[0]->adress1; ?></p>
        <?php } ?>
		<?php if($user_info[0]->adress2 !=''){ ?>
<h4 class="add"><b><?php echo lang("another_address") ; ?>:</b></h4>
          <p class="center"> <?php echo $user_info[0]->adress2; ?></p>
        <?php } ?>
         </div>
         <?php } ?>
         
     <div class="adres">
          <h4 class="email"><b><?php echo lang("emaill") ;?>:</b></h4>
          <p class="mail"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>
          
         </div>
  		</div>
  	</div>
  
    <div class="link">
      <div class="containe">
      <?php foreach ($social as $item) { ?>
      <div class="links">

          <img class="g" src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'fbb.png';}elseif($item->name=='twitter'){echo 'twet.png';}elseif($item->name=='linkledin'){echo 'in.png';}elseif($item->name=='google'){echo 'go.png';}elseif($item->name=='behance'){echo 'behancee.png';} ?>">
          <a class="fb" href="<?php if( !(empty($item->link)))  echo $item->link ; ?>"><?php if( !(empty($item->link)))  echo $item->link ; ?></a>
          
       </div>
      <?php } ?>
            
          
                  </div>
    </div>
  </div>
  <?php if( !(empty($cv->objective))){ ?>  
  <div class="objective">
    <div class="container">
      <h2><?php echo strtoupper(lang("objective")) ;?></h2>
      <p><?php echo $cv->objective ;?></p>
      <hr>
    </div>
  </div>
 <?php } ?> 
  <div class="clr">
    </div>
  <div class="person">
    <div class="container">
      <h2><?php echo strtoupper(lang("personal_info")) ; ?></h2>
      <div class="person-wraper">
      <div class="personal">
      <?php if($user_info[0]->nationality !=''){ ?>
        <p><?php echo lang("nationality") ;?>: <?php echo $user_info[0]->nationality; ?></p>
      <?php } ?> 
      <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>   
        <p><?php echo lang("birth_date") ; ?>: <?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></p>
      <?php } ?> 
      <?php if($user_info[0]->license !='no'){ 
				 if($user_info[0]->license =='y'){
					$License=lang("yes");
				}elseif($user_info[0]->license =='n'){
					$License=lang("no");
				}
		?>   
        <p><?php echo lang("License") ; ?>: <?php echo $License ; ?></p>
        <?php } ?>
      </div>
      <div class="personal" style="width:45% !important;">
       <?php if($user_info[0]->gender !='n'){ 
				if($user_info[0]->gender =='m'){
					$gender=lang("male");
				}elseif($user_info[0]->gender =='f'){
					$gender=lang("female");
				}
		?>    
        <p><?php echo lang("gender") ; ?>: <?php echo $gender ; ?></p>
        <?php } ?>
        <?php if($user_info[0]->marital_status !='n'){ 
		 	if($user_info[0]->marital_status =='s'){
				$m_status=lang("single");
			}elseif($user_info[0]->marital_status =='m'){
				$m_status=lang("married");
			}elseif($user_info[0]->marital_status =='d'){
				$m_status=lang("divercoed");
			}
		 ?>  
        <p><?php echo lang("marital_rstatus"); ?> : <?php echo $m_status ; ?></p>
        <?php } ?>
        <?php if($user_info[0]->military !='n'){ 
				if($user_info[0]->military =='e'){
					$military=lang("exemption");
				}elseif($user_info[0]->military =='c'){
					$military=lang("Complete_service");
				}elseif($user_info[0]->military =='p'){
					$military=lang("Postponed");
				}elseif($user_info[0]->military =='s'){
					$military=lang("Currently_serving");
				}elseif($user_info[0]->military =='d'){
					$military=lang("Doesnt_apply");
				}	 
			?> 
        <p><?php echo lang("military") ; ?>: <?php echo $military; ?></p>
        <?php } ?>
      </div>
    
    </div>
  </div>
  </div>
  <div class="clr">
    </div>
      <hr>
      <?php if( !(empty($education))){ ?>
  <div class="education">
    <div class="container">
      <h2><?php echo strtoupper(lang("education")); ?></h2>
      <?php foreach ($education as $item) { ?>
      <h6><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        <div class="date">
			<?php echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year .")" ; ?>
       </div>     
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        <div class="date">
		<?php	echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month).")" ; ?>
        </div>    
	<?php	} ?></h6>
      
      <h6><?php if( !(empty($item->name)))  echo $item->name ; ?> </h6>
      <h6><?php if( !(empty($item->type)))  echo $item->type ; ?> </h6>
      
      <p style="margin-bottom:20px;"><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>
	<?php } ?>	   
        <hr>
    </div>
  </div>
    <?php } ?>
  <div class="clr">
    </div>
    <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
  <div class="courses">
    <div class="container">
      <h2><?php if( !(empty($courses))) { echo strtoupper(lang("courses")) ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo strtoupper(lang("certifcates")) ; ?></h2>
      <?php foreach ($courses as $item) { ?>
      <h6><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                <div class="date">
				<?php echo "(".$item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year.")" ; ?>
                </div>
				<?php } ?></h6>
      
      <h6><?php  echo strtoupper($item->name) ; ?></h6>
    
           <h6 style="margin-bottom:40px !important;"><?php if( !(empty($item->institute))) echo $item->institute ; ?> </h6>
		<?php } ?>
        
        <?php foreach ($certifcates as $item) { ?>
      <h6><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                <div class="date">
				<?php echo "( ".$item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year." )" ; ?>
                </div>
				<?php } ?></h6>
      
      <h6><?php  echo strtoupper($item->name) ; ?></h6>
    
           <h6 ><?php if( !(empty($item->institute))) echo $item->institute ; ?> </h6>
		<?php } ?>
        
        
        <hr>
    </div>
  </div>
<?php } ?>
  <div class="clr">
    </div>
    <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
   <div class="experience">
    <div class="container">
      <h2><?php if( !(empty($experience))) { echo strtoupper(lang("experience")) ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo strtoupper(lang("projects")) ; ?><br><span style="font-weight:lighter !important; font-size:15px !important;"><?php if (!(empty($user_info[0]->exp_no)))
                            {
                                echo lang('no_exp')."&nbsp;"."(".$user_info[0]->exp_no."&nbsp;".")";
                            }
                            ?></span></h2>
      <?php foreach ($experience as $item) { ?>
      <h6><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        <div class="date">
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        </div>
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    <div class="date">
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
            </div>
			<?php } ?></h6>
      
      <h6 ><b><?php if( !(empty($item->company)))  echo $item->company ; ?></b> </h6>
      <h6 ><?php if( !(empty($item->name)))  echo "(".$item->name .")" ; ?></h6>
 <?php if(!empty($item->role)){ ?>          
<div class="list" style="padding-bottom:0px;margin-bottom:0px;">
<?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
  <p  >-<?php echo $r_item ; ?> </p>
<?php } ?>   
</div>
<?php } ?>
<p style="padding-top:-10px;margin-top:0px;padding-bottom:10px;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
<?php } ?>


<?php foreach ($projects as $item) { ?>
      
      <h6><?php if( !(empty($item->name)))  echo "(".$item->name .")" ; ?></h6>
      <h6 style="padding-bottom:0px;margin-bottom:0px;"><?php if( !(empty($item->link))) ?><a style="font-size:16px; color: #404041;"href="<?php echo $item->link ; ?>"><?php echo $item->link ; ?></a></h6>
          
<p style="padding-top:0px;margin-top:0px;padding-bottom:10px;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
<?php } ?>


        <hr>
    </div>
  </div>
  <?php } ?>
  <div class="clr">
    </div>
    <?php if( !(empty($langs))){ ?> 
  <div class="languages">
    <div class="container">
      <h2><?php echo strtoupper(lang("langs")) ; ?></h2>
      <?php foreach ($langs as $item) {  ?>
      <h6 style="text-align:center;"><?php if( !(empty($item->name))) echo lang($item->name)."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='p') echo lang('poor');
				elseif($item->grade=='f') echo lang('fair');
				elseif($item->grade=='g') echo lang('good');
				elseif($item->grade=='v') echo lang('very_good');
				elseif($item->grade=='fl') echo lang('fluent');
				elseif($item->grade=='t') echo lang('tongue'); ?></h6>
      <?php } ?>
           
        <hr>
    </div>
  </div>
  <?php } ?>
  <div class="clr">

    </div>
   <?php if( !(empty($skills))){ ?> 
  <div class="t-skills">
    <div class="container">
      <h2><?php echo strtoupper(lang("skills")) ; ?></h2>
      <?php foreach ($skills as $item) { ?>
      <div class="t-skil" >
      <ul class="t-skill" style="text-align:center;list-style:none;">
       <li style="text-align:center;"> <?php if( !(empty($item->name))) echo $item->name."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='i') echo lang('interemdate');
																				elseif($item->grade=='g') echo lang('good');
																				elseif($item->grade=='w') echo lang('weak'); ?></li>
       </ul>
       </div>
      
       <?php } ?>
    </div>
  </div>
  
  <div class="clr">
    </div>
     <hr>
     <?php } ?>
     <?php if( !(empty($qualifications))){ ?>
  <div class="p-skills">
    <div class="container">
      <h2><?php echo strtoupper(lang("qualifications")) ; ?></h2>
      <?php foreach ($qualifications as $item) { ?>
            <div class="p-skil">

      <ul class="p-skill">
        
        <li><?php echo $item->name ; ?></li>
        </ul>
        </div>
      <?php } ?>
        
        

    
      </div>


        
    </div>
  </div>
  
  <div class="clr">
    </div>
    <hr>
    <?php } ?>
    <?php if( !(empty($references))){ ?>
   <div class="refrences">
    <div class="container">
    
      <h2><?php echo strtoupper(lang("references")) ; ?></h2>
      <?php foreach ($references as $item) { ?>
      <h6 class="bold"><b><?php if( !(empty($item->name))) echo $item->name; ?></b></h6>
      <h6><?php if( !(empty($item->mail))) echo $item->mail ; ?> </h6>
       <h6 style="margin-bottom:30px;"><?php if( !(empty($item->phone))) echo $item->phone ; ?> </h6>
     <?php } ?>          


        <hr>
    </div>
  </div>
  <?php } ?>
  </body>
  </html>