<h1 class="blue-title"><?php echo lang('refrences'); ?></h1>
<br class="clr">
<form action="<?php echo site_url(); ?>/add_cv/update_ref/<?php echo $cv_id; ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" value="<?php echo $percent; ?>" name="percent" />
    <input type="hidden" value="<?php echo $cv_id; ?>" name="cv_id" />
    <input type="hidden" value="<?php echo count($items); ?>" name="count" id="count" />
    <div class="step4">
        <div id="refrences" >
            <div class="content">
                <?php if (count($items) == 0) { ?>
                    <div id="cont" class="courses" >

                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td><label><?php echo lang('ref_name'); ?>:</label><input type="text" name="ref_name[]" value="" /></td>
                                <td><label> <?php echo lang('mail'); ?>:</label><input type="mail" name="ref_mail[]" value="" /></td>
                                <td><label><?php echo lang('phone'); ?> :</label><input type="text" name="ref_phone[]"value="" /></td>
                            </tr>
                        </table>
                    </div>
                <?php } ?>
                <?php foreach ($items as $key => $item) { ?>
                    <div id="cont<?php echo $key; ?>" class="courses-added">
                        <input type="hidden" name="id[]" value="<?php echo $item->id; ?>" id="id<?php echo $key; ?>" />
                        <div class="courses">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td> <label> <?php echo lang('ref_name'); ?>:</label><input type="text" name="ref_name[]" value="<?php echo $item->name; ?>"></td>
                                    <td> <label> <?php echo lang('mail'); ?>:</label><input type="text" name="ref_mail[]" value="<?php echo $item->mail; ?>" /></td>
                                    <td> <label><?php echo lang('phone'); ?>:</label><input type="text" name="ref_phone[]"value="<?php echo $item->phone; ?>" /></td>

                                </tr>
                            </table>

                        </div>

                        <?php if ($key > 0) { ?>
                            <a href="javascript:void(0);" onClick="del_unit(<?php echo $key; ?>)" class="delete"></a>
                        <?php } ?>

                    </div><!--end of content-->
                <?php } ?>
            </div>
        </div><!--end of content-->
        <br class="clr">
        <a href="javascript:void(0);" onClick="add_unit()" class="add"></a>
    </div>
    <br class="clr">
    <div class="buttons">
        <a href="<?php echo site_url(); ?>/add_cv/back_lang/<?php echo $cv_id; ?>" class="previous-step"><?php echo lang('last'); ?></a>

        <div class="save-next">
            <input type="submit" value="<?php echo lang('save'); ?>" class="save" name="save_button">
            <input type="submit" class="next-step" value="<?php echo lang('next'); ?>" name="next_button" ></a>
        </div>
    </div>

</div><!--end of refrences-->
</form>

<script>
    var count = $("#count").val();
    //var count = 0;
    function add_unit() {
        var cv_id = $("#cv").val();
        var cont_edu = '<div id="cont' + count + '" class="courses-added">';
        cont_edu += '<div class="courses"><table cellpadding="0" cellspacing="0"><tr><td> <label> <?php echo lang('ref_name'); ?>:</label><input type="text" name="ref_name[]"/></td><td><label><?php echo lang('mail'); ?>:</label><input type="text" name="ref_mail[]"/></td><td><label><?php echo lang('phone'); ?>:</label><input type="text" name="ref_phone[]"/></td></tr></table></div>';
        cont_edu += '<a href="javascript:void(0);" onClick="del_unit(' + count + ')" class="delete"></a></div>';
        $(".content").append(cont_edu);
        count++;
    }
    //
    function del_unit(id) {
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            unit_id = $("#id" + id).val();
            $("#cont" + id).remove();
        } else
            return false;
    }
</script>