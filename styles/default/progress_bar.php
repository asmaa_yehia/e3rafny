<?php
if (isset($start))
    $percent = 0;
else {
    $this->session->set_userdata('percent', $percent);
    $percent = $this->session->userdata('percent');
}
?>
<div class="personal-info data-area">
    <div class="progress-bar">
        <div class="done"  style="width:<?php echo $percent; ?>%"><span><?php echo $percent; ?>%</span></div>
    </div>
    <!-- hint -->
    <?php if ($percent <= 35) { ?>
        <p class="progress-hint">*<?php echo lang('publish_error'); ?></p>
    <?php } ?>
    <!-- hint end -->
    <br class="clr"> 

