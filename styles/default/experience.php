<h1 class="blue-title"><?php echo lang('exp'); ?></h1>
<br class="clr">
<form action="<?php echo site_url(); ?>/add_cv/update_exp/<?php echo $cv_id; ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" value="<?php echo $percent; ?>" name="percent" />
    <input type="hidden" name="cv_id" value="<?php echo $cv_id; ?>" id="cv" />
    <input type="hidden" name="exp_count" value="<?php echo count($items); ?>" id="exp_count" />
    <input type="hidden" name="proj_count" value="<?php echo count($projects); ?>" id="proj_count" />
    <div class="step5">
        <div id="expereince">
            <div class="content">
                <?php if (count($items) == 0) { ?>
                    <div class="experince-years">
                        <label><?php echo lang('exp_year'); ?>:</label>
                        <input type="text" name="exp_no" value="" />
                    </div>
                    <div id="cont" class="experience">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td><label><?php echo lang('company'); ?>:</label><input type="text" name="exp_comp[]" value="" /></td>
                                <td><label> <?php echo lang('job'); ?>:</label><input type="text" name="exp_name[]" value="" /></td>

                                <td><label><?php echo lang('from'); ?>:</label>
                                    <select name="from_month[]" class="small-select">
                                        <option value="" ><?php echo lang('month') ?> </option>
                                        <option value="01" ><?php echo lang('January'); ?></option>
                                        <option value="02" ><?php echo lang('February'); ?></option>
                                        <option value="03" ><?php echo lang('March'); ?></option>
                                        <option value="04" ><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06" ><?php echo lang('June'); ?></option>
                                        <option value="07"><?php echo lang('July'); ?></option>
                                        <option value="08" ><?php echo lang('August'); ?></option>
                                        <option value="09" ><?php echo lang('September'); ?></option>
                                        <option value="10" ><?php echo lang('October'); ?></option>
                                        <option value="11"><?php echo lang('November'); ?></option>
                                        <option value="12"><?php echo lang('December'); ?></option>
                                    </select>
                                    <select name="from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>" ><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                                <td>

                                    <label>  <?php echo lang('to'); ?>:</label>
                                    <select name="to_month[]"  class=" to_month0 small-select" onchange="dim_year(0)">
                                        <option value="" ><?php echo lang('month') ?> </option>
                                        <option value="now" ><?php echo lang('now') ?> </option>
                                        <option value="01" ><?php echo lang('January'); ?></option>
                                        <option value="02" ><?php echo lang('February'); ?></option>
                                        <option value="03" ><?php echo lang('March'); ?></option>
                                        <option value="04" ><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06" ><?php echo lang('June'); ?></option>
                                        <option value="07" ><?php echo lang('July'); ?></option>
                                        <option value="08"><?php echo lang('August'); ?></option>
                                        <option value="09"><?php echo lang('September'); ?></option>
                                        <option value="10" ><?php echo lang('October'); ?></option>
                                        <option value="11" ><?php echo lang('November'); ?></option>
                                        <option value="12" ><?php echo lang('December'); ?></option>
                                    </select>
                                    <select name="to_year[]"  class="to_year0 small-select"><option value="" ><?php echo lang('year') ?> </option>
                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>" ><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?></select> 
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div id="roles0">
                                        <label>  <?php echo lang('role'); ?>:</label><input type="text" name="role0[]" value="" class="wide-input" />
                                    </div>
                                    <a href="javascript:void(0)" onclick="add_role(this.id)" id="0" >add Role</a>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">   
                                    <label><?php echo lang('desc'); ?></label>
                                    <textarea name="desc[]"></textarea>
                                </td>
                            </tr>
                        </table>
                    </div><!--end of cont0-->
                <?php } else { ?>
                    <div class="experince-years">
                        <label><?php echo lang('exp_year'); ?>:</label>
                        <input type="text" name="exp_no" value="<?php echo $exp_no; ?>" />
                    </div>
                <?php } ?>

                <?php foreach ($items as $key => $item) { ?>

                    <div id="cont<?php echo $key; ?>" class="experience-added">
                        <input type="hidden" name="cv_id" value="<?php echo $cv_id; ?>" id="cv" />
                        <input type="hidden" name="id[]" value="<?php echo $item->id; ?>"  id="id<?php echo $key; ?>" />
                        <div class="experience">

                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><label><?php echo lang('company'); ?>:</label><input type="text" name="exp_comp[]" value="<?php echo $item->company; ?>" /></td>
                                    <td><label><?php echo lang('job'); ?>:</label><input type="text" name="exp_name[]" value="<?php echo $item->name; ?>" /></td>
                                    <td><label><?php echo lang('from'); ?>:</label>
                                        <select name="from_month[]" class="small-select">
                                            <option value="" ><?php echo lang('month') ?> </option>
                                            <option value="01" <?php
                                            if ($item->from_month == "01") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('January'); ?></option>
                                            <option value="02" <?php
                                            if ($item->from_month == "02") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('February'); ?></option>
                                            <option value="03" <?php
                                            if ($item->from_month == "03") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('March'); ?></option>
                                            <option value="04" <?php
                                            if ($item->from_month == "04") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('April'); ?></option>
                                            <option value="05" <?php
                                            if ($item->from_month == "05") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('May'); ?></option>
                                            <option value="06" <?php
                                            if ($item->from_month == "06") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('June'); ?></option>
                                            <option value="07" <?php
                                            if ($item->from_month == "07") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('July'); ?></option>
                                            <option value="08" <?php
                                            if ($item->from_month == "08") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('August'); ?></option>
                                            <option value="09" <?php
                                            if ($item->from_month == "09") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('September'); ?></option>
                                            <option value="10" <?php
                                            if ($item->from_month == "10") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('October'); ?></option>
                                            <option value="11" <?php
                                            if ($item->from_month == "11") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('November'); ?></option>
                                            <option value="12" <?php
                                            if ($item->from_month == "12") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('December'); ?></option>
                                        </select>
                                        <select name="from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                            <?php
                                            for ($y = 1975; $y <= year; $y++) {
                                                ?>
                                                <option value="<?php echo $y; ?>"<?php if ($item->from_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select></td>

                                    <td><label> <?php echo lang('to') ?>:</label>
                                        <select name="to_month[]" class="to_month<?php echo $key; ?> small-select" onchange="dim_year(<?php echo $key; ?>)">
                                            <option value="" ><?php echo lang('month') ?> </option>
                                            <option value="now" <?php
                                            if ($item->to_month == "now") {
                                                echo "selected";
                                            }
                                            ?> ><?php echo lang('now') ?> </option>
                                            <option value="01" <?php
                                            if ($item->to_month == "01") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('January'); ?></option>
                                            <option value="02" <?php
                                            if ($item->to_month == "02") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('February'); ?></option>
                                            <option value="03" <?php
                                            if ($item->to_month == "03") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('March'); ?></option>
                                            <option value="04" <?php
                                            if ($item->to_month == "04") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('April'); ?></option>
                                            <option value="05" <?php
                                            if ($item->to_month == "05") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('May'); ?></option>
                                            <option value="06" <?php
                                            if ($item->to_month == "06") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('June'); ?></option>
                                            <option value="07" <?php
                                            if ($item->to_month == "07") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('July'); ?></option>
                                            <option value="08" <?php
                                            if ($item->to_month == "08") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('August'); ?></option>
                                            <option value="09" <?php
                                            if ($item->to_month == "09") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('September'); ?></option>
                                            <option value="10" <?php
                                            if ($item->to_month == "10") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('October'); ?></option>
                                            <option value="11" <?php
                                            if ($item->to_month == "11") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('November'); ?></option>
                                            <option value="12" <?php
                                            if ($item->to_month == "12") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('December'); ?></option>
                                        </select>
                                        <select name="to_year[]" class="to_year<?php echo $key; ?> small-select"><option value="" ><?php echo lang('year') ?> </option>
                                            <?php
                                            for ($y = 1975; $y <= year; $y++) {
                                                ?>
                                                <option value="<?php echo $y; ?>"<?php if ($item->to_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                <?php
                                            }
                                            ?></select> 
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <div id="roles<?php echo $key; ?>">
                                            <?php
                                            if (isset($item->role) && $item->role != '') {
                                                $roles = unserialize($item->role);
                                                foreach ($roles as $role) {
                                                    ?>
                                                    <label> <?php echo lang('role'); ?>:</label>
                                                    <input type="text" name="role<?php echo $key; ?>[]" value="<?php echo ($role); ?>" class="wide-input" />
                                                    <?php
                                                }
                                            } else {
                                                ?> <input type="text" name="role<?php echo $key; ?>[]" value="" class="wide-input" /><?php } ?>



                                        </div>
                                        <a href="javascript:void(0)" onclick="add_role(this.id)" id="<?php echo $key; ?>" >add Role</a>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="4">
                                        <label> <?php echo lang('desc'); ?>:</label>
                                        <textarea name="desc[]"><?php echo $item->description; ?></textarea>

                                    </td>
                                </tr>
                            </table>
                        </div><!-- end of experience--->
                        <?php if ($key > 0) { ?>    
                            <a href="javascript:void(0);" onClick="del_unit(<?php echo $key; ?>)" class="delete"></a>
                        <?php } ?>
                    </div>

                <?php } ?>

            </div><!--end of content-->

            <br class="clr">
            <a href="javascript:void(0);" onClick="add_unit()" class="add"></a>
        </div>
        <!----------project section------------------>
        <br class="clr">
        <hr class="sep">
        <br class="clr">
        <h1 class="blue-title"><?php echo lang('projects'); ?></h1>
        <div id="prjects">


            <?php if (count($projects) == 0) { ?>
                <div id="cont" class="prev-projects">
                    <table cellpadding="0" cellspacing="0">
                        <tr>
                            <td><label><?php echo lang('name'); ?>:</label><input type="text" name="proj_name[]"  value=""/></td>
                        <div class="error"><?php echo form_error('proj_name[]'); ?></div>
                        <td><label> <?php echo lang('link'); ?>:</label><input  type="url" name="proj_link[]" value="" /></td></tr>
                        <tr>
                            <td colspan="2">  <label><?php echo lang('desc'); ?>:</label> <textarea name="proj_desc[]"></textarea></td></tr>
                    </table>
                </div><!--end of cont projects-->
            <?php } ?>
            <?php foreach ($projects as $key => $item) { ?>

                <div id="cont<?php echo $key; ?>" class="prev-projects-added">
                    <input type="hidden" name="proj_id[]" value="<?php echo $item->id; ?>"  id="id<?php echo $key; ?>" />
                    <div class="prev-projects">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td> <label><?php echo lang('name'); ?>:</label><input type="text" name="proj_name[]"  value="<?php echo $item->name; ?>"/></td>
       <!-- <div class="error"><?php echo form_error('proj_name[0]'); ?></div>-->
                                <td> <label><?php echo lang('link'); ?>:</label><input  type="url" name="proj_link[]" value="<?php echo $item->link; ?>" /></td>
                            </tr>
                            <tr > <td colspan="2"><label><?php echo lang('desc'); ?>:</label> <textarea name="proj_desc[]"><?php echo $item->description; ?></textarea></td>
                            </tr>
                        </table>

                    </div>
                    <?php if ($key > 0) { ?>
                        <a href="javascript:void(0);" onClick="del_proj(<?php echo $key; ?>)" class="delete"></a>
                    <?php } ?>
                </div><!--end of projects-->
            <?php } ?>
        </div>
        <br class="clr">
        <a href="javascript:void(0);" onClick="add_project()" class="add"></a>

    </div>
    <br class="clr">
    <!-- buttons start -->
    <div class="buttons">
        <a href="<?php echo site_url(); ?>/add_cv/back_course/<?php echo $cv_id; ?>" class="previous-step"><?php echo lang('last'); ?></a>

        <div class="save-next">
            <input type="submit" name="save_button" value="<?php echo lang('save'); ?>" class="save">
            <input type="submit"  name="next_button"class="next-step" value="<?php echo lang('next'); ?>"></a>
        </div>
    </div>

</form>
<script>
    var count = $("#exp_count").val();
    var proj_count = $("#proj_count").val();
    function add_unit() {
        count++;
        var d = new Date();
        var year = d.getFullYear();
        var cv_id = $("#cv").val();
        var type;
        var cont_edu = '<div id="cont' + count + '" class="experience-added"> <div class="experience"><table cellpadding="0" cellspacing="0"><tr>';
        cont_edu += '<td><label><?php echo lang('company'); ?>:</label><input type="text" name="exp_comp[]" value="" /></td><td><label><?php echo lang('job'); ?>:</label><input type="text" name="exp_name[]"/></td><td><label> <?php echo lang('from'); ?>:</label>';
        cont_edu += '<select name="from_month[]" class="small-select"><option value="" ><?php echo lang('month') ?> </option><option value="01" ><?php echo lang('January'); ?></option><option value="02" ><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select><select name="from_year[]" class="small-select">';
        for (var ty = 1975; ty <= year; ty++) {
            cont_edu += '<option value="' + ty + '">' + ty + '</option>';
        }
        cont_edu += '</select></td><td><label><?php echo lang('to'); ?>:</label>';
        cont_edu += '</select><select name="to_month[]" class=" to_month' + count + ' small-select" onchange="dim_year(' + count + ')"><option value="" ><?php echo lang('month') ?> </option> <option value="now" ><?php echo lang('now') ?> </option><option value="01" ><?php echo lang('January'); ?></option><option value="02"><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select><select name="to_year[]" class="to_year' + count + ' small-select"><option value="" ><?php echo lang('year') ?> </option>';
        for (var ty = 1975; ty <= year; ty++) {
            cont_edu += '<option value="' + ty + '">' + ty + '</option>';
        }
        cont_edu += '</select></td><tr ><td colspan="4"><div id="roles' + count + '"><label><?php echo lang('role'); ?>:</label><input type="text" name="role' + count + '[]" class="wide-input"/></div><a href="javascript:void(0)" onclick="add_role(this.id)" id=' + count + ' >add Role</a></td></tr>';
        cont_edu += '<tr><td colspan="4"><label><?php echo lang('desc'); ?>:</label><textarea name="desc[]"></textarea>';
        cont_edu += '</td></tr></table></div>';
        cont_edu += '<a href="javascript:void(0);" onClick="del_unit(' + count + ')" class="delete"></a></div>';
        //
        $("#expereince .content").append(cont_edu);

    }
//
    function del_unit(id) {
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            unit_id = $("#id" + id).val();
            $("#cont" + id).remove();
        } else
            return false;

    }
//add projectin runtime
    function add_project() {
        var cv_id = $("#cv").val();
        var cont_edu = '<div id="cont' + proj_count + '" class="prev-projects-added"> <div class="prev-projects"><table cellpadding="0" cellspacing="0">';
        cont_edu += '<tr><td><label><?php echo lang('name'); ?>:</label><input type="text" name="proj_name[]"/></td><td><label><?php echo lang('link'); ?></label><input type="url" name="proj_link[]"/></td></tr><tr><td colspan="2"><label><?php echo lang('desc'); ?>:</label><textarea name="proj_desc[]"></textarea></td></tr></table></div>';
        cont_edu += '<a href="javascript:void(0);" onClick="del_unit(' + proj_count + ')" class="delete"></a></div>';
        //
        $("#prjects").append(cont_edu);
        count++;
    }
//
    function del_proj(id) {
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            unit_id = $("#prjects #cont" + id + " #id" + id).val();
            $("#prjects #cont" + id).remove();
        } else
            return false;
    }
    function hide_date(id) {
        $("#to" + id).show();
        if ($("#ck_box" + id).attr('checked')) {

            $("#to" + id).hide();
        }
    }
    function add_role(role_id) {
        $("#roles" + role_id).append('<td><label><?php echo lang('role'); ?>:</label><input type="text" name="role' + role_id + '[]" id="' + role_id + '" class="wide-input" /><a href="javascript:void(0)"  onclick="del_role">-</a></td>');
    }
    //
    //disable year when value of mont now
    function dim_year(ct) {
        $("#foo").remove();
        $(".to_year" + ct).val('');
        $(".to_year" + ct).prop("disabled", false);
        if ($(".to_month" + ct).val() == "now") {
            $(".to_year" + ct).prop("disabled", true);
            $('<input>').attr({type: 'hidden', name: 'to_year[]', id: 'foo'}).appendTo("#cont" + ct);
        }

    }

    $(document).ready(function() {
        for (ct = 0; ct < count; ct++) {
            if ($(".to_month" + ct).val() == "now") {
                $(".to_year" + ct).prop("disabled", true);
                $('<input>').attr({type: 'hidden', name: 'to_year[]', id: 'foo'}).appendTo("#cont" + ct);
            }
        }
    });
</script>