<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
    <meta property="og:type" content="article" />
    <meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
    <meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
    <meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
    <meta property="og:site_name" content="e3rafny" />
    <meta name="twitter:card" value="summary" />
    <meta name="twitter:site" value="e3rafny" />
    <meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
    <meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
    <meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
    <meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
    <meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
    <meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
    <meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">
    <title><?php echo $user_info[0]->user_cv_name ; ?></title>

    <!-- Bootstrap -->
    
    <link href="<?php echo base_url(); ?>styles/css/web4/<?php echo $style_name."_".$cv[0]->lang.".css"; ?>" rel="stylesheet">
    <link href="<?php echo base_url("styles/css/web4/bootstrap_".$cv[0]->lang.".css") ?>" rel="stylesheet">
   
  </head>
  <body >
<div class="row blue">
<div class="container">
<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
<div class="col-md-3 pic">
<img class="img-responsive" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>" >
</div>
<?php } ?>
<div class="col-md-4 col-md-offset-1">
<div class="row">
<h1 class="name"><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
<h1 class="position"><?php echo $user_info[0]->title ; ?></h1>
</div>
    
<div class"row">
<?php foreach ($social as $item) { ?>
<div class="col-md-1 margin">
<ul>
<li class="<?php if($item->name=='facebook'){echo 'fb';}elseif($item->name=='twitter'){echo 'twet';}elseif($item->name=='linkledin'){echo 'linn';}elseif($item->name=='google'){echo 'g';}elseif($item->name=='behance'){echo 'be';} ?>"><a href="<?php echo $item->link ; ?>"></a></li>
</ul>
</div>
<?php } ?>

</div>
</div>
<div class="col-md-4 br">
<div class="row">

<?php foreach ($phones as $item) {?>
    <div class="col-md-1 mob">
</div>
<div class="col-md-4">
<p class="no"><?php echo $item->code.$item->number ; ?></p>
</div>
<?php } ?>
    
</div>
<div class="row">
<div class="col-md-1 env">
</div>
<div class="col-md-8">
<p class="mail"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>

</div>
</div>
<?php if($user_info[0]->adress1 !=''){ ?>
<div class="row">
<div class="col-md-1 adress">
</div>
<div class="col-md-8">
<p class="mail"><?php echo $user_info[0]->adress1; ?></p>

</div>
</div>
<?php } ?>
<?php if($user_info[0]->adress2 !=''){ ?>
<div class="row">
<div class="col-md-1 adress">
</div>
<div class="col-md-8">
<p class="mail"><?php echo $user_info[0]->adress2; ?></p>

</div>
</div>
<?php } ?>
</div>
</div>
</div>
<div class="clr">
</div>
<div class="row content">

<div class="col-md-4">
<hr>


    <!-- Nav tabs -->
<ul class="nav nav-tabs hidden-sm" id="myTab">
  <li class="active"><a class="q" href="#home" data-toggle="tab"><?php echo lang("objective") ;?></a></li>
  <li><a href="#profile" data-toggle="tab"><?php echo lang("personalinfo") ; ?></a></li>
  <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
  	<li><a href="#messages" data-toggle="tab"><?php echo lang("skils") ; ?></a></li>
 <?php } ?>  
 <?php if( !(empty($education))){ ?> 
  	<li><a href="#settings" data-toggle="tab"><?php echo lang("education") ; ?></a></li>
 <?php } ?> 
 <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
 	<li><a href="#lang" data-toggle="tab"><?php if( !(empty($courses))) { echo lang("courses") ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo lang("certifcates") ; ?></a></li>
 <?php } ?> 
 <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>   
  	<li><a href="#tec" data-toggle="tab"><?php if( !(empty($experience))) { echo lang("experience") ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo lang("projects") ; ?></a></li>
  <?php } ?>
  <?php if( !(empty($langs))){ ?> 
  	<li><a href="#per" data-toggle="tab"><?php echo lang("langs") ; ?></a></li>
  <?php } ?>
  <?php if( !(empty($references))){ ?>
 	 <li><a href="#ref" data-toggle="tab"><?php echo lang("references") ; ?></a></li>
  <?php } ?>
  <li><a href="#rec" data-toggle="tab"><?php echo lang('recommendation'); ?></a></li>

</ul>
<hr style="margin-top:20px !important;">
</div>
<div class="col-md-8 white">
 
 <div class="tab-content">
  <div class="tab-pane active" id="home">
  <?php echo $cv[0]->objective ;?>
      </div>
   <div class="tab-pane" id="profile">
<div class="row">
<div class="col-md-6">
<?php if($user_info[0]->nationality !=''){ ?>
<h6 class="title"><?php echo lang("nationality");?> : <span class="nation"><?php echo ucfirst($user_info[0]->nationality); ?></span></h6>
<?php } ?>
<?php if($user_info[0]->marital_status !='n'){ 
		if($user_info[0]->marital_status =='s'){
			$m_status=lang("single");
		}elseif($user_info[0]->marital_status =='m'){
			$m_status=lang("married");
		}elseif($user_info[0]->marital_status =='d'){
			$m_status=lang("divercoed");
		}
?>    
<h6 class="titl"><?php echo lang("marital_rstatus"); ?> : <span class="nation"><?php echo $m_status ; ?></span></h6>
<?php } ?>
 <?php if($user_info[0]->license !='no'){ 
		 if($user_info[0]->license =='y'){
			$License=lang("yes");
		}elseif($user_info[0]->license =='n'){
			$License=lang("no");
		}
?>   
<h6 class="titl"><?php echo lang("License"); ?> :<span class="nation"><?php echo $License ; ?></span></h6>
<?php } ?>

</div>
<div class="col-md-6">
<?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>   
<h6 class="title"><?php echo lang("birth_date") ; ?> : <span class="nation"><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->gender !='n'){ 
		if($user_info[0]->gender =='m'){
			$gender=lang("male");
		}elseif($user_info[0]->gender =='f'){
			$gender=lang("female");
		}
?>    
<h6 class="titl"><?php echo lang("gender"); ?> : <span class="nation"><?php echo $gender ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->military !='n'){ 
		if($user_info[0]->military =='e'){
			$military=lang("exemption");
		}elseif($user_info[0]->military =='c'){
			$military=lang("Complete_service");
		}elseif($user_info[0]->military =='p'){
			$military=lang("Postponed");
		}elseif($user_info[0]->military =='s'){
			$military=lang("Currently_serving");
		}elseif($user_info[0]->military =='d'){
			$military=lang("Doesnt_apply");
		}	 
					 ?>  
<h6 class="titl"><?php echo lang("military"); ?> : <span class="nation"><?php echo $military; ?></span></h6>
<?php } ?>

</div>
</div>  
  </div>
  <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
   <div class="tab-pane" id="messages">
   <?php if( !(empty($skills))){ ?> 
   <?php //echo strtoupper(lang("skills")); ?>
   <?php foreach ($skills as $item) { ?>
    <div class="row">
     <div class="col-md-1 select">
     </div>
     <div class="col-md-4">
      <h6 class="skill"><?php echo $item->name ; ?> .. </h6>
     </div>
     <div class="col-md-6">
     <?php if($item->grade=='i') $perc=50;
			elseif($item->grade=='g') $perc=100;
			elseif($item->grade=='w') $perc=20; ?>
      <progress value="<?php echo $perc ; ?>" max="100">
</progress>
     </div>
     </div>
     <?php } ?> 
     <?php } ?>
     
    <?php if( !(empty($qualifications))){ ?> 
   <?php //echo strtoupper(lang("qualifications")); ?>
   <?php foreach ($qualifications as $item) { ?>
    <div class="row">
     <div class="col-md-1 select">
     </div>
     <div class="col-md-4">
      <h6 class="skill"><?php echo $item->name ; ?> .. </h6>
     </div>
     
     </div>
     <?php } ?> 
     <?php } ?> 
     
      
  </div>
  <?php } ?>
  <?php if( !(empty($education))){ ?>
  <div class="tab-pane" id="settings">
  <?php foreach ($education as $item) { ?>
    <div class="row">
  <div class="row">
      <div class="col-md-1 select">
     
     </div>
     <div class="col-md-4">
         <h6 class="skill">-<?php if( !(empty($item->type)))  echo $item->type ; ?></h6></div>
    <div cass="col-md-6" style="margin-top:50px;">
     </div>   
  </div>
        <div class="row">
      <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
      <h6 class="skil">-<?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year  ; ?>
          
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo  $item->from_month."/".$item->from_year." - ".lang($item->to_month) ; ?>
        
	<?php	} ?></h6></div><div cass="col-md-6" style="margin-top:50px;">
     </div>   
  </div>
         <div class="row">
      <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
         <h6 class="skil">-<?php if( !(empty($item->name)))  echo $item->name ; ?></h6></div><div cass="col-md-6" style="margin-top:50px;">
     </div>   
  </div>
         <div class="row">
      <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
      <h6 class="skil">-<?php if( !(empty($item->desc))) echo $item->desc ; ?></h6></div><div cass="col-md-6" style="margin-top:50px;">
     </div>   
  </div>

     </div>
    
   
    <?php } ?>
   
  </div>
  <?php } ?>
  <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
  <div class="tab-pane" id="lang">
  <?php foreach ($courses as $item) { ?>
   <div class="row">
       <?php if( !(empty($item->name))){ ?>
   <div class="row">
   <div class="col-md-1 select">
     
     </div>
     <div class="col-md-4">
     
      <h6 class="skill">-<?php  echo $item->name ; ?></h6>
      </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
     <?php } ?> 
      <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){ ?>
      <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
       <h6 class="skil">-<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ;  ?></h6></div>
<div cass="col-md-6" style="margin-top:50px;">
</div>
   </div>     
 <?php } ?>
      <?php if( !(empty($item->institute))){ ?> 
          <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
      <h6 class="skil">-<?php echo $item->institute ; ?></h6></div>
      <div cass="col-md-6" style="margin-top:50px;">
      </div>
   </div>    
      <?php } ?>

    
    
   </div>
   <?php } ?>
     <?php foreach ($certifcates as $item) { ?>
   <div class="row">
  
     
     <?php if( !(empty($item->name))){ ?>
       <div class="row">
   <div class="col-md-1 select">
     
     </div>
     <div class="col-md-4">
      <h6 class="skill">-<?php  echo $item->name ; ?></h6>
      </div>
      <div cass="col-md-6" style="margin-top:50px;">
      </div>
   </div>    
     <?php } ?> 
      <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){ ?>
      <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
       <h6 class="skil">-<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ;  ?></h6>
       </div>
      <div cass="col-md-6" style="margin-top:50px;">
      </div>
   </div>   
      <?php } ?>
      <?php if( !(empty($item->institute))){ ?>
       <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
      <h6 class="skil">-<?php echo $item->institute ; ?></h6>
      <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
       </div>
      <?php } ?>

     
   </div>
   <?php } ?>
  </div>
  <?php } ?>
  <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>      
  <div class="tab-pane" id="tec">
<?php if (!(empty($user_info[0]->exp_no))){ ?>
        <span><?php echo lang('no_exp'); ?> : <?php echo $user_info[0]->exp_no ?></span>
   <?php } ?> 

<?php foreach ($experience as $item) { ?> 
     <div class="row">
    <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) || ($item->to_month=='now' )){ ?>     
    <div class="row">
   <div class="col-md-1 select">
     
     </div>
     <div class="col-md-4">
          <h6 class="skill"style="margin-bottom:45px;">-
              <?php if(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ 
                  echo  $item->from_month."/".$item->from_year." - ".lang($item->to_month) ;}else{ 
                  echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year  ;} ?> </h6>

     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
   <?php } ?>  
   <?php if(!(empty($item->name))){ ?>       
   <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">

            <h6 class="skil"style="margin-bottom:45px;">-<?php echo $item->name  ; ?> .. </h6>
     
     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
   <?php } ?>  
  <?php if(!(empty($item->company))){ ?>       
    <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">

   <h6 class="skil"style="margin-bottom:45px;"><?php echo $item->company  ; ?> : </h6>
     
     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
  <?php } ?>  
  <?php  if(!empty($item->role)){ ?>  
  <?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>       
   <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
     
      <h6 class="skil">-<?php echo $r_item ; ?> .. </h6>

     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
  <?php } ?> 
  <?php } ?>
   <?php if( !(empty($item->description))){ ?>        
    <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
     
   <h6 class="skil">-<?php echo $item->description ; ?> : </h6>

     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
   <?php } ?>
   </div>
<?php } ?>
        
 <?php foreach ($projects as $item) { ?> 
     <div class="row">  
   <?php if(!(empty($item->name))){ ?>       
   <div class="row">
   <div class="col-md-1 select">
     
     </div>
     <div class="col-md-4">
          <h6 class="skill"style="margin-bottom:45px;">-<?php echo $item->name  ; ?> </h6>

           
    
     

     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
   <?php } ?>  
  <?php if(!(empty($item->link))){ ?>       
    <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">

   <h6 class="skil"style="margin-bottom:45px;"><a style="color:#323A45;" href="<?php echo $item->link  ; ?>">< <?php echo $item->link  ; ?> ></a> : </h6>
     
     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
  <?php } ?>  
 
   <?php if( !(empty($item->description))){ ?>        
    <div class="row">
   <div class="col-md-1 selec">
     
     </div>
     <div class="col-md-4">
     
   <h6 class="skil">-<?php echo $item->description ; ?> : </h6>

     </div>
     <div cass="col-md-6" style="margin-top:50px;">
     </div>
   </div>
   <?php } ?>
   </div>
<?php } ?>       
    
   </div>
  <?php } ?>  
  <?php if( !(empty($langs))){ ?>    
    <div class="tab-pane" id="per">
    <?php foreach ($langs as $item) {  ?>    
   <div class="row">
     <div class="col-md-1 select">
     </div>
     <div class="col-md-4">
      <h6 class="skill" >-<?php echo lang($item->name); ?> </h6>
     </div>
     <div class="col-md-6">
      <?php if($item->grade=='p') $percnt=20;
		elseif($item->grade=='f') $percnt=40;
			elseif($item->grade=='g') $percnt=60;
			elseif($item->grade=='v') $percnt=80;
			elseif($item->grade=='t') $percnt=100;
			elseif($item->grade=='fl') $percnt=90;
	?>   
      <progress value="<?php echo $percnt ?>" max="100">
</progress>
     </div>
     </div>
    <?php } ?>   
     
  </div>
  <?php } ?> 
  <?php if( !(empty($references))){ ?>   
   <div class="tab-pane" id="ref">
    <?php foreach ($references as $item) { ?>   
    <div class="row">
   <div class="col-md-1 sele">
            
     </div>
     <div class="col-md-4">
      <h6 class="skll"><?php if( !(empty($item->name))){ echo" - " .$item->name." : "; }?> </h6>
      <?php if( !(empty($item->phone))) { ?>
      <h6 class="ski"><?php echo $item->phone ; ?>  </h6>
     <?php } ?> 
     <?php if( !(empty($item->mail))) { ?>
      <h6 class="ski"> <?php echo $item->mail ; ?></h6>
    <?php } ?>  

     </div>
    
   </div>
    <?php } ?>  
         
      </div>
  <?php } ?>
<div class="tab-pane" id="rec">
  <div id="all">
    <?php if( !(empty($recommends))){ ?>
  <?php foreach ($recommends as $item) { ?>  
    <div class="row">
   <div class="col-md-1 sele">
            
     </div>
     <div class="col-md-4">
      <h6 class="skll"><?php echo date("d/m/Y" ,$item->date); ?> </h6>
            <h6 class="ski">-<?php echo $item->name; ?></h6>
            <h6 class="ski"><?php echo $item->job; ?></h6>

      <h6 class="ski"><?php echo $item->recommend; ?></h6>

     </div>
    
   </div>
  <?php } ?>
    <?php } ?>     
 </div>
   <div class="row lin">
    <div class="btn-wrapper">
                            <a class="btn" id="show-form"><?php echo strtoupper(lang('leave_recommendation')); ?> </a>
                        </div>
    <div id="form-wrapper">
                            <span class="form-success" id="form-success" style="display:none"></span>
                            <form class="recom-form">
                            <input name="cv_lang" type="hidden" value="<?php echo $cv[0]->lang ; ?>">
                            <input name="cv" type="hidden" value="<?php echo $cv[0]->id ; ?>">
                            <input name="user_id" type="hidden" value="<?php echo $user_id ; ?>">
                                <span class="error_message" id="error_message_name" style="display:none"></span>
                                <input type="text" placeholder="<?php echo lang('name') ?>" name="name" class="recom_form" />
                                <span class="error_message" id="error_message_job" style="display:none"></span>
                                <input type="text" placeholder="<?php echo lang('job') ?>" name="job" class="recom_form" />
                                <span class="error_message" id="error_message_recommend" style="display:none"></span>
                                <textarea rows="4" placeholder="<?php echo lang('recommend') ?>" name="recommend" class="recom_form" ></textarea>
                                <input class="btn-submit" type="button" value="<?php echo lang('recommend') ?>" id="save_btn">
                            </form>
                        </div><!--form-wrapper-->
                        
    </div>   
      </div>
</div>
</div>
<div class="clr">
</div>
<div class="row footer">
  </div>





  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>styles/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>styles/css/web4/bootstrap-tabcollapse.js"></script>

    <script>
            $(document).ready(function() {
                $('#show-form').click(function() {
                    $('#form-wrapper').slideToggle("fast");
                                        $('#all').slideToggle("fast");

                });
            });
            
            $('#save_btn').click(function () {
				
				dataString = $(".recom-form").serialize();
				
				var request2 = $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>profile/save",
                     data: dataString,
					 dataType : 'json',
                     success: function(result) {
					
						if(result=='<?php echo lang('done') ?>'){
							
							$('.error_message').hide();
							$('#form-success').show();
							$('#form-success').html("<?php echo lang('done') ?>");
							$('.recom_form').val("");
							
							//$('#form-wrapper').slideToggle("slow");
                   			//$('#all').slideToggle("slow");
							
						}else{
							$('#form-success').hide();
							$('.error_message').hide();
							if(result['name']){
								$('#error_message_name').show();
								$('#error_message_name').html(result['name']);
							}
							if(result['job']){	
								$('#error_message_job').show();
								$('#error_message_job').html(result['job']);
							}
							if(result['recommend']){
								$('#error_message_recommend').show();
								$('#error_message_recommend').html(result['recommend']);
							}
							
						}
						
					},
											
				});
				
   			 });

        </script>
        <script>
        $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
        </script>
        <script>
        $('#myTab').tabCollapse();
        </script>
        <script>
        $(document).ready(function() { var NewHeight = $('#profile-img').width(); $('#profile-img').css('height', NewHeight); }); 
</script>
  </body>
 
  
   
   
  
 
  </html>