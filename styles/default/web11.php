<!DOCTYPE>
<html lang="en">
<head>
<link href='<?php echo base_url("styles/css/pdf_style_".LANG.".css") ?>' rel="stylesheet" type="text/css" />
<meta charset="UTF-8">
<meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
<meta property="og:type" content="article" />
<meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
<meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
<meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
<meta property="og:site_name" content="e3rafny" />
<meta name="twitter:card" value="summary" />
<meta name="twitter:site" value="e3rafny" />
<meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
<meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
<meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
<meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
<meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
<meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
<meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">
<title></title>
</head>
<body>
<div class="title">
<?php echo $cv[0]->title ; ?>
</div>
<div class="name">
<?php echo lang("name").": ". $user_info[0]->user_cv_name ; ?>
</div>
<?php  if($user_info[0]->title !=''){ ?>
<div class="user_title">
<?php echo lang("user_title").": ". $user_info[0]->title ; ?>
</div>
<?php } ?>
<div class="image">
<img src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
</div> 
<?php if( !(empty($cv->objective))){ ?>
<div class="obj">
	<?php 
		echo lang("objective").": ".$cv[0]->objective ;
	?>
</div>
<?php } ?>
<div class="basic_info">
	<div class="mail">
    <?php 
		echo lang("email").": ";
		if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user[0]->mail;
	 ?>
    </div>
    <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>
    <div class="b_date">
     <?php 
		 echo lang("birth_date").": ".$user_info[0]->day."-".$user_info[0]->month."-".$user_info[0]->year;
		?>
    </div>
   <?php } ?>
   <?php if($user_info[0]->nationality !=''){ ?>
    <div class="nationality">
    <?php 
		echo lang("nationality").": ".$user_info[0]->nationality;
		?>
    </div>
    <?php } ?>
    <?php if($user_info[0]->gender !='n'){ ?>
    <div class="gender">
    <?php
	if($user_info[0]->gender =='m'){
		$gender=lang("male");
	}elseif($user_info[0]->gender =='f'){
		$gender=lang("female");
	}
	 echo lang("gender").": ".$gender; 
	 ?>
    </div>
    <?php } ?>
    <?php if($user_info[0]->marital_status !='n'){ ?>
    <div class="marital_rstatus">
    <?php
	if($user_info[0]->marital_status =='s'){
		$m_status=lang("single");
	}elseif($user_info[0]->marital_status =='m'){
		$m_status=lang("married");
	}elseif($user_info[0]->marital_status =='d'){
		$m_status=lang("divercoed");
	}
	 echo lang("marital_rstatus").": ".$m_status; 
	 ?>
    </div>
    <?php } ?>
    <?php if($user_info[0]->military !='n'){ ?>
    <div class="military">
    <?php
	if($user_info[0]->military =='e'){
		$military=lang("exemption");
	}elseif($user_info[0]->military =='c'){
		$military=lang("Complete_service");
	}elseif($user_info[0]->military =='p'){
		$military=lang("Postponed");
	}elseif($user_info[0]->military =='s'){
		$military=lang("Currently_serving");
	}elseif($user_info[0]->military =='d'){
		$military=lang("Doesnt_apply");
	}	 
	echo lang("military").": ".$military; 
	 ?>
    </div>
    <?php } ?>
    <?php if($user_info[0]->license !='n'){ ?>
    <div class="military">
    <?php
	if($user_info[0]->license =='y'){
		$License=lang("yes");
	}elseif($user_info[0]->license =='no'){
		$License=lang("no");
	}
	echo lang("License").": ".$License; 
	 ?>
    </div>
    <?php } ?>
    
    <?php if($user_info[0]->adress1 !=''){ ?>
    <div class="address1">
    <?php 
		echo lang("address")."1".": ".$user_info[0]->adress1;
	 ?>
    </div>
    <?php } ?>
    <?php if($user_info[0]->adress2 !=''){ ?>
    <div class="address2">
    <?php 
		echo lang("address")."2".": ".$user_info[0]->adress2;
	 ?>
    </div>
    <?php } ?>
     <?php if( !(empty($phones))){ ?>
    <div class="phones">
    <?php 
		echo lang("phones").": ";

	foreach ($phones as $item) {
		echo $item->code.$item->number."<br>" ;
		
	}
	?>
    </div>
    <?php } ?>
</div>

<?php if( !(empty($education))){ ?>
<div class="education">
	<?php 
		echo lang("education").": " ;
		
	foreach ($education as $item) {
		if( !(empty($item->type)))	echo $item->type."<br>" ;
		if( !(empty($item->name)))  echo $item->name."<br>" ;
		if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){ 
		 echo lang("from")." : ".$item->from_month."-".$item->from_year." ".lang("to")." : ".$item->to_month."-".$item->to_year."<br>"  ; }
		if( !(empty($item->desc)))	echo $item->desc."<br>" ;
		
	} ?>
</div>
<?php } ?>
<?php if( !(empty($courses))){ ?>
<div class="courses">
	<?php 
		echo lang("courses").": " ;
		
	foreach ($courses as $item) {
	 	if( !(empty($item->name)))	echo $item->name."<br>" ;
		if( !(empty($item->institute)))	echo $item->institute ."<br>" ;
		if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){				echo lang("from")." : ".$item->from_month."-".$item->from_year." ".lang("to")." : ".$item->to_month."-".$item->to_year."<br>" ; }
		
	} ?>
</div>
<?php } ?>
<?php if( !(empty($certifcates))){ ?>
<div class="certifcates">
	<?php 
		echo lang("certifcates").": " ;
		
	foreach ($certifcates as $item) {
		if( !(empty($item->name)))	echo $item->name."<br>" ;
		if( !(empty($item->institute)))	echo $item->institute ."<br>" ;
		if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){
			echo lang("from")." : ".$item->from_month."-".$item->from_year." ".lang("to")." : ".$item->to_month."-".$item->to_year."<br>" ; }
		
	} ?>
</div>
<?php } ?>
<?php if( !(empty($experience))){ ?>
<div class="experience">
	<?php 
		echo lang("experience").": " ;
		
	foreach ($experience as $item) {
		if( !(empty($item->company)))  echo $item->company."<br>" ;
		if( !(empty($item->name)))  echo $item->name."<br>" ;
		if( !(empty($item->role)))  echo $item->role ."<br>" ;
		if( !(empty($item->description)))  echo $item->description  ."<br>" ;
		if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_year !=lang('now'))){
			echo lang("from")." : ".$item->from_month."-".$item->from_year." ".lang("to")." : ".$item->to_month."-".$item->to_year."<br>" ; 
		}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_year==lang("now")){
			echo lang("from")." : ".$item->from_month."-".$item->from_year." ".lang("to")." ".$item->to_year."<br>" ;
		}
		
	} ?>
</div>
<?php } ?>
<?php if( !(empty($projects))){ ?>
<div class="projects">
	<?php 
		echo lang("projects").": " ;
	
	foreach ($projects as $item) {
		if( !(empty($item->name)))	echo $item->name."<br>" ;
		if( !(empty($item->description)))  echo $item->description  ."<br>" ;
		if( !(empty($item->link))) echo $item->link."<br>" ;
		
	} ?>
</div>
<?php } ?>
<?php if( !(empty($skills))){ ?>
<div class="skills">
	<?php 
		echo lang("skills").": " ;
	
	foreach ($skills as $item) {
		if( !(empty($item->name))) echo $item->name."<br>" ;
		if($item->grade=='i') echo lang('interemdate')."<br>";
		elseif($item->grade=='g') echo lang('good')."<br>";
		elseif($item->grade=='w') echo lang('weak')."<br>";
		
	} ?>
</div>
<?php } ?>
<?php if( !(empty($langs))){ ?>
<div class="langs">
	<?php 
		echo lang("langs").": " ;
	
	foreach ($langs as $item) {
		if( !(empty($item->name)))	echo $item->name."<br>" ;
		if($item->grade=='p') echo lang('poor')."<br>";
		elseif($item->grade=='f') echo lang('fair')."<br>";
		elseif($item->grade=='g') echo lang('good')."<br>";
		elseif($item->grade=='v') echo lang('very_good')."<br>";
		elseif($item->grade=='fl') echo lang('fluent')."<br>";
		elseif($item->grade=='t') echo lang('tongue')."<br>";
		
	} ?>
</div>
<?php } ?>

<?php if( !(empty($qualifications))){ ?>
<div class="qualifications">
	<?php 
		echo lang("qualifications").": " ;
	
	foreach ($qualifications as $item) {
		echo $item->name."<br>" ;
		
	} ?>
</div>
<?php } ?>


<?php if( !(empty($social))){ ?>
<div class="social">
	<?php 
		echo lang("social_links").": " ."<br>" ;
	foreach ($social as $item) {
		if( !(empty($item->name)))  echo $item->name.": " ;
		if( !(empty($item->link)))  echo $item->link  ."<br>" ;
		
	} ?>
</div>
<?php } ?>
<?php if( !(empty($references))){ ?>
<div class="references">
	<?php 
		echo lang("references").": " ;
	foreach ($references as $item) {
		if( !(empty($item->name)))   echo $item->name."<br>" ;
		if( !(empty($item->phone)))  echo $item->phone  ."<br>" ;
		if( !(empty($item->mail)))   echo $item->mail  ."<br>" ;
		
	} ?>
</div>
<?php } ?>
<div ><?php echo lang('recommendation'); ?>
<?php if(count($recommends) !=0){ ?>
<div id="recomnds">
<?php foreach($recommends as $item){ ?>
<div>
<?php echo $item->name; ?>
</div>
<div>
<?php echo $item->job; ?>
</div>
<div>
<?php echo $item->recommend; ?>
</div>
<?php } ?>
</div>
<?php } ?>
<div id="leave_recomnd">
<?php echo lang('leave_recommendation'); ?><?php if(!empty($result)) { echo $result ; } ?>
<form action="<?php echo base_url().$user[0]->user_name."/".$cv[0]->url_title ; ?>" method="post">
<input name="cv_id" type="hidden" value="<?php echo $cv[0]->id ; ?>">
<div>
<input name="name" type="text" placeholder="<?php echo lang('name') ?>">*
<?php echo form_error('name'); ?>
</div>
<div>
<input name="job" type="text" placeholder="<?php echo lang('job') ?>">*
<?php echo form_error('job'); ?>
</div>
<div>
<textarea name="recommend" placeholder="<?php echo lang('recommend') ?>">
</textarea>*
<?php echo form_error('recommend'); ?>
<input type="submit" value="<?php echo lang('save'); ?>" />
</div>
</form>
</div>
</div>

</body>
</html>
