<html>
<head>
 <link href="<?php echo base_url("styles/css/style5_".$cv->lang.".css") ?>" rel="stylesheet">
 <!--<link href="format-4-arabic.css" rel="stylesheet">-->
</head>
<body >
<div class="side-bar">
<div class="empty">
</div>
<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
<div class="back">
<img class="pic" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>" >
</div>
<?php } ?>
<h1><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
<h3><?php echo $user_info[0]->title ; ?></h3>

<h6 class="nation" style="padding-top:32px !important;">
<?php if($user_info[0]->nationality !=''){ ?><?php echo lang("nationality");?> : <span><?php echo $user_info[0]->nationality; ?></span>
<?php } ?>
</h6>
<?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>  
<h6 class="nation"><?php echo lang("birth_date") ; ?> : <span><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->marital_status !='n'){ 
		 if($user_info[0]->marital_status =='s'){
			$m_status=lang("single");
		}elseif($user_info[0]->marital_status =='m'){
			$m_status=lang("married");
		}elseif($user_info[0]->marital_status =='d'){
			$m_status=lang("divercoed");
		}
?>  
<h6 class="nation"><?php echo lang("marital_rstatus"); ?> : <span><?php echo $m_status ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->gender !='n'){ 
				if($user_info[0]->gender =='m'){
					$gender=lang("male");
				}elseif($user_info[0]->gender =='f'){
					$gender=lang("female");
				}
?>    
<h6 class="nation"><?php echo lang("gender"); ?> : <span><?php echo $gender ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->license !='no'){ 
				 if($user_info[0]->license =='y'){
					$License=lang("yes");
				}elseif($user_info[0]->license =='n'){
					$License=lang("no");
				}
?>   
<h6 class="nation"><?php echo lang("License"); ?> : <span><?php echo $License ; ?></span></h6>
<?php } ?>

<h6 class="nation" style="margin-bottom:130px !important;"><?php if($user_info[0]->military !='n'){ 
				if($user_info[0]->military =='e'){
					$military=lang("exemption");
				}elseif($user_info[0]->military =='c'){
					$military=lang("Complete_service");
				}elseif($user_info[0]->military =='p'){
					$military=lang("Postponed");
				}elseif($user_info[0]->military =='s'){
					$military=lang("Currently_serving");
				}elseif($user_info[0]->military =='d'){
					$military=lang("Doesnt_apply");
				}	 
?> 
<?php echo lang("military"); ?> : <span><?php echo $military; ?></span>
<?php } ?>
</h6>
<div class="ddd">
<?php if($user_info[0]->adress1 !=''){ ?>
<div class="info">
<img src="<?php echo base_url(); ?>assets/images/home.png" alt="home">
</div>
<p class="parag"><?php echo $user_info[0]->adress1; ?></p>
 <?php } ?>
 <?php if($user_info[0]->adress2 !=''){ ?>
 <div class="info">
<img src="<?php echo base_url(); ?>assets/images/home.png" alt="home">
</div>
<p class="parag"><?php echo $user_info[0]->adress2; ?></p>
<?php } ?>
<?php if( !(empty($phones))){ ?>  
<?php
		foreach ($phones as $item) {
?> 
<div class="info">
<img src="<?php echo base_url(); ?>assets/images/tele.png" alt="telephone">
</div>
<p class="parag"><?php echo $item->code.$item->number ; ?></p>
<?php } }?>
<div class="info">
<img src="<?php echo base_url(); ?>assets/images/email.png" alt="email">
</div>
<p class="parag"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>
<?php if( !(empty($social))){ ?>        
<?php foreach ($social as $item) { ?>        
<div class="info">
<img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'face.png';}elseif($item->name=='twitter'){echo 'twitter.png';}elseif($item->name=='linkledin'){echo 'linkledin.png';}elseif($item->name=='google'){echo 'google.png';}elseif($item->name=='behance'){echo 'behance.png';} ?>" alt="s-image">
</div>
<p class="parag"><a href="<?php echo $item->link; ?>"><?php echo $item->link; ?></a></p>
<?php }} ?>
</div>
</div>
<div class="right-side">
<div class="objective">
<div class="container">
<?php if( !(empty($cv->objective))){ ?>
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/quotes.png" alt="quotes">
</div>
<h2 class="ob"><?php echo strtoupper(lang("objective")) ;?></h2>
<p class="text"><?php echo $cv->objective ;?></p>
<?php } ?>
</div>
</div>
<div class="objective">
<div class="container">
<?php if( !(empty($education))){ ?>
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/grad.png" alt="graduation">
</div>
<h2 class="tt" style="font-size:20px; color:#8bc53f;font-weight:lighter;"><?php echo strtoupper(lang("education")); ?></h2><hr style="color:#d0d0d0;">
<div class="contentt">
<?php foreach ($education as $item) { ?>
<h2><?php if( !(empty($item->name)))  echo $item->name ; ?></h2>
<h4><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo  $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year  ; ?>
         
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=="now"){ ?>
        
		<?php	echo  $item->from_month."/".$item->from_year." - ".lang($item->to_month) ; ?>
         
<?php	} ?></h4>
<p style="padding-top:0px !important;padding-bottom:20px !important;"><?php if( !(empty($item->type)))  echo $item->type ; ?></p>
<p style="padding-top:0px !important;padding-bottom:20px !important;"><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>

<?php }} ?>
</div>
</div>
<?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/course.png" alt="course">
</div>
<h2 class="t"  style="font-size:20px; color:#8bc53f;font-weight:lighter;width:125px;"><?php if( !(empty($courses))) { echo strtoupper(lang("courses")) ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($certifcates))) echo strtoupper(lang("certifcates")) ; ?></h2><hr style="color:#d0d0d0;">
<div class="contentt">
<?php foreach ($courses as $item) { ?>
<h2><?php  echo strtoupper($item->name) ; ?></h2>
<h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                
				<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></h2>
<p style="padding-top:0px !important;padding-bottom:20px !important;"><?php if( !(empty($item->institute))) echo $item->institute ; ?></p>
<?php } ?>
<?php foreach ($certifcates as $item) { ?>
<h2><?php  echo strtoupper($item->name) ; ?></h2>
<h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                
				<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></h2>
<p style="padding-top:0px !important;padding-bottom:20px !important;"><?php if( !(empty($item->institute))) echo $item->institute ; ?></p>
<?php } ?>
</div>
</div>
<?php } ?>
<?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/ex.png" alt="experience">
</div>
<h2 class="t" style="font-size:20px; color:#8bc53f;font-weight:lighter;width:155px;"><?php if( !(empty($experience))) { echo strtoupper(lang("experience")) ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang('and'),"&nbsp;" ; } if(!(empty($projects))) echo strtoupper(lang("projects")) ; ?></h2><span style="font-size:12px;color:#484748;"><?php if(!(empty($user_info[0]->exp_no))){ echo lang('no_exp') ." (". $user_info[0]->exp_no ." ".")" ;} ?></span><hr style="color:#d0d0d0;">
<div class="contentt">
<?php foreach ($experience as $item) {  ?>
<h2><?php echo $item->company ; ?></h2>
<h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=="now"){ ?>
    
			<?php echo $item->from_month."/".$item->from_year." - ".lang($item->to_month) ; ?>
            
			<?php } ?></h2>
<p style="padding-top:0px !important;padding-bottom:20px !important;"><?php echo $item->name ; ?></p>
<div style="margin-left:20px;margin-top:0px;padding-top:-25px;margin-bottom:0px;padding-bottom:0px;">
<?php if(!empty($item->role)){ ?>
<ul style="margin:0px;padding:0px;">
<?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
<li style="font-size:16px;color:#2f5791;list-style:none;">-<?php echo $r_item ; ?></li>
<?php } ?>
</ul>
<?php } ?>
</div>
<p style="padding-top:0px;padding-bottom:40px;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
<?php } ?>

<?php foreach ($projects as $item) {  ?>
<h2><a href="<?php echo $item->link ; ?>" style="color:#2f5791;"><?php echo $item->link ; ?></a></h2>
<h2><?php echo $item->name ; ?></h2>
<p style="padding-top:0px !important;padding-bottom:0px !important;"></p>
<div style="margin-left:20px;margin-top:0px;padding-top:-25px;margin-bottom:0px;padding-bottom:0px;">

</div>
<p style="padding-top:0px;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
<?php } ?>


</div>
</div>
<?php } ?>
<?php if( !(empty($langs))){?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/exp.png" alt="language">
</div>
<h2 class="ts" style="font-size:20px; color:#8bc53f;font-weight:lighter;"><?php echo strtoupper(lang("langs")) ; ?></h2><hr style="color:#d0d0d0;">
<div class="contenntt">
<?php foreach ($langs as $item) {  ?>
<h2 style="padding-top:10px !important;padding-bottom:5px;"><?php echo " -"."&nbsp;"; ?><?php if( !(empty($item->name))) echo lang($item->name) ; ?><span style="font-size:16px; color:#363636;font-weight:lighter;"><?php echo " -"."&nbsp;"; ?><?php if($item->grade=='p') echo lang('poor');
				elseif($item->grade=='f') echo lang('fair');
				elseif($item->grade=='g') echo lang('good');
				elseif($item->grade=='v') echo lang('very_good');
				elseif($item->grade=='fl') echo lang('fluent');
				elseif($item->grade=='t') echo lang('tongue'); ?></span></h2>
<?php } ?>
</div>

</div>
</div>
<?php } ?>
<?php if( !(empty($skills))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/ts.png" alt="technical skills">
</div>

<h2 class="tss" style="font-size:20px; color:#8bc53f;font-weight:lighter;"><?php echo strtoupper(lang("skills")) ; ?></h2><hr style="color:#d0d0d0;">
<div class="contentt">
<?php foreach ($skills as $item) { ?>
<h2 style="font-size: 16px;
              color: #2f5791  !important;"><?php echo " -"."&nbsp;"; ?><?php if( !(empty($item->name))) echo $item->name ; ?><span style="font-size:16px; color:#363636;font-weight:lighter;"><?php echo " -"."&nbsp;"; ?><?php if($item->grade=='i') echo lang('interemdate');
																				elseif($item->grade=='g') echo lang('good');
																				elseif($item->grade=='w') echo lang('weak'); ?></span></h2>
<?php } ?>              

</div>

</div>
</div>
<?php } ?>
<?php if( !(empty($qualifications))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/ts.png" alt="personal skills">
</div>
<h2 class="tss" style="font-size:20px; color:#8bc53f;font-weight:lighter;"><?php echo strtoupper(lang("qualifications")) ; ?></h2><hr style="color:#d0d0d0;">
<div class="contentst">
<?php foreach ($qualifications as $item) { ?>
<div class="sk">
	<ul style="list-style:dashed;">
		<li style="font-size:16px;color:#414142;"><?php echo $item->name ; ?></li>
	</ul>
	</div>
<!--<p style="font-size:16px;color:#414142;"></p>-->
<?php } ?>
</div>
<!--<div class="person">
<ul>
<li>-Name of Qualification</li>
<li>-Name of Qualification</li>

</ul>
</div>
<div class="person2" >
<ul>
<li>-Name of Qualification</li>
<li>-Name of Qualification</li>

</ul>
</div>-->

</div>
</div>
<?php } ?>
<?php if( !(empty($references))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/ref.png" alt="refernce">
</div>
<h2 class="ts" style="font-size:20px; color:#8bc53f;font-weight:lighter;"><?php echo strtoupper(lang("references")) ; ?></h2><hr style="color:#d0d0d0;">
<div class="conteentt">
<?php foreach ($references as $item) { ?>
<h2>-<?php if( !(empty($item->name))) echo $item->name; ?></h2>

<p ><?php echo $item->mail ; ?></p>
<p style="padding-top:-10px;"><?php if(!empty($item->phone)){echo "(".$item->phone.")"; } ?></p>
<?php } ?>
</div>
</div>
<?php } ?>
</div>
</div>
</body>
</html>