<div class="about-content faq">
    <!-- right side -->
    <div class="right-side">
        <div class="search-div">
            <p>ابحث عن معلومة</p>
            <div class="search">
                <input type="text" placeholder="بحث" name="search" mnbaa-pointer="faq_search_text">
                <!-- <input type="submit" value="بحث" mnbaa-pointer="faq_search_submit"> -->
            </div>
        </div>
    </div>
    <!-- right side end -->

    <!-- left side start -->
    <div class="left-side" mnbaa-pointer="left-side">
        <?php foreach ($faq as $item) { ?>
            <div class="single-question">
                <div class="question">
                	<p><?php echo (LANG == 'ar') ? $item->question : $item->question_en?></p>
                </div>
                <p class="answer">
                	<?php echo (LANG == 'ar') ? $item->answer : $item->answer_en?>
                </p>
            </div>
        <?php } ?>





        <br class="clr">
        <?php if (count($faq)) {?>
            <!-- pagination -->
            <div class="box-bt-bar">  
                <ul class="pagination" class="box-nav">
                    <?php echo $this->pagination->create_links(); ?>
                   <!-- <li class="next"><a href="#"><</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                    <li class="prev"><a href="#">></a></li>-->
                </ul>
            </div>
        <?php } ?> 


    </div>
    
    <script type="text/javascript" charset="utf-8">
        $(window).load(
        	function () {
        		$("[mnbaa-pointer='faq_search_text']").keyup(function () {
					var Search = $("[mnbaa-pointer='faq_search_text']").val();
					$.get('<?php echo site_url(); ?>/index/search_faq/'+Search.trim(), function(data) {
						$("[mnbaa-pointer='left-side']").html(data)
					});

				})
        	})
    </script>
    <!-- left side end -->
</div>
<br class="clr">

<style type="text/css" media="screen">
	.highlighted{
		color: #40a9e0;
		font-weight: bold;
	}
</style>

