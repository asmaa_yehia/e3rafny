<?php
function highlightText($search, $text){
	if (!strlen($search)) {
		return $text;
	} else {
		$highlighted = "<span class='highlighted'>" . $search . "</span>";
		$pieces = explode($search, $text);
		return implode($highlighted, $pieces);
	}
}
?>
        <?php foreach ($faq as $item) { ?>
            <div class="single-question">
            	
                <div class="question">
                	<p><?php echo (LANG == 'ar') ? highlightText($search, $item->question) : highlightText($search, $item->question_en)?></p>
                </div>
                <p class="answer">
                	<?php echo (LANG == 'ar') ? highlightText($search, $item->answer) : highlightText($search, $item->answer_en)?>
                </p>
            </div>
        <?php } ?>


        <br class="clr">
        <?php if (count($faq)) { ?>
            <!-- pagination -->
            <div class="box-bt-bar">  
                <ul class="pagination" class="box-nav">
                    <?php echo $this -> pagination -> create_links()?>
                </ul>
            </div>
        <?php } ?>

<script>
$(function(){
   $(".pagination a").click(function(){
	  $.get($(this).attr("href"), function(data) {
	  		if (data.length > 0) {
				$("[mnbaa-pointer='left-side']").html(data)
			}
		});
	   return false;
   });
});
</script>