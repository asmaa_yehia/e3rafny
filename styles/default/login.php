<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>E3rfny</title>


        <!-- my styleshhets -->
        <link href="<?php echo base_url(); ?>styles/css/stylesheet.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>styles/fonts/stylesheet.css" rel="stylesheet">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="top-bar"></div>

        <div class="page-wrap">
            <div class="container">

                <!-- logo and navbar -->
                <div class="nav-logo-area login-top-area">

                    <div class="left-area">
                        <!-- lang -->
                        <div class="user-lang-area">

                            <ul>
                                <li><a href="<?php echo base_url(); ?>ar" class="ar"><?php echo lang('arabic'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>en" class="en"><?php echo lang('english'); ?></a></li>
                            </ul>
                        </div>
                        <!-- lang end -->

                    </div>
                </div>
                <!-- logo and navbar end-->

                <div class="clr"></div>

                <a href="#" class="login-logo"><img src="<?php echo base_url()?>styles/images/logo-large.png" alt=""></a>

                <!-- content -->
                <div class="steps-content">

                    <div class="register-login">
                        <?php
                        $msg = $this->session->flashdata('msg');
                        if (!empty($msg)) {
                            echo $msg;
                        }
						if(isset($error_msg)){
							
							 echo $error_msg;
						}
                        ?>
                        


                        <form action="<?php echo site_url(); ?>/auth" method="post">

                            <table class="login-form">
                                <tr>
                                    <td><input placeholder="<?php echo lang('email') ?>" name="mail" id="mail" type="text"  />
                                        <div class="form-msg-error-text"><?php echo form_error('mail'); ?></div></td>
                                </tr>
                                <tr>
                                    <td><input placeholder="<?php echo lang('password') ?>" name="password" id="password" type="password"  />
                                        <div class="form-msg-error-text"><?php echo form_error('password'); ?></div>
                                    </td>
                                </tr>
                                <tr><td><a href="<?php echo site_url('auth/fgpass') ?>" ><?php echo lang('forget_password'); ?></a></td>
                                </tr>
                                <tr><td><input type="checkbox" class="ceck" name="remember" value="1" /><?php echo lang('remember'); ?></td></tr>
                                <tr><td><input type="submit" value="<?php echo lang('login'); ?>" /></td></tr>
                                <tr><td><a href="<?php echo site_url('auth/register') ?>" ><?php echo lang('register'); ?></a></td></tr>

                            </table>
                        </form>


                    </div>
                    <!-- content end -->

                </div>
                <!-- container end -->
            </div>
            <!-- page wrap end -->


            <!-- footer wrapper start -->
            <div class="footer-wrapper">

            </div>


            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="js/jquery-1.7.1.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="js/bootstrap.min.js"></script>
            <script src="js/jquery-ui.js"></script>

    </body>

    <script type="text/javascript">
        $(document).ready(function() {
            $('li.msg').click(function() {
                $('li.msg .panel').slideToggle();
            });

            $('li.ntf').click(function() {
                $('li.ntf .panel').slideToggle();
            });

            $('.panel').click(function(event) {
                event.stopPropagation();
            });
        });
    </script>

    <script type="text/javascript" charset="utf-8">
        $(document).ready(function() {
            $("#tabs").tabs({show: {effect: "fade", duration: 800}});
        });

    </script>

</html>





















