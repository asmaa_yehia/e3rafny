<!--<a href="<?php echo site_url(); ?>/finish/pdf_file/<?php echo $cv_id; ?>" ><?php echo lang('download'); ?></a>  -->
<!--<input type="text" value="<?php echo base_url() . $user->user_name . "/" . $cv->url_title; ?>" name="link" readonly size="50" >  -->
<form name="form1" action="<?php echo site_url(); ?>/finish/edit/<?php echo $cv_id; ?>" method="post" enctype="multipart/form-data">  
    <div class="step9">
        <h3><?php echo lang('cv_finish'); ?> </h3>
        <!-- download side -->
        <div class="download-pdf">
            <p class="download-it"><a href="<?php echo site_url(); ?>/finish/pdf_file/<?php echo $cv_id; ?>"><?php echo lang('download'); ?></a></p>
            <p class="edit-it">
                <?php echo lang('download_edit'); ?><a href="<?php echo site_url();?>/add_cv/back_cv/<?php echo $cv_id;?>" ><?php echo lang('here'); ?></a>  
            </p>
        </div>

      
        <div class="form-msg-error-text"><?php echo form_error('user_name'); ?></div></td>
        <input type="hidden" value="<?php echo $cv_id; ?>" name="cv_id" id="cv_id" >  
       <!-- <input type="submit" name="save" id="save" value="<?php echo lang('save'); ?>"  class="save"/>-->
        <img src="<?php echo base_url(); ?>styles/images/finish-sep.png" class="finish-sep">

        <!-- online side -->
        <div class="online-pdf">
            <p class="link-it"><a href="<?php echo base_url() . $user->user_name . "/" . $cv->url_title; ?>" target="_blank"><?php echo base_url() . $user->user_name . "/" . $cv->url_title; ?></a></p>
            <p class="info">
                <?php echo lang('link_edit'); ?>
            </p>
            <div class="edit-it">
                <input type="text" value="<?php echo $user->user_name; ?>" name="user_name"  placeholder="<?php echo lang('user_name'); ?>" >
            <!--    <input type="text" placeholder="<?php echo lang('user_name'); ?>" value="<?php echo $user->user_name; ?>">-->
                <input type="submit" value="<?php echo lang('save'); ?>" class="save" name="save">
            </div>
        </div>

    </div>
</form>
<br class="clr">

<!-- share cv -->
<div class="share-cv">
    <h1 class="blue-title"><?php echo lang('cv_sahre'); ?></h1>

    <br class="clr">
    <ul class="social-share">
        <li>  <?php if ($cv->percent < 35 || $user->status == 0) { ?>
                                   <a href="<?php echo site_url() . "/finish/publish_error";?>" target="_blank" class="facebook"></a>
                               <?php } else {
                                    echo share_button('facebook', array('url' => base_url() . $user->user_name . "/" . $cv->url_title . "/", 'text' => 'share'));
                                }
        ?>
        </li>
        <li><?php
                                if ($cv->percent < 35 || $user->status == 0) { ?>
                                   <a href="<?php echo site_url() . "/finish/publish_error"; ?>" target="_blank" class="twitter"></a>
                               <?php } else {
                                    echo share_button('twitter', array('url' => base_url() . $user->user_name . "/" . $cv->url_title));
                                }
                                ?> </li>
        <li><?php
                                if ($cv->percent < 35 || $user->status == 0) { ?>
                                   <a href="<?php echo site_url() . "/finish/publish_error"; ?>" target="_blank" class="gplus"></a>
                               <?php } else {?>
                               <div class="g-plusone" data-annotation="bubble" data-width="600" data-href="<?php echo base_url() . $user->user_name . "/" . $cv->url_title; ?>"></div>

<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
							   <?php } ?></li>
    </ul>
</div>






