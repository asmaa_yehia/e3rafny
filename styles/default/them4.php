<?php $i=1; ?>
<html>
<head>
 <link href="<?php echo base_url("styles/css/style4_".$cv->lang.".css") ?>" rel="stylesheet">
</head>
<body >
<div class="side-bar">
</div>
<div class="first">
<div class="name">
<div class="continer">
<h1 ><b><?php echo ucwords($user_info[0]->user_cv_name) ; ?></b></h1>
<h3><?php echo $user_info[0]->title ; ?></h3>
<?php if($user_info[0]->nationality !=''){ ?>
<h6 class="nation"><?php echo lang("nationality");?> : <span><?php echo $user_info[0]->nationality; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>  
<h6 class="nation"><?php echo lang("birth_date") ; ?> : <span> <?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->marital_status !='n'){ 
		 	if($user_info[0]->marital_status =='s'){
				$m_status=lang("single");
			}elseif($user_info[0]->marital_status =='m'){
				$m_status=lang("married");
			}elseif($user_info[0]->marital_status =='d'){
				$m_status=lang("divercoed");
			}
?>  
<h6 class="nation"><?php echo lang("marital_rstatus"); ?> : <span><?php echo $m_status ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->gender !='n'){ 
				if($user_info[0]->gender =='m'){
					$gender=lang("male");
				}elseif($user_info[0]->gender =='f'){
					$gender=lang("female");
				}
?>    
<h6 class="nation"><?php echo lang("gender"); ?> : <span><?php echo $gender ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->license !='no'){ 
				 if($user_info[0]->license =='y'){
					$License=lang("yes");
				}elseif($user_info[0]->license =='n'){
					$License=lang("no");
				}
?>   
<h6 class="nation"><?php echo lang("License"); ?> : <span><?php echo $License ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->military !='n'){ 
				if($user_info[0]->military =='e'){
					$military=lang("exemption");
				}elseif($user_info[0]->military =='c'){
					$military=lang("Complete_service");
				}elseif($user_info[0]->military =='p'){
					$military=lang("Postponed");
				}elseif($user_info[0]->military =='s'){
					$military=lang("Currently_serving");
				}elseif($user_info[0]->military =='d'){
					$military=lang("Doesnt_apply");
				}	 
?> 
<h6 class="nation"><?php echo lang("military"); ?> : <span><?php echo $military; ?></span></h6>
<?php } ?>
<h6 class="nation"><?php echo lang("emaill") ;?> : <span><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></span></h6>
<?php if( !(empty($phones))){ ?>   
<?php
		foreach ($phones as $item) {
?>     
<h6 class="nation"><?php echo lang("phone"); ?> : 

<span><?php echo $item->code.$item->number ; ?></span>

</h6>
<?php } ?>
<?php } ?>

</div>
</div>

<div class="adress">
<div class="continer">
<?php if($user_info[0]->adress1 !=''){ ?>
<h6 class="nation"><?php echo lang("address") ?> : <span><?php echo $user_info[0]->adress1; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->adress2 !=''){ ?>
<h6 class="nation"><?php echo lang("another_address") ?> : <span><?php echo $user_info[0]->adress2; ?></span></h6>
<?php } ?>
</div>
</div>
<div class="square">
<div class="continer">
<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
<img src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>">
<?php } ?>
</div>
</div>
<div class="social" style="margin-bottom:10px !important;padding-bottom:10px !important;">
<?php if( !(empty($social))){ ?>
<div class="continer">
<?php foreach ($social as $item) { ?>
<h6 class="link"><?php echo ucwords($item->name); ?> : <br><span><a href="<?php echo $item->link; ?>" style="font-size: 16px !important;
     color: #404041 !important;
     text-decoration: none;"><?php echo $item->link; ?></a></span></h6>
<?php } ?>     
</div>
<?php } ?>
</div>
<?php if( !(empty($cv->objective))){
	global $i;
	$i++;
?>  
<div class="objective">
<div class="title">

<ul >
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php  echo  $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php echo strtoupper(lang("objective")) ;?></li>
</ul>
</div>
<div class="content">
<p><?php echo $cv->objective ;?></p>
</div>
</div>
<?php } ?>
<?php if( !(empty($education))){ global $i;
	$i++; ?>
<div class="objective">
<div class="title">
<ul>
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php global $i; echo  $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php echo strtoupper(lang("education")); ?></li></ul>
</div>
<?php foreach ($education as $item) { ?>
<div class="contentt">
<h2><?php if( !(empty($item->name)))  echo $item->name ; ?></h2>
<h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo  $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year  ; ?>
         
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo  $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
         
	<?php	} ?></h2>
<h3><?php if( !(empty($item->type)))  echo $item->type ; ?></h3>
<p><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>
</div>
<?php } ?>
</div>
<?php } ?>
<?php if( !(empty($experience)) || !(empty($projects)) ){ global $i;
	$i++; ?>
<div class="objective">
<div class="title" >
<ul >
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php echo $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php if( !(empty($experience))) { echo strtoupper(lang("experience")) ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($projects))) echo strtoupper(lang("projects")) ; ?></li>
   </ul>
   <ul>
   <li style="font-size: 13px;
   color: #3e3e3c;list-style:none;"><?php if(!(empty($user_info[0]->exp_no))){ echo lang('no_exp')."&nbsp;"."(". $user_info[0]->exp_no ."&nbsp;".")" ;} ?></li></ul>
</div>
<?php foreach ($experience as $item) {  ?>
<div class="contentt">
<?php if( !(empty($item->company))){ ?><h2> <?php echo $item->company ; ?></h2><?php } ?>
<h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
            
			<?php } ?></h2>
<?php if( !(empty($item->name))) {  ?><p style="padding-top:0px !important;padding-bottom:0px !important;margin-bottom:0px;"><?php echo $item->name ; ?></p><?php } ?>
<?php if(!empty($item->role)){ ?>
<div style="margin-left:20px;margin-top:0px;pading-top:0px;margin-bottom:0px;padding-bottom:0px;">
<ul style="margin:0px;padding:0px;">

<?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
<li style="margin-top:-20px !important;">-<?php echo $r_item ; ?></li>
<?php } ?>
</ul>
</div>
<?php } ?>
<p style="padding-top:10px;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
</div>
<?php } ?>

<?php foreach ($projects as $item) { ?>
<div class="contentt">
<h2> </h2>
<h2><?php if( !(empty($item->link)))?><a href="<?php  echo $item->link  ; ?>" style="font-size:16px;color:#0099cc;font-weight:lighter;"><?php  echo $item->link  ; ?></a></h2>
<?php if( !(empty($item->name))) {  ?><p style="padding-top:0px !important;"><?php echo $item->name ; ?></p><?php } ?>

<p style="padding-top:10px;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
</div>
<?php } ?>


</div>
<?php } ?>
<?php if( !(empty($courses)) || !(empty($certifcates)) ){ global $i;
	$i++; ?>
<div class="objective">
<div class="title">

<ul>
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php echo $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php if( !(empty($courses))) { echo strtoupper(lang("courses")) ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($certifcates))) echo strtoupper(lang("certifcates")) ; ?></li></ul>
</div>
<?php foreach ($courses as $item) { ?>
<div class="contentt">
<h2><?php  echo strtoupper($item->name) ; ?></h2>
<h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                
				<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></h2>
<p style="padding-top:0px !important;"><?php if( !(empty($item->institute))) echo $item->institute ; ?>
</p>
</div>
<?php } ?>

<?php foreach ($certifcates as $item) { ?>
<div class="contentt">
<h2><?php  echo strtoupper($item->name) ; ?></h2>
<h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                
				<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></h2>
<p style="padding-top:0px !important;"><?php if( !(empty($item->institute))) echo $item->institute ; ?>
</p>
</div>
<?php } ?>

</div>
<?php } ?>
<?php if( !(empty($langs))){ global $i;
	$i++; ?> 
<div class="objective">
<div class="title">
<ul>
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php echo $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php echo strtoupper(lang("langs")) ; ?></li></ul>
</div>
<div class="contenntt">
<?php foreach ($langs as $item) {  ?>
<h2><?php if( !(empty($item->name))) echo lang($item->name) ; ?><span style="font-size:16px; color:#363636;font-weight:lighter;"><?php echo " -"."&nbsp;"; ?><?php if($item->grade=='p') echo lang('poor');
				elseif($item->grade=='f') echo lang('fair');
				elseif($item->grade=='g') echo lang('good');
				elseif($item->grade=='v') echo lang('very_good');
				elseif($item->grade=='fl') echo lang('fluent');
				elseif($item->grade=='t') echo lang('tongue'); ?></span></h2>
<?php } ?>
</div>

</div>
<?php } ?>
<?php if( !(empty($skills))){ global $i;
	$i++; ?> 
<div class="objectiv">
<div class="title">
<ul>
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php echo $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php echo strtoupper(lang("skills")) ; ?></li></ul>
</div>
<div class="contenntt">
<?php foreach ($skills as $item) { ?>
<h2><?php if( !(empty($item->name))) echo $item->name ; ?><span style="font-size:16px; color:#363636;font-weight:lighter;"><?php echo " -"."&nbsp;"; ?><?php if($item->grade=='i') echo lang('interemdate');
																				elseif($item->grade=='g') echo lang('good');
																				elseif($item->grade=='w') echo lang('weak'); ?></span></h2>
<?php } ?>

</div>

</div>
<?php } ?>
<?php if( !(empty($qualifications))){ global $i;
	$i++; ?>
<div class="objectiv">
<div class="title">
<ul>
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php echo $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php echo strtoupper(lang("qualifications")) ; ?></li></ul>
</div>
<div class="contenntt">
	<div class="gt">
<ul style="margin:0px;padding:0px;">
<?php foreach ($qualifications as $item) { ?>
<li style="font-size:16px; color:#363636;font-weight:lighter;margin-top:0px;"><?php echo $item->name ; ?></li>
<?php } ?>
</ul>
</div>
</div>
</div>
<?php } ?>
<?php if( !(empty($references))){ global $i;
	$i++; ?>
<div class="objectiv">
<div class="title">
<ul>
<li style="font-size: 27px;
   color: #0099cc;float:left;width:30px;"><b><?php echo $i; ?></b></li> <li style="font-size: 16px;
   color: #3e3e3c;float:left;list-style:none;width:90px;"><?php echo strtoupper(lang("references")) ; ?></li></ul>
</div>
<?php foreach ($references as $item) { ?>
<div class="contenntt">
<h2><?php if( !(empty($item->name))) echo $item->name; ?></h2>
<p><?php echo $item->mail ; ?></p>
<p><?php echo $item->phone ; ?></p>

</div>
<?php } ?>
</div>
</div>
<?php } ?>
<div class="clr">
</div>

</body>
</html>