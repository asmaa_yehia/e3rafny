<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
        <meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
        <meta property="og:site_name" content="e3rafny" />
        <meta name="twitter:card" value="summary" />
        <meta name="twitter:site" value="e3rafny" />
        <meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
        <meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
        <meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
        <meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
        <meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
        <meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">
        <title><?php echo $user_info[0]->user_cv_name ; ?></title>
		<link href="<?php echo base_url("styles/css/normalize.css") ?>" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Ubuntu+Condensed' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url("styles/css/tab/css/easy-responsive-tabs_".$cv[0]->lang.".css") ?>" />
        <link href="<?php echo base_url(); ?>styles/css/<?php echo $style_name."_".$cv[0]->lang.".css"; ?>" rel="stylesheet">
		
  </head>
    <body>
        <div class="full-width social-wrapper">
            <div class="offset"></div>
            
            <div class="social-icons">
				<?php foreach ($social as $item) { ?>
                <a href="<?php echo $item->link ; ?>" id="<?php if($item->name=='facebook'){echo 'fb-icon';}elseif($item->name=='twitter'){echo 't-icon';}elseif($item->name=='linkledin'){echo 't-icon';}elseif($item->name=='google'){echo 'g-icon';}elseif($item->name=='behance'){echo 'g-icon';} ?>"><i class="<?php if($item->name=='facebook'){echo 'fa fa-facebook';}elseif($item->name=='twitter'){echo 'fa fa-twitter';}elseif($item->name=='linkledin'){echo 'fa fa-linkedin';}elseif($item->name=='google'){echo 'fa fa-google';}elseif($item->name=='behance'){echo 'fa fa-behance';} ?>"></i></a>
                <?php } ?>
                
            </div>
            
            <div class="offset"></div>
        </div>
        <div class="cv-head">
            <div class="img-wrapper">
                <?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
                <div class="img-container">
                    <img src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>" id="profile-img"/>
                </div>
               <?php } ?> 
            </div>
            <div class="name-wrapper">
                <h1><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
                <h2><?php echo $user_info[0]->title ; ?></h2>
            </div>
            <div class="contact-info">
                <div class="contact-row">
                    <h3><?php echo lang("emaill"); ?> :</h3>
                    <span><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></span>
                </div>
                <div class="contact-row">
                    <h3><?php echo lang("phones"); ?> :</h3>
                    <?php foreach ($phones as $item) {?>
                    <span><?php echo $item->code.$item->number ; ?></span>
                    <?php } ?>
                </div>
                <?php if($user_info[0]->adress1 !=''){ ?>
                <div class="contact-row">
                    <h3><?php echo lang("address") ; ?> :</h3>
                    <span> <?php echo $user_info[0]->adress1; ?>
                    </span>
                </div>
				<?php } ?>
                <?php if($user_info[0]->adress2 !=''){ ?>
                <div class="contact-row">
                    <h3><?php echo lang("another_address") ; ?> :</h3>
                    <span> <?php echo $user_info[0]->adress2; ?>
                    </span>
                </div>
				<?php } ?>
            </div>
        </div> 
        <div class="cv-content">
            <!--vertical Tabs-->
            
            <div id="verticalTab">
                <ul class="resp-tabs-list tab-head">

                    <li><?php echo lang("personalinfo") ; ?></li>
                    <?php if( !(empty($cv[0]->objective))){ ?>  
                    	<li><?php echo lang("objective") ;?></li>
                    <?php } ?> 
                    <?php if( !(empty($education))){ ?>   
                    	<li><?php echo lang("education") ; ?></li>
                    <?php } ?>  
                    <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>   
                    	<li><?php if( !(empty($courses))) { echo lang("courses") ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo lang("certifcates") ; ?></li>
                    <?php } ?>  
                    <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>  
                    	<li><?php if( !(empty($experience))) { echo lang("experience") ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo lang("projects") ; ?></li>
                    <?php } ?>
                    <?php if( !(empty($langs))){ ?> 
                    	<li><?php echo lang("langs") ; ?></li>
                   <?php } ?>  
                   <?php if( !(empty($skills))){ ?>    
                    	<li><?php echo lang("skills") ; ?></li>
                  <?php } ?> 
                  <?php if( !(empty($qualifications))){ ?>     
                    <li><?php echo lang("qualifications") ; ?></li>
                  <?php } ?>  
                  <?php if( !(empty($references))){ ?>
                    <li><?php echo lang("references") ; ?></li>
                  <?php } ?>  
                    <li><?php echo lang('recommendation'); ?></li>
                </ul>
                <div class="resp-tabs-container">
                    <div>
                    <?php if($user_info[0]->nationality !=''){ ?>
                        <div class="p-info-row"><h3><?php echo lang("nationality");?> : </h3><span><?php echo ucfirst($user_info[0]->nationality); ?></span></div>
                    <?php } ?> 
                    <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>      
                        <div class="p-info-row"><h3><?php echo lang("birth_date") ; ?> : </h3><span><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></div> 
                    <?php } ?>  
                    <?php if($user_info[0]->marital_status !='n'){ 
						if($user_info[0]->marital_status =='s'){
							$m_status=lang("single");
						}elseif($user_info[0]->marital_status =='m'){
							$m_status=lang("married");
						}elseif($user_info[0]->marital_status =='d'){
							$m_status=lang("divercoed");
						}
					 ?>    
                        <div class="p-info-row"><h3><?php echo lang("marital_rstatus"); ?> : </h3><span><?php echo $m_status ; ?></span></div>
                      <?php } ?>  
                      <?php if($user_info[0]->gender !='n'){ 
							if($user_info[0]->gender =='m'){
								$gender=lang("male");
							}elseif($user_info[0]->gender =='f'){
								$gender=lang("female");
							}
					 ?>    
                        <div class="p-info-row"><h3><?php echo lang("gender"); ?> : </h3><span><?php echo $gender ; ?></span></div> 
                     <?php } ?>   
                     <?php if($user_info[0]->license !='no'){ 
							 if($user_info[0]->license =='y'){
								$License=lang("yes");
							}elseif($user_info[0]->license =='n'){
								$License=lang("no");
							}
					?>   
                        <div class="p-info-row"><h3><?php echo lang("License"); ?> : </h3><span><?php echo $License ; ?></span></div> 
                     <?php } ?>  
                     <?php if($user_info[0]->military !='n'){ 
							if($user_info[0]->military =='e'){
								$military=lang("exemption");
							}elseif($user_info[0]->military =='c'){
								$military=lang("Complete_service");
							}elseif($user_info[0]->military =='p'){
								$military=lang("Postponed");
							}elseif($user_info[0]->military =='s'){
								$military=lang("Currently_serving");
							}elseif($user_info[0]->military =='d'){
								$military=lang("Doesnt_apply");
							}	 
					 ?>  
                        <div class="p-info-row"><h3><?php echo lang("military"); ?> : </h3><span><?php echo $military; ?></span> </div>
                     <?php } ?>   
                    </div>
                    <?php if( !(empty($cv[0]->objective))){ ?>  
                    <div>

                        <p><?php echo $cv[0]->objective ;?></p>

                    </div>
                    <?php } ?>
                    <?php if( !(empty($education))){ ?>
                    <div>
                    <?php foreach ($education as $item) { ?>
                        <div class="edu-row">
                            <h4 class="edu-head"><?php if( !(empty($item->type)))  echo $item->type ; ?></h4>
                            <div class="edu-desc">
                                <h3><?php if( !(empty($item->name)))  echo $item->name ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo "(". $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year .")" ; ?>
          
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo "(". $item->from_month."/".$item->from_year." - ".lang($item->to_month).")" ; ?>
        
	<?php	} ?></h3>
                                <p><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>
                            </div>
                        </div><!--edu-row-->
                  <?php } ?>   
                    </div>
                    <?php } ?>
                    <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
                    <div>
                    <?php foreach ($courses as $item) { ?>
                        <div class="edu-row">
                            <h4 class="edu-head"><?php  echo $item->name ; ?></h4>
                            <div class="edu-desc">
                                <h3><?php if( !(empty($item->institute))) echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
             
				<?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?>
              
				<?php } ?></h3>

                            </div>
                        </div><!--edu-row-->
                    <?php } ?>  
                    <?php foreach ($certifcates as $item) { ?>  
                        <div class="edu-row">
                            <h4 class="edu-head"><?php echo $item->name ; ?></h4>
                            <div class="edu-desc">
                                <h3><?php if( !(empty($item->institute))) echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                
				<?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?>
              
				<?php } ?></h3>

                            </div>
                        </div><!--edu-row-->
                    <?php } ?>    
                    </div>
                    <?php } ?>
                    <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
                    <div>
                    <?php if (!(empty($user_info[0]->exp_no))){ ?>
                        <div class="exp-head">
                        
                            <h2><?php echo lang('no_exp'); ?>: <span><?php echo $user_info[0]->exp_no ?></span></h2>
                           
                        </div>
                    <?php } ?> 
                    <?php foreach ($experience as $item) { ?>    
                        <div class="exp-row">
                            <h4 class="date"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
       
			<?php } ?></h4>
                            <div class="exp-desc">
                                <h3><?php if( !(empty($item->company)))  echo $item->company ; ?> <span><?php if( !(empty($item->name)))  echo "(".$item->name .")" ; ?></span></h3>
                                <?php  if(!empty($item->role)){ ?>  
                                <ul>
                                    <?php $roles=unserialize($item->role);
									 foreach ($roles as $r_item) { ?>
                                    <li><?php echo $r_item ; ?></li>
                                    <?php } ?>
                                </ul>
                                <?php } ?>
                                <p> <?php if( !(empty($item->description)))  echo $item->description ; ?></p>
                            </div><!--exp-desc-->
                        </div><!--exp-row-->
                        <?php } ?>
                        <?php foreach ($projects as $item) { ?>
                        <div class="exp-row">
                            <h4 class="date"><?php if( !(empty($item->name)))  echo "(".$item->name .")" ; ?></h4>
                            <div class="exp-desc">
                            <?php if( !(empty($item->link))){ ?>
                                <h3><a href="<?php echo $item->link ; ?>" style="color: #00A99D;font-size: 18px;"><?php echo $item->link ; ?></a><span></span></h3>
                             <?php } ?>    
                                <p> <?php if( !(empty($item->description)))  echo $item->description ; ?></p>
                            </div><!--exp-desc-->
                        </div><!--exp-row-->
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <?php if( !(empty($langs))){ ?> 
                    <div>
                    <?php foreach ($langs as $item) {  ?>
                        <div class="lang-row">
                            <h4><?php echo lang($item->name); ?></h4>
                            <span><?php if($item->grade=='p') echo lang('poor');
										elseif($item->grade=='f') echo lang('fair');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='v') echo lang('very_good');
										elseif($item->grade=='fl') echo lang('fluent');
										elseif($item->grade=='t') echo lang('tongue'); ?></span>
                        </div><!--lang-row-->
                    <?php } ?>    
                        
                    </div>
                    <?php } ?>
                    <?php if( !(empty($skills))){ ?> 
                    <div>
                    <?php foreach ($skills as $item) { ?>
                        <div class="lang-row">
                            <h4><?php echo $item->name ; ?></h4>
                            <span><?php if($item->grade=='i') echo lang('interemdate');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='w') echo lang('weak'); ?></span>
                        </div><!--lang-row-->
                   <?php } ?>     
                    </div>
                    <?php } ?>
                    <?php if( !(empty($qualifications))){ ?>
                    <div>
                        <div class="skills">
                        <?php foreach ($qualifications as $item) { ?>
                            <h4><?php echo $item->name ; ?></h4>
                        <?php } ?>
                        
                        </div>
                    </div>
                    <?php } ?>
                    <?php if( !(empty($references))){ ?>
                    <div>
                    <?php foreach ($references as $item) { ?>
                        <div class="ref-row">
                            <h4><?php if( !(empty($item->name))) echo $item->name; ?></h4>
                            <div class="ref-desc">
                                <span class="email"><?php if( !(empty($item->mail))) echo $item->mail ; ?></span>
                                <span class="phone"><?php if( !(empty($item->phone))) echo $item->phone ; ?></span>
                            </div>
                        </div><!--ref-row-->
                    <?php } ?>    
                    </div>
                    <?php } ?>
                    <div>
                     <div class="btn-wrapper">
                            <a class="btn" id="show-form"><?php echo lang('leave_recommendation'); ?></a>
                        <div id="form-wrapper">
                        
                        <span id="form-success"></span>
                            <form action="" method="post" class="recom-form">
                                <input name="cv_lang" type="hidden" value="<?php echo $cv[0]->lang ; ?>">
                                <input name="cv" type="hidden" value="<?php echo $cv[0]->id ; ?>">
                                <input name="user_id" type="hidden" value="<?php echo $user_id ; ?>">
                                <span  id="error_message_name"></span>
                                <input type="text" placeholder="<?php echo lang('name') ?>" name="name" />
                                <span  id="error_message_job"></span>
                                <input type="text" placeholder="<?php echo lang('job') ?>" name="job" />
                                <span  id="error_message_recommend"></span>
                                <textarea rows="4" placeholder="<?php echo lang('recommend') ?>" name="recommend" ></textarea>
                                <input class="btn-submit" id="save_btn" type="button" value="<?php echo lang('recommend') ?>">
                            </form>
                        </div><!--form-wrapper-->
                       
                        </div> 
                        <?php foreach($recommends as $item){ ?> 
                        <div class="reccom-row">
                            <h4><?php echo $item->name; ?><span>(<?php echo date("d/m/Y" ,$item->date); ?>)</span></h4>
                            <span class="recom-pos"><?php echo $item->job; ?></span>
                            <p class="recom-text"><?php echo $item->recommend; ?></p>
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </div>

        </div>
        <div class="footer">
            <span>copyright&#169;e3rfny.com</span>
        </div>
		<script src="<?php echo base_url(); ?>styles/js/jquery.js"></script>
        <script src="<?php echo base_url("styles/css/tab/js/easyResponsiveTabs.js") ?>" type="text/javascript"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {
               
                $('#verticalTab').easyResponsiveTabs({
                    type: 'vertical',
                    width: 'auto',
                    fit: true,
                    closed: 'accordion', // Start closed if in accordion view
                    
                });

            });
        </script>
        <script>
            $(document).ready(function() {
				
                $('#show-form').click(function() {
                    $('#form-wrapper').slideToggle("fast");
                });
            });
			
			$('#save_btn').click(function () {
				
				dataString = $(".recom-form").serialize();
				
				var request2 = $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>profile/save",
                     data: dataString,
					 dataType : 'json',
                     success: function(result) {
					
						if(result=='<?php echo lang('done') ?>'){
							$('.error_message').removeClass("error_message").html("");
							$('#form-success').addClass("form-success").html("<?php echo lang('done') ?>");
							//setTimeout(function() { $("#form-success").fadeOut(1500); }, 5000);
							
							
						}else{
							
							$('#form-success').removeClass("form-success").html("");
							$('.error_message').removeClass("error_message").html("");
							if(result['name'])
								$('#error_message_name').addClass("error_message").html(result['name']);
							if(result['job'])	
								$('#error_message_job').addClass("error_message").html(result['job']);
							if(result['recommend'])
								$('#error_message_recommend').addClass("error_message").html(result['recommend']);
						}
						
					},
											
				});
				
   			 });

        </script>
        <script>
            $(document).ready(function() {
              
                var NewHeight = $('#profile-img').width();
                $('#profile-img').css('height', NewHeight);
            });

        </script>
    </body>
</html>