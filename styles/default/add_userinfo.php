<form name="form1" action="<?php echo site_url(); ?>/user_info/add" method="post" enctype="multipart/form-data">
  <table width="200" border="1">
   <tr>
      <td><input type="text" tabindex="4" name="title" value="<?php echo set_value('title'); ?>">
       <div class="form-msg-error-text"></div></td>
      <td><?php echo lang('job'); ?></td>
    </tr>
    <tr> 
    <tr>
      <td><input type="text" tabindex="4" name="nationality" value="<?php echo set_value('nationality'); ?>">
       <div class="form-msg-error-text"></div></td>
      <td><?php echo lang('nationality'); ?></td>
    </tr>
    
    <tr>
      <td><select id="month" name="month">
      <option selected="1" value="0"><?php echo lang('month'); ?></option>
        <option value="1"><?php echo lang('January'); ?></option>
        <option value="2"><?php echo lang('February'); ?></option>
        <option value="3"><?php echo lang('March'); ?></option>
        <option value="4"><?php echo lang('April'); ?></option>
        <option value="5"><?php echo lang('May'); ?></option>
        <option value="6"><?php echo lang('June'); ?></option>
        <option value="7"><?php echo lang('July'); ?></option>
        <option value="8"><?php echo lang('August'); ?></option>
        <option value="9"><?php echo lang('September'); ?></option>
        <option value="10"><?php echo lang('October'); ?></option>
        <option value="11"><?php echo lang('November'); ?></option>
        <option value="12"><?php echo lang('December'); ?></option>
        </select>
        
        <select id="day" name="day">
        <option selected="1" value="0"><?php echo lang('day'); ?></option>
        <?php for($i=1;$i<=31;$i++){ ?>
        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
        <?php } ?>
        </select>
        
        <select id="year" name="year">
        <option selected="1" value="0"><?php echo lang('year'); ?></option>
        <?php for($i=1975;$i<=year;$i++){ ?>
        <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
        <?php } ?>
        </select>
        
        </td>
      <td><?php echo lang('birth_date'); ?></td>
    </tr>
    <tr>
    <td><select name="marital_rstatus">
    	<option selected="1" value="n"><?php echo lang('select_from_list');?></option>
        <option value="s"><?php echo lang('single');?></option>
        <option value="m"><?php echo lang('married');?></option>
        <option value="d"><?php echo lang('divercoed');?></option>
    </select>
    </td>
    <td><?php echo lang('marital_rstatus'); ?>
    </td>
    </tr>
    <tr>
    <td><textarea name="address1"></textarea></td>
    <td><?php echo lang('Address1'); ?></td>
    </tr>
     <tr>
    <td><textarea name="address2"></textarea></td>
    <td><?php echo lang('Address2'); ?></td>
    </tr>
    <a href="javascript:void(0);" onclick="add_phone()"><?php echo lang('add'); ?> </a>
    <div id="phones"><input type="text" name="code[]"  size="5"  />
    <input type="text" name="number[]"  />
    <?php echo lang('phone'); ?>
   </div>
   <div id="upload" ><?php echo lang('image'); ?></div><span id="status" >
   </span>
  	<div class="flexform">
	<ul id="media-type-1"></ul>
     </div>
   
    <tr>
      <td colspan="2">
      <input type="hidden" name="count" id="count"  />
      <input type="submit" name="button" id="button" value="<?php echo lang('next'); ?>" /> </td>
      
    </tr>
  </table>
</form>
<script>
var count=0;
function add_phone(){
	
	count++;
	//alert(count);
	$('#phones').append('<div id="phone'+count+'"><input type="text" name="code[]" id="code'+count+'" size="5" /><input type="text" id="number'+count+'" name="number[]" /><a href="javascript:void(0);" id="'+count+'" onclick="delete_phone(this.id)"><?php echo lang('delete'); ?> </a></div>');
	
	$('#count').val(count)	;
}
function delete_phone(id) {
	 
  	var checkstr =  confirm('are you sure you want to delete this?');
    if(checkstr == true){
		//alert(id);
		count--;
		$("#phone"+id).remove();
		$('#count').val(count)	;
		
    }else{
    	return false;
    }
}


</script>
 <script type="text/javascript" src="<?php echo base_url() ?>styles/js/ajaxupload.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>styles/css/ajaxupload.css" />
<script type="text/javascript" >
                    
                    var allownum = 1;
                        var uploadnum = <?php echo (isset($img) && count($img))?count($img):0  ?>;
                    $(function(){
                       
                        var status=$('#status');
                        new AjaxUpload($('#upload') , {
                            action: '<?php echo site_url('user_info/user_data/') ?>',
                            name: 'ajaxuploader',
                            onSubmit: function(file, ext){
                                if (! (ext && /^(jpg|png|jpeg|gif)$/.test(ext)))
                                { 
                                    status.text
                                    ('<?php echo lang('img_typemsg'); ?>');
                                    return false;
                                }
                                /*if(uploadnum >= allownum){
                                    status.text
                                    ('Max count of uploaded files is: '+allownum);
                                    return false;
                                }*/
                                status.text('Uploading...');
                            },
                            onComplete: function(file, response){
                                //On completion clear the status
                                $('#files div.clear').remove();
                                status.text('');
                                var responeobj = $.parseJSON(response);
                                if(responeobj.status== true){
                                    uploadnum++;
									
									$('#media-type-1').html('<img width="150" height="150" src="<?php echo base_url() ?>'
                                        +responeobj.full_url+'" alt="" /> <span class="ctrls" style="display:block"><a alt="'+responeobj.full_url+'\"  class="removeupload edit gradient-btn" title="delete photo" ><?php echo lang('delete'); ?></a></span><input type="hidden" name="ajaxupload" value="'+responeobj.client_name+'\" /><br />').addClass('success');
                                  
                                    $('.removeupload').click(function(){
										 removeobject = $(this);
										 var removeurl ='<?php echo site_url("user_info/delete_img/"); ?>';
                                        $.post(removeurl,{'img_name':$(this).attr('alt')},function(data){
											
                                        });
										
										removeobject.parents("li.success").slideUp().remove();
										//uploadnum--;
										$("#removeimages").append("<input type='hidden' name='removeimd' value='"+$(this).attr
										('alt')+"' />");
										
										$('#media-type-1').html('');
                                    });

                                   

                                    }
                                    $('<div class="clear"></div>').appendTo("media-type-1")      ;
                                    }
                            });
                            
                            
                            
                }); 
		
 
</script>