<!DOCTYPE HTML>
<html>
<head>
      <link href="<?php echo base_url("styles/css/style1_".$cv->lang.".css") ?>" rel="stylesheet">
     
  </head>
  <body>
    
      <div class="containe">
     <div class="name-bar">
     <?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
          <img src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="square">
        <?php } ?> 
          <h1 class="fullname"> <?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
          <?php  //if($user_info[0]->title !=''){ ?>
          	<h3 class="position"><?php echo $user_info[0]->title ; ?></h3>
         <?php //} ?> 
         <?php //if( !(empty($cv->objective))){ ?>  
        <p style="color:#000;"><?php echo $cv->objective ;?></p>
        <?php //} ?>  
  </div>
     	 
        
      </div>
    
    
      <div class="contain">
        <div class="coll">
          <h2><?php echo strtoupper(lang("personal_info")) ; ?></h2>
        </div>
        <div class="col">
        <?php if($user_info[0]->nationality !=''){ ?>
          <h1><?php echo lang("nationality");?>: <span class="nation"><?php echo $user_info[0]->nationality; ?></span></h1>
       <?php } ?>  
        <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>  
          <h1><?php echo lang("birth_date") ; ?>: <span class="nation"><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h1>
        <?php } ?>   
         <?php if($user_info[0]->marital_status !='n'){ 
		 	if($user_info[0]->marital_status =='s'){
				$m_status=lang("single");
			}elseif($user_info[0]->marital_status =='m'){
				$m_status=lang("married");
			}elseif($user_info[0]->marital_status =='d'){
				$m_status=lang("divercoed");
			}
		 ?>  
          <h1><?php echo lang("marital_rstatus"); ?>: <span class="nation"><?php echo $m_status ; ?></span></h1>
		<?php } ?> 
        
        <?php if($user_info[0]->gender !='n'){ 
				if($user_info[0]->gender =='m'){
					$gender=lang("male");
				}elseif($user_info[0]->gender =='f'){
					$gender=lang("female");
				}
		?>    
          <h1><?php echo lang("gender"); ?>: <span class="nation"><?php echo $gender ; ?></span></h1>
        <?php } ?>  
        <?php if($user_info[0]->license !='n'){ 
				 if($user_info[0]->license =='y'){
					$License=lang("yes");
				}elseif($user_info[0]->license =='no'){
					$License=lang("no");
				}
		?>  
          <h1><?php echo lang("License"); ?>: <span class="nation"><?php echo $License ; ?></span></h1>
          <?php } ?>
          <?php if($user_info[0]->military !='n'){ 
				if($user_info[0]->military =='e'){
					$military=lang("exemption");
				}elseif($user_info[0]->military =='c'){
					$military=lang("Complete_service");
				}elseif($user_info[0]->military =='p'){
					$military=lang("Postponed");
				}elseif($user_info[0]->military =='s'){
					$military=lang("Currently_serving");
				}elseif($user_info[0]->military =='d'){
					$military=lang("Doesnt_apply");
				}	 
			?> 
          <h1><?php echo lang("military"); ?>: <span class="nation"><?php echo $military; ?></span></h1>
		 <?php } ?>
        </div>
        <div class="cole">
          <h1><?php echo lang("emaill") ;?>: <span class="nation"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></span></h1>
        <?php if( !(empty($phones))){ ?>
          <h1><?php echo lang("phones"); ?>:</h1>
          <?php
				foreach ($phones as $item) {
		  ?>
          <span class="nation"><?php echo $item->code.$item->number ; ?></span>
          <br>
          <?php } } ?>
      
		 <?php if($user_info[0]->adress1 !=''){ ?>
          <h1><?php echo lang("address") ; ?>:</h1>
          <span class="nation"> <?php echo $user_info[0]->adress1; ?></span>
          <br>
          <?php } ?>
          <?php if($user_info[0]->adress2 !=''){ ?>
           <h1><?php echo lang("another_address") ; ?>:</h1>
          <span class="nation"> <?php echo $user_info[0]->adress2; ?></span>
          <br>
          <?php } ?>

             </div>
         <?php if( !(empty($social))){ ?>
         <div class="col-2">
          <?php foreach ($social as $item) { ?>
          <h1><?php if( !(empty($item->name)))  echo $item->name ; ?>:</h1>
          <span class="nation"><a style="color: #404041;font-size:13px;" href="<?php if( !(empty($item->link)))  echo $item->link ; ?>"><?php if( !(empty($item->link)))  echo $item->link ; ?></a></span>
          <?php } ?>
         </div>
         <?php } ?>
      </div>
     
        <?php if( !(empty($education))){ ?>
         <div class="contai">
        <div class="col-3">
          <h3><?php echo strtoupper(lang("education")); ?></h3>
        </div>
        <div class="col-4">
        <?php foreach ($education as $item) { ?>
        <div class="two">
        <h1><?php if( !(empty($item->type)))	echo $item->type ; ?></h1>
        <?php if($this->data['cv']->lang=='en'){ ?>
        <ul class="notes">
          <li><span class="nation">
		  <?php if( !(empty($item->name)))  echo $item->name ; ?> 
		  <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
       <div class="date">

			<?php echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year .")" ; ?>
       </div>     
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        <div class="date">
		<?php	echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month).")" ; ?>

        </div>    
	<?php	} ?>
    </span>
            <br>
            <span class="nation"><?php if( !(empty($item->desc))) echo $item->desc ; ?>  </span> 
            <br>
           </li>
            </ul>
            <?php }elseif($this->data['cv']->lang=='ar'){ ?>
            <ul class="notes">
          <li><span class="nation">
		  <?php if( !(empty($item->name)))  echo $item->name ; ?> 
		  <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
        
       <div class="date">

			<?php echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year .")" ; ?>

       </div> 
           
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
        <div class="date">
        

		<?php	echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month).")" ; ?></div>
         
	<?php	} ?>
    </span>
            <br>
            <span class="nation"><?php if( !(empty($item->desc)))	echo $item->desc ; ?>  </span> 
            <br>
           </li>
            </ul>
            <?php } ?>
        </div>
        <?php } ?>
        
        </div>
      </div>
   <?php } ?>  
   <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
	<div class="contai">
        <div class="col-3">
          <h3><?php if( !(empty($courses))) { echo strtoupper(lang("courses")) ;} ?> </h3>
          <br>
          <h4><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($certifcates))) echo strtoupper(lang("certifcates")) ; ?></h4>
        </div>
        <div class="col-4">
        <?php foreach ($courses as $item) { ?>
           <div class="one">
            <ul class="notes">
            <?php if( !(empty($item->name))){ ?>
              <li><span class="diploma"><b><?php  echo strtoupper($item->name) ; ?></b></span></li>
              
               <br>
               <?php } ?>
                <span class="diploma"><?php if( !(empty($item->institute))) echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                <div class="date">

				<?php echo "(".$item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year.")" ; ?>

                </div>
				<?php } ?></span> 
               </ul>
               </div>
         <?php } ?>      
            
            <?php foreach ($certifcates as $item) { ?>
           <div class="one">
            <ul class="notes">
            <?php if( !(empty($item->name))){ ?>
              <li><span class="diploma"><b><?php echo strtoupper($item->name) ; ?></b></span></li>
              
               <br>
            <?php } ?>   
                <span class="diploma"><?php if( !(empty($item->institute))) echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                <div class="date">
			<?php	echo "(".$item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year.")" ;?>

            </div>
			<?php } ?></span> 
               </ul>
               </div>
         <?php } ?>      
            </div>
        
      </div>
      <?php } ?> 
      <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
<div class="conta ">
        <div class="col-3">
          <h3><?php if( !(empty($experience))) { echo strtoupper(lang("experience")) ;} ?> </h3>
         <br>
          <h4><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($projects))) echo strtoupper(lang("projects")) ; ?></h4><?php if(!(empty($user_info[0]->exp_no))){ echo lang('no_exp') ."&nbsp;"."(". $user_info[0]->exp_no ."&nbsp;".")" ;} ?>
        </div>
        <div class="col-4">
        <?php foreach ($experience as $item) { ?>
       <div class="three">
        <ul class="notes">
          <li ><h3  ><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>

        <div class="date">

		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        </div>
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    <div class="date">
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
            </div>
			<?php } ?></h3></li>
         <br>
            <span class="diploma"><b><?php if( !(empty($item->company)))  echo $item->company ; ?></b> <?php if( !(empty($item->name)))  echo "(".$item->name .")" ; ?></span>
            <br> 
            <?php  if(!empty($item->role) !=0){ ?>  
             <ul  class="notes">
             <?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
          <li >  <span  class="nation"><?php echo $r_item ; ?></span> 
		  </li>
           <?php } ?>
            
                 </ul>
               <?php } ?>  
            
                 </ul>
                     <span  class="diploma"><?php if( !(empty($item->description)))  echo $item->description ; ?></span>

        </div>
        <?php } ?>
        
        <?php foreach ($projects as $item) { ?>   
     <div class="three">
        <ul class="notes">
       
            <span class="diploma"><b><?php if( !(empty($item->name)))  echo $item->name ; ?></b> <?php if( !(empty($item->link))){ ?><a style="color: #404041;font-size:13px;" href="<?php echo $item->link  ; ?>"><?php echo "(".$item->link .")" ; ?></a><?php } ?></span>
            <br> 
            <span class="diploma"><?php if( !(empty($item->description)))  echo $item->description ; ?></span>
            
                 </ul>
         
        </div>
        <?php } ?>
        </div>
       </div>
       <?php } ?> 
      <?php if( !(empty($langs))){ ?> 
 <div class="con">
        <div class="col-66">
          <h3><?php echo strtoupper(lang("langs")) ; ?> </h3>
          
        </div>
        <div class="col-4">
       <div class="four">
        <ul class="notes">
        <?php foreach ($langs as $item) {  ?>
          <li><span class="nation"><?php if( !(empty($item->name))) echo lang($item->name)."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='p') echo lang('poor');
				elseif($item->grade=='f') echo lang('fair');
				elseif($item->grade=='g') echo lang('good');
				elseif($item->grade=='v') echo lang('very_good');
				elseif($item->grade=='fl') echo lang('fluent');
				elseif($item->grade=='t') echo lang('tongue'); ?></span> 
		  </li>
          <?php } ?>
           
                 </ul>
         
        </div>
        </div>
      </div>
      <?php } ?>
      <?php if( !(empty($qualifications))){ ?>
      <div class="con">
        <div class="col-7">
          <h3><?php echo strtoupper(lang("qualifications")) ; ?> </h3>
          
        </div>
        <div class="col-12">
       
        <div class="note">
        <ul style="float:left; width:400px; ">
       
        <?php foreach ($qualifications as $item) { ?>
                <li  >   <li  >  <span class="nation"><?php echo $item->name ; ?></span></li>

        <?php } ?>
</ul>
            
            
               </div>
            
         
        </div>
        
      </div>
    <?php } ?>  
    <?php if( !(empty($skills))){ ?>
 <div class="con">
        <div class="col-8">
          <h3><?php echo strtoupper(lang("skills")) ; ?> </h3>
          
        </div>
        <div class="col-4">
       
        
          
       <div class="four">
        <div class="note">
                  <ul>
                  <?php foreach ($skills as $item) { ?>
                <li>  <span class="nation"><?php if( !(empty($item->name))) echo $item->name."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='i') echo lang('interemdate');
																				elseif($item->grade=='g') echo lang('good');
																				elseif($item->grade=='w') echo lang('weak'); ?></span> </li>
                <?php } ?>


</ul>
            
            
            
               </div>
         
                 
         
                 
        </div>
        </div>
      </div>
      <?php } ?>
      <?php if( !(empty($references))){ ?>
<div class="cnta">
        <div class="col-6">
          <h3><?php echo strtoupper(lang("references")) ; ?> </h3>
          
        </div>
        <div class="col-4">
        <?php foreach ($references as $item) { ?>
       <div class="five">
        <ul class="notes">
          <li><span class="diploma"><b><?php if( !(empty($item->name))) echo $item->name; ?></b></span></li>
          <?php if( !(empty($item->mail))) { ?>
           <br>
            <span class="diploma"><?php echo $item->mail ; ?></span>
            <?php } ?>
            <?php if( !(empty($item->phone))){?>
            <br> 
            <span class="diploma"><?php echo $item->phone ; ?></span>
            <?php } ?>
                 </ul>
         
        </div>
       <?php } ?>
     

        </div>
        </div>
       <?php } ?> 
  </body>
  </html>