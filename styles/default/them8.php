<html>
<head>
 <link href="<?php echo base_url("styles/css/style8_".$cv->lang.".css") ?>" rel="stylesheet">
 <!--<link href="final-arabic.css" rel="stylesheet">-->
</head>
<body> 
     <div class="head-wrapper">
        <div class="contact">
            
            <div class="contact-icon"> <img src="<?php echo base_url(); ?>assets/images/contact.jpg" /></div>
            <div class="contact-head"><span><?php echo strtoupper(lang("contacts")); ?></span></div>
            <?php if( !(empty($phones))){ ?> 
           <div class="phone">
             <table>
               <tr>
               <?php
					foreach ($phones as $item) {
			   ?>  
                <td><?php echo lang("phone"); ?> : <?php echo $item->code.$item->number ; ?></td>
               <?php } ?>
               </tr>
              </table>
           </div>
           <?php } ?>
           <div class="email">
            <span><?php echo lang("emaill") ;?> :</span>
            <span><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></span> 
           </div> 
        </div><!--contact-->
        <?php if( !(empty($social))){ ?>   
        <div class="social">
          <?php foreach ($social as $item) { ?>  
            <div class="social-row">
              <?php if($this->data['cv']->lang=='en'){ ?>
               <div class="social-img"><img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'fb.jpg';}elseif($item->name=='twitter'){echo 'twitter.jpg';}elseif($item->name=='linkledin'){echo 'ln.png';}elseif($item->name=='google'){echo 'gogle.png';}elseif($item->name=='behance'){echo 'bc.png';} ?>"/></div>
               <div class="social-link"><span><a style="color:#464646" href="<?php echo $item->link; ?>"><?php echo $item->link; ?></a></span></div>	<?php }elseif($this->data['cv']->lang=='ar'){ ?>
               <div class="social-link"><span><a style="color:#464646" href="<?php echo $item->link; ?>"><?php echo $item->link; ?></a></span></div>
               <div class="social-img"><img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'fb.jpg';}elseif($item->name=='twitter'){echo 'twitter.jpg';}elseif($item->name=='linkledin'){echo 'ln.png';}elseif($item->name=='google'){echo 'gogle.png';}elseif($item->name=='behance'){echo 'bc.png';} ?>"/></div>
               
        <?php } ?>       
            </div>
           <?php } ?> 
            
        </div><!--social-->
        <?php } ?>
     </div><!--head-wrapper-->
     <div class="profile">
         <div class="profile-img">
         <?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
         <img src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>"/>
         <?php } ?>
         </div>  
         <div class="name-wrapper">
           <div class="name"><span><?php echo ucwords($user_info[0]->user_cv_name) ; ?></span></div>
           <div class="position"><span><?php echo $user_info[0]->title ; ?></span></div>
         </div><!--name-wrapper-->  
         <?php if($user_info[0]->adress1 !=''){ ?>
         <div class="add">
            <div class="loc-icon"> <img src="<?php echo base_url(); ?>assets/images/loc.png" /></div>
            <div class="add-head"><span><?php echo lang("address") ; ?></span></div>
             <div class="address"><span><?php echo $user_info[0]->adress1; ?></span></div>

         </div><!--add-->
         <?php } ?>
         <?php if($user_info[0]->adress2 !=''){ ?>
         <div class="add">
            <div class="loc-icon"> <img src="<?php echo base_url(); ?>assets/images/loc.png" /></div>
            <div class="add-head"><span><?php echo lang("another_address") ; ?></span></div>
             <div class="address"><span>  <?php echo $user_info[0]->adress2; ?></span></div>

         </div><!--add-->
         <?php } ?>
     </div><!--profile-->
     <div class="cv-content">
      
       
       <div class="cv-title-wrapper">
          <div class="cv-title-head">
              <div class="side-img-quote"><img src="<?php echo base_url(); ?>assets/images/quote.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php echo strtoupper(lang("objective")) ;?></span></div>
          </div><!--cv-title-head-->
          <div class="cv-title-content">
                   <div class="p">
                  <p><?php echo $cv->objective ;?>
                  </p></div>
                  <div class="info">
                  <?php if($user_info[0]->nationality !=''){ ?>
                    <div><span><?php echo lang("nationality");?> :</span><?php echo $user_info[0]->nationality; ?></div>
                  <?php } ?>  
                  <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?> 
                    <div><span><?php echo lang("birth_date") ; ?> :</span><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></div>
                 <?php } ?>  
                 <?php if($user_info[0]->marital_status !='n'){ 
						 if($user_info[0]->marital_status =='s'){
							$m_status=lang("single");
						}elseif($user_info[0]->marital_status =='m'){
							$m_status=lang("married");
						}elseif($user_info[0]->marital_status =='d'){
							$m_status=lang("divercoed");
						}
				?>  
                    <div><span><?php echo lang("marital_rstatus"); ?> :</span><?php echo $m_status ; ?></div>
                <?php } ?> 
                <?php if($user_info[0]->gender !='n'){ 
						if($user_info[0]->gender =='m'){
							$gender=lang("male");
						}elseif($user_info[0]->gender =='f'){
							$gender=lang("female");
						}
				?>      
                    <div><span><?php echo lang("gender"); ?> :</span><?php echo $gender ; ?></div>
                <?php } ?>   
                <?php if($user_info[0]->license !='no'){ 
						if($user_info[0]->license =='y'){
							$License=lang("yes");
						}elseif($user_info[0]->license =='n'){
							$License=lang("no");
						}
				?>    
                    <div><span><?php echo lang("License"); ?> :</span><?php echo $License ; ?></div> 
                <?php } ?>
                <?php if($user_info[0]->military !='n'){ 
						if($user_info[0]->military =='e'){
							$military=lang("exemption");
						}elseif($user_info[0]->military =='c'){
							$military=lang("Complete_service");
						}elseif($user_info[0]->military =='p'){
							$military=lang("Postponed");
						}elseif($user_info[0]->military =='s'){
							$military=lang("Currently_serving");
						}elseif($user_info[0]->military =='d'){
							$military=lang("Doesnt_apply");
						}	 
				?>  
                <div><span><?php echo lang("military"); ?> :</span><?php echo $military; ?></div> 
                <?php } ?>    
                  </div>
          </div><!--cv-title-content-->
             <?php if( !(empty($education))){ ?>                                 
            <div class="cv-school-wrapper">
           
             <div class="cv-school-head">
              <div class="side-img-school"><img src="<?php echo base_url(); ?>assets/images/hat.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php echo strtoupper(lang("education")); ?></span></div>
          </div><!--cv-school-head-->
          <div class="cv-school-content">
                    <!-- loop item-->
                    <?php $education_size=sizeof($education); $i=1; foreach ($education as $item) { ?>
                   <div class="date"><span><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) && ($item->to_month !='now')){?>
        
			<?php echo  $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year  ; ?>
      
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
          
	<?php	} ?></span></div>
                   <div class="edu-name"><span><?php if( !(empty($item->name)))  echo $item->name ; ?>  (<?php if( !(empty($item->type)))  echo $item->type ; ?>)</span></div>
                   <div class="edu-detail"><span><?php if( !(empty($item->desc)))	echo $item->desc ; ?>
                   </span></div>
                   <!-- end loop item-->
                   <?php if($i != $education_size){ ?>
                  	<div class="hr"></div>
                   <?php } ?> 
                  <?php $i++; } ?>
                     
          </div><!--cv-school-content-->
          <?php } ?>
          <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>
          <div class="cv-courses-wrapper">
           
            <div class="cv-course-head">
              <div class="side-img-course"><img src="<?php echo base_url(); ?>assets/images/coursee.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php if( !(empty($courses))) { echo strtoupper(lang("courses")) ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo strtoupper(lang("certifcates")) ; ?></span></div>
          </div><!--cv-school-head-->
          <div class="cv-school-content">
                    <!-- loop item-->
                    <?php $corses_size=sizeof($courses); $i=1; foreach ($courses as $item) { ?>
                   <div class="date"><span><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
				<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></span></div>
                   <div class="edu-name"><span><?php if( !(empty($item->name)))  echo strtoupper($item->name) ; ?>  <?php if( !(empty($item->institute)))  echo "( ". $item->institute ." )" ; ?></span></div>
                   <div class="edu-detail"><span>
                   </span></div>
                   <!-- end loop item-->
                   <?php if($i != $corses_size || !(empty($certifcates))){ ?>
                  	<div class="hr"></div>
                  <?php } ?>
                  <?php $i++; } ?>
                     <?php $certifcates_size=sizeof($certifcates); $i=1; foreach ($certifcates as $item) { ?>
                   <div class="date"><span><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
				<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></span></div>
                   <div class="edu-name"><span><?php if( !(empty($item->name)))  echo strtoupper($item->name) ; ?>  <?php if( !(empty($item->institute)))  echo "( ". $item->institute ." )" ; ?></span></div>
                   <div class="edu-detail"><span>
                   </span></div>
                   <!-- end loop item-->
                   <?php if($i != $certifcates_size){ ?>
                  	<div class="hr"></div>
                   <?php } ?> 
                  <?php $i++; } ?>
          </div><!--cv-school-content-->

                  
          </div><!--cv-school-content-->
       </div><!--cv-exp-wrapper-->
       <?php } ?>
       <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
       <div class="cv-exp-wrapper">
             
            <div class="cv-exp-head">
              <div class="side-img-exp"><img src="<?php echo base_url(); ?>assets/images/expp.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php if( !(empty($experience))) { echo strtoupper(lang("experience")) ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo strtoupper(lang("projects")) ; ?></span></div>
          </div><!--cv-school-head-->
          <div class="cv-school-content">
                    <div class="exp-years"><span><?php if(!(empty($user_info[0]->exp_no))){ echo "&nbsp;"."(".lang('no_exp') ."&nbsp;".":"."&nbsp;". $user_info[0]->exp_no ."&nbsp;".")" ;} ?> </span></div>
                    <!-- loop item-->
                    <?php $experience_size=sizeof($experience); $i=1; foreach ($experience as $item) {  ?>
                   <div class="date"><span><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
            
			<?php } ?></span></div>
                   <div class="edu-name"><span><?php echo $item->company ; ?>  <?php if(!empty($item->name)) {echo "( ". $item->name ." )" ;}?></span></div>
                   <?php if(!empty($item->role)){ ?>  
                   <div class="edu-name"><ul style="list-style:square;">
					<?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
                    <li><?php echo $r_item ; ?></li>
                    <?php } ?>
                    </ul></div>
                    <?php } ?>
                   <div class="edu-detail"><span><?php if( !(empty($item->description)))  echo $item->description ; ?>
                   </span></div>
                   <!-- end loop item-->
                  <?php if($i != $experience_size || !(empty($projects))){ ?>
                  	<div class="hr"></div>
                   <?php } ?> 
                  <?php $i++; } ?>
                  
                  <!-- loop item-->
                    <?php $projects_size=sizeof($projects); $i=1; foreach ($projects as $item) {  ?>
                   <div class="date"><span><a style="color:#464646" href="<?php echo $item->link ; ?>"><?php echo $item->link ; ?></a></span></div>
                   <div class="edu-name"><span><?php if(!empty($item->name)) {echo  $item->name  ;}?></span></div>
                   <div class="edu-detail"><span><?php if( !(empty($item->description)))  echo $item->description ; ?>
                   </span></div>
                   <!-- end loop item-->
                  <?php if($i != $projects_size){ ?>
                  	<div class="hr"></div>
                   <?php } ?> 
                  <?php $i++; } ?>
                     
          </div><!--cv-exp-content-->

                  
          </div><!--cv-school-content-->
       </div><!--cv-courses-wrapper-->



       
       </div><!--cv-title-wrapper-->
     </div><!--cv-content--> 
	<?php } ?>
    <?php if( !(empty($skills))){ ?>
      <div class="cv-skills-wrapper">
              <div class="cv-skills-head">
                 <div class="side-img-skills"><img src="<?php echo base_url(); ?>assets/images/star.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php echo strtoupper(lang("skills")) ; ?></span></div>
              </div><!--skills-head-->
              <div class="cv-skills-content">
                  <div class="ul-wrapper"><ul>
                  <?php foreach ($skills as $item) { ?>
                     <li><?php if( !(empty($item->name))) echo $item->name."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='i') echo lang('interemdate');
																				elseif($item->grade=='g') echo lang('good');
																				elseif($item->grade=='w') echo lang('weak'); ?></li>
                 <?php } ?>
                     
                  </ul></div>
              </div><!--cv-skills-content-->
      </div><!---skills-wrapper-->
		<?php } ?>
		<?php if( !(empty($qualifications))){ ?>
      <div class="cv-pskills-wrapper">
              <div class="cv-pskills-head">
                 <div class="side-img-skills"><img src="<?php echo base_url(); ?>assets/images/star.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php echo strtoupper(lang("qualifications")) ; ?></span></div>
              </div><!--skills-head-->
              <div class="cv-skills-content">
                  <div class="ul-wrapper"><ul>
                     <?php foreach ($qualifications as $item) { ?>
                     	<li><?php echo $item->name ; ?></li>
                     <?php } ?>
                  </ul></div>
              </div><!--cv-skills-content-->
      </div><!---skills-wrapper-->
	<?php } ?>
    <?php if( !(empty($langs))){?>
        <div class="cv-lang-wrapper">
              <div class="cv-lang-head">
                 <div class="side-img-lang"><img src="<?php echo base_url(); ?>assets/images/lang.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php echo strtoupper(lang("langs")) ; ?></span></div>
              </div><!--lang-head-->
              <div class="cv-lang-content">
                  <div class="ul-wrapper"><ul>
                     <?php foreach ($langs as $item) {  ?>
                     <li><?php if( !(empty($item->name))) echo lang($item->name)."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='p') echo lang('poor');
				elseif($item->grade=='f') echo lang('fair');
				elseif($item->grade=='g') echo lang('good');
				elseif($item->grade=='v') echo lang('very_good');
				elseif($item->grade=='fl') echo lang('fluent');
				elseif($item->grade=='t') echo lang('tongue'); ?></li>
                     <?php } ?>
                  </ul></div>
              </div><!--cv-lang-content-->
      </div><!---skills-wrapper-->
      <?php } ?>
      <?php if( !(empty($references))){ ?>
       <div class="cv-refs-wrapper">
           
            <div class="cv-ref-head">
              <div class="side-img-ref"><img src="<?php echo base_url(); ?>assets/images/reff.png"/></div>
              <div style="width:100%; color:#fff; font-size:25px; padding:5% 10% 10% 10%; text-align:center;"><span><?php echo strtoupper(lang("references")) ; ?></span></div>
          </div><!--cv-ref-head-->
          <div class="cv-ref-content">
                    <!-- loop item-->
                   <?php $references_size=sizeof($references); $i=1; foreach ($references as $item) { ?>
                   <div class="ref-name"><span><?php if( !(empty($item->name))) echo $item->name; ?></span></div>
                   <div class="edu-detail"><span><?php echo $item->mail ; ?></span></div>
                   <div class="edu-detail"><span><?php echo $item->phone ; ?></span></div>
                   <!-- end loop item-->
                  <?php if($i != $references_size){ ?>
                  	<div class="hr"></div>
                   <?php } ?> 
                  <?php $i++; } ?>
                    <!-- end loop item-->
                    
          </div><!--ref-content-->

                  
          </div><!--cv-school-content-->
       </div><!--cv-exp-wrapper-->
       <?php } ?>
   </body>
   </html>    