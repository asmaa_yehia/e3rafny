  <?php if (isset($cv_id)&&isset($userinfo)&& (!empty($userinfo)) ) { ?>
        <ul class="tabs">
            <li data-step="1" <?php if ($step == 1) { ?> class="active" <?php } ?>  ><a href="<?php echo site_url() ?>/add_cv/back_cv/<?php echo $cv_id; ?>"><?php echo lang('cv_step');?></a></li>
            <li data-step="2" <?php if ($step == 2) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/add_cv/view_user/<?php echo $cv_id; ?>"><?php echo lang('personal_info');?></a></li>
            <li data-step="3"  <?php if ($step == 3) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/add_cv/back_edu/<?php echo $cv_id; ?>"><?php echo lang('edu_step');?></a></li>
            <li data-step="4"  <?php if ($step == 4) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/add_cv/back_course/<?php echo $cv_id; ?>"><?php echo lang('courses');?></a></li>
            <li data-step="5"  <?php if ($step == 5) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/add_cv/back_exp/<?php echo $cv_id; ?>"><?php echo lang('exp_step');?></a></li>
            <li data-step="6"  <?php if ($step == 6) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/add_cv/back_lang/<?php echo $cv_id; ?>"><?php echo lang('skills_step');?></a></li>
            <li data-step="7"  <?php if ($step == 7) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/add_cv/back_ref/<?php echo $cv_id; ?>"><?php echo lang('refrences');?></a></li>
            <li data-step="8"  <?php if ($step == 8) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/thems/index/<?php echo $cv_id; ?>"><?php echo lang('theme_step');?></a></li>
            <li data-step="9"  <?php if ($step == 9) { ?> class="active" <?php } ?> ><a href="<?php echo site_url() ?>/finish/index/<?php echo $cv_id; ?>"><?php echo lang('preview_step');?></a></li>
        </ul>
    <?php } else { ?>
        <ul class="tabs">
            <li data-step="1" <?php if ($step == 1) { ?> class="active" <?php } ?>  ><a href="javascript:void(0);"><?php echo lang('cv_step');?></a></li>
            <li data-step="2" <?php if ($step == 2) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('personal_info');?></a></li>
            <li data-step="3"  <?php if ($step == 3) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('edu_step');?></a></li>
            <li data-step="4"  <?php if ($step == 4) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('courses');?></a></li>
            <li data-step="5"  <?php if ($step == 5) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('exp_step');?></a></li>
            <li data-step="6"  <?php if ($step == 6) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('skills_step');?></a></li>
            <li data-step="7"  <?php if ($step == 7) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('refrences');?></a></li>
            <li data-step="8"  <?php if ($step == 8) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('theme_step');?></a></li>
            <li data-step="9"  <?php if ($step == 9) { ?> class="active" <?php } ?> ><a href="javascript:void(0);"><?php echo lang('preview_step');?></a></li>
        </ul>
    <?php } ?>
    <br class="clr">
    <!-- tab 1 -->
    <div class="cv-title">