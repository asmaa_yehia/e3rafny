<h1 class="blue-title"><?php echo lang('personal_skills'); ?></h1>
<br class="clr">
<form action="<?php echo site_url(); ?>/add_cv/update_lang/<?php echo $cv_id; ?>" method="post" enctype="multipart/form-data">
    <div class="step6">
        <input type="hidden" value="<?php echo $percent; ?>" name="percent" />
        <input type="hidden" value="<?php echo $cv_id; ?>" name="cv_id" />
        <input type="hidden" value="<?php echo count($items); ?>" name="lang_count" id="lang_count" />
        <input type="hidden" value="<?php echo count($skills); ?>" name="skill_items" id="skills_count" />
        <div id="qual">
            <div class="content">
                <?php foreach ($query as $key => $item) { ?>
                    <ul class="personal-activites">

                        <li id="cont<?php echo $key; ?>">
                            <input type="checkbox"  value="<?php echo $item['qual']->id; ?>" name="qual[]"  <?php
                            if ($item['user_qual']) {
                                echo 'checked=checked';
                            }
                            ?>   />
                            <label><?php echo $item['qual']->{'name_' . LANG}; ?></label>
                        </li>
                    </ul>
                <?php } ?>
            </div><!--end of qual_content-->
        </div><!-- end of qual-->
        <br class="clr">
        <hr class="sep">
        <br class="clr">
        <h1 class="blue-title"><?php echo lang('lang_skills'); ?></h1>

        <br class="clr">
        <br class="clr">
        <!-- lang start -->
        <div id="langs">
            <div class="content">
                <div class="languages">
                    <label><?php echo lang('tongue'); ?> :</label>
                    <?php
                    $lang = ""; //var_dump($item_tong );
                    if (!empty($item_tong))
                        foreach ($item_tong as $item) {
                            $lang = $item->name;
                        }
                    ?>
                    <select name="name_tong">
                        <option  value=""><?php echo lang("choose_lang"); ?></option>
                        <option value="Afrikaans" <?php if ($lang == 'Afrikaans') echo "selected"; ?> ><?php echo lang('Afrikaans'); ?></option>
                        <option value="Albanian" <?php if ($lang == 'Albanian') echo "selected"; ?> ><?php echo lang('Albanian'); ?></option>
                        <option value="Arabic" <?php if ($lang == 'Arabic') echo "selected"; ?>><?php echo lang('Arabic'); ?></option>
                        <option value="Armenian" <?php if ($lang == 'Armenian') echo "selected"; ?> ><?php echo lang('Armenian'); ?></option>
                        <option value="Basque"  <?php if ($lang == 'Basque') echo "selected"; ?>><?php echo lang('Basque'); ?></option>
                        <option value="Bengali" <?php if ($lang == 'Bengali') echo "selected"; ?> ><?php echo lang('Bengali'); ?></option>
                        <option value="Bulgarian" <?php if ($lang == 'Bulgarian') echo "selected"; ?> ><?php echo lang('Bulgarian'); ?></option>
                        <option value="Catalan"  <?php if ($lang == 'Catalan') echo "selected"; ?>><?php echo lang('Catalan'); ?></option>
                        <option value="Cambodian" <?php if ($lang == 'Cambodian') echo "selected"; ?>><?php echo lang('Cambodian'); ?></option>
                        <option value="Chinese" <?php if ($lang == 'Chinese') echo "selected"; ?>><?php echo lang('Chinese'); ?></option>
                        <option value="Croatian" <?php if ($lang == 'Croatian') echo "selected"; ?> ><?php echo lang('Croatian'); ?></option>
                        <option value="Czech" <?php if ($lang == 'Czech') echo "selected"; ?>><?php echo lang('Czech'); ?></option>
                        <option value="Danish" <?php if ($lang == 'Danish') echo "selected"; ?> ><?php echo lang('Danish'); ?></option>
                        <option value="Dutch" <?php if ($lang == 'Dutch') echo "selected"; ?>><?php echo lang('Dutch'); ?></option>
                        <option value="English" <?php if ($lang == 'English') echo "selected"; ?>><?php echo lang('English'); ?></option>
                        <option value="Estonian" <?php if ($lang == 'Estonian') echo "selected"; ?>><?php echo lang('Estonian'); ?></option>
                        <option value="Finnish" <?php if ($lang == 'Finnish') echo "selected"; ?>><?php echo lang('Finnish'); ?></option>
                        <option value="French" <?php if ($lang == 'French') echo "selected"; ?>><?php echo lang('French'); ?></option>
                        <option value="Georgian" <?php if ($lang == 'Georgian') echo "selected"; ?>><?php echo lang('Georgian'); ?></option>
                        <option value="German" <?php if ($lang == 'German') echo "selected"; ?>><?php echo lang('German'); ?></option>
                        <option value="Greek"<?php if ($lang == 'Greek') echo "selected"; ?> ><?php echo lang('Afrikaans'); ?></option>
                        <option value="Gujarati" <?php if ($lang == 'Gujarati') echo "selected"; ?> ><?php echo lang('Gujarati'); ?></option>
                        <option value="Hebrew" <?php if ($lang == 'Hebrew') echo "selected"; ?>><?php echo lang('Hebrew'); ?></option>
                        <option value="Hindi" <?php if ($lang == 'Hindi') echo "selected"; ?> ><?php echo lang('Hindi'); ?></option>
                        <option value="Hungarian" <?php if ($lang == 'Hungarian') echo "selected"; ?>  ><?php echo lang('Hungarian'); ?></option>
                        <option value="Icelandic" <?php if ($lang == 'Icelandic') echo "selected"; ?>><?php echo lang('Icelandic'); ?></option>
                        <option value="Indonesian" <?php if ($lang == 'Indonesian') echo "selected"; ?> ><?php echo lang('Indonesian'); ?></option>
                        <option value="Irish"  <?php if ($lang == 'Irish') echo "selected"; ?>><?php echo lang('Irish'); ?></option>
                        <option value="Italian" <?php if ($lang == 'Italian') echo "selected"; ?> ><?php echo lang('Italian'); ?></option>
                        <option value="Japanese" <?php if ($lang == 'Japanese') echo "selected"; ?> ><?php echo lang('Japanese'); ?></option>
                        <option value="Korean" <?php if ($lang == 'Korean') echo "selected"; ?>><?php echo lang('Korean'); ?></option>
                        <option value="Latin" <?php if ($lang == 'Latin') echo "selected"; ?>><?php echo lang('Latin'); ?></option>
                        <option value="Mongolian" <?php if ($lang == 'Mongolian') echo "selected"; ?> ><?php echo lang('Mongolian'); ?></option>
                        <option value="Nepali" <?php if ($lang == 'Nepali') echo "selected"; ?> ><?php echo lang('Nepali'); ?></option>
                        <option value="Norwegian"  <?php if ($lang == 'Norwegian') echo "selected"; ?>><?php echo lang('Norwegian'); ?></option>
                        <option value="Persian" <?php if ($lang == 'Persian') echo "selected"; ?> ><?php echo lang('Persian'); ?></option>
                        <option value="Polish" <?php if ($lang == 'Polish') echo "selected"; ?>><?php echo lang('Polish'); ?></option>
                        <option value="Portuguese"  <?php if ($lang == 'Portuguese') echo "selected"; ?>><?php echo lang('Portuguese'); ?></option>
                        <option value="Russian" <?php if ($lang == 'Russian') echo "selected"; ?>><?php echo lang('Russian'); ?></option>
                        <option value="Spanish" <?php if ($lang == 'Spanish') echo "selected"; ?> ><?php echo lang('Spanish'); ?></option>
                        <option value="Swahili" <?php if ($lang == 'Swahili') echo "selected"; ?>><?php echo lang('Swahili'); ?></option>
                        <option value="Swedish" <?php if ($lang == 'Swedish') echo "selected"; ?> ><?php echo lang('Swedish'); ?> </option>
                        <option value="Turkish"<?php if ($lang == 'Turkish') echo "selected"; ?> ><?php echo lang('Turkish'); ?></option>
                        <option value="Ukrainian" <?php if ($lang == 'Ukrainian') echo "selected"; ?> ><?php echo lang('Ukrainian'); ?></option>
                        <option value="Urdu" <?php if ($lang == 'Urdu') echo "selected"; ?>><?php echo lang('Urdu'); ?></option>
                        <option value="Uzbek" <?php if ($lang == 'Uzbek') echo "selected"; ?>><?php echo lang('Uzbek'); ?></option>
                        <option value="Vietnamese" <?php if ($lang == 'Vietnamese') echo "selected"; ?> ><?php echo lang('Afrikaans'); ?></option>
                    </select>

                </div>
<?php if (count($items) == 0) { ?>
                    <div id="cont" class="languages-added">
                        <div class="languages">
                            <label> <?php echo lang('language'); ?> :</label> <select name="name[]" id="tags">
                                <option  value=""><?php echo lang("choose_lang"); ?></option>
                                <option value="Afrikaans"  ><?php echo lang('Afrikaans'); ?></option>
                                <option value="Albanian" ><?php echo lang('Albanian'); ?></option>
                                <option value="Arabic" ><?php echo lang('Arabic'); ?></option>
                                <option value="Armenian" ><?php echo lang('Armenian'); ?></option>
                                <option value="Basque" ><?php echo lang('Basque'); ?></option>
                                <option value="Bengali" ><?php echo lang('Bengali'); ?></option>
                                <option value="Bulgarian" ><?php echo lang('Bulgarian'); ?></option>
                                <option value="Catalan" ><?php echo lang('Catalan'); ?></option>
                                <option value="Cambodian"><?php echo lang('Cambodian'); ?></option>
                                <option value="Chinese" ><?php echo lang('Chinese'); ?></option>
                                <option value="Croatian" ><?php echo lang('Croatian'); ?></option>
                                <option value="Czech" ><?php echo lang('Czech'); ?></option>
                                <option value="Danish" ><?php echo lang('Danish'); ?></option>
                                <option value="Dutch" ><?php echo lang('Dutch'); ?></option>
                                <option value="English" ><?php echo lang('English'); ?></option>
                                <option value="Estonian" ><?php echo lang('Estonian'); ?></option>
                                <option value="Finnish" ><?php echo lang('Finnish'); ?></option>
                                <option value="French" ><?php echo lang('French'); ?></option>
                                <option value="Georgian"><?php echo lang('Georgian'); ?></option>
                                <option value="German" ><?php echo lang('German'); ?></option>
                                <option value="Greek" ><?php echo lang('Afrikaans'); ?></option>
                                <option value="Gujarati" ><?php echo lang('Gujarati'); ?></option>
                                <option value="Hebrew"><?php echo lang('Hebrew'); ?></option>
                                <option value="Hindi" ><?php echo lang('Hindi'); ?></option>
                                <option value="Hungarian"  ><?php echo lang('Hungarian'); ?></option>
                                <option value="Icelandic" ><?php echo lang('Icelandic'); ?></option>
                                <option value="Indonesian" ><?php echo lang('Indonesian'); ?></option>
                                <option value="Irish" ><?php echo lang('Irish'); ?></option>
                                <option value="Italian" ><?php echo lang('Italian'); ?></option>
                                <option value="Japanese" ><?php echo lang('Japanese'); ?></option>
                                <option value="Korean"><?php echo lang('Korean'); ?></option>
                                <option value="Latin" ><?php echo lang('Latin'); ?></option>
                                <option value="Mongolian" ><?php echo lang('Mongolian'); ?></option>
                                <option value="Nepali" ><?php echo lang('Nepali'); ?></option>
                                <option value="Norwegian" ><?php echo lang('Norwegian'); ?></option>
                                <option value="Persian" ><?php echo lang('Persian'); ?></option>
                                <option value="Polish" ><?php echo lang('Polish'); ?></option>
                                <option value="Portuguese" ><?php echo lang('Portuguese'); ?></option>
                                <option value="Russian" ><?php echo lang('Russian'); ?></option>
                                <option value="Spanish" ><?php echo lang('Spanish'); ?></option>
                                <option value="Swahili" ><?php echo lang('Swahili'); ?></option>
                                <option value="Swedish" ><?php echo lang('Swedish'); ?> </option>
                                <option value="Turkish" ><?php echo lang('Turkish'); ?></option>
                                <option value="Ukrainian" ><?php echo lang('Ukrainian'); ?></option>
                                <option value="Urdu" ><?php echo lang('Urdu'); ?></option>
                                <option value="Uzbek" ><?php echo lang('Uzbek'); ?></option>
                                <option value="Vietnamese" ><?php echo lang('Afrikaans'); ?></option>
                            </select>
                            <select name="grade[]">

                                <option value="" ><?php echo lang('choose_grade'); ?></option>
                                <option value="p" ><?php echo lang('poor'); ?></option>
                                <option value="f"><?php echo lang('fair'); ?></option>
                                <option value="g" ><?php echo lang('good'); ?></option>
                                <option value="v" ><?php echo lang('very_good'); ?></option>
                                <option value="fl"><?php echo lang('fluent'); ?></option>

                            </select>
                        </div><!--end of cont-->
                    </div>
<?php } ?>
<?php foreach ($items->result() as $key => $item) { ?>

                    <div id="cont<?php echo $key; ?>" class="languages-added" >
                        <div class="languages">
                            <input type="hidden" name="cv_id" value="<?php echo $item->cv_id; ?>"  id="cv" />
                            <input type="hidden" name="id[]" value="<?php echo $item->id; ?>"  id="id<?php echo $key; ?>" />
                            <label><?php echo lang('language'); ?> : </label><select name="name[]">
                                <option  value=""><?php echo lang("choose_lang"); ?></option>
                                <option value="Afrikaans" <?php if ($item->name == "Afrikaans") echo "selected"; ?> ><?php echo lang('Afrikaans'); ?></option>
                                <option value="Albanian" <?php if ($item->name == "Albanian") echo "selected"; ?>><?php echo lang('Albanian'); ?></option>
                                <option value="Arabic" <?php if ($item->name == "Arabic") echo "selected"; ?>><?php echo lang('Arabic'); ?></option>
                                <option value="Armenian" <?php if ($item->name == "Armenian") echo "selected"; ?>><?php echo lang('Armenian'); ?></option>
                                <option value="Basque" <?php if ($item->name == "Basque") echo "selected"; ?>><?php echo lang('Basque'); ?></option>
                                <option value="Bengali" <?php if ($item->name == "Bengali") echo "selected"; ?>><?php echo lang('Bengali'); ?></option>
                                <option value="Bulgarian" <?php if ($item->name == "Bengali") echo "selected"; ?>><?php echo lang('Bulgarian'); ?></option>
                                <option value="Catalan" <?php if ($item->name == "Catalan") echo "selected"; ?>><?php echo lang('Catalan'); ?></option>
                                <option value="Cambodian" <?php if ($item->name == "Cambodian") echo "selected"; ?>><?php echo lang('Cambodian'); ?></option>
                                <option value="Chinese" <?php if ($item->name == "Chinese") echo "selected"; ?>><?php echo lang('Chinese'); ?> </option>
                                <option value="Croatian"<?php if ($item->name == "Croatian") echo "selected"; ?> ><?php echo lang('Croatian'); ?></option>
                                <option value="Czech" <?php if ($item->name == "Czech") echo "selected"; ?>><?php echo lang('Czech'); ?></option>
                                <option value="Danish" <?php if ($item->name == "Danish") echo "selected"; ?>><?php echo lang('Danish'); ?></option>
                                <option value="Dutch" <?php if ($item->name == "Dutch") echo "selected"; ?>><?php echo lang('Dutch'); ?></option>
                                <option value="English" <?php if ($item->name == "English") echo "selected"; ?>><?php echo lang('English'); ?></option>
                                <option value="Estonian" <?php if ($item->name == "Estonian") echo "selected"; ?>><?php echo lang('Estonian'); ?></option>
                                <option value="Finnish" <?php if ($item->name == "Finnish") echo "selected"; ?>><?php echo lang('Finnish'); ?></option>
                                <option value="French" <?php if ($item->name == "French") echo "selected"; ?>><?php echo lang('French'); ?></option>
                                <option value="Georgian" <?php if ($item->name == "Georgian") echo "selected"; ?>><?php echo lang('Georgian'); ?></option>
                                <option value="German" <?php if ($item->name == "German") echo "selected"; ?>><?php echo lang('German'); ?></option>
                                <option value="Greek" <?php if ($item->name == "Greek") echo "selected"; ?>><?php echo lang('Greek'); ?></option>
                                <option value="Gujarati" <?php if ($item->name == "Gujarati") echo "selected"; ?>><?php echo lang('Gujarati'); ?></option>
                                <option value="Hebrew" <?php if ($item->name == "Hebrew") echo "selected"; ?>><?php echo lang('Hebrew'); ?></option>
                                <option value="Hindi" <?php if ($item->name == "Hindi") echo "selected"; ?>><?php echo lang('Hindi'); ?></option>
                                <option value="Hungarian" <?php if ($item->name == "Hungarian") echo "selected"; ?> ><?php echo lang('Hungarian'); ?></option>
                                <option value="Icelandic" <?php if ($item->name == "Icelandic") echo "selected"; ?>><?php echo lang('Icelandic'); ?></option>
                                <option value="Indonesian" <?php if ($item->name == "Indonesian") echo "selected"; ?>><?php echo lang('Indonesian'); ?></option>
                                <option value="Irish" <?php if ($item->name == "Irish") echo "selected"; ?>><?php echo lang('Irish'); ?></option>
                                <option value="Italian" <?php if ($item->name == "Italian") echo "selected"; ?>><?php echo lang('Italian'); ?></option>
                                <option value="Japanese" <?php if ($item->name == "Japanese") echo "selected"; ?>><?php echo lang('Japanese'); ?></option>
                                <option value="Korean" <?php if ($item->name == "Korean") echo "selected"; ?>><?php echo lang('Korean'); ?></option>
                                <option value="Latin" <?php if ($item->name == "Latin") echo "selected"; ?>><?php echo lang('Latin'); ?></option>
                                <option value="Mongolian" <?php if ($item->name == "Mongolian") echo "selected"; ?>><?php echo lang('Mongolian'); ?></option>
                                <option value="Nepali" <?php if ($item->name == "Nepali") echo "selected"; ?>><?php echo lang('Nepali'); ?></option>
                                <option value="Norwegian" <?php if ($item->name == "Norwegian") echo "selected"; ?>><?php echo lang('Norwegian'); ?></option>
                                <option value="Persian" <?php if ($item->name == "Persian") echo "selected"; ?>><?php echo lang('Persian'); ?></option>
                                <option value="Polish" <?php if ($item->name == "Polish") echo "selected"; ?>><?php echo lang('Polish'); ?></option>
                                <option value="Portuguese" <?php if ($item->name == "Portuguese") echo "selected"; ?>><?php echo lang('Portuguese'); ?></option>
                                <option value="Russian" <?php if ($item->name == "Russian") echo "selected"; ?>><?php echo lang('Russian'); ?></option>
                                <option value="Spanish" <?php if ($item->name == "Spanish") echo "selected"; ?>><?php echo lang('Spanish'); ?></option>
                                <option value="Swahili" <?php if ($item->name == "Swahili") echo "selected"; ?>><?php echo lang('Swahili'); ?></option>
                                <option value="Swedish" <?php if ($item->name == "Swedish") echo "selected"; ?> ><?php echo lang('Swedish'); ?> </option>
                                <option value="Turkish" <?php if ($item->name == "Turkish") echo "selected"; ?>><?php echo lang('Turkish'); ?></option>
                                <option value="Ukrainian" <?php if ($item->name == "Ukrainian") echo "selected"; ?>><?php echo lang('Ukrainian'); ?></option>
                                <option value="Urdu" <?php if ($item->name == "Urdu") echo "selected"; ?>><?php echo lang('Urdu'); ?></option>
                                <option value="Uzbek" <?php if ($item->name == "Uzbek") echo "selected"; ?>><?php echo lang('Uzbek'); ?></option>
                                <option value="Vietnamese" <?php if ($item->name == "Vietnamese") echo "selected"; ?>><?php echo lang('Vietnamese'); ?></option>
                            </select>
                            <select name="grade[]">
                                <option value="" ><?php echo lang('choose_grade'); ?></option>
                                <option value="p" <?php
                                        if ($item->grade == 'p') {
                                            echo "selected";
                                        }
                                        ?> ><?php echo lang('poor'); ?></option>
                                <option value="f"  <?php
                                        if ($item->grade == 'f') {
                                            echo "selected";
                                        }
                                        ?>><?php echo lang('fair'); ?></option>
                                <option value="g" <?php
                                        if ($item->grade == 'g') {
                                            echo "selected";
                                        }
                                        ?> ><?php echo lang('good'); ?></option>
                                <option value="v" <?php
                                        if ($item->grade == 'v') {
                                            echo "selected";
                                        }
                                        ?> ><?php echo lang('very_good'); ?></option>
                                <option value="fl" <?php
                                        if ($item->grade == 'fl') {
                                            echo "selected";
                                        }
                                        ?>><?php echo lang('fluent'); ?></option>

                            </select>

                        </div><!--end of cont-->
                    <?php if ($key > 0) { ?>
                            <a href="javascript:void(0);" onClick="del_lang(<?php echo $key; ?>)" class="delete"></a>
    <?php } ?>
                    </div>
<?php } ?>
            </div>

            <br class="clr">
            <a href="javascript:void(0);" onClick="add_lang()" class="add"></a>
        </div><!-- end of lang-->
        <br class="clr">
        <hr class="sep">
        <br class="clr">
        <h1 class="blue-title"><?php echo lang('tech_skills'); ?></h1>
        <br class="clr">
        <br class="clr">
        <div id="skills" >
            <div class="content">
<?php if (count($skills) == 0) { ?>
                    <div id="cont" class="languages">

                        <label> <?php echo lang('skill'); ?>:</label><input type="text" name="skill_name[]"  value=""/>
                        <select name="skill_grade[]">
                            <option value="" ><?php echo lang('choose_grade'); ?></option>
                            <option value="g" ><?php echo lang('good'); ?></option>
                            <option value="i" ><?php echo lang('interemdate'); ?></option>
                            <option value="w" ><?php echo lang('weak'); ?></option>

                        </select>
                    </div>
<?php } ?>
<?php foreach ($skills as $key => $item) { ?>
                    <div id="cont<?php echo $key; ?>" class="languages-added">
                        <div class="languages">
                            <label> <?php echo lang('skill'); ?>:</label><input type="text" name="skill_name[]" value="<?php echo $item->name; ?>" />
                            <select name="skill_grade[]">
                                <option value="" ><?php echo lang('choose_grade'); ?></option>
                                <option value="g" <?php
                                        if ($item->grade == 'g') {
                                            echo "selected";
                                        }
                                        ?>><?php echo lang('good'); ?></option>
                                <option value="i" <?php
                                        if ($item->grade == 'i') {
                                            echo "selected";
                                        }
                                        ?>><?php echo lang('interemdate'); ?></option>

                                <option value="w" <?php
                                        if ($item->grade == 'w') {
                                            echo "selected";
                                        }
                                        ?>><?php echo lang('weak'); ?></option>
                            </select>

                        </div>
    <?php if ($key > 0) { ?>
                            <a href="javascript:void(0);" onClick="del_skill(<?php echo $key; ?>)" class="delete"></a>
                    <?php } ?>

                    </div>
<?php } ?>

            </div><!--end of CONTENT-->
            <br class="clr">
            <a href="javascript:void(0);" onClick="add_skill()" class="add"></a>
        </div>
        <br class="clr">
        <!-- buttons start -->
        <div class="buttons">
            <a href="<?php echo site_url(); ?>/add_cv/back_exp/<?php echo $cv_id; ?>" class="previous-step"><?php echo lang('last'); ?></a>

            <div class="save-next">
                <input type="submit"  name="save_button" value="<?php echo lang('save') ?>" class="save">
                <input type="submit" class="next-step"  name="next_button" value="<?php echo lang('next'); ?>"></a>
            </div>
        </div>
    </div>

</form>
<script>
    var count = $("#lang_count").val();
    var skill_count = $("#skills_count").val();
    function add_lang() {
        var cv_id = $("#cv").val();
        var unit_id;
        var cont_edu = '<div id="cont' + count + '" class="languages-added">';
        cont_edu += '<div class="languages"><label><?php echo lang('language'); ?>:</label><select name="name[]"><option  value=""><?php echo lang("choose_lang"); ?></option> <option value="Afrikaans"  ><?php echo lang('Afrikaans'); ?></option><option value="Albanian" ><?php echo lang('Albanian'); ?></option><option value="Arabic" ><?php echo lang('Arabic'); ?></option><option value="Armenian" ><?php echo lang('Armenian'); ?></option><option value="Basque" ><?php echo lang('Basque'); ?></option><option value="Bengali" ><?php echo lang('Bengali'); ?></option><option value="Bulgarian" ><?php echo lang('Bulgarian'); ?></option><option value="Catalan" ><?php echo lang('Catalan'); ?></option><option value="Cambodian"><?php echo lang('Cambodian'); ?></option><option value="Chinese" ><?php echo lang('Chinese'); ?></option><option value="Croatian" ><?php echo lang('Croatian'); ?></option><option value="Czech" ><?php echo lang('Czech'); ?></option><option value="Danish" ><?php echo lang('Danish'); ?></option><option value="Dutch" ><?php echo lang('Dutch'); ?></option><option value="English" ><?php echo lang('English'); ?></option><option value="Estonian" ><?php echo lang('Estonian'); ?></option><option value="Finnish" ><?php echo lang('Finnish'); ?></option><option value="French" ><?php echo lang('French'); ?></option><option value="Georgian"><?php echo lang('Georgian'); ?></option><option value="German" ><?php echo lang('German'); ?></option><option value="Greek" ><?php echo lang('Afrikaans'); ?></option><option value="Gujarati" ><?php echo lang('Gujarati'); ?></option><option value="Hebrew"><?php echo lang('Hebrew'); ?></option><option value="Hindi" ><?php echo lang('Hindi'); ?></option><option value="Hungarian"  ><?php echo lang('Hungarian'); ?></option><option value="Icelandic" ><?php echo lang('Icelandic'); ?></option><option value="Indonesian" ><?php echo lang('Indonesian'); ?></option><option value="Irish" ><?php echo lang('Irish'); ?></option><option value="Italian" ><?php echo lang('Italian'); ?></option><option value="Japanese" ><?php echo lang('Japanese'); ?></option><option value="Korean"><?php echo lang('Korean'); ?></option><option value="Latin" ><?php echo lang('Latin'); ?></option><option value="Mongolian" ><?php echo lang('Mongolian'); ?></option><option value="Nepali" ><?php echo lang('Nepali'); ?></option><option value="Norwegian" ><?php echo lang('Norwegian'); ?></option><option value="Persian" ><?php echo lang('Persian'); ?></option><option value="Polish" ><?php echo lang('Polish'); ?></option><option value="Portuguese" ><?php echo lang('Portuguese'); ?></option><option value="Russian" ><?php echo lang('Russian'); ?></option><option value="Spanish" ><?php echo lang('Spanish'); ?></option><option value="Swahili" ><?php echo lang('Swahili'); ?></option><option value="Swedish" ><?php echo lang('Swedish'); ?> </option><option value="Turkish" ><?php echo lang('Turkish'); ?></option><option value="Ukrainian" ><?php echo lang('Ukrainian'); ?></option><option value="Urdu" ><?php echo lang('Urdu'); ?></option><option value="Uzbek" ><?php echo lang('Uzbek'); ?></option><option value="Vietnamese" ><?php echo lang('Afrikaans'); ?></option></select> <select name="grade[]"><option value="" ><?php echo lang('choose_grade'); ?></option><option value="p" ><?php echo lang('poor'); ?></option><option value="f"><?php echo lang('fair'); ?></option> <option value="g" ><?php echo lang('good'); ?></option><option value="v" ><?php echo lang('very_good'); ?></option><option value="fl"><?php echo lang('fluent'); ?></option></select></div>';
        cont_edu += '<input type="hidden" name="id[]" id="id' + count + '" value="' + unit_id + '"/><a href="javascript:void(0);" onClick="del_unit(' + count + ')" class="delete"></a></div>';
        //
        $("#langs .content").append(cont_edu);
        count++;
    }
//
    function del_lang(id) {
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            unit_id = $("#id" + id).val();
            $("#langs .content #cont" + id).remove();
        }
    }
//
    function add_skill() {
        var unit_id;
        var cont_edu = '<div id="cont' + count + '" class="languages-added">';
        cont_edu += '<div class="languages"><label><?php echo lang('skill'); ?></label><input type="text" name="skill_name[]"/> <select name="skill_grade[]"><option value="" ><?php echo lang('choose_grade'); ?></option><option value="i"><?php echo lang('interemdate'); ?></option><option value="v" ><?php echo lang('good'); ?></option><option value="w" ><?php echo lang('weak'); ?></option></select></div>';

        cont_edu += '<input type="hidden" name="skill_id[]" id="id' + count + '" value="' + unit_id + '"/><a href="javascript:void(0);" onClick="del_skill(' + count + ')" class="delete"></a></div>';
        $("#skills .content").append(cont_edu);
        count++;
    }
//
    function del_skill(id) {
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            unit_id = $("#id" + id).val();
            $("#skills .content #cont" + id).remove();
        }
        else
            return false;
        //count--;
    }
</script>
