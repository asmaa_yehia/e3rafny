<html>
<head>
 <link href="<?php echo base_url("styles/css/style6_".$cv->lang.".css") ?>" rel="stylesheet">

</head>
<body >
<div class="social-bar" style="padding-bottom:20px;">
<div class="container">
<div class="right-side">
<?php
		foreach ($phones as $item) {
?> 

<p style="padding-top:0px;margin-top:0px;padding-bottom:0px;margin-bottom:0px;font-size:14px;color:#666666;width:100px;" class="rr"><?php echo $item->code.$item->number ; ?></p>
<?php } ?>
<div class="right-2">
<p style="font-size:15px;color:#666666;margin-top:0px;" class="r"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>
</div>
</div>

<div class="social-links">
<?php foreach ($social as $item) { ?>  
<img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'fbb.png';}elseif($item->name=='twitter'){echo 'twet.png';}elseif($item->name=='linkledin'){echo 'in.png';}elseif($item->name=='google'){echo 'gooogle.png';}elseif($item->name=='behance'){echo 'behancee.png';} ?>">
<a href="<?php echo $item->link; ?>" style="font-size:16px;color:#666666;margin-top:-60px;"><?php echo $item->link; ?></a>
<!--<ul style="list-style:none;padding-top:0px;margin-top:0px;">
  
<li><a style="font-size:16px;color:#666666;margin-top:-60px;" href=""></a></li>

</ul>-->
<br>
<?php } ?>
</div>


</div>
</div>     
        
<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
<div class="personal" >
<div class="pic">
<img class="ff" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>" >
</div>
</div>
<?php } ?>

<div class="name-bar">
<div class="container">
<div class="name" style="padding-bottom:20px;">
<h1><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
<h3 style="margin-bottom:30px;"><?php echo $user_info[0]->title ; ?></h3>
<?php if($user_info[0]->adress1 !=''){ ?>
<h6 class="address"><?php echo lang("address") ; ?> : <span> <?php echo $user_info[0]->adress1; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->adress2 !=''){ ?>
<h6 class="address"><?php echo lang("another_address") ; ?> : <span> <?php echo $user_info[0]->adress2; ?></span></h6>
<?php } ?>

</div>
<div class="nation">
<h6 style="margin-top:21px;" class="address"><?php if($user_info[0]->nationality !=''){ ?><?php echo lang("nationality");?> : <span><?php echo $user_info[0]->nationality; ?></span><?php } ?></h6>
<?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?> 
<h6 class="address"><?php echo lang("birth_date") ; ?> : <span> <?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->marital_status !='n'){ 
		 if($user_info[0]->marital_status =='s'){
			$m_status=lang("single");
		}elseif($user_info[0]->marital_status =='m'){
			$m_status=lang("married");
		}elseif($user_info[0]->marital_status =='d'){
			$m_status=lang("divercoed");
		}
?>  
<h6 class="address"><?php echo lang("marital_rstatus"); ?> : <span> <?php echo $m_status ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->gender !='n'){ 
		if($user_info[0]->gender =='m'){
			$gender=lang("male");
		}elseif($user_info[0]->gender =='f'){
			$gender=lang("female");
		}
?>    
<h6 class="address"><?php echo lang("gender"); ?> : <span> <?php echo $gender ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->license !='no'){ 
		if($user_info[0]->license =='y'){
			$License=lang("yes");
		}elseif($user_info[0]->license =='n'){
			$License=lang("no");
		}
?>   
<h6 class="address"><?php echo lang("License"); ?> : <span> <?php echo $License ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->military !='n'){ 
		if($user_info[0]->military =='e'){
			$military=lang("exemption");
		}elseif($user_info[0]->military =='c'){
			$military=lang("Complete_service");
		}elseif($user_info[0]->military =='p'){
			$military=lang("Postponed");
		}elseif($user_info[0]->military =='s'){
			$military=lang("Currently_serving");
		}elseif($user_info[0]->military =='d'){
			$military=lang("Doesnt_apply");
		}	 
?> 
<h6 class="address"><?php echo lang("military"); ?> : <span> <?php echo $military; ?></span></h6>
<?php } ?>
</div>
</div>
</div>



</div>
</div>
<?php if( !(empty($cv->objective))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/obj.png" alt="objective">
</div>
<h2 class="ob"><?php echo strtoupper(lang("objective")) ;?>
</h2>
<p class="text"><?php echo $cv->objective ;?>
</p>
<hr>
</div>
</div>
<?php } ?>
<?php if( !(empty($education))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/gg.png" alt="graduation">
</div>
<h2  style="font-size:20px; color:#1cbbb4;"><?php echo strtoupper(lang("education")); ?></h2>
<div class="contentt">
<?php foreach ($education as $item) { ?>
<div class="t">
<ul>
<li style="font-size:16px;color:#464646;" class="t"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo  $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year  ; ?>
         
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=="now"){ ?>
        
		<?php	echo  $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
         
<?php	} ?></li>
</ul>
</div>
<p><?php if( !(empty($item->name)))  echo $item->name ; ?></p>
<p><?php if( !(empty($item->type)))  echo $item->type ; ?></p>
<p><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>
<?php } ?>
<hr>
</div>
</div>
</div>
<?php } ?>
<?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/gg.png" alt="graduation">
</div>
<h2  style="font-size:20px; color:#1cbbb4;"><?php if( !(empty($courses))) { echo strtoupper(lang("courses")) ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang('and')."&nbsp;" ;} if(!(empty($certifcates))) echo strtoupper(lang("certifcates")) ; ?></h2>
<div class="contentt">
<?php foreach ($courses as $item) { ?>
<div class="t">
<ul>
<li style="font-size:16px;color:#464646;" class="t"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                
				<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></li>
</ul>
</div>
<p><?php if( !(empty($item->name)))  echo strtoupper($item->name) ; ?></p>
<p><?php if( !(empty($item->institute)))  echo $item->institute ; ?></p>
<?php } ?>

<?php foreach ($certifcates as $item) { ?>
<div class="t">
<ul>
<li style="font-size:16px;color:#464646;" class="t"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                
				<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year ; ?>
                
				<?php } ?></li>
</ul>
</div>
<p><?php if( !(empty($item->name)))  echo strtoupper($item->name) ; ?></p>
<p><?php if( !(empty($item->institute)))  echo $item->institute ; ?></p>
<?php } ?>

<hr>
</div>
</div>
</div>
<?php } ?>
<?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/qqq.png" alt="experience">
</div>
<h2  style="font-size:20px; color:#1cbbb4;"><?php if( !(empty($experience))) { echo strtoupper(lang("experience")) ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($projects))) echo strtoupper(lang("projects")) ; ?><span style="font-size:16px;color:#363636;"><?php if(!(empty($user_info[0]->exp_no))){ echo "&nbsp;"."(".lang('no_exp') ."&nbsp;". $user_info[0]->exp_no.")"  ;} ?></span></h2>
<div class="contentt">
<?php foreach ($experience as $item) {  ?>
<div class="t">
<ul>
<li style="font-size:16px;color:#464646;" class="t"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=="now"){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
            
			<?php } ?></li>
</ul>
</div>

<p style="padding-top:0px !important;padding-bottom:20px !important;"><?php echo $item->company ; ?> <?php if( !(empty($item->name))){ echo "(".$item->name.")" ; } ?></p>
<div class="dd" style="margin-top:5px;padding-top:-25px;margin-bottom:0px;padding-bottom:0px;">
<?php if(!empty($item->role)){ ?>
<ul style="margin:0px;padding:0px;">
<?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
<li style="font-size:16px;color:#464646;list-style:none;">-<?php echo $r_item ; ?>
</li>
<?php } ?>
</ul>
<?php } ?>
</div>
<p style="padding-top:15px;padding-bottom:30px;"><?php if( !(empty($item->description)))  echo $item->description ; ?>
</p>
<?php } ?>

<?php foreach ($projects as $item) {  ?>
<div class="t">
<ul>
<li style="font-size:16px;color:#464646;" class="t"><a href="<?php echo $item->link ; ?>" style="font-size: 14px;
        color: #000;text-decoration:none;"><?php echo $item->link ; ?></a></li>
</ul>
</div>

<p style="padding-top:0px !important;padding-bottom:20px !important;"><?php echo $item->name ; ?></p>
<div class="dd" style="margin-top:5px;padding-top:-25px;margin-bottom:0px;padding-bottom:0px;">
</div>
<p style="padding-top:15px;"><?php if( !(empty($item->description)))  echo $item->description ; ?>
</p>
<?php } ?>

<hr>
</div>
</div>
</div>
<?php } ?>
<?php if( !(empty($skills))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/tt.png" alt="technical skills">
</div>

<h2 class="t" style="font-size:20px; color:#1cbbb4;"><?php echo strtoupper(lang("skills")) ; ?></h2>
<div class="contentt">
<div class="t">
<ul>
<?php foreach ($skills as $item) { ?>
<li style="font-size:16px;color:#464646;"> <?php if( !(empty($item->name))) echo $item->name."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='i') echo lang('interemdate');
																				elseif($item->grade=='g') echo lang('good');
																				elseif($item->grade=='w') echo lang('weak'); ?> </li>
<?php } ?>
</ul>
</div>
<hr>
</div>

</div>
</div>
<?php } ?>
<?php if( !(empty($qualifications))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/tt.png" alt="technical skills">
</div>

<h2 class="t" style="font-size:20px; color:#1cbbb4;"><?php echo strtoupper(lang("qualifications")) ; ?></h2>
<div class="contentt">
<div class="t">
<ul>
<?php foreach ($qualifications as $item) { ?>
<li style="font-size:16px;color:#464646;"> <?php echo $item->name ; ?> </li>
<?php } ?>

</ul>
</div>
<hr>
</div>

</div>
</div>
<?php } ?>
<?php if( !(empty($langs))){?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/ll.png" alt="language">
</div>

<h2 class="t" style="font-size:20px; color:#1cbbb4;"><?php echo strtoupper(lang("langs")) ; ?></h2>
<div class="contentt">
<div class="t">
<ul>
<?php foreach ($langs as $item) {  ?>
<li style="font-size:16px;color:#464646;"> <?php if( !(empty($item->name))) echo lang($item->name)."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='p') echo lang('poor');
				elseif($item->grade=='f') echo lang('fair');
				elseif($item->grade=='g') echo lang('good');
				elseif($item->grade=='v') echo lang('very_good');
				elseif($item->grade=='fl') echo lang('fluent');
				elseif($item->grade=='t') echo lang('tongue'); ?> </li>
<?php } ?>
</ul>
</div>
<hr>
</div>

</div>
</div>
<?php } ?>
<?php if( !(empty($references))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<img src="<?php echo base_url(); ?>assets/images/rr.png" alt="refrence">
</div>

<h2 class="t" style="font-size:20px; color:#1cbbb4;"><?php echo strtoupper(lang("references")) ; ?></h2>
<div class="contentt">
<?php foreach ($references as $item) { ?>
<div class="t">
<ul>
<li style="font-size:16px;color:#464646;"><?php if( !(empty($item->name))) echo $item->name; ?>  </li>

</ul>
</div>
<p class="dd" style="font-size:16px; color:#464646;margin-top:0px;padding-top:0px;"><?php echo $item->mail ; ?></p>
<p class="dd" style="font-size:16px; color:#464646;margin-top:0px;padding-top:0px;"><?php echo $item->phone ; ?></p>
<?php } ?>
<hr>
</div>

</div>
</div>
<?php } ?>
</body>
</html>