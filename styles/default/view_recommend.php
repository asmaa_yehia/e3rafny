    <div class="notifications-approve">

                    <span class="date"><?php echo lang('day');?>: <?php echo date("d/m/Y" , $recommend_row->date ); ?></span>

                    <br class="clr">

                    <!-- notification review -->
                    <div class="notification-text">
                        <label><?php echo $recommend_row->name." ".lang('recommended'); ?> <a href="<?php echo base_url().$user_data->user_name."/".$recommend_row->url_title ; ?>" target="_blank"><?php echo $recommend_row->cv_name; ?></a></label>
                        <p>
                          <?php echo $recommend_row->recommend ; ?>
                        </p>

                        <br class="clr">

                        <ul class="actions">
                            <li><a href="<?php echo site_url("index/delete/$recommend_row->id") ?>" onClick="if(!confirm('<?php echo lang("notifcofirm_delete"); ?>'))return false;" class="refuse"><?php echo lang('refuse_notif'); ?></a></li>
                            <li><a href="<?php echo site_url("index/publish/$recommend_row->id") ?>" class="accept" ><?php echo lang('accept_notif'); ?></a></li>
                        </ul>
                    </div>
                    <!-- notification review end -->

                </div>

