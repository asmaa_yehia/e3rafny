<h1 class="blue-title"><?php echo lang('personal_info'); ?></h1>
<br class="clr">
<form action="<?php echo site_url(); ?>/user_data/add/<?php echo $cv_id; ?>" method="post"  enctype="multipart/form-data">
    <input type="hidden" value="<?php echo $percent; ?>" name="percent" />
    <?php if (isset($cv_id)) { ?>
        <input type="hidden" value="<?php echo $cv_id; ?>" name="cv_id" />
    <?php } ?>
    <table class="wizard-form step2" cellpadding="0" cellspacing="20">
        <tr>
            <td><label><?php echo lang('name'); ?>:</label></td>
            <td><input type="text" name="user_name" value="<?php echo set_value('user_name'); ?>">
                <div class="error"><?php echo form_error('user_name'); ?></div></td>
        </tr>

        <tr>
            <td><label><?php echo lang('nationality'); ?>:</label></td>
            <td><input type="text" name="nationality" value="<?php echo set_value('nationality'); ?>">
                <div class="error"><?php echo form_error('nationality'); ?></div></td>
        </tr>

        <tr>
            <td><label><?php echo lang('birth_date'); ?>:</label></td>
            <td>
                <select class="small-select" name="day">
                    <option value ><?php echo lang('day'); ?></option>
                    <?php for ($i = 1; $i <= 31; $i++) { ?>
                        <option  value="<?php echo $i; ?>" <?php echo set_select('day', $i); ?>><?php echo $i; ?></option>
                    <?php } ?>
                </select>

                <select class="small-select" name="month">
                    <option value><?php echo lang('month'); ?></option>
                    <option value="1" <?php echo set_select('month', 1); ?>><?php echo lang('January'); ?></option>
                    <option value="2" <?php echo set_select('month', 2); ?>><?php echo lang('February'); ?></option>
                    <option value="3" <?php echo set_select('month', 3); ?>><?php echo lang('March'); ?></option>
                    <option value="4" <?php echo set_select('month', 4); ?>><?php echo lang('April'); ?></option>
                    <option  value="5" <?php echo set_select('month', 5); ?>><?php echo lang('May'); ?></option>
                    <option  value="6" <?php echo set_select('month', 6); ?>><?php echo lang('June'); ?></option>
                    <option  value="7" <?php echo set_select('month', 7); ?>><?php echo lang('July'); ?></option>
                    <option  value="8" <?php echo set_select('month', 8); ?>><?php echo lang('August'); ?></option>
                    <option  value="9" <?php echo set_select('month', 9); ?>><?php echo lang('September'); ?></option>
                    <option  value="10" <?php echo set_select('month', 10); ?>><?php echo lang('October'); ?></option>
                    <option  value="11" <?php echo set_select('month', 11); ?>><?php echo lang('November'); ?></option>
                    <option  value="12" <?php echo set_select('month', 12); ?>><?php echo lang('December'); ?></option>
                </select>

                <select class="small-select" name="year">
                    <option value ><?php echo lang('year'); ?></option>
                    <?php for ($i = 1975; $i <= year; $i++) { ?>
                        <option  value="<?php echo $i; ?><?php echo set_select('year', $i); ?>"><?php echo $i; ?></option>
                    <?php } ?>
                </select>
                <div class="error"><?php echo form_error('year'); ?></div>
                <div class="error"><?php echo form_error('month'); ?></div>
                <div class="error"><?php echo form_error('day'); ?></div>
            </td>
        </tr>

        <tr>
            <td><label><?php echo lang('alter'); ?>:</label></td>
            <td><input type="email" name="alter_mail" value="<?php echo set_value('alter_mail'); ?>" ></td>
        </tr>

        <tr>
            <td><label><?php echo lang('job'); ?>:</label></td>
            <td><input type="text" name="title" value="<?php echo set_value('title'); ?>">
                <div class="error"><?php echo form_error('title'); ?></div>
            </td>
        </tr>

        <tr>
            <td><label><?php echo lang('marital_rstatus'); ?>:</label></td>
            <?php //echo $status;?>
            <td><select name="marital_status">
                    <option value="n" <?php if ($status == 'n') { ?> selected="selected" <?php } ?> ><?php echo lang('select_from_list'); ?></option>
                    <option value="s" <?php if ($status == 's') { ?> selected="selected" <?php } ?> ><?php echo lang('single'); ?></option>
                    <option value="m" <?php if ($status == 'm') { ?> selected="selected" <?php } ?> ><?php echo lang('married'); ?></option>
                    <option value="d" <?php if ($status == 'd') { ?> selected="selected" <?php } ?>><?php echo lang('divercoed'); ?></option>
                </select></td>

        </tr>

        <tr>
            <td><label><?php echo lang('address'); ?>:</label></td>
            <td><input type="text" name="adress1" value="<?php echo set_value('adress1'); ?>">
                <div class="error"><?php echo form_error('adress1'); ?></div></td>
        </tr>

        <tr>
            <td><label><?php echo lang('address1'); ?>:</label></td>
            <td><input type="text" name="adress2" value="<?php echo set_value('adress2'); ?>"></td>
        </tr>

        <tr>
            <td><label><?php echo lang('gender'); ?>:</label></td>
            <td>

                <input type="radio" name="gender"  value="m"  class="gender" <?php if ($gender == 'm') { ?> checked <?php } ?>><?php echo lang('male'); ?>
                <input type="radio" name="gender" value="f" class="gender" <?php if ($gender == 'f') { ?> checked <?php } ?>><?php echo lang('female'); ?>
            </td>
        </tr>

        <tr >
            <td><label><?php echo lang('licence'); ?>:</label></td>
            <td>
                <input  name="licence" type="radio" value="y" /><?php echo lang('yes'); ?>
                <input name="licence"  type="radio" value="n" checked /> <?php echo lang('no'); ?>
            </td>
        </tr>

        <tr id="mil" <?php
        if (isset($user->gender) && $user->gender == 'f') {
            echo 'style="display:none"';
        }
        ?>>
            <td><label><?php echo lang('military') ?>:</label></td>
            <td>
                <select name="military">
                    <option value="n"  <?php if ($value == 'n') { ?> selected="selected" <?php } ?>><?php echo lang('military'); ?></option>
                    <option value="e" <?php if ($value == 'e') { ?> selected="selected" <?php } ?>><?php echo lang('exemption'); ?></option>
                    <option value="c" <?php if ($value == 'c') { ?> selected="selected" <?php } ?>><?php echo lang('Complete_service'); ?></option>
                    <option value="p" <?php if ($value == 'p') { ?> selected="selected" <?php } ?>><?php echo lang('Postponed'); ?></option>
                    <option value="s" <?php if ($value == 's') { ?> selected="selected" <?php } ?>><?php echo lang('Currently_serving'); ?></option>
                    <option value="d" <?php if ($value == 'd') { ?> selected="selected" <?php } ?>><?php echo lang('Doesnt_apply'); ?></option>
                </select>
            </td>
        </tr>
    </table>

    <!-- right form end -->

    <!-- left side pic -->

    <div class="left-side-form">
        <div id="upload"  class="settings"><?php echo lang('image'); ?>  </div><span id="status" ></span>
        <img src="<?php echo base_url() ?>/styles/images/seperator.png" alt="" class="seperator">

        <div class="user-pic">
            <div class="flexform">
                <ul id="media-type-1">
                    <?php if (isset($img)) { ?>
                        <input type="hidden" name="ajaxupload" id="ajaxupload" value="<?php echo $img; ?>" />

                    <?php }if (!empty($img)) { ?>

                        <li  class="success"><img width="150" height="150" 
                                                  src="<?php
                                                  echo base_url('assets/users_img/') . '/' . $img;
                                                  ?>"/><span class="ctrls" style="height:25px; 
                                                  background:#eeeeee; display:block">

                                <a class="removeuploaded edit gradient-btn"
                                   title="delete photo" alt="<?php
                                   echo $img;
                                   ?>"><?php echo lang("delete"); ?></a>
                            </span></li>

                    <?php } ?>
                </ul>
                <div style="display: none;" id="removeimages" ></div>
            </div>

       <!-- <img src="<?php echo base_url(); ?>styles/images/user-pic.png">-->
    <!-- <a href="#" class="settings" ><?php echo lang('image_setting'); ?></a>-->
        </div>

    </div>

    <!-- left side end -->
    <br class="clr">

    <hr class="sep">
    <br class="clr">

    <!-- bottom form start -->

    <div class="bottom-form">
        <h1 class="blue-title"><?php echo lang('methods'); ?></h1>
        <br class="clr">

        <!-- right side start -->
        <div class="right-side">
            <div class="social-links">
                <label><?php echo lang('social') ?></label>
                <select onchange="add_val(this.value)" id="social_links">
                    <option value=""><?php echo lang('chooce_social'); ?></option>
                    <option value="facebook">Facebook</option>
                    <option value="twitter">Twitter</option>
                    <option value="linkledin">Linkledin</option>
                    <option value="google">Google</option>
                    <option value="behance">Behance</option>
                </select>
                <div id="txt_lnk">

                </div>



            </div>
        </div>
        <!-- right side end -->

        <!-- left side start -->
        <div class="left-side">
            <img src="<?php echo base_url() ?>styles/images/seperator1.png" alt="" class="seperator">


            <div id="phones">
                <input type="hidden" value="1" name="count" id="count"  />
                <div class="phone-no" id="phone0">
                    <label><?php echo lang('phone'); ?>:</label>
                    <input type="text" class="small-input" name="code[]" value="<?php echo set_value('code'); ?>">
                    <input type="text" name="number[]" value="<?php echo set_value('number'); ?>">
                </div>
                <div class="error"><?php echo form_error('code[0]'); ?></div>
                <div class="phone-no-delete" id="phone1">
                    <div class="phone-no">
                        <label><?php echo lang('phone'); ?>:</label>
                        <input type="text" class="small-input" name="code[]" value="<?php echo set_value('code'); ?>">
                        <input type="text" name="number[]" value="<?php echo set_value('number'); ?>">

                    </div>
                    <a href="javascript:void(0);" id="1" class="delete" onclick="delete_phone(this.id)"></a>
                </div>
            </div>


            <div class="clr"></div>
            <!-- add -->
            <a href="javascript:void(0);" class="add-phone" onclick="add_phone()"></a>
            <!-- add end -->

        </div>
        <!-- left side end -->

    </div>



    <br class="clr">
    <!-- buttons start -->
    <div class="buttons">
        <a href="<?php echo site_url(); ?>/add_cv/back_cv/<?php echo $cv_id; ?>" class="previous-step"><?php echo lang('last'); ?></a>

        <div class="save-next">
            <input type="submit" value="<?php echo lang('save') ?>" name="save_button" class="save">
            <input type="submit" class="next-step" value="<?php echo lang('next') ?>" name="next_button"></a>
        </div>
    </div>
</form>
</div><!-- tab 2 end -->


</div><!-- wizard end -->



<script type="text/javascript" src="<?php echo base_url() ?>assets/js/ajaxupload.js" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>styles/css/ajaxupload.css" />
<script>
                var count = $('#count').val();
                function add_phone() {

                    count++;
                    //alert(count);
                    $('#phones').append('<div  class="phone-no-delete" id="phone' + count + '"><div class="phone-no"><label><?php echo lang('phone'); ?>:</label><input type="text" name="code[]" id="code' + count + '" size="5"  class="small-input"  /><input type="text" id="number' + count + '" name="number[]"/></div><a href="javascript:void(0);" id="' + count + '" class="delete" onclick="delete_phone(this.id)" ></a></div>');

                }
                function delete_phone(id) {

                    var checkstr = confirm('are you sure you want to delete this?');
                    if (checkstr == true) {
                        //alert(id);
                        $("#phone" + id).remove();

                    } else {
                        return false;
                    }

                }
</script>

<script type="text/javascript" >

    var allownum = 1;
    var uploadnum = <?php echo (isset($img) && count($img)) ? count($img) : 0 ?>;
    $(function() {

        var status = $('#status');
        new AjaxUpload($('#upload'), {
            action: '<?php echo site_url('user_data/upload_img/') ?>',
            name: 'ajaxuploader',
            onSubmit: function(file, ext) {
                if (!(ext && /^(jpg|png|jpeg|gif)$/.test(ext)))
                {
                    status.text
                            ('<?php echo lang('img_typemsg'); ?>');
                    return false;
                }
                /*if(size=='10'){
                 status.text
                 ('<?php echo "hoda"; ?>');
                 return false;
                 }*/
                /*if(uploadnum >= allownum){
                 status.text
                 ('Max count of uploaded files is: '+allownum);
                 return false;
                 }*/
                status.text('Uploading...');
            },
            onComplete: function(file, response) {
                //On completion clear the status
                $('#files div.clear').remove();
                status.text('');
                var responeobj = $.parseJSON(response);
                if (responeobj.status == true) {
                    uploadnum++;

                    $('#media-type-1').html('<img width="150" height="150" src="<?php echo base_url() ?>'
                            + responeobj.full_url + '" alt="" /> <span class="ctrls" style="display:block"><a alt="' + responeobj.full_url + '\"  class="removeuploaded edit gradient-btn" title="delete photo" ><?php echo lang('delete'); ?></a></span><input type="hidden" name="ajaxupload" value="' + responeobj.client_name + '\" /><br />').addClass('success');

                    $('.removeuploaded').click(function() {

                        removeobject = $(this);
                        var removeurl = '<?php echo site_url("user_info/delete_img/"); ?>';
                        $.post(removeurl, {'img_name': $(this).attr('alt')}, function(data) {

                        });

                        removeobject.parents("li.success").slideUp().remove();
                        //uploadnum--;
                        $("#removeimages").append("<input type='hidden' name='removeimd' value='" + $(this).attr
                                ('alt') + "' />");

                        $('#media-type-1').html('');
                    });



                }
                $('<div class="clear"></div>').appendTo("media-type-1");
            }
        });



    });




//onchange gender show militray service 
    $(".gender").change(function() {
        //alert($("#mil").css);
        $("#mil").css("display", "block");
        if (this.value == "f") {
            $("#mil").css('display', 'none');
        }

    });
//
    function add_val(val) {


        if ($('#social_links option').length == 2) {
            $('#social_links').hide();
        }

        $("#txt_lnk").append('<div class="' + val + ' social"><input type="url" name="link[]" value="" /><img src="<?php echo base_url(); ?>/styles/images/' + val + '.png"/><input type="hidden" value="' + val + '"  name="name[]"/><a href="javascript:void(0);" id="' + val + '" onclick="del_social(this.id)">-</a></div>');
        $("#social_links  option[value='" + val + "']").remove();
    }
    function  del_social(soc_id) {
        // alert(soc_id)
        $('.' + soc_id).remove();
        $("#social_links").css("display", "inline");
        $("#social_links").append('<option value="' + soc_id + '">' + soc_id + '</option>');
    }
    //
    var cv_id = $("#cv_id").val();
    $(document).ready(function() {
        $.ajax({
            type: "POST",
            url: "<?php echo site_url(); ?>/soc/get_count/" + cv_id,
            async: false,
            success: function(data) {
                //alert(data);
                //edu_id=data;
                var items = data.split("#");
                var count = (items.length) - 1;
                //alert(count);
                if (count == 5) {
                    $("#social_links").css("display", "none");
                }
                for (var t = 0; t < count; t++) {
                    //alert(items[t])
                    $("#social_links option[value='" + items[t] + "']").remove();
                }
            }
        });
    });

</script>
<script type="text/javascript">

    $(function() {

        $('.removeuploaded').click(function() {
            removeobject = $(this);
            removeobject.parents("li.success").slideUp().remove();
            uploadnum--;
            $("#removeimages").append("<input type='hidden' name='removeimd' value='" + $(this).attr
                    ('alt') + "' />");
            $("#ajaxupload").val('');

        });
    });
</script>