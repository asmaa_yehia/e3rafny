<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>E3rfny</title>


       <!-- my styleshhets -->
        <link href="<?php echo base_url(); ?>styles/css/stylesheet.css" rel="stylesheet"   type="text/css"/>
        <link href="<?php echo base_url(); ?>styles/fonts/stylesheet.css" rel="stylesheet">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>

    <body class="home-page">
        <div class="top-bar"></div>

        <div class="page-wrap">
            <div class="container">

                <!-- logo and navbar -->
                <div class="nav-logo-area">

                    <div class="left-area">
                        <!-- lang -->
                        <div class="user-lang-area">

                            <ul>
                                 <li><a href="<?php echo base_url(); ?>ar" class="ar"><?php echo lang('arabic'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>en" class="en"><?php echo lang('english'); ?></a></li>
                            </ul>
                        </div>
                        <!-- lang end -->

                    </div>
                </div>
                <!-- logo and navbar end-->

                <div class="clr"></div>

                <!-- content -->
                <div class="about-content home">
                    <!-- right side -->
                    <div class="right-side" style="display:none;">

                        <img src="<?php echo base_url(); ?>styles/images/logo-large.png" alt="" class="logo">
                        <img src="<?php echo base_url(); ?>styles/images/guy-suit.png">
                    </div>
                    <!-- right side end -->

                    <!-- left side start -->
                    <div class="left-side">

                        <h1>هناك حقيقة مثبتة منذ زمن طويل</h1>

                        <p>
                            هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشك ح للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشك ح للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.
                        </p>

                        <!-- 4 points start -->
                        <ul class="points">
                            <li>هناك حقيقة مثبتة منذ زمن طويل</li>
                            <li>هناك حقيقة مثبتة منذ زمن طويل</li>
                            <li>هناك حقيقة مثبتة منذ زمن طويل</li>
                            <li>هناك حقيقة مثبتة منذ زمن طويل</li>
                        </ul>
                        <!-- 4 points end -->

                        <div class="clr"></div>

                        <!-- steps result -->
                        <ul class="step-result">
                            <li><img src="<?php echo base_url(); ?>styles/images/step1.png"></li>
                            <li class="arrows"><img src="<?php echo base_url(); ?>styles/images/arrows.gif"></li>
                            <li><img src="<?php echo base_url(); ?>styles/images/result.png"></li>
                        </ul>
                        <!-- steps result end -->

                        <div class="clr"></div>


                        <!-- supporting lang -->
                        <p class="support-lang">مدعّم للغة الإنجليزية</p>
                        <!-- supporting lang end -->

                        <!-- buttons -->
                        <div class="buttons">
                            <a href="<?php echo site_url();?>/auth/index" class="login">تسجيل الدخول</a>
                            <a href="<?php echo site_url();?>/auth/facebook_login" class="fb">الدخول عبر الفيس بوك</a>
                            <a href="<?php echo site_url();?>/auth/twitter_login" class="tw">قم بالإتصال عبر تويتر</a>
                        </div>



                    </div>
                    <!-- left side end -->
                </div>

            </div>
            <!-- container end -->
        </div>
        <!-- page wrap end -->


        <!-- footer wrapper start -->
        <div class="footer-wrapper">

        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="<?php  echo base_url()?>styles/js/jquery-1.7.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php  echo base_url()?>styles/js/bootstrap.min.js"></script>

        <script type="text/javascript">

            var NHeight = $(window).height();
            $('body').css('height', NHeight);
            $('.home-page .container').css('height', NHeight -= 59);


            $(window).resize(function() {
                var NHeight = $(window).height();
                $('body').css('height', NHeight);
                $('.home-page .container').css('height', NHeight -= 59);
            });

            $(document).ready(function() {
                $('.right-side').show('slow').effect('shake');
            });
        </script>
        <script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
    </body>

</html>