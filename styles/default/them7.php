<html>
<head>
 <link href="<?php echo base_url("styles/css/style7_".$cv->lang.".css") ?>" rel="stylesheet">
 <!--<link href="final-arabic.css" rel="stylesheet">-->
</head>
<body >

<div class="empty">
</div>

<!-- started name div -->
<div class="name">
<div class="container">
<div class="first">
<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
<div class="l">
<img class="x" style="border:1px dotted #838383;margin-top:15px;"  src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>" width="70px;">
</div>
<?php } ?>
<div class="np">
<h1><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
<h3><?php echo $user_info[0]->title ; ?></h3>
</div>
</div>
<div class="second">
<div class="aa">
<?php if($user_info[0]->adress1 !=''){ ?>
<div class="info" >
<img src="<?php echo base_url(); ?>assets/images/home1.png" alt="home">
</div>
<p class="parag"><?php echo $user_info[0]->adress1; ?></p>
 <?php } ?>
 <?php if($user_info[0]->adress2 !=''){ ?>
<div class="info">
<img src="<?php echo base_url(); ?>assets/images/home1.png" alt="home">
</div>
<p class="parag"><?php echo $user_info[0]->adress2; ?></p>
<?php } ?>
<?php if( !(empty($phones))){ ?> 
<?php foreach($phones as $item){ ?>
<div class="info">
<img src="<?php echo base_url(); ?>assets/images/ee.png" alt="telephone">
</div>

    <p class="parag"><?php echo $item->code.$item->number ; ?></p>
<?php } ?>
<?php } ?>
<div class="infoo">
<img src="<?php echo base_url(); ?>assets/images/rrr.png" alt="email">
</div>
<p class="para"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>

</div>
</div>

<div class="third"> 
<div class="aa">
<?php if( !(empty($social))){ ?>   
<?php foreach ($social as $item) { ?>    
<div class="info">
<img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'f.png';}elseif($item->name=='twitter'){echo 'y.png';}elseif($item->name=='linkledin'){echo 'li.png';}elseif($item->name=='google'){echo 'go.png';}elseif($item->name=='behance'){echo 'bh.png';} ?>" alt="s-image">
</div>
<p class="par"><a style="color: #404041;font-size:14px;" href="<?php echo $item->link; ?>" ><?php echo $item->link; ?></a></p>
<?php } ?>
<?php } ?>
</div>
</div>
</div>

</div>

<!-- the end of name div -->

<!-- started personal div -->
<div class="empt">
</div>
<div class="personal">
<div class="container">
<div class="title">
<h1><?php echo strtoupper(lang("personal_info")) ; ?></h1>
</div>
<div class="info2">
<h6 style="padding-top:10px;" class="nation"><?php if($user_info[0]->nationality !=''){ ?><b><?php echo lang("nationality");?>: </b><span><?php echo $user_info[0]->nationality; ?></span><?php } ?></h6>
<?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?> 
<h6 class="nation"><b><?php echo lang("birth_date") ; ?>: </b><span><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->marital_status !='n'){ 
		 if($user_info[0]->marital_status =='s'){
			$m_status=lang("single");
		}elseif($user_info[0]->marital_status =='m'){
			$m_status=lang("married");
		}elseif($user_info[0]->marital_status =='d'){
			$m_status=lang("divercoed");
		}
?>  
<h6 class="nation"><b><?php echo lang("marital_rstatus"); ?>: </b><span><?php echo $m_status ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->gender !='n'){ 
		if($user_info[0]->gender =='m'){
			$gender=lang("male");
		}elseif($user_info[0]->gender =='f'){
			$gender=lang("female");
		}
?>    
<h6 class="nation"><b><?php echo lang("gender"); ?>: </b><span><?php echo $gender ; ?></span></h6>
<?php } ?>
</div>
<div class="info3">
<?php if($user_info[0]->license !='no'){ 
		if($user_info[0]->license =='y'){
			$License=lang("yes");
		}elseif($user_info[0]->license =='n'){
			$License=lang("no");
		}
?>   
<h6 style="padding-top:10px;" class="nation"><b><?php echo lang("License"); ?>: </b><span><?php echo $License ; ?></span></h6>
<?php } ?>
<?php if($user_info[0]->military !='n'){ 
		if($user_info[0]->military =='e'){
			$military=lang("exemption");
		}elseif($user_info[0]->military =='c'){
			$military=lang("Complete_service");
		}elseif($user_info[0]->military =='p'){
			$military=lang("Postponed");
		}elseif($user_info[0]->military =='s'){
			$military=lang("Currently_serving");
		}elseif($user_info[0]->military =='d'){
			$military=lang("Doesnt_apply");
		}	 
?> 
<h6 class="nation"><b><?php echo lang("military"); ?>: </b><span><?php echo $military; ?></span></h6>
<?php } ?>
</div>
</div>
</div>
<!-- the end of personal div -->

<!-- starting of objective div -->
<div class="empt">
</div>
<?php if( !(empty($cv->objective))){ ?>
<div class="objective">
<div class="container">
<div class="title">
<h1><?php echo strtoupper(lang("objective")) ;?></h1>
</div>
<div class="info4">
<p><?php echo $cv->objective ;?></p>
</div>
</div>
</div>
<!-- the end of objective div -->

<!-- started of courses div -->
<div class="empt">
</div>
<?php } ?>
<?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>
<div class="courses">
<div class="container">
<div class="titles">
<h1><?php if( !(empty($courses))) { echo strtoupper(lang("courses")) ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($certifcates))) echo strtoupper(lang("certifcates")) ; ?></h1>
</div>
<div class="info2">
<?php foreach ($courses as $item) { ?>

<p style="font-size:19px; color:#404041;padding:0px;padding-top:-10px !important;"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
				<?php echo "(".$item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year.")" ; ?>
                
				<?php } ?></p>
<p style="font-size:19px; color:#404041;padding:0px;padding-top:-15px !important;"><b><?php if( !(empty($item->name)))  echo strtoupper($item->name) ; ?></b></p>
<p style="font-size:19px; color:#404041;padding:0px;padding-top:-20px !important;padding-bottom:20px !important;"><?php if( !(empty($item->institute)))  echo $item->institute ; ?></p>
<?php } ?>

<?php foreach ($certifcates as $item) { ?>

<p style="font-size:19px; color:#404041;padding:0px;padding-top:-10px !important;"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
				<?php echo "(".$item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year.")" ; ?>
                
				<?php } ?></p>
<p style="font-size:19px; color:#404041;padding:0px;padding-top:-15px !important;"><b><?php if( !(empty($item->name)))  echo strtoupper($item->name) ; ?></b></p>
<p style="font-size:19px; color:#404041;padding:0px;padding-top:-20px !important;padding-bottom:20px !important;"><?php if( !(empty($item->institute)))  echo $item->institute ; ?></p>
<?php } ?>

</div>
</div>
</div>
<!-- the end of courses div -->

<!-- started of eductaion div -->
<div class="empt">
</div>
<?php } ?>
<?php if( !(empty($education))){ ?>
<div class="education">
<div class="container">
<div class="title">
<h1><?php echo strtoupper(lang("education")); ?></h1>
</div>
<div class="info4">
<?php foreach ($education as $item) { ?>
<p style="padding-bottom:0px !important;"> <b><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year .")" ; ?>
      
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=="now"){ ?>
        
		<?php	echo "(". $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month).")" ; ?>
          
	<?php	} ?></b></p>
<p style="padding-top:0px !important;margin-bottom:0px !important;margin-top:0px !important ;padding-bottom:0px !important;"> <b><?php if( !(empty($item->name)))  echo $item->name ; ?> (<?php if( !(empty($item->type)))  echo $item->type ; ?>)</b></p>
<p style="font-size:14px;"><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>
<?php } ?>
</div>

</div>
</div>
<!-- the end of education div -->

<!-- start of language div -->
<div class="empt">
</div>
<?php } ?>
<?php if( !(empty($langs))){?>
<div class="language">
<div class="container">
<div class="title">
<h1><?php echo strtoupper(lang("langs")) ; ?></h1>
</div>
<div class="info2">
<?php foreach ($langs as $item) {  ?>
<p style="font-size:19px; color:#404041;padding:0px;padding-top:-10px !important;"><?php if( !(empty($item->name))) echo lang($item->name)."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='p') echo lang('poor');
				elseif($item->grade=='f') echo lang('fair');
				elseif($item->grade=='g') echo lang('good');
				elseif($item->grade=='v') echo lang('very_good');
				elseif($item->grade=='fl') echo lang('fluent');
				elseif($item->grade=='t') echo lang('tongue'); ?></p>
<?php } ?>
</div>
</div>
</div>
<!-- end of language div -->

<!-- start of exp div -->
<div class="empt">
</div>
<?php } ?>
<?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
<div class="exp">
<div class="container">
<div class="title">
<h1><?php if( !(empty($experience))) { echo strtoupper(lang("experience")) ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang('and')."&nbsp;" ; } if(!(empty($projects))) echo strtoupper(lang("projects")) ; ?></h1><?php if(!(empty($user_info[0]->exp_no))){ echo " (".lang('no_exp') ."&nbsp;". $user_info[0]->exp_no.")" ;} ?>
</div>
<div class="info4">
<?php foreach ($experience as $item) {  ?>
<p style="padding-bottom:0px !important;"><b><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=="now"){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
            
			<?php } ?></span> </p>
<p style="padding-bottom:0px !important;"><b><?php echo $item->company ; ?></b><span style="padding-bottom:0px !important;"><?php if(!empty($item->name)) {echo "(". $item->name .")" ;}?></span> </p>
<div class="list">
<?php  if(!empty($item->role) !=0){ ?>  
<ul style="list-style:square;">
<?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
<li style="font-size:14px;color:#404041;"><?php echo $r_item ; ?></li>
<?php } ?>
</ul>
<?php } ?>
</div>
<p style="font-size:14px;padding-top:0px !important;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
<?php } ?>

<?php foreach ($projects as $item) {  ?>
<p style="padding-bottom:-20px !important;"><b><a style="color:#404041;" href="<?php echo $item->link ; ?>"><?php echo $item->link ; ?></a></b><span style="padding-bottom:0px !important;"><?php if(!empty($item->name)) {echo "(". $item->name .")" ;}?></span> </p>
<div class="list">
<ul style="list-style:square;">
<?php //foreach ($item->role as $roles) { ?>
<li style="font-size:14px;color:#404041;"></li>
<?php //} ?>
</ul>
</div>
<p style="font-size:14px;padding-top:0px !important;"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
<?php } ?>

</div>
</div>
</div>
<!-- end of exp div -->

<!-- starting t-skills div -->
<div class="empt">
</div>
<?php } ?>
<?php if( !(empty($skills))){ ?>
<div class="t-skills">
<div class="container">
<div class="title">
<h1><?php echo strtoupper(lang("skills")) ; ?></h1>
</div>
<div class="info2">
<div class="list2">
<ul style="list-style:square;">
<?php foreach ($skills as $item) { ?>
<li style="font-size:19px;color:#404041;"><?php if( !(empty($item->name))) echo $item->name."&nbsp;"."-"."&nbsp;" ; ?><?php if($item->grade=='i') echo lang('interemdate');
																				elseif($item->grade=='g') echo lang('good');
																				elseif($item->grade=='w') echo lang('weak'); ?></li>
<?php } ?>
</ul>
</div>
</div>
</div>
</div>
<!-- end of t-skills div -->

<!-- start of p-skills div -->
<div class="empt">
</div>
<?php } ?>
<?php if( !(empty($qualifications))){ ?>
<div class="t-skills">
<div class="container">
<div class="title" >
<h1><?php echo strtoupper(lang("qualifications")) ; ?></h1>
</div>
<?php foreach ($qualifications as $item) { ?>
<div class="info2">
<ul style="list-style:square;">
<li style="font-size:19px;color:#404041;"><?php echo $item->name ; ?></li>

</ul>


</div>
<?php } ?>

</div>
</div>
<!-- end of p-skills div -->

<!-- starting ref div -->
<div class="empt">
</div>
<?php } ?>
<?php if( !(empty($references))){ ?>
<div class="ref">
<div class="container">
<div class="title">
<h1><?php echo strtoupper(lang("references")) ; ?></h1>
</div>
<div class="info4">
<?php foreach ($references as $item) { ?>
<p style="padding-bottom:0px !important;margin-bottom:0px !important;"> <b><?php if( !(empty($item->name))) echo $item->name; ?></b></p>
<p style="padding-bottom:0px !important;padding-top:0px !important;margin-bottom:0px !important;margin-top:0px !important;"> <?php echo $item->mail ; ?></p>
<p style="padding-bottom:10px !important;padding-top:0px !important;margin-top:0px!important;"><?php echo $item->phone ; ?></p>
<?php } ?>

</div>
</div>
</div>
<?php } ?>
</body>
</html>