<h1 class="blue-title"><?php echo lang('your_edu'); ?></h1>
<br class="clr"> 
<form action="<?php echo site_url(); ?>/add_cv/update_edu/<?php echo $cv_id; ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" value="<?php echo $this->session->userdata('percent'); ?>" name="percent" />
    <input type="hidden" value="<?php echo $cv_id; ?>" name="cv_id" />
    <input type="hidden" value="<?php echo count($items); ?>" name="count" id="count" />
    <input type="hidden" id="lang" value="<?php echo LANG; ?>" />
    <div class="step3">
        <div class="content">
            <span class="error"></span>
            <?php if (count($items) == 0) { ?>
                <div id="cont0">
                    <div class="education">
                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <label> <?php echo lang('type'); ?>:</label>
                                    <select name="type[]" class="type">
                                        <option value=""><?php echo lang('chooce_type'); ?></option>
                                        <?php foreach ($types as $type) { ?>

                                            <option value="<?php echo $type->id ?>" > <?php echo $type->{"type_" . LANG}; ?> </option><?php } ?>

                                    </select>
                                </td>

                                <td>
                                    <label> <?php echo lang('name'); ?>:</label>
                                    <input type="text" name="name[]" value="<?php echo set_value('name[]'); ?>" class="name" />
                                    <div><?php if (isset($error)) echo $error; ?></div>
                                </td>

                                <td>
                                    <label> <?php echo lang('from'); ?>:</label>
                                    <select name="from_month[]" class="frm_month small-select">
                                        <option value="" ><?php echo lang('month') ?> </option>
                                        <option value="01" ><?php echo lang('January'); ?></option>
                                        <option value="02"><?php echo lang('February'); ?></option>
                                        <option value="03" ><?php echo lang('March'); ?></option>
                                        <option value="04" ><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06"><?php echo lang('June'); ?></option>
                                        <option value="07" ><?php echo lang('July'); ?></option>
                                        <option value="08" ><?php echo lang('August'); ?></option>
                                        <option value="09" ><?php echo lang('September'); ?></option>
                                        <option value="10" ><?php echo lang('October'); ?></option>
                                        <option value="11"><?php echo lang('November'); ?></option>
                                        <option value="12" ><?php echo lang('December'); ?></option>
                                    </select>

                                    <select name="from_year[]" class="frm_year small-select" >
                                        <option value="" ><?php echo lang('year') ?> </option>
                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>" ><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>


                                </td>

                                <td>
                                    <label> <?php echo lang('to'); ?>:</label>
                                    <select name="to_month[]" class="to_month0 small-select" onchange="dim_year(0)">
                                        <option value="" ><?php echo lang('month') ?> </option>
                                        <option value="now" ><?php echo lang('now') ?> </option>
                                        <option value="01" ><?php echo lang('January'); ?></option>
                                        <option value="02" ><?php echo lang('February'); ?></option>
                                        <option value="03" ><?php echo lang('March'); ?></option>
                                        <option value="04" ><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06" ><?php echo lang('June'); ?></option>
                                        <option value="07" ><?php echo lang('July'); ?></option>
                                        <option value="08" ><?php echo lang('August'); ?></option>
                                        <option value="09" ><?php echo lang('September'); ?></option>
                                        <option value="10" ><?php echo lang('October'); ?></option>
                                        <option value="11" ><?php echo lang('November'); ?></option>
                                        <option value="12" ><?php echo lang('December'); ?></option>
                                    </select>

                                    <select name="to_year[]" class="to_year0 small-select">
                                        <option value="" ><?php echo lang('year') ?> </option>

                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>"  ><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select> 
                                </td>
                            </tr>

                            <tr>
                                <td colspan="4">
                                    <label> <?php echo lang('desc'); ?>:</label>
                                    <textarea name="desc[]" ></textarea>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div> <!--end of cont----> 
                <?php
            } else {
                foreach ($items as $key => $item) { //echo $val  
                    ?>
                    <input type="hidden" name="cv_id"  id="cv" value="<?php echo $item->cv_id; ?>" />
                    <input type="hidden" name="id[]" id="id<?php echo $key; ?>" value="<?php echo $item->id; ?>"  />
                    <div id="cont<?php echo $key; ?>">

                        <div class="education-added">
                            <div class="education">
                                <table cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <label><?php echo lang('type'); ?>:</label>
                                            <select name="type[]" class="type ">
                                                <option value=""><?php echo lang('chooce_type'); ?></option>
                                                <?php
                                                foreach ($types as $type) {

                                                    if ($type->id == $item->type_id) {
                                                        ?> <option value="<?php echo $type->id ?>" selected="selected" > <?php echo $type->{"type_" . LANG}; ?> </option>
                                                    <?php } else { ?> <option value="<?php echo $type->id ?>"  > <?php echo $type->{"type_" . LANG}; ?></option> <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>

                                        <td>
                                            <label> <?php echo lang('name'); ?>:</label>
                                            <input type="text" name="name[]" value="<?php echo $item->name; ?>"  class="name"/>
                                        </td>

                                        <td>

                                            <label><?php echo lang('from'); ?>:</label>
                                            <select name="from_month[]" class="frm_month small-select">
                                                <option value="" ><?php echo lang('month') ?> </option>
                                                <option value="01" <?php
                                                if ($item->from_month == "01") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('January'); ?></option>
                                                <option value="02" <?php
                                                if ($item->from_month == "02") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('February'); ?></option>
                                                <option value="03" <?php
                                                if ($item->from_month == "03") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('March'); ?></option>
                                                <option value="04" <?php
                                                if ($item->from_month == "04") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('April'); ?></option>
                                                <option value="05" <?php
                                                if ($item->from_month == "05") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('May'); ?></option>
                                                <option value="06" <?php
                                                if ($item->from_month == "06") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('June'); ?></option>
                                                <option value="07" <?php
                                                if ($item->from_month == "07") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('July'); ?></option>
                                                <option value="08" <?php
                                                if ($item->from_month == "08") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('August'); ?></option>
                                                <option value="09" <?php
                                                if ($item->from_month == "09") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('September'); ?></option>
                                                <option value="10" <?php
                                                if ($item->from_month == "10") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('October'); ?></option>
                                                <option value="11" <?php
                                                if ($item->from_month == "11") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('November'); ?></option>
                                                <option value="12" <?php
                                                if ($item->from_month == "12") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('December'); ?></option>
                                            </select>

                                            <select name="from_year[]" class="frm_year small-select">
                                                <option value="" ><?php echo lang('year') ?> </option>
                                                <?php
                                                for ($y = 1975; $y <= year; $y++) {
                                                    ?>

                                                    <option value="<?php echo $y; ?>"<?php if ($item->from_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </td>

                                        <td>
                                            <label> <?php echo lang('to'); ?>:</label>
                                            <select name="to_month[]" class="to_month<?php echo $key; ?> small-select" onchange="dim_year(<?php echo $key; ?>)">
                                                <option value="" ><?php echo lang('month') ?> </option>
                                                <option value="now" <?php
                                                if ($item->to_month == "now") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('now') ?> </option>
                                                <option value="01" <?php
                                                if ($item->to_month == "01") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('January'); ?></option>
                                                <option value="02" <?php
                                                if ($item->to_month == "02") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('February'); ?></option>
                                                <option value="03" <?php
                                                if ($item->to_month == "03") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('March'); ?></option>
                                                <option value="04" <?php
                                                if ($item->to_month == "04") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('April'); ?></option>
                                                <option value="05" <?php
                                                if ($item->to_month == "05") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('May'); ?></option>
                                                <option value="06" <?php
                                                if ($item->to_month == "06") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('June'); ?></option>
                                                <option value="07" <?php
                                                if ($item->to_month == "07") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('July'); ?></option>
                                                <option value="08" <?php
                                                if ($item->to_month == "08") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('August'); ?></option>
                                                <option value="09" <?php
                                                if ($item->to_month == "09") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('September'); ?></option>
                                                <option value="10" <?php
                                                if ($item->to_month == "10") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('October'); ?></option>
                                                <option value="11" <?php
                                                if ($item->to_month == "11") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('November'); ?></option>
                                                <option value="12" <?php
                                                if ($item->to_month == "12") {
                                                    echo "selected";
                                                }
                                                ?>><?php echo lang('December'); ?></option>
                                            </select>

                                            <select name="to_year[]" class="to_year<?php echo $key; ?> small-select">
                                                <option value="" ><?php echo lang('year') ?> </option>
                                                <?php
                                                for ($y = 1975; $y <= year; $y++) {
                                                    ?>

                                                    <option value="<?php echo $y; ?>"<?php if ($item->to_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                    <?php
                                                }
                                                ?>
                                            </select> 
                                        </td>
                                    </tr>

                                    <tr>
                                        <td colspan="4">
                                            <label> <?php echo lang('desc'); ?>:</label>
                                            <textarea name="desc[]"><?php echo $item->desc; ?></textarea>

                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <?php if ($key > 0) { ?>
                                <a href="javavasript:void(0)" class="delete" onClick="del_unit(<?php echo $key; ?>)"></a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div><!--end of content--->

        <br class="clr">
        <!-- add new -->
        <a href="javascript:void(0)" class="add" onClick="add_unit()" ></a>

    </div><!-- step 3 end -->


    <!-- right form end -->

    <br class="clr">
    <!-- buttons start -->
    <div class="buttons">
        <a href="<?php echo site_url(); ?>/add_cv/view_user/<?php echo $cv_id; ?>" class="previous-step"><?php echo lang('last'); ?></a>

        <div class="save-next">
            <input type="submit"  name="save_button" value ="<?php echo lang('save') ?>"  class="save"/>
            <input type="submit"  name="next_button"  value ="<?php echo lang('next') ?>" id="submit"  class="next-step"/>
        </div>
    </div>
</form>

<script>
    var count = $("#count").val();
    // alert(count);
    function add_unit() {
        //get now  year
        var d = new Date();
        var year = d.getFullYear();
        var cv_id = $("#cv").val();
        var lang = $("#lang").val();

        var cont_edu = '<br class="clr"><div id="cont' + count + '"><div class="education-added"><div class="education"><table cellpadding="0" cellspacing="0"><tr><td><label><?php echo lang('type'); ?>: </label><select name="type[]" class="type"><option value=""><?php echo lang('chooce_type'); ?></option>';



        //get education type
        var type;
        $.ajax({
            type: "POST",
            url: "<?php echo site_url(); ?>/edu_type/index/" + lang,
            async: false,
            success: function(data) {
                types = data.split("#");
                for (var t = 0; t < (types.length) - 1; t++) {
                    type = types[t].split("*");
                    //alert(type);
                    cont_edu += '<option value="' + type[1] + '">' + type[0] + '</option>';
                }

            }
        });
        cont_edu += '</select></td><td><label><?php echo lang('name'); ?>:</label><input type="text" name="name[]" class="name"/></td>';
        cont_edu += '<td><label><?php echo lang('from'); ?>:</label><select name="from_month[]" class="frm_month small-select"><option value="" ><?php echo lang('month') ?> </option><option value="01" ><?php echo lang('January'); ?></option><option value="02" ><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select>';
        cont_edu += '<select name="from_year[]" class="frm_year small-select"><option value="" ><?php echo lang('year') ?> </option>';
        //
        for (var y = 1975; y <= year; y++) {
            cont_edu += '<option value="' + y + '">' + y + '</option>';
        }


        cont_edu += '</select></td><td><label><?php echo lang('to'); ?>:</label><select name="to_month[]" class="to_month' + count + ' small-select" onchange="dim_year(' + count + ')"><option value="" ><?php echo lang('month') ?> </option> <option value="now" ><?php echo lang('now') ?> </option><option value="01" ><?php echo lang('January'); ?></option><option value="02"><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select><select name="to_year[]" class="to_year' + count + ' small-select"><option value="" ><?php echo lang('year') ?> </option>';
        for (var ty = 1975; ty <= year; ty++) {
            cont_edu += '<option value="' + ty + '">' + ty + '</option>';
        }
        cont_edu += '</select></td></tr><tr><td colspan="4"><label><?php echo lang('desc'); ?>:</label><textarea name="desc[]"></textarea> </td></tr></table></div><a href="javascript:void(0);" class="delete" onClick="del_unit(' + count + ')"></a></div>';
        $(".content").append(cont_edu);
        count++;
        //alert(count);
    }
//
    function del_unit(id) {
        //alert(id);
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            edu_id = $("#id" + id).val();
            $("#cont" + id).remove();
        }
        else {
            return false;
        }
        //count--;
    }

//
    $("form").submit(function() {

        var names = $(".name");
        for (var i = 0; i < names.length; i++) {

            if ($(".type").eq(i).val() == '' && $(".name").eq(i).val() == '' && $(".frm_year").eq(i).val() == '' && $(".frm_month").eq(i).val() == '' && $(".to_year").eq(i).val() == '' && $(".to_month").eq(i).val() == '') {
                return true;
            }
            else if ($(".type").eq(i).val() == '' || $(".name").eq(i).val() == '' || $(".frm_year").eq(i).val() == '' || $(".frm_month").eq(i).val() == '' || $(".to_month").eq(i).val() == '') {
                //alert(i);
                $(".error").html('you should complete you data');
                return false;
            }

        }
    });

//disable year when value of mont now
    function dim_year(ct) {
        $("#foo").remove();
        $(".to_year" + ct).val('');
        $(".to_year" + ct).prop("disabled", false);
        if ($(".to_month" + ct).val() == "now") {
            $(".to_year" + ct).prop("disabled", true);
            $('<input>').attr({type: 'hidden', name: 'to_year[]', id: 'foo'}).appendTo("#cont" + ct);
        }

    }

    $(document).ready(function() {
        for (ct = 0; ct < count; ct++) {
            if ($(".to_month" + ct).val() == "now") {
                $(".to_year" + ct).prop("disabled", true);
                $('<input>').attr({type: 'hidden', name: 'to_year[]', id: 'foo'}).appendTo("#cont" + ct);
            }
        }
    });

</script>