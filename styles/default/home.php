<div class="user-content">
    <div class="user-data">
        <p>الإسم بالكامل</p>
        <p>اسم المستخدم</p>
        <p>usedemail@hobba.com</p>
        <a href="#">(تعديل البيانات)</a>
    </div>

    <form name="form1" action="<?php echo site_url(); ?>/index/home" method="post" enctype="multipart/form-data">
        <table width="200" border="1">
            <tr>
            <input type="hidden" name="id" value="<?php echo $user->id; ?>" /> 
            <td><input type="text" tabindex="1" name="first_name" value="<?php echo set_value('first_name', $user->first_name); ?>">
                <div class="form-msg-error-text"><?php echo form_error('first_name'); ?></div></td>
            <td><?php echo lang('first_name'); ?></td>
            </tr>
            <tr>
                <td><input type="text" tabindex="2" name="last_name" value="<?php echo set_value('last_name', $user->last_name); ?>">
                    <div class="form-msg-error-text"><?php echo form_error('last_name'); ?></div></td>
                <td><?php echo lang('last_name'); ?></td>
            </tr>
            <tr>
                <td><input type="text" tabindex="3" name="user_name" value="<?php echo set_value('user_name', $user->user_name); ?>">
                    <div class="form-msg-error-text"><?php echo form_error('user_name'); ?></div></td>
                <td><?php echo lang('user_name'); ?></td>
            </tr>
            <tr>
                <td><input type="password" tabindex="4" name="password" value="<?php echo set_value('password'); ?>">
                    <div class="form-msg-error-text"><?php echo form_error('password'); ?></div></td>
                <td><?php echo lang('password'); ?></td>
            </tr>
            <tr>
                <td><input type="password" tabindex="5" name="re_password" value="<?php echo set_value('re_password'); ?>">
                    <div class="form-msg-error-text"><?php echo form_error('re_password'); ?></div></td>
                <td><?php echo lang('re_password'); ?></td>
            </tr>

            <td colspan="2">
                <input type="submit" name="button" id="button" value="<?php echo lang('save'); ?>" /></td>
            </tr>
        </table>
    </form>
    <?php
    $result = $this->session->flashdata('result');
    if (!empty($result)) {
        echo $result;
    }
    ?>
    <br class="clr">

    <div class="prv-cv">
        <a href="#"><?php echo lang('prev_cv'); ?></a>
        <p><?php echo lang('home_title'); ?></p>
    </div>
    <br class="clr">

    <?php foreach ($items as $item) { ?>
        <div class="cv-status">
            <a href="<?php echo base_url() . $users->user_name . "/" . $item->url_title; ?>"><h3><?php echo $item->title; ?></h3></a>
            <p class="print-allowed"><?php echo lang('comp_precent'); ?> <?php echo $item->percent; ?><?php echo lang('can_publish'); ?> </p>

            <br class="clr">

            <div class="cv-pic-actions">
                <div>
                    <?php if (!empty($item->img)) { ?>
                        <img src="<?php echo base_url() . "assets/" . $folder_name . "/" . $item->img; ?>" height="50" width="50" onclick="preview(<?php echo $item->id ?>,<?php echo $item->pdf_them ?>)" class="choosen-cv"  />
                    <?php } else { ?> 
                        <img src="<?php echo base_url() . "assets/images/" . $pdf_themsdefault[0]->img; ?>" height="50" width="50" onclick="preview(<?php echo $item->id ?>)" class="choosen-cv" />   
                    <?php } ?>   
                    <div class="cv-actions"><ul>
                            <li>
                                <p><?php echo lang('cv_publish'); ?></p>
                                <?php
                                if ($item->percent < 35 || $users->status == 0) { ?>
                                   <a href="<?php echo site_url() . "/finish/publish_error";?>" target="_blank" class="facebook"></a>
                               <?php } else {
                                    echo share_button('facebook', array('url' => base_url() . $users->user_name . "/" . $item->url_title . "/", 'text' => 'share'));
                                }
                                ?>
                                <?php
                                if ($item->percent < 35 || $users->status == 0) { ?>
                                   <a href="<?php echo site_url() . "/finish/publish_error"; ?>" target="_blank" class="twitter"></a>
                               <?php } else {
                                    echo share_button('twitter', array('url' => base_url() . $users->user_name . "/" . $item->url_title));
                                }
                                ?>
                                <?php
                                if ($item->percent < 35 || $users->status == 0) { ?>
                                   <a href="<?php echo site_url() . "/finish/publish_error"; ?>" target="_blank" class="gplus"></a>
                               <?php } else {?>
                               <div class="g-plusone" data-annotation="bubble" data-width="600" data-href="<?php echo base_url() . $users->user_name . "/" . $item->url_title; ?>"></div>

<script type="text/javascript">
  (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
  })();
</script>
							   <?php } ?>
                            </li>
                            <li>
                                <p><?php echo lang('pdf'); ?></p>
                                <a href="<?php echo site_url(); ?>/finish/pdf_file/<?php echo $item->id; ?>" class="pdf"></a>
                            </li>
                            <li>
                                <p><?php echo lang('cv_url'); ?></p>
                                <input type="text" readonly="true" value="<?php echo base_url() . $users->user_name . "/" . $item->url_title; ?>">
                            </li>
                            <li>
                                <p><?php echo lang('edit_cv'); ?> <a href="<?php echo site_url(); ?>/add_cv/back_cv/<?php echo $item->id; ?>" class="edit"><?php echo lang('edit'); ?></a> <?php echo lang('or_delete') ?> <a href="<?php echo site_url(); ?>/index/delete_cv/<?php echo $item->id; ?>" class="delete" onClick="if (!confirm('<?php echo lang("cofirm_delete"); ?>'))
                                                return false;"><?php echo lang('delete'); ?></a></p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

    <?php } ?>




    <script>

        function preview(cv_id, style_id) {
            if (style_id)
                window.open("<?php echo site_url(); ?>/thems/preview/" + cv_id + "/" + style_id);
            else
                window.open("<?php echo site_url(); ?>/thems/preview/" + cv_id);
        }

    </script>