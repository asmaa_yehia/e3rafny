<div class="clr"></div>
<div class="about-content contact">
    <div class="right-side">

        <div class="full-info">
            <img src="<?php echo base_url() ?>styles/images/contact-logo.png" alt="">

            <ul class="social">
                <li><a href="" class="facebook"></a></li>
                <li><a href="" class="twitter"></a></li>
                <li><a href="" class="gplus"></a></li>
            </ul>
        </div>

    </div>
    <div class="left-side">
        <?php
        if (isset($msg)) {
            echo $msg;
        };
        ?>
        <p class="contact-text">
            في حاجة محتاج تسال عنها وملقتهاش ، او عندك استفسار او اقتراح  لنا  ،، ابعت رسالة علي ايميلنا 
            INFO@Earafny.com   او بشكل مباشر من هنا ، الرد يوصلك في اقرب وقت
        </p>

        <div class="contact-form">
            <img src="<?php echo base_url() ?>styles/images/letter.png" alt="">

            <br class="clr">

            <div class="contact-us">
                <form method="post" action="<?php echo base_url(); ?>index/contact" >
                    <table class="c-form" cellspacing="15">
                        <tr>
                            <td>
                                <label><?php echo lang('name'); ?>:</label><input type="text" name="name" value="<?php echo set_value('name'); ?>">
                                <?php echo form_error('name'); ?></td>
                            <td> <label><?php echo lang('mail'); ?>:</label> <input type="email" name="mail" value="<?php echo set_value('mail'); ?>">
                                <?php echo form_error('mail'); ?></td>
                        </tr>

       <!-- <tr><td><label><?php echo lang('phone'); ?></label> <input type="text" name="phone" value="<?php //echo value_field('phone');      ?>" ></td></tr>-->
                        <tr><td colspan="2"><label><?php echo lang('address'); ?>:</label> <input type="text" name="subject" value="<?php echo set_value('subject'); ?>" class="subject">
                            </td></tr>
                        <tr> <td colspan="2" ><label><?php echo lang('message'); ?>:</label><textarea name="message"><?php echo set_value('message'); ?></textarea>
                                <?php echo form_error('message'); ?></td>
                        </tr>
                        <tr>
                            <td colspan="2"> <input type="submit" value="<?php echo lang('contact'); ?>"></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>



