<a href="#" class="messages"></a>
<span class="unread-counter"><?php echo $count_msg;?></span>

<div class="panel">
    <div class="head-area">
        <p class="new"><?php echo $count_msg; ?> <?php echo lang('new_message'); ?> </p>
        <p class="all"><?php echo $all_count; ?> <?php echo lang('message'); ?> </p>

    </div>
    <ul>

        <?php if (!empty($all_messages)) { ?>  
            <?php
            foreach ($all_messages as $item) {
                ?>
                <li>
                    <div <?php
                    if ($item->seen == '0') {
                        echo 'class="comment_ui2"';
                    } else {
                        echo 'class="comment_ui"';
                    }
                    ?>>

                        <p><a href="<?php echo site_url(); ?>/index/view_message/<?php echo $item->id; ?>" class="view_msg"><?php echo $item->title; ?> <?php echo date("F j, Y, g:i a", $item->date); ?></a></p>


                </li>

            <?php } ?>





        </ul>
    <?php } ?>  
    <a href="<?php echo site_url(); ?>/index/all_messages" class="view-all"><?php echo lang('message_read');?></a>
</div>