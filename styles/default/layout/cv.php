<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>E3rfny</title>

        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
        <!-- my styleshhets -->
        <link href="<?php echo base_url(); ?>styles/css/stylesheet.css" rel="stylesheet"   type="text/css"/>
        <link href="<?php echo base_url(); ?>styles/fonts/stylesheet.css" rel="stylesheet">
        <script src="<?php echo base_url(); ?>styles/js/jquery-1.7.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="<?php echo base_url(); ?>styles/js/bootstrap.min.js"></script>

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>



    <body>
        <div class="top-bar"></div>

        <div class="page-wrap">
            <div class="container">

                <!-- logo and navbar -->
                <div class="nav-logo-area">
                    <a href="<?php echo site_url(); ?>" class="logo"></a>

                    <div class="left-area">
                        <!-- lang -->
                        <div class="user-lang-area">
                            <p><?php echo lang('hello'); ?>:<a href=" <?php echo site_url(); ?>/index/home"><?php echo $user_status->first_name; ?> </a></p>

                            <ul>
                                <li><a href="<?php echo base_url(); ?>ar" class="ar"><?php echo lang('arabic'); ?></a></li>
                                <li><a href="<?php echo base_url(); ?>en" class="en"><?php echo lang('english'); ?></a></li>
                            </ul>
                        </div>
                        <!-- lang end -->
                        <div class="clr"></div>

                        <div class="blue-navigation-area"></div>
                        <div class="gray-navigation-area">
                            <ul class="menu">
                                <li><a href="<?php echo site_url(); ?>/index/home"><?php echo lang('mypage'); ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/add_cv"><?php echo lang('create_cv'); ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/index/home"><?php echo lang('store'); ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/index/faq"><?php echo lang('faq'); ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/index/about"><?php echo lang('about'); ?></a></li>
                                <li><a href="<?php echo site_url(); ?>/index/contact"><?php echo lang('contact'); ?></a></li>
                            </ul>
                            <ul class="controls">
                                <li class="msg">
                                    
                                       <!-- <div id="messages"></div>-->
                                        
                                    <!-- panel end -->
                                </li>

                                <li class="ntf">
                                </li>
                                <li><a href="<?php echo site_url(); ?>/auth/logout" class="logout"></a></li>
                            </ul>
                        </div>
                    </div><!-- end of left area-->
                </div><!-- end logo and navbar end-->

                <div class="clr"></div>

                <!-- content -->
                <div class="steps-content">
                    <?php if ($user_status->status == 0) { ?>
                        <div class="mail-confirm">
                            <p><?php echo lang('active_status'); ?> </p>
                        </div>
                    <?php } ?>

                    <br class="clr">
                    <div class="create-wizard">
                        {yield}
                    </div><!-- tab 1 end -->
                </div><!-- end of step content--->
            </div><!-- cv-title end-->
        </div><!-- personal info---->


    </div> <!-- container end -->




</div><!-- page wrap end -->

<br class="clr">

<!-- footer wrapper start -->
<div class="footer-wrapper">

</div><!--end of footer-->


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->

<script type="text/javascript">
    $(document).ready(function() {
        $('li.msg').click(function() {
            $('li.msg .panel').slideToggle();
        });

        $('li.ntf').click(function() {
            $('li.ntf .panel').slideToggle();
        });

        $('.panel').click(function(event) {
            event.stopPropagation();
        });
    });
</script>

<script>
    $(document).ready(function() {
        //alert("ass");
        $(".ntf").load("<?php echo site_url('index/notifcations'); ?>");
        var refreshId = setInterval(function() {
            $(".ntf").load('<?php echo site_url('index/notifcations'); ?>');
        }, 5000);
        $.ajaxSetup({cache: false});

        $(".msg").load("<?php echo site_url('index/messages'); ?>");
        var refreshId = setInterval(function() {
            $(".msg").load('<?php echo site_url('index/messages'); ?>');
        }, 5000);
        $.ajaxSetup({cache: false});
    });
</script>
</body>
</html>