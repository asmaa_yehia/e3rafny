<!DOCTYPE html>

<html>
    <head>
        <title><?php echo $user_info[0]->user_cv_name ; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
        <meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
        <meta property="og:site_name" content="e3rafny" />
        <meta name="twitter:card" value="summary" />
        <meta name="twitter:site" value="e3rafny" />
        <meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
        <meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
        <meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
        <meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
        <meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
    <meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">
        <link href='http://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css'>
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
    
        <link href="<?php echo base_url(); ?>styles/css/<?php echo $style_name."_".$cv[0]->lang.".css"; ?>" rel="stylesheet">
        
    </head>
    <body>

        <div class="head-wrapper">


            <div class=" name-wrapper">
                <div class="profile-img-container">
		<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
                    <img id="profile-img" class="profile-img img-responsive" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>"/>
                    <?php } ?>
                </div>
                <div class="name-inner-wrapper">
                <div class="<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ echo 'name';}else {echo 'name name-no-img';}  ?>"> <h1><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1></div>
                <div class="<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ echo 'position';}else {echo 'position pos-no-img';}  ?>"><h2> <?php echo $user_info[0]->title ; ?></h2></div>
                <div class="<?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ echo 'social-wrapper';}else {echo 'social-wrapper social-wrapper-no-img';}  ?>">
                <?php foreach ($social as $item) { ?>
                    <a href="<?php echo $item->link ; ?>" id="<?php if($item->name=='facebook'){echo 'fb-icon';}elseif($item->name=='twitter'){echo 't-icon';}elseif($item->name=='linkledin'){echo 't-icon';}elseif($item->name=='google'){echo 'g-icon';}elseif($item->name=='behance'){echo 'g-icon';} ?>"><i class="<?php if($item->name=='facebook'){echo 'fa fa-facebook';}elseif($item->name=='twitter'){echo 'fa fa-twitter';}elseif($item->name=='linkledin'){echo 'fa fa-linkedin';}elseif($item->name=='google'){echo 'fa fa-google';}elseif($item->name=='behance'){echo 'fa fa-behance';} ?>"></i></a>
                <?php } ?>    
                    
                </div>
                </div>
            </div>
            <div class=" contact-info">
                <div class="v-hr"></div>
                <div class="email contact-row">
                    
                    <span><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></span>
                </div>
                <div class="phone contact-row">
                    
                    <?php foreach ($phones as $item) {?>
                    <span><?php echo $item->code.$item->number ; ?></span>
                    <?php } ?>
                </div>
                 <?php if($user_info[0]->adress1 !=''){ ?>
                <div class="address contact-row">
                    
                    <span><?php echo $user_info[0]->adress1; ?></span>
                </div>
                <?php } ?>
                <?php if($user_info[0]->adress2 !=''){ ?>
                <div class="address contact-row">
                    
                    <span><?php echo $user_info[0]->adress2; ?></span>
                </div>
                <?php } ?>
                                
            </div>
        </div>

        <div class="container-wrapper">
            <div class="container">
            <?php if( !(empty($cv[0]->objective))){ ?>  
                <div class="row objective">
                    <div class="col-md-2 objective-head"><h2><?php echo strtoupper(lang("objective")); ?></h2></div>
                    <div class="col-md-10 objective-desc"><p><?php echo $cv[0]->objective ;?></p>
                    </div>

                </div><!--objective-->
           <?php } ?>     
                <div class="row cv-content tabcordion">
                    <!-- Nav tabs -->
                    <div class="col-md-12 tab-conatiner">
                        <ul id="myTab" class="nav nav-tabs  ">
                            <li class="active"><a href="#personal-info" data-toggle="tab"><?php echo lang("personalinfo") ; ?></a></li>
                            <?php if( !(empty($education))){ ?>
                            	<li><a href="#education" data-toggle="tab"><?php echo lang("education") ; ?></a></li>
                            <?php } ?>
                            <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
                            	<li><a href="#courses" data-toggle="tab"><?php if( !(empty($courses))) { echo lang("courses") ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo lang("certifcates") ; ?></a></li>
                           <?php } ?>  
                           <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>   
                            <li><a href="#exp" data-toggle="tab"><?php if( !(empty($experience))) { echo lang("experience") ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo lang("projects") ; ?></a></li>
                          <?php } ?>  
                           <?php if( !(empty($langs))){ ?> 
                            <li><a href="#lang" data-toggle="tab"><?php echo lang("langs") ; ?></a></li>
                          <?php } ?> 
                          <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?>   
                            <li><a href="#skills" data-toggle="tab"><?php echo lang("skils") ; ?></a></li>
                          <?php } ?>  
                          <?php if( !(empty($references))){ ?>
                            <li><a href="#ref" data-toggle="tab"><?php echo lang("references") ; ?></a></li>
                          <?php } ?>  
                            <li><a href="#recom" data-toggle="tab"><?php echo lang('recommendation'); ?></a></li>
                        </ul>
                    </div>
                    <!-- Tab panes -->
                    <div>
                        <div id="myTabContent" class="tab-content hidden-sm row ">
                            
                            <div class="tab-pane collapse active col-md-12" id="personal-info">
                                <ul>
                                <?php if($user_info[0]->nationality !=''){ ?>
                                    <li><strong><?php echo lang("nationality");?> : </strong><?php echo ucfirst($user_info[0]->nationality); ?></li>
                               <?php } ?>  
                               <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>   
                                    <li><strong><?php echo lang("birth_date") ; ?> : </strong><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></li>
                              <?php } ?> 
                               <?php if($user_info[0]->marital_status !='n'){ 
									if($user_info[0]->marital_status =='s'){
										$m_status=lang("single");
									}elseif($user_info[0]->marital_status =='m'){
										$m_status=lang("married");
									}elseif($user_info[0]->marital_status =='d'){
										$m_status=lang("divercoed");
									}
								?>         
                                    <li><strong><?php echo lang("marital_rstatus"); ?> : </strong><?php echo $m_status ; ?></li>
                                <?php } ?>  
                                <?php if($user_info[0]->gender !='n'){ 
										if($user_info[0]->gender =='m'){
											$gender=lang("male");
										}elseif($user_info[0]->gender =='f'){
											$gender=lang("female");
										}
								 ?>      
                                    <li><strong><?php echo lang("gender"); ?> : </strong><?php echo $gender ; ?></li>
                                 <?php } ?>  
                                 <?php if($user_info[0]->license !='no'){ 
										 if($user_info[0]->license =='y'){
											$License=lang("yes");
										}elseif($user_info[0]->license =='n'){
											$License=lang("no");
										}
								?>   
                                    <li><strong><?php echo lang("License"); ?> : </strong><?php echo $License ; ?></li>
                                <?php } ?>    
                                <?php if($user_info[0]->military !='n'){ 
										if($user_info[0]->military =='e'){
											$military=lang("exemption");
										}elseif($user_info[0]->military =='c'){
											$military=lang("Complete_service");
										}elseif($user_info[0]->military =='p'){
											$military=lang("Postponed");
										}elseif($user_info[0]->military =='s'){
											$military=lang("Currently_serving");
										}elseif($user_info[0]->military =='d'){
											$military=lang("Doesnt_apply");
										}	 
								 ?>  
                                    <li><strong><?php echo lang("military"); ?> : </strong><?php echo $military; ?></li>
                                 <?php } ?>   
                                </ul>
                            </div>
                           <?php if( !(empty($education))){ ?> 
                            <div class="tab-pane col-md-12" id="education">
                                <ul class="outter-ul">
                                <?php foreach ($education as $item) { ?>
                                    <li>
                                        <h2><?php if( !(empty($item->type)))  echo $item->type ; ?></h2>
                                        <ul>
                                            <li>
                                                <span><?php if( !(empty($item->name)))  echo $item->name ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo "(". $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year .")" ; ?>
          
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo "(". $item->from_month."/".$item->from_year." - ".lang($item->to_month).")" ; ?>
        
	<?php	} ?></span>
                                                <p><?php if( !(empty($item->desc)))	echo $item->desc ; ?></p>
                                            </li>
                                        </ul>
                                    </li>
								<?php } ?>
                                </ul>
                            </div>
                            <?php } ?>
                            <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
                            <div class="tab-pane col-md-12" id="courses">
                                <ul class="outter-ul">
                                <?php foreach ($courses as $item) { ?>
                                    <li>
                                        <h2><?php  echo $item->name ; ?></h2>
                                        <?php if( ( !(empty($item->institute))) || !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                                        <ul>
                                            <li>
                                                <span><?php echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
             
				<?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?><?php } ?>
              
				</span>

                                            </li>
                                        </ul>
                                        <?php } ?>
                                    </li>
								<?php } ?>
                                
                                
                                <?php foreach ($certifcates as $item) { ?>
                                    <li>
                                        <h2><?php  echo $item->name ; ?></h2>
                                        <?php if( ( !(empty($item->institute))) || !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
                                        <ul>
                                            <li>
                                                <span><?php  echo $item->institute ; ?> <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?>
             
				<?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?>
              
				<?php } ?></span>

                                            </li>
                                        </ul>
                                      <?php } ?>  
                                    </li>
								<?php } ?>
                                    
                                </ul>
                            </div>
                            <?php } ?>
                            <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>
                            <div class="tab-pane col-md-12" id="exp">
                            <?php if (!(empty($user_info[0]->exp_no))){ ?>
                                <span class="pane-lg-head"><?php echo lang('no_exp'); ?> : <?php echo $user_info[0]->exp_no ?></span>
                            <?php } ?>    
                                <ul class="outter-ul">
                                <?php foreach ($experience as $item) { ?>    
                                    <li>
                                        <h2><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
       
			<?php } ?></h2>
                                        <ul>
                                            <li>
                                                <span><?php if( !(empty($item->company)))  echo $item->company ; ?> <?php if( !(empty($item->name)))  echo "(".$item->name .")" ; ?></span>
                                                <?php  if(!empty($item->role)){ ?>  
                                                <ul class="dashed-ul">
                                                    <?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
                                                    <li><?php echo $r_item ; ?></li>
                                                    <?php } ?>
                                                </ul>
                                                <?php } ?>

                                            </li>
                                        </ul>
                                        <p class="exp-desc"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
                                    </li>
                                    <?php } ?>
                                    
                                    <?php foreach ($projects as $item) { ?>    
                                    <li>
                                        <h2><?php if( !(empty($item->name)))  echo $item->name  ; ?></h2>
                                        <?php if( !(empty($item->link))){ ?>
                                        <ul>
                                            <li>
                                                <span><a style="color:#333333;" href="<?php echo $item->link ; ?>"><?php echo $item->link ; ?></a> </span>

                                            </li>
                                        </ul>
                                        <?php } ?>
                                        <p class="exp-desc"><?php if( !(empty($item->description)))  echo $item->description ; ?></p>
                                    </li>
                                    <?php } ?>

                                </ul>
                            </div>
                            <?php } ?>
                             <?php if( !(empty($langs))){ ?> 
                            <div class="tab-pane col-md-12" id="lang">
                                <ul class="outter-ul">
                                <?php foreach ($langs as $item) {  ?>
                                    <li>
                                        <h2><?php echo lang($item->name); ?></h2>
                                        <ul>
                                            <li><?php if($item->grade=='p') echo lang('poor');
										elseif($item->grade=='f') echo lang('fair');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='v') echo lang('very_good');
										elseif($item->grade=='fl') echo lang('fluent');
										elseif($item->grade=='t') echo lang('tongue'); ?></li>
                                        </ul>
                                    </li>
								<?php } ?>
                                    
                                </ul>
                            </div>
                            <?php } ?>
                            <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?>  
                            <div class="tab-pane col-md-12" id="skills">
                            <?php if( !(empty($skills))){ ?> 
                                <div>
                                    <h1 class="pane-lg-head"><?php echo strtoupper(lang("skills")); ?></h1>
                                    <ul> 
                                    <?php foreach ($skills as $item) { ?>
                                        <li>
                                            <h2 class="bold-h2"><?php echo $item->name ; ?></h2>
                                            <ul>
                                                <li><?php if($item->grade=='i') echo lang('interemdate');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='w') echo lang('weak'); ?></li>
                                            </ul>
                                        </li>
									<?php } ?>
                                    </ul>
                                </div>
                                <?php } ?>
                                <?php if( !(empty($qualifications))){ ?> 
                                <div>
                                    <h1 class="pane-lg-head"><?php echo strtoupper(lang("qualifications")); ?></h1>
                                    <ul>
                                        <?php foreach ($qualifications as $item) { ?>
                                        	<li><h2 class="bold-h2"><?php echo $item->name ; ?></h2></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                             <?php } ?>   
                            </div>
                            <?php } ?>
                            <?php if( !(empty($references))){ ?>
                            <div class="tab-pane col-md-12" id="ref">
                                <ul class="outter-ul">
                                <?php foreach ($references as $item) { ?>
                                    <li>
                                        <h2 class="bold-h2"><?php if( !(empty($item->name))) echo $item->name; ?></h2>
                                        <ul>
                                       <?php if( !(empty($item->mail))) { ?>
                                            <li><?php echo $item->mail ; ?></li>
                                       <?php } ?> 
                                       <?php if( !(empty($item->phone))){ ?>    
                                            <li><?php echo $item->phone ; ?></li>
                                       <?php } ?>     
                                        </ul>
                                    </li>
                                 <?php } ?>   
                                    
                                </ul>
                            </div>
                            <?php } ?>
                            <div class="tab-pane col-md-12" id="recom">
                            <?php if( !(empty($recommends))){ ?>
                                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">


                                    <!-- Wrapper for slides -->
                                    <div class="carousel-inner">
                                    <?php $nr_elm = count($recommends); 
									$i=1; ?>
                                    <div class="item active">
                                            <ul class="outter-ul recom-ul">
                                            <?php for ($i = 0; $i < $nr_elm; $i++) { ?>
                                        
                                                <li>
                                                    <span><?php echo date("d/m/Y" ,$recommends[$i]->date); ?></span>
                                                    <ul>
                                                        <li><?php echo $recommends[$i]->name; ?></li>
                                                        <li><?php echo $recommends[$i]->job; ?></li>
                                                    </ul>
                                                    <p><?php echo $recommends[$i]->recommend; ?></p>
                                                </li>
                                             <?php $col_to_add = ($i+1) % 2; if($i+1==$nr_elm || $col_to_add== 0){?>
                                             
                                             <?php if(($i+1) !=$nr_elm){  ?>
                                             </ul></div><div class="item">
                                            <ul class="outter-ul recom-ul">
                                             <?php  }} ?>
                                             
											 <?php  } ?>   
                                           </ul></div>
									
                                        
                                        <!--<div class="item">
                                            <ul class="outter-ul recom-ul">
                                                <li>
                                                    <span>14/13/2013</span>
                                                    <ul>
                                                        <li>Name of recommender</li>
                                                        <li>position</li>
                                                    </ul>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, a!</p>
                                                </li>
                                                <li>
                                                    <span>13/13/2013</span>
                                                    <ul>
                                                        <li>Name of recommender</li>
                                                        <li>position</li>
                                                    </ul>
                                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iusto, a!</p>
                                                </li>
                                            </ul>

                                        </div>-->
                                        
                                        

                                    </div>

                                    <!-- Controls -->
                                    <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                                        <span><i class="fa fa-caret-left"></i></span>
                                    </a>
                                    <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                                        <span><i class="fa fa-caret-right"></i></span>
                                    </a>
                                </div><!--carousel-->
                                <?php } ?>
                                <div class="row form-wrapper">
                                    <div class="btn-wrapper col-md-4 col-md-offset-4">
                                        <a class=" btn-black" id="show-form"><?php echo strtoupper(lang('leave_recommendation')); ?></a>
                                    </div>
                                    <div id="form-wrapper" class="col-sm-8 col-sm-offset-2 "  role="form">
                                        <div class="form-success" id="form-success" style="display:none;"><span id="span_success"> </span><i class="fa fa-check-circle-o"></i></div>

                                        <form class="recom-form">
                                        <input name="cv_lang" type="hidden" value="<?php echo $cv[0]->lang ; ?>">
                                        <input name="cv" type="hidden" value="<?php echo $cv[0]->id ; ?>">
                                        <input name="user_id" type="hidden" value="<?php echo $user_id ; ?>">
                                            <div id="error_message_name" class="error_message" style="display:none"><i class="fa fa-exclamation-triangle"></i><span id="span_name"> </span></div>
                                            <input class="form-control" type="text" placeholder="<?php echo lang('name') ?>" name="name" />
                                            <div id="error_message_job" class="error_message" style="display:none"><i class="fa fa-exclamation-triangle"></i><span id="span_job"> </span></div>
                                            <input class="form-control" type="text" placeholder="<?php echo lang('job') ?>" name="job" />
                                            <div id="error_message_recommend" class="error_message" style="display:none"><i class="fa fa-exclamation-triangle"></i><span id="span_recommend"> </span></div>
                                            <textarea class="form-control" rows="4" placeholder="<?php echo lang('recommend') ?>" name="recommend" ></textarea>
                                            <div class="col-sm-3 col-sm-offset-5  ">
                                                <input class="btn-submit " type="button" value="<?php echo lang('recommend') ?>" id="save_btn">
                                            </div>
                                        </form>
                                    </div><!--form-wrapper-->

                                </div>
                            </div><!--#recom-->
                        </div>
                    </div>
                </div>


            </div><!--container-->
            <div class="footer">
                <div class="hr-wrapper">
                    <hr class="bold-hr"/>
                    <hr class="thin-hr-left">
                </div>
                <div class="copyright">
                    <span>copyright@e3rfny.com </span>
                </div>
                <div class="hr-wrapper">
                    <hr class="bold-hr"/>
                    <hr class="thin-hr-right">
                </div>
            </div>
        </div><!--container-wrapper-->
        <script src="<?php echo base_url(); ?>styles/js/web3/jquery.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>styles/js/web3/tab.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>styles/js/web3/carousel.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>styles/js/web3/collapse.js" type="text/javascript"></script>
        <script src="<?php echo base_url(); ?>styles/js/web3/bootstrap-tabcollapse.js" type="text/javascript"></script>
        <script>
            $('#myTab a').click(function(e) {
                e.preventDefault()
                $(this).tab('show')
            })
        </script>
        <script>
             $('#myTab').tabCollapse();
        </script>
        <script>
            $(document).ready(function() {
              
                var NewHeight = $('#profile-img').width();
                $('#profile-img').css('height', NewHeight);
            });
			
			
			$('#save_btn').click(function () {
				
				dataString = $(".recom-form").serialize();
				
				var request2 = $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>profile/save",
                     data: dataString,
					 dataType : 'json',
                     success: function(result) {
					
						if(result=='<?php echo lang('done') ?>'){
							
							$('.error_message').hide();
							$('#form-success').show();
							$('#span_success').html("<?php echo lang('done') ?>");
							//setTimeout(function() { $("#form-success").fadeOut(1500); }, 5000);
							
							
						}else{
							$('#form-success').hide();
							$('.error_message').hide();
							if(result['name']){
								$('#error_message_name').show();
								$('#span_name').html(result['name']);
							}
							if(result['job']){	
								$('#error_message_job').show();
								$('#span_job').html(result['job']);
							}
							if(result['recommend']){
								$('#error_message_recommend').show();
								$('#span_recommend').html(result['recommend']);
							}
							
						}
						
					},
											
				});
				
   			 });

        </script>
       
        
    </body>
</html>
