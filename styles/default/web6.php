<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <title><?php echo $user_info[0]->user_cv_name ; ?></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
        <meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
        <meta property="og:site_name" content="e3rafny" />
        <meta name="twitter:card" value="summary" />
        <meta name="twitter:site" value="e3rafny" />
        <meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
        <meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
        <meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
        <meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
        <meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
        <meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">

        <link href="<?php echo base_url("styles/css/web6/normalize.css") ?>" rel="stylesheet">
        <link href="http://netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
        
        <link type="text/css" rel="stylesheet" href="<?php echo base_url("styles/css/web6/tab/css/easy-responsive-tabs_".$cv[0]->lang.".css") ?>" />
        <link href="<?php echo base_url("styles/css/web6/web_style6_".$cv[0]->lang.".css") ?>" rel="stylesheet">
       <!-- <link href="tab/css/easy-responsive-tabs_ltr.css" rel="stylesheet" type="text/css"/>
        <link href="css/stye-ltr.css" rel="stylesheet" type="text/css"/>-->

    </head>
    <body>
        <div class="head-wrapper">
            <div class="name-wrapper">
                <h1 class="name"><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
                <h1 class="position"><?php echo $user_info[0]->title ; ?></h1>
            </div>
            
            <div class="profile-img-container">
            <?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>
                <img class="profile-img" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>"/> <?php } ?>
            </div>
           
        </div>
        <div class="cv-content">
            <!--vertical Tabs-->
            <div id="verticalTab">
                <ul class="resp-tabs-list tab-head">

                    <li class="objective-head"><?php echo lang("objective") ;?></li>
                    <li class="personal-info-head"><?php echo lang("personalinfo") ; ?></li>
                    <?php if( !(empty($education))){ ?>
                    	<li class="edu-head"><?php echo lang("education") ; ?></li>
                    <?php } ?>
                    <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
                    	<li class="cer-head"><?php if( !(empty($courses))) { echo lang("courses") ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo lang("certifcates") ; ?></li>
                    <?php } ?> 
                    <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>     
                    	<li class="exp-head"><?php if( !(empty($experience))) { echo lang("experience") ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo lang("projects") ; ?></li>
                    <?php } ?>    
                    <?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
                    	<li class="skills-head"><?php echo lang("skils") ; ?></li>
                    <?php } ?>    
                    <?php if( !(empty($langs))){ ?> 
                    	<li class="lang-head"><?php echo lang("langs") ; ?> </li>
                    <?php } ?>    
                    <?php if( !(empty($references))){ ?>
                    	<li class="ref-head"><?php echo lang("references") ; ?></li>
                    <?php } ?>
                    <li class="recom-head"><?php echo lang('recommendation'); ?></li>

                </ul>
                <div class="rope">

                    <img id="front-rope" src="<?php echo base_url(); ?>assets/images/web6/front-rope.png" alt=""/>
                    <img id="back-rope" src="<?php echo base_url(); ?>assets/images/web6/back-rope.png" alt=""/>
                    <div class="big-circle">
                    <div class="sm-circle"></div>
                    <div class="contact-info"> 
                    <?php foreach ($phones as $item) {?>
                        <div class="contact-row">

                            <span class="phone"><?php echo $item->code.$item->number ; ?></span>

                        </div>
                        <?php } ?>
                        <div class="contact-row">

                            <span class="mail"><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></span>

                        </div>
                        <?php if($user_info[0]->adress1 !=''){ ?>
                            <div class="contact-row">
    
                                <span class="add"><?php echo $user_info[0]->adress1; ?></span>
    
                            </div>
                        <?php } ?>    
                        <?php if($user_info[0]->adress2 !=''){ ?>
                            <div class="contact-row">
    
                                <span class="add"><?php echo $user_info[0]->adress2; ?></span>
    
                            </div>
                        <?php } ?>    
                    </div> 
                </div>
                </div>

                <div class="resp-tabs-container">
                    <div><!--tab-start-->
                        <h3 class="dot"><?php echo lang("objective") ;?></h3>
                        <p><?php echo $cv[0]->objective ;?></p>

                    </div>
                    <div><!--tab-start-->
                    <?php if($user_info[0]->nationality !=''){ ?>
                        <div class="p-info-row"><h3><?php echo lang("nationality")."&nbsp;".":"."&nbsp;"; ?></h3><span><?php echo ucfirst($user_info[0]->nationality); ?></span></div>
                    <?php } ?>
                    <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?>       
                        <div class="p-info-row"><h3><?php echo lang("birth_date")."&nbsp;".":"."&nbsp;"; ?></h3><span><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></div>
                    <?php } ?>   
                    <?php if($user_info[0]->marital_status !='n'){ 
						if($user_info[0]->marital_status =='s'){
							$m_status=lang("single");
						}elseif($user_info[0]->marital_status =='m'){
							$m_status=lang("married");
						}elseif($user_info[0]->marital_status =='d'){
							$m_status=lang("divercoed");
						}
					 ?>     
                        <div class="p-info-row"><h3><?php echo lang("marital_rstatus")."&nbsp;".":"."&nbsp;"; ?></h3><span><?php echo $m_status ; ?></span></div>
                     <?php } ?>   
                     <?php if($user_info[0]->gender !='n'){ 
							if($user_info[0]->gender =='m'){
								$gender=lang("male");
							}elseif($user_info[0]->gender =='f'){
								$gender=lang("female");
							}
					 ?>    
                        <div class="p-info-row"><h3><?php echo lang("gender")."&nbsp;".":"."&nbsp;"; ?></h3><span><?php echo $gender ; ?></span></div>
                     <?php } ?>   
                     <?php if($user_info[0]->license !='no'){ 
							 if($user_info[0]->license =='y'){
								$License=lang("yes");
							}elseif($user_info[0]->license =='n'){
								$License=lang("no");
							}
					?>   
                        <div class="p-info-row"><h3><?php echo lang("License")."&nbsp;".":"."&nbsp;"; ?></h3><span><?php echo $License ; ?></span></div>
                    <?php } ?>   
                    <?php if($user_info[0]->military !='n'){ 
							if($user_info[0]->military =='e'){
								$military=lang("exemption");
							}elseif($user_info[0]->military =='c'){
								$military=lang("Complete_service");
							}elseif($user_info[0]->military =='p'){
								$military=lang("Postponed");
							}elseif($user_info[0]->military =='s'){
								$military=lang("Currently_serving");
							}elseif($user_info[0]->military =='d'){
								$military=lang("Doesnt_apply");
							}	 
					 ?>   
                        <div class="p-info-row"><h3><?php echo lang("military")."&nbsp;".":"."&nbsp;"; ?></h3><span><?php echo $military; ?></span></div>
                     <?php } ?>   
                        <div style="clear:both;"></div>
                    </div>
					<?php if( !(empty($education))){ ?>
                    <div><!--tab-start-->
                    <?php foreach ($education as $item) { ?>
                        <div class="edu-row dot">
                            <div class="row-wrapper">
                                <span><?php if( !(empty($item->type)))  echo $item->type ; ?></span>
                                <span><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo "(". $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year .")" ; ?>
          
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo "(". $item->from_month."/".$item->from_year." - ".lang($item->to_month).")" ; ?>
        
	<?php	} ?></span>
                                <span><?php if( !(empty($item->name)))  echo $item->name ; ?></span>
                                <span><?php if( !(empty($item->desc)))  echo $item->desc ; ?></span>
                            </div>  

                        </div><!--edu-row-->
                    <?php } ?>    

                    </div>
                    <?php } ?>
                    <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?> 
                    <div><!--tab-start-->
                    <?php foreach ($courses as $item) { ?>
                        <div class="edu-row dot">
                            <div class="row-wrapper">
                            <?php if( !(empty($item->name))){ ?>
                                <span><?php  echo $item->name ; ?></span>
                            <?php } ?>  
                            <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?><span><?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?></span><?php } ?>
                			<?php if( !(empty($item->institute))){ ?>
                                <span><?php echo $item->institute ; ?></span>
                            <?php } ?>    
                            </div>  
                        </div><!--edu-row-->
                   <?php } ?>     
                   
                   <?php foreach ($certifcates as $item) { ?>
                        <div class="edu-row dot">
                            <div class="row-wrapper">
                            <?php if( !(empty($item->name))){ ?>
                                <span><?php  echo $item->name ; ?></span>
                            <?php } ?>  
                            <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){	?><span><?php echo "(".$item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year.")" ; ?></span><?php } ?>
                			<?php if( !(empty($item->institute))){ ?>
                                <span><?php echo $item->institute ; ?></span>
                            <?php } ?>    
                            </div>  
                        </div><!--edu-row-->
                   <?php } ?>     
                       
                    </div>
                    <?php } ?>
                    <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>  
                    <div><!--tab-start-->
                    <?php if (!(empty($user_info[0]->exp_no))){ ?>
                        <div class="exp-head">
                            <h2><?php echo lang('no_exp'); ?> : <span><?php echo $user_info[0]->exp_no ?></span></h2>
                        </div>
					<?php } ?>
                    <?php foreach ($experience as $item) { ?>    
                        <div class="exp-row dot">
                            <div class="row-wrapper">
                            <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
                                <span class="date dash"><?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
                                    <?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
                                        <?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
</span><?php } ?>
								<?php if( ( !(empty($item->name))) || ( !(empty($item->company)))){ ?>
                                	<span class="tag"><?php echo $item->name  ; ?><?php if(!empty($item->name) && !empty($item->company)){ ?> .. <?php } ?> <?php echo $item->company ; ?></span>
                                <?php } ?>
                                <?php  if(!empty($item->role)){ ?>  
                                    <ul>
                                        <?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
                                        	<li class="tag"><?php echo $r_item ; ?></li>
                                        <?php } ?>
                                    </ul>
                              <?php } ?>  
                              <?php if(!(empty($item->description))){ ?>    
                                <p class="tag"><?php echo $item->description ; ?></p>
                              <?php } ?>  
                            </div><!--row-wrapper-->
                        </div><!--exp-row-->
                    <?php } ?>    
                    
                    <?php foreach ($projects as $item) { ?>    
                        <div class="exp-row dot">
                            <div class="row-wrapper">
                            <?php if(!(empty($item->name))){ ?>
                                <span class="date dash"><?php echo $item->name ; ?>
                            </span><?php } ?>
                            <?php if(!(empty($item->link))){ ?>
                                	<span class="tag"><a style="color:#666666;font-size:18px;" href="<?php echo $item->link  ; ?>"><?php echo $item->link  ; ?></a></span>
                                <?php } ?>
                                <?php if(!(empty($item->description))){ ?>
                                	<p class="tag"><?php echo $item->description ; ?></p>
                                <?php } ?>
                            </div><!--row-wrapper-->
                        </div><!--exp-row-->
                    <?php } ?>    
                    
                    </div>
                    <?php } ?>
					<?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
                    <div><!--tab-start-->
                    <?php foreach ($skills as $item) { ?>
                        <div class="skill-row">
                            <span class="dash skill-name"><?php echo $item->name ; ?></span>
                            <div class="progress my-progress-style">
                            <?php if($item->grade=='i') $perc=50;
								elseif($item->grade=='g') $perc=100;
								elseif($item->grade=='w') $perc=20; ?>
                                <span style="width: <?php echo $perc; ?>%;"></span>
                            </div><!--progress-->
                        </div><!--skill-row-->
                    <?php } ?>    
                    <?php foreach ($qualifications as $item) { ?>
                        <div class="skill-row">
                            <span class="dash skill-name"><?php echo $item->name ; ?></span>
                            
                        </div><!--skill-row-->
                    <?php } ?>    
                    </div>
                    <?php } ?>
                    <?php if( !(empty($langs))){ ?> 
                    <div><!--tab-start-->
                    <?php foreach ($langs as $item) {  ?>
                        <div class="lang-row dot">
                            <span class="dash lang-name"><?php echo lang($item->name); ?></span>
                            <div class="progress my-progress-style">
                            <?php if($item->grade=='p') $percnt=20;
								elseif($item->grade=='f') $percnt=40;
								elseif($item->grade=='g') $percnt=60;
								elseif($item->grade=='v') $percnt=80;
								elseif($item->grade=='t') $percnt=100;
								elseif($item->grade=='fl') $percnt=90;
						?>
                                <span style="width: <?php echo $percnt; ?>%;"></span>
                            </div><!--progress-->
                        </div><!--lang-row-->
                    <?php } ?>    
                    </div>
                    <?php } ?>
					<?php if( !(empty($references))){ ?>
                    <div><!--tab-start-->
                    <?php foreach ($references as $item) { ?>
                        <div class="ref-row dot">
                            <div class="row-wrapper">
                            <?php if( !(empty($item->name))){ ?>
                                <span class="dash"><?php echo $item->name; ?></span>
                            <?php } ?>    
                                <span class="ref-phone"><?php if( !(empty($item->phone))) echo $item->phone ; ?></span>
                                <span><?php if( !(empty($item->mail))) echo $item->mail ; ?></span>


                            </div>
                        </div><!--ref-row-->
                    <?php } ?>    
                    </div>
					<?php } ?>
                    <div><!--tab-start-->
                    <?php foreach ($recommends as $item) { ?>
                        <div class="recom-row dot">
                        
                            <div class="row-wrapper">
                                <span class="bold"><?php echo date("d/m/Y" ,$item->date); ?></span>
                                <span class="dash"><?php echo $item->name; ?></span>
                                <span class="recom-pos"><?php echo $item->job; ?></span>
                                <p class="recom-text"><?php echo $item->recommend; ?></p>
                            </div>
                       
                        </div><!--recom-row-->
                         <?php } ?>    
                        <div class="btn-wrapper">
                            <a class="btn" id="show-form"><?php echo strtoupper(lang('leave_recommendation')); ?></a>

                            <div id="form-wrapper">
                                <span class="form-success" id="form-success" style="display:none"><i class="fa fa-check-circle-o"></i><span id="span_success"> </span></span>
                                <form class="recom-form">
                                	<input name="cv_lang" type="hidden" value="<?php echo $cv[0]->lang ; ?>">
                                	<input name="cv" type="hidden" value="<?php echo $cv[0]->id ; ?>">
                                    <input name="user_id" type="hidden" value="<?php echo $user_id ; ?>">
                                        <span class="error_message" id="error_message_name" style="display:none"> <i class="fa fa-exclamation-triangle"></i><span id="span_name"> </span></span>
                                    <input type="text" placeholder="<?php echo lang('name') ?>" name="name" />
                                    <span class="error_message" id="error_message_job" style="display:none"><i class="fa fa-exclamation-triangle"></i><span id="span_job"> </span></span>
                                    <input type="text" placeholder="<?php echo lang('job') ?>" name="job" />
                                    <span class="error_message" id="error_message_recommend" style="display:none"><i class="fa fa-exclamation-triangle"></i><span id="span_recommend"> </span></span>
                                    <textarea rows="4" placeholder="<?php echo lang('recommend') ?>" name="recommend" ></textarea>
                                    <input class="btn-submit" type="button" value="<?php echo lang('recommend') ?>" id="save_btn">
                                </form>
                            </div><!--form-wrapper-->
                        </div> <!--btn-wrapper-->

                    </div><!--end tab-->

                </div>

                

            </div><!--vertical tab-->
            <div class="social-wrapper">
                <div class="social">
                <?php foreach ($social as $item) { ?>
                    <a href="<?php echo $item->link ; ?>" id="<?php if($item->name=='facebook'){echo 'fb-icon';}elseif($item->name=='twitter'){echo 't-icon';}elseif($item->name=='linkledin'){echo 'ln-icon';}elseif($item->name=='google'){echo 'g-icon';}elseif($item->name=='behance'){echo 'be-icon';} ?>"><i class="<?php if($item->name=='facebook'){echo 'fa fa-facebook';}elseif($item->name=='twitter'){echo 'fa fa-twitter';}elseif($item->name=='linkledin'){echo 'fa fa-linkedin';}elseif($item->name=='google'){echo 'fa fa-google';}elseif($item->name=='behance'){echo 'fa fa-behance';} ?>"></i></a>
                <?php } ?>    
                    
                </div><!---social-->
            </div>

        </div><!--cv-content-->

        <div class="copyright"><span>copyright&#169;e3rfny.com</span></div>
        <div class="footer"></div>

        <script src="<?php echo base_url(); ?>styles/js/jquery.js"></script>
        <script src="<?php echo base_url("styles/css/web6/tab/js/easyResponsiveTabs.js") ?>" type="text/javascript"></script>
        
        <script type="text/javascript">
            $(document).ready(function() {

                $('#verticalTab').easyResponsiveTabs({
                    type: 'vertical',
                    width: 'auto',
                    fit: true,
                    closed: 'accordion'// Start closed if in accordion view

                });
            });
        </script>
        <script>
            $(document).ready(function() {
                $('#show-form').click(function() {
                    $('#form-wrapper').slideToggle("fast");
                });
            });
        </script>

        <script>
            $(document).ready(function() {

                var NewHeight = $('#profile-img').width();
                $('#profile-img').css('height', NewHeight);
            });
			
			$('#save_btn').click(function () {
				
				dataString = $(".recom-form").serialize();
				
				var request2 = $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>profile/save",
                     data: dataString,
					 dataType : 'json',
                     success: function(result) {
					
						if(result=='<?php echo lang('done') ?>'){
							
							$('.error_message').hide();
							$('#form-success').show();
							$('#span_success').html("<?php echo lang('done') ?>");
							//setTimeout(function() { $("#form-success").fadeOut(1500); }, 5000);
							
							
						}else{
							$('#form-success').hide();
							$('.error_message').hide();
							if(result['name']){
								$('#error_message_name').show();
								$('#span_name').html(result['name']);
							}
							if(result['job']){	
								$('#error_message_job').show();
								$('#span_job').html(result['job']);
							}
							if(result['recommend']){
								$('#error_message_recommend').show();
								$('#span_recommend').html(result['recommend']);
							}
							
						}
						
					},
											
				});
				
   			 });

        </script>


    </body>
</html>
