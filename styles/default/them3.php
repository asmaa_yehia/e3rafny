<link href="<?php echo base_url("styles/css/style3_" . $cv->lang . ".css") ?>" rel="stylesheet">
<body> 



    <div class="head-wrapper" >
        <div class="cv-head">
            <div>
                <div class="cv-name" >
                    <h1><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
                    <h3><?php echo $user_info[0]->title; ?></h3>

                </div>
                <?php
                if (!(empty($phones)))
                {
                    ?>
                    <div class="phone-info" >
                        <h4><?php echo lang("phones"); ?>:</h4>
                        <?php $nr_elm = count($phones); ?>
                        <table><tr>
                                <?php
                                $nr_col = 2;
                                if ($nr_elm > 0)
                                {

                                    for ($i = 0; $i < $nr_elm; $i++)
                                    {
                                        ?>
                                        <td><?php echo $phones[$i]->code . $phones[$i]->number; ?></td>
                                        <?php
                                        $col_to_add = ($i + 1) % $nr_col;
                                        if ($col_to_add == 0)
                                        {
                                            ?>
                                        </tr><tr>
                                            <?php
                                        }
                                    }
                                    ?>   
                                    <?php
                                    if ($col_to_add != 0)
                                    {
                                        ?><td colspan="<?php echo ($nr_col - $col_to_add) ?>">&nbsp;</td><?php
                                    }
                                }
                                ?>
                            </tr>    
                        </table>


                    </div>
                <?php } ?>
                <div class="email" >
                    <h4><?php echo lang("emaill"); ?>:</h4>
                    <span><?php
                        if (!empty($user_info[0]->alter_mail))
                            echo $user_info[0]->alter_mail;
                        else
                            echo $user->mail;
                        ?></span>
                </div>

            </div>
            <div style="width:100%; clear:both; ">
                <div class="adds" >
                    <?php
                    if ($user_info[0]->adress1 != '')
                    {
                        ?>
                        <div class="address">
                            <h4><?php echo lang("address"); ?>:</h4>
                            <p> <?php echo $user_info[0]->adress1; ?>

                            </p>
                        </div>
                    <?php } ?>       
                    <?php
                    if ($user_info[0]->adress2 != '')
                    {
                        ?>   
                        <div class="address">
                            <h4><?php echo lang("another_address"); ?>:</h4>
                            <p>
                                <?php echo $user_info[0]->adress2; ?>
                            </p>
                        </div>
                    <?php } ?>
                </div>
                <div class="img-container">
                    <?php
                    if (!(empty($user_info[0]->image)) || ($user_info[0]->image != 0))
                    {
                        ?>
                        <img  src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image; ?>" />
                    <?php } ?>      
                </div>

            </div>
        </div><!--cv-head-->
        <?php
            if (!(empty($social)))
            { ?>
        <div style="width:100%;  ">
            <div class="social-wrap">
            <?php foreach ($social as $item) { ?>  
                <div class="social-row">
                    <div class="social-img"><img src="<?php echo base_url(); ?>assets/images/<?php if($item->name=='facebook'){echo 'fb_cv3.png';}elseif($item->name=='twitter'){echo 'twitter_cv3.png';}elseif($item->name=='linkledin'){echo 'linkedin_cv3.png';}elseif($item->name=='google'){echo 'gplus_cv3.png';}elseif($item->name=='behance'){echo 'behance_cv3.png';} ?>" /></div><div class="social-link"><a href="<?php echo $item->link; ?>"><?php echo $item->link; ?></a></div>
                </div><!--social-row-->
              <?php } ?>  
 </div>

        </div>
<?php } ?>


        <div class="cv-content" > 
            <hr/>
            <?php
            if (!(empty($cv->objective)))
            {
                ?>  
                <div class="bordered" style=" width:100%;">
                    <h4><?php echo strtoupper(lang("objective")); ?></h4>
                    <p>
                        <?php echo $cv->objective; ?></p>
                </div><!--borderd-->

                <hr/>
            <?php } ?>
            <div class="bordered" style=" width:100%;">
                <h4><?php echo strtoupper(lang("personal_info")); ?></h4>
                <div class="p-info">
                    <?php
                    if ($user_info[0]->nationality != '')
                    {
                        ?>
                        <div><span class="bold"><?php echo lang("nationality"); ?> : </span><?php echo $user_info[0]->nationality; ?></div>
                    <?php } ?>
                    <?php
                    if ($user_info[0]->day != '0' && $user_info[0]->month != '0' && $user_info[0]->year != '0')
                    {
                        ?>   
                        <div><span class="bold"><?php echo lang("birth_date"); ?> : </span><span><?php echo $user_info[0]->day . "/" . $user_info[0]->month . "/" . $user_info[0]->year; ?></span></div>
                    <?php } ?>
                    <?php
                    if ($user_info[0]->marital_status != 'n')
                    {
                        if ($user_info[0]->marital_status == 's')
                        {
                            $m_status = lang("single");
                        }
                        elseif ($user_info[0]->marital_status == 'm')
                        {
                            $m_status = lang("married");
                        }
                        elseif ($user_info[0]->marital_status == 'd')
                        {
                            $m_status = lang("divercoed");
                        }
                        ?>  
                        <div><span class="bold"><?php echo lang("marital_rstatus"); ?> : </span><span><?php echo $m_status; ?></span></div>
                    <?php } ?>
                    <?php
                    if ($user_info[0]->gender != 'n')
                    {
                        if ($user_info[0]->gender == 'm')
                        {
                            $gender = lang("male");
                        }
                        elseif ($user_info[0]->gender == 'f')
                        {
                            $gender = lang("female");
                        }
                        ?>    
                        <div><span class="bold"><?php echo lang("gender"); ?> : </span><span><?php echo $gender; ?></span></div>
                    <?php } ?>
                    <?php
                    if ($user_info[0]->military != 'n')
                    {
                        if ($user_info[0]->military == 'e')
                        {
                            $military = lang("exemption");
                        }
                        elseif ($user_info[0]->military == 'c')
                        {
                            $military = lang("Complete_service");
                        }
                        elseif ($user_info[0]->military == 'p')
                        {
                            $military = lang("Postponed");
                        }
                        elseif ($user_info[0]->military == 's')
                        {
                            $military = lang("Currently_serving");
                        }
                        elseif ($user_info[0]->military == 'd')
                        {
                            $military = lang("Doesnt_apply");
                        }
                        ?> 
                        <div><span class="bold"><?php echo lang("military"); ?> : </span><span><?php echo $military; ?></span></div>
                    <?php } ?>
                    <?php
                    if ($user_info[0]->license != 'no')
                    {
                        if ($user_info[0]->license == 'y')
                        {
                            $License = lang("yes");
                        }
                        elseif ($user_info[0]->license == 'n')
                        {
                            $License = lang("no");
                        }
                        ?>   
                        <div><span class="bold"><?php echo lang("License"); ?> : </span><span><?php echo $License; ?></span></div>
                    <?php } ?>
                </div>
            </div><!--borderd-->
            <hr/>
            <?php
            if (!(empty($education)))
            {
                ?>
                <div class="bordered" style=" width:100%;">
                     <div class="section-title">
                    <h4><?php echo strtoupper(lang("education")); ?></h4>
                    </div><!--section-title-->
                    <?php
                    foreach ($education as $item)
                    {
                        ?>
                        <div class="e-info">
                            <div class="date"><?php
                                if (!(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year)) && ($item->to_month != 'now'))
                                {
                                    ?>
                                    <div class="e_date">
                                        <?php echo "(" . $item->from_month . "/" . $item->from_year ."&nbsp;"."-"."&nbsp;". $item->to_month . "/" . $item->to_year . ")"; ?>
                                    </div>
                                    <?php
                                }
                                elseif (!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month == 'now')
                                {
                                    ?>
                                    <div class="e_date">
                                        <?php echo "(" . $item->from_month . "/" . $item->from_year ."&nbsp;"."-"."&nbsp;". lang($item->to_month) . ")"; ?>
                                    </div>
                                <?php } ?></div>
                            <div class="edu">
                                <div><h5><?php if (!(empty($item->name))) echo $item->name; ?></h5>
                                <h5><?php if (!(empty($item->type))) echo $item->type; ?></h5>
                                <?php if (!(empty($item->desc))) echo $item->desc; ?>
                                </div></div>
                        </div><!--e-info-->
                    <?php } ?>
                </div><!--borderd-->
                <hr/>
            <?php } ?>
            <?php
            if (!(empty($courses)) || !(empty($certifcates)))
            {
                ?> 
                <div class="bordered" style=" width:100%;">
                    <div class="courses-title">
                        <h4><?php
                            if (!(empty($courses)))
                            {
                                echo strtoupper(lang("courses"));
                            }
                            ?><?php
                            if (!(empty($courses)) && !(empty($certifcates)))
                            {
                                echo "&nbsp;".lang("and")."&nbsp;";
                            } if (!(empty($certifcates)))
                                echo strtoupper(lang("certifcates"));
                            ?></h4>
                    </div>
                    <?php
                    foreach ($courses as $item)
                    {
                        ?>
                        <div class="e-info">
                            <div class="date"><?php
                                if (!(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year)))
                                {
                                    ?>

                                    <?php echo "(" . $item->from_month . "/" . $item->from_year ."&nbsp;"."-"."&nbsp;". $item->to_month . "/" . $item->to_year . ")"; ?>

                                <?php } ?></div>
                            <div class="edu">
                                <div><h5><?php echo strtoupper($item->name); ?></h5>
                                    <?php if (!(empty($item->institute))) echo $item->institute; ?>
                                </div>
                            </div><!--edu-->
                        </div><!--e-info-->

                    <?php } ?>

                    <?php
                    foreach ($certifcates as $item)
                    {
                        ?>
                        <div class="e-info">
                            <div class="date"><?php
                                    if (!(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year)))
                                    {
                                        ?>

                                        <?php echo "(" . $item->from_month . "/" . $item->from_year ."&nbsp;"."-"."&nbsp;". $item->to_month . "/" . $item->to_year . ")"; ?>

                                    <?php } ?></div>
                            <div class="edu">
                                <div><h5><?php echo strtoupper($item->name); ?></h5>
                                    <?php if (!(empty($item->institute))) echo $item->institute; ?>
                                </div></div>
                        </div><!--e-info-->
                    <?php } ?>


                </div><!--borderd-->
                <hr/>
            <?php } ?>
            <?php
            if (!(empty($experience)) || !(empty($projects)))
            {
                ?>
                <div class="bordered exp-proj" style=" width:100%;">
                    <div class="section-title">
                        <h4><?php
                            if (!(empty($experience)))
                            {
                                echo strtoupper(lang("experience"));
                            }
                            ?><?php
                            if (!(empty($experience)) && !(empty($projects)))
                            {
                                echo "&nbsp;".lang("and")."&nbsp;";
                            } if (!(empty($projects)))
                                echo strtoupper(lang("projects"));
                            ?></h4><h5><?php
                            if (!(empty($user_info[0]->exp_no)))
                            {
                                echo lang('no_exp') ."&nbsp;". "(" . $user_info[0]->exp_no ."&nbsp;" . ")";
                            }
                            ?></h5>
                    </div><!--section-title-->
                    <?php
                    foreach ($experience as $item)
                    {
                        ?>
                        <div class="e-info">
                            <div class="date"><?php
                                if (!(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year)) && ($item->to_month != 'now'))
                                {
                                    ?>

                                    <?php echo "(" . $item->from_month . "/" . $item->from_year ."&nbsp;"."-"."&nbsp;". $item->to_month . "/" . $item->to_year . ")" . "<br>"; ?>

                                    <?php
                                }
                                elseif (!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month == 'now')
                                {
                                    ?>

                                    <?php echo "(" . $item->from_month . "/" . $item->from_year ."&nbsp;"."-"."&nbsp;". lang($item->to_month) . ")"; ?>

                                <?php } ?></div> 

                            <div class="exp-head">
                                <div><span><strong><?php if (!(empty($item->name))) echo $item->name; ?></strong>  <?php if (!(empty($item->company))) echo "(" . $item->company . ")"; ?></span> 
                                    <?php
                                    if (!empty($item->role))
                                    {
                                        ?>
                                        <div class="exp">
                                            <?php $roles=unserialize($item->role); foreach ($roles as $r_item) {    ?>
                                            <div class="role"><?php echo $r_item; ?></div>
                                            <?php }   ?>
                                        </div>
                                    <?php } ?>
                                    <div class="desc"><span><?php if (!(empty($item->description))) echo $item->description; ?></span></div>
                                </div>
                            </div><!--exp-head-->

                        </div><!--e-info-->
                    <?php } ?>

                    <?php
                    foreach ($projects as $item)
                    {
                        ?>
                        <div class="e-info">
                            <div class="date"></div> 
                            <div class="projects">
                                <h5><strong><?php if (!(empty($item->link)))  ?><a href="<?php echo $item->link; ?>"><?php echo $item->link; ?></a></strong>  <?php if (!(empty($item->name))) echo "(" . $item->name . ")"; ?></h5> 

                                <span class="exp-desc"><?php if (!(empty($item->description))) echo $item->description; ?></span> 
                            </div>
                        </div><!--e-info-->
                    <?php } ?>

                </div><!--borderd-->
                <hr/>
            <?php } ?>
            <?php
            if (!(empty($langs)))
            {
                ?> 
                <div class="bordered" style=" width:100%;">
                    <div class="section-title">
                    <h4><?php echo strtoupper(lang("langs")); ?></h4>
                   </div>
                    <div class="e-info">
                        <?php
                        foreach ($langs as $item)
                        {
                            ?>
                            <div><span><?php if (!(empty($item->name))) echo lang($item->name); ?></span><span><?php echo "&nbsp;"."-"."&nbsp;" ?></span><span><?php
                                    if ($item->grade == 'p')
                                        echo lang('poor');
                                    elseif ($item->grade == 'f')
                                        echo lang('fair');
                                    elseif ($item->grade == 'g')
                                        echo lang('good');
                                    elseif ($item->grade == 'v')
                                        echo lang('very_good');
                                    elseif ($item->grade == 'fl')
                                        echo lang('fluent');
                                    elseif ($item->grade == 't')
                                        echo lang('tongue');
                                    ?></span></div>
                        <?php } ?>
                    </div><!--e-info-->
                </div><!--borderd-->
                <hr/>
            <?php } ?>
            <?php
            if (!(empty($skills)))
            {
                ?> 
                <div class="bordered" style=" width:100%;">

                    <h4><?php echo strtoupper(lang("skills")); ?></h4>
                    <div class="t-skills">
                        <?php
                        foreach ($skills as $item)
                        {
                            ?>
                            <div class="t-skill">
                                <ul>
                                    <li><?php if (!(empty($item->name))) echo $item->name."&nbsp;"."-"."&nbsp;" ; ?><?php
                                        if ($item->grade == 'i')
                                            echo lang('interemdate');
                                        elseif ($item->grade == 'g')
                                            echo lang('good');
                                        elseif ($item->grade == 'w')
                                            echo lang('weak');
                                        ?></li>
                                </ul>
                            </div>
                        <?php } ?>    
                    </div>


                </div><!--borderd-->
                <hr/>
            <?php } ?>
            <?php
            if (!(empty($qualifications)))
            {
                ?>
                <div class="bordered" style=" width:100%;">

                    <h4><?php echo strtoupper(lang("qualifications")); ?></h4>
                    <div class="p-skills">
                        <?php
                        foreach ($qualifications as $item)
                        {
                            ?>
                            <div class="p-skill">
                                <ul>
                                    <li><?php echo $item->name; ?></li>
                                </ul>
                            </div>
                        <?php } ?>   


                    </div>


                </div><!--borderd-->
                <hr/>
            <?php } ?>
            <?php
            if (!(empty($references)))
            {
                ?>
                <div class="bordered" style=" width:100%;">
                    <div class="section-title">
                    <h4><?php echo strtoupper(lang("references")); ?></h4>
                    </div><!--section-title-->
                    <?php
                    foreach ($references as $item)
                    {
                        ?>
                        <div class="e-info">
                            <div class="ref-name">
                                <span><?php if (!(empty($item->name))) echo $item->name; ?></span>
                            </div> 
                            <div class="ref-data">
                                <?php
                                if (!(empty($item->mail)))
                                {
                                    ?> <div><?php echo $item->mail; ?></div> <?php } ?>
                                <?php
                                if (!(empty($item->phone)))
                                {
                                    ?>  <div><?php echo $item->phone; ?></div><?php } ?>

                            </div>
                        </div><!--e-info-->
                    <?php } ?>
                </div><!--borderd-->  
            <?php } ?> 


        </div><!--cv-content-->

    </div><!--head-wrapper-->
