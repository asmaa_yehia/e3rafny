<h1 class="blue-title"><?php echo lang('course_name'); ?></h1>
<br class="clr">
<form action="<?php echo site_url(); ?>/add_cv/update_course/<?php echo $cv_id; ?>" method="post" enctype="multipart/form-data">
    <div class="step4">
        <input type="hidden" value="<?php echo $percent; ?>" name="percent" />
        <input type="hidden" value="<?php echo count($items); ?>" name="course_count"  id="course_count"/>
        <input type="hidden" value="<?php echo count($certifcates); ?>" name="cer_count"  id="cer_count"/>
        <input type="hidden" name="cv_id" value="<?php echo $cv_id ?>" id="cv" />
        <div id="course">
            <div class="content">
                <?php if (count($items) == 0) { ?>
                    <div id="cont" class="courses">

                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    <label><?php echo lang('course_name'); ?>:</label>
                                    <input type="text" name="course[]" value=""/>
                                </td>
                                <td> 
                                    <label><?php echo lang('course_place'); ?>:</label><input type="text" name="institute[]" value="" />
                                </td>
                                <td>
                                    <label> <?php echo lang('from'); ?>:</label>
                                    <select name="from_month[]" class="small-select"> 
                                        <option value="" ><?php echo lang('month') ?> </option>
                                        <option value="01"><?php echo lang('January'); ?></option>
                                        <option value="02" ><?php echo lang('February'); ?></option>
                                        <option value="03" ><?php echo lang('March'); ?></option>
                                        <option value="04" ><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06" ><?php echo lang('June'); ?></option>
                                        <option value="07"><?php echo lang('July'); ?></option>
                                        <option value="08" ><?php echo lang('August'); ?></option>
                                        <option value="09" ><?php echo lang('September'); ?></option>
                                        <option value="10" ><?php echo lang('October'); ?></option>
                                        <option value="11" ><?php echo lang('November'); ?></option>
                                        <option value="12" ><?php echo lang('December'); ?></option>
                                    </select>
                                    <select name="from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>" ><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                </td>
                                <td>
                                    <label> <?php echo lang('to'); ?>:</label><select name="to_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>"><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>

                                    <select name="to_month[]" class="small-select">
                                        <option value="" ><?php echo lang('month') ?> </option>
                                        <option value="01" ><?php echo lang('January'); ?></option>
                                        <option value="02" ><?php echo lang('February'); ?></option>
                                        <option value="03"><?php echo lang('March'); ?></option>
                                        <option value="04" ><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06"><?php echo lang('June'); ?></option>
                                        <option value="07" ><?php echo lang('July'); ?></option>
                                        <option value="08" ><?php echo lang('August'); ?></option>
                                        <option value="09" ><?php echo lang('September'); ?></option>
                                        <option value="10"><?php echo lang('October'); ?></option>
                                        <option value="11"><?php echo lang('November'); ?></option>
                                        <option value="12" ><?php echo lang('December'); ?></option>
                                    </select>
                                </td>
                            </tr>
                        </table>

                    </div><!--end of cont-->

                <?php } ?>
                <?php foreach ($items as $key => $item) { ?>

                    <div id="cont<?php echo $key; ?>" class="courses-added">
                        <input type="hidden" name="id[]" value="<?php echo $item->id; ?>" id="id<?php echo $key; ?>"  />
                        <div class="courses">
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><label><?php echo lang('course_name'); ?>:</label><input type="text" name="course[]" value="<?php echo $item->name; ?>"/></td>
                                    <td><label><?php echo lang('course_place'); ?>:</label>   <input type="text" name="institute[]" value="<?php echo $item->institute; ?>" /></td>
                                    <td><label> <?php echo lang('from'); ?>:</label>
                                        <select name="from_month[]" class="small-select"> 
                                            <option value="" ><?php echo lang('month') ?> </option>
                                            <option value="01" <?php
                                            if ($item->from_month == "01") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('January'); ?></option>
                                            <option value="02" <?php
                                            if ($item->from_month == "02") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('February'); ?></option>
                                            <option value="03" <?php
                                            if ($item->from_month == "03") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('March'); ?></option>
                                            <option value="04" <?php
                                            if ($item->from_month == "04") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('April'); ?></option>
                                            <option value="05" <?php
                                            if ($item->from_month == "05") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('May'); ?></option>
                                            <option value="06" <?php
                                            if ($item->from_month == "06") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('June'); ?></option>
                                            <option value="07" <?php
                                            if ($item->from_month == "07") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('July'); ?></option>
                                            <option value="08" <?php
                                            if ($item->from_month == "08") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('August'); ?></option>
                                            <option value="09" <?php
                                            if ($item->from_month == "09") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('September'); ?></option>
                                            <option value="10" <?php
                                            if ($item->from_month == "10") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('October'); ?></option>
                                            <option value="11" <?php
                                            if ($item->from_month == "11") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('November'); ?></option>
                                            <option value="12" <?php
                                            if ($item->from_month == "12") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('December'); ?></option>
                                        </select>
                                        <select name="from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                            <?php
                                            for ($y = 1975; $y <= year; $y++) {
                                                ?>
                                                <option value="<?php echo $y; ?>"<?php if ($item->from_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>

                                    <td>
                                        <label>   <?php echo lang('to'); ?>:</label>

                                        <select name="to_month[]" class="small-select">
                                            <option value="" ><?php echo lang('month') ?> </option>
                                            <option value="01" <?php
                                            if ($item->to_month == "01") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('January'); ?></option>
                                            <option value="02" <?php
                                            if ($item->to_month == "02") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('February'); ?></option>
                                            <option value="03" <?php
                                            if ($item->to_month == "03") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('March'); ?></option>
                                            <option value="04" <?php
                                            if ($item->to_month == "04") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('April'); ?></option>
                                            <option value="05" <?php
                                            if ($item->to_month == "05") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('May'); ?></option>
                                            <option value="06" <?php
                                            if ($item->to_month == "06") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('June'); ?></option>
                                            <option value="07" <?php
                                            if ($item->to_month == "07") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('July'); ?></option>
                                            <option value="08" <?php
                                            if ($item->to_month == "08") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('August'); ?></option>
                                            <option value="09" <?php
                                            if ($item->to_month == "09") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('September'); ?></option>
                                            <option value="10" <?php
                                            if ($item->to_month == "10") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('October'); ?></option>
                                            <option value="11" <?php
                                            if ($item->to_month == "11") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('November'); ?></option>
                                            <option value="12" <?php
                                            if ($item->to_month == "12") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('December'); ?></option>
                                        </select>
                                        <select name="to_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                            <?php
                                            for ($y = 1975; $y <= year; $y++) {
                                                ?>
                                                <option value="<?php echo $y; ?>"<?php if ($item->to_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>


                            </table>
                        </div>
                        <?php if ($key > 0) { ?>
                            <a href="javascript:void(0);" onClick="del_unit(<?php echo $key; ?>)" class="delete"></a>
                        <?php } ?>
                    </div><!--end of cont-->

                <?php } ?>

            </div><!--end of content-->
            <br class="clr">
            <a href="javascript:void(0);" onClick="add_unit()" class="add"></a>
            <!-- end of courses-------->
        </div>

        <br class="clr">
        <hr class="sep">
        <br class="clr">
        <div id="certif">
            <div class="content">
                <h1 class="blue-title"><?php echo lang('your_certif'); ?></h1>
                <br class="clr">
                <?php if (count($certifcates) == 0) { ?>
                    <div id="cont" class="courses" >

                        <table cellpadding="0" cellspacing="0">
                            <tr>
                                <td><label><?php echo lang('cetifi_name'); ?></label><input type="text" name="certf_course[]" value=""/></td>
                                <td><label><?php echo lang('cetifi_place'); ?></label><input type="text" name="certf_institute[]" value="" /></td>
                                <td><label> <?php echo lang('from'); ?>:</label>
                                    <select name="certf_from_month[]" class="small-select">
                                        <option value="" ><?php echo lang('month') ?> </option> 
                                        <option value="01" ><?php echo lang('January'); ?></option>
                                        <option value="02" ><?php echo lang('February'); ?></option>
                                        <option value="03" ><?php echo lang('March'); ?></option>
                                        <option value="04"><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06"><?php echo lang('June'); ?></option>
                                        <option value="07" ><?php echo lang('July'); ?></option>
                                        <option value="08" ><?php echo lang('August'); ?></option>
                                        <option value="09"><?php echo lang('September'); ?></option>
                                        <option value="10" ><?php echo lang('October'); ?></option>
                                        <option value="11" ><?php echo lang('November'); ?></option>
                                        <option value="12"><?php echo lang('December'); ?></option>
                                    </select>
                                    <select name="certf_from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>" ><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>

                                <td>
                                    <label>  <?php echo lang('to'); ?>:</label>

                                    <select name="certf_to_month[]" class="small-select">
                                        <option value="" ><?php echo lang('month') ?> </option>
                                        <option value="01" ><?php echo lang('January'); ?></option>
                                        <option value="02" ><?php echo lang('February'); ?></option>
                                        <option value="03" ><?php echo lang('March'); ?></option>
                                        <option value="04" ><?php echo lang('April'); ?></option>
                                        <option value="05" ><?php echo lang('May'); ?></option>
                                        <option value="06" ><?php echo lang('June'); ?></option>
                                        <option value="07" ><?php echo lang('July'); ?></option>
                                        <option value="08" ><?php echo lang('August'); ?></option>
                                        <option value="09" ><?php echo lang('September'); ?></option>
                                        <option value="10" ><?php echo lang('October'); ?></option>
                                        <option value="11" ><?php echo lang('November'); ?></option>
                                        <option value="12" ><?php echo lang('December'); ?></option>
                                    </select>
                                    <select name="certf_to_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                        <?php
                                        for ($y = 1975; $y <= year; $y++) {
                                            ?>
                                            <option value="<?php echo $y; ?>" ><?php echo $y; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <?php //echo lang('desc');?>
                       <!-- <textarea name="certf_desc[]"><?php //echo $item->description;           ?></textarea>-->

                    </div><!-- end of cont 0-->
                <?php } ?>
                <?php foreach ($certifcates as $key => $item) { ?>

                    <div id="cont<?php echo $key; ?>" class="courses" >
                        <div class="courses-added">
                            <input type="hidden" name="cv_id" value="<?php echo $item->cv_id ?>" id="cv" />
                            <input type="hidden" name="certf_id[]" value="<?php echo $item->id; ?>" id="id<?php echo $key; ?>"  />
                            <table cellpadding="0" cellspacing="0">
                                <tr>
                                    <td><label><?php echo lang('cetifi_name'); ?>:</label><input type="text" name="certf_course[]" value="<?php echo $item->name; ?>"/></td>
                                    <td><label><?php echo lang('cetifi_place'); ?>:</label> <input type="text" name="certf_institute[]" value="<?php echo $item->institute; ?>" /></td>
                                    <td><label> <?php echo lang('from'); ?>:</label>
                                        <select name="certf_from_month[]" class="small-select"> 
                                            <option value="" ><?php echo lang('month') ?> </option>
                                            <option value="01" <?php
                                            if ($item->from_month == "01") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('January'); ?></option>
                                            <option value="02" <?php
                                            if ($item->from_month == "02") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('February'); ?></option>
                                            <option value="03" <?php
                                            if ($item->from_month == "03") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('March'); ?></option>
                                            <option value="04" <?php
                                            if ($item->from_month == "04") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('April'); ?></option>
                                            <option value="05" <?php
                                            if ($item->from_month == "05") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('May'); ?></option>
                                            <option value="06" <?php
                                            if ($item->from_month == "06") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('June'); ?></option>
                                            <option value="07" <?php
                                            if ($item->from_month == "07") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('July'); ?></option>
                                            <option value="08" <?php
                                            if ($item->from_month == "08") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('August'); ?></option>
                                            <option value="09" <?php
                                            if ($item->from_month == "09") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('September'); ?></option>
                                            <option value="10" <?php
                                            if ($item->from_month == "10") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('October'); ?></option>
                                            <option value="11" <?php
                                            if ($item->from_month == "11") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('November'); ?></option>
                                            <option value="12" <?php
                                            if ($item->from_month == "12") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('December'); ?></option>
                                        </select>
                                        <select name="certf_from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>
                                            <?php
                                            for ($y = 1975; $y <= year; $y++) {
                                                ?>
                                                <option value="<?php echo $y; ?>"<?php if ($item->from_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <label> <?php echo lang('to'); ?>:</label>
                                        <select name="certf_to_month[]" class="small-select">
                                            <option value="" ><?php echo lang('month') ?> </option>
                                            <option value="01" <?php
                                            if ($item->to_month == "01") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('January'); ?></option>
                                            <option value="02" <?php
                                            if ($item->to_month == "02") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('February'); ?></option>
                                            <option value="03" <?php
                                            if ($item->to_month == "03") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('March'); ?></option>
                                            <option value="04" <?php
                                            if ($item->to_month == "04") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('April'); ?></option>
                                            <option value="05" <?php
                                            if ($item->to_month == "05") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('May'); ?></option>
                                            <option value="06" <?php
                                            if ($item->to_month == "06") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('June'); ?></option>
                                            <option value="07" <?php
                                            if ($item->to_month == "07") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('July'); ?></option>
                                            <option value="08" <?php
                                            if ($item->to_month == "08") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('August'); ?></option>
                                            <option value="09" <?php
                                            if ($item->to_month == "09") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('September'); ?></option>
                                            <option value="10" <?php
                                            if ($item->to_month == "10") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('October'); ?></option>
                                            <option value="11" <?php
                                            if ($item->to_month == "11") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('November'); ?></option>
                                            <option value="12" <?php
                                            if ($item->to_month == "12") {
                                                echo "selected";
                                            }
                                            ?>><?php echo lang('December'); ?></option>
                                        </select>
                                        <select name="certf_to_year[]" class="small-select"><option value="" ><?php echo lang('chooce_year') ?> </option>
                                            <?php
                                            for ($y = 1975; $y <= year; $y++) {
                                                ?>
                                                <option value="<?php echo $y; ?>"<?php if ($item->to_year == $y) echo "selected"; ?> ><?php echo $y; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                            <?php if ($key > 0) { ?>
                                <a href="javascript:void(0);" onClick="del_unit(<?php echo $key; ?>)" class="delete"></a>
                            <?php } ?>
                        </div><!--end of cont-->
                    </div>
                <?php } ?>


            </div><!--end of content-->
            <br class="clr">
            <a href="javascript:void(0);" onClick="add_certf()" class="add"></a>

        </div><!--end of certificate-->
        <br class="clr">
        <!-- buttons start -->
        <div class="buttons">
            <a href="<?php echo site_url(); ?>/add_cv/back_edu/<?php echo $cv_id; ?>" class="previous-step"><?php echo lang('last'); ?></a>

            <div class="save-next">
                <input type="submit"  name="save_button" value="<?php echo lang('save') ?>" class="save">
                <input type="submit"  name="next_button" class="next-step" value="<?php echo lang('next') ?>"></a>
            </div>
        </div>

    </div>
</div>
</form>
<script>
    var course_count = $("#course_count").val();
    var cer_count = $("#cer_count").val();
    function add_unit() {
        //get now  year
        var d = new Date();
        var year = d.getFullYear();
        //
        var cv_id = $("#cv").val();
        var cont_edu = '<br class="clr"><div id="cont' + course_count + '" class="courses-added"> <div class="courses"><table cellpadding="0" cellspacing="0"><tr><td><label>';
        cont_edu += '<?php echo lang('course_name'); ?>:</label><input type="text" name="course[]"/></td><td><label><?php echo lang('course_place'); ?>:</label><input type="text" name="institute[]"/></td><td><label><?php echo lang('from'); ?>:</label>';
        //

        cont_edu += '<select name="from_month[]" class="small-select"> <option value="" ><?php echo lang('month') ?></option><option value="01" ><?php echo lang('January'); ?></option><option value="02"><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select>';

        cont_edu += '</select><select name="from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>';
        for (var y = 1975; y <= year; y++) {
            cont_edu += '<option value="' + y + '">' + y + '</option>';
        }
        cont_edu += '</select></td><td><label><?php echo lang('to'); ?>:</label><select class="small-select" name="to_month[]"> <option value="" ><?php echo lang('month') ?> </option><option value="01" ><?php echo lang('January'); ?></option><option value="02"><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select>';
        cont_edu += '<select name="to_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>';
        for (var y = 1975; y <= year; y++) {
            cont_edu += '<option value="' + y + '">' + y + '</option>';
        }
        cont_edu += '</select></td></tr></table></div>';


        //insert edu in db
        var unit_id;
        var cv_id = $("#cv").val();
        cont_edu += '<input type="hidden" name="id[]" id="id[]" value="' + unit_id + '"/><a href="javascript:void(0);" onClick="del_unit(' + course_count + ')" class="delete"></a></div>';
        //
        $("#course .content").append(cont_edu);
        course_count++;
    }
//
    function add_certf() {
        //get now  year
        var d = new Date();
        var year = d.getFullYear();
        //
        var cv_id = $("#cv").val();
        var cont_edu = ' <br class="clr"><div id="cont' + cer_count + '" class="courses-added"><div class="courses"><table cellpadding="0" cellspacing="0"><tr><td>';
        cont_edu += '<label><?php echo lang('cetifi_name'); ?>:</label><input type="text" name="certf_course[]"/></td><td><label><?php echo lang('cetifi_place'); ?>:</label><input type="text" name="certf_institute[]"/></td><td><label><?php echo lang('from'); ?></label>';
        cont_edu += '<select name="certf_from_month[]" class="small-select"> <option value="" ><?php echo lang('month') ?> </option><option value="01" ><?php echo lang('January'); ?></option><option value="02"><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select>';
        cont_edu += '<select name="certf_from_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>';
        //
        for (var y = 1975; y <= year; y++) {
            cont_edu += '<option value="' + y + '">' + y + '</option>';
        }
        cont_edu += '</select></td><td><label><?php echo lang('to'); ?>:</label><select name="certf_to_month[]" class="small-select"> <option value="" ><?php echo lang('month') ?> </option><option value="01" ><?php echo lang('January'); ?></option><option value="02"><?php echo lang('February'); ?></option><option value="03"><?php echo lang('March'); ?></option><option value="04"><?php echo lang('April'); ?></option><option value="05" ><?php echo lang('May'); ?></option><option value="06" ><?php echo lang('June'); ?></option><option value="07" ><?php echo lang('July'); ?></option><option value="08" ><?php echo lang('August'); ?></option><option value="09" ><?php echo lang('September'); ?></option><option value="10"><?php echo lang('October'); ?></option><option value="11" ><?php echo lang('November'); ?></option><option value="12"><?php echo lang('December'); ?></option></select>';
        cont_edu += '<select name="certf_to_year[]" class="small-select"><option value="" ><?php echo lang('year') ?> </option>';
        for (var ty = 1975; ty <= year; ty++) {
            cont_edu += '<option value="' + ty + '">' + ty + '</option>';
        }

        cont_edu += '</select></td></tr></table></div>';


        //insert edu in db
        var unit_id;
        var cv_id = $("#cv").val();
        cont_edu += '<input type="hidden" name="certf_id[]" id="id[]" value="' + unit_id + '"/><a href="javascript:void(0);" onClick="del_certf(' + cer_count + ')" class="delete"></a>';
        //
        $("#certif .content").append(cont_edu);
        cer_count++;
    }
//
    function del_unit(id) {
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            unit_id = $("#id" + id).val();
            $("#cont" + id).remove();
        } else
            return false;
        //count--;
    }
    function del_certf(id) {
        var checkstr = confirm('<?php echo lang('confirm-delete'); ?>');
        if (checkstr == true) {
            unit_id = $("#id" + id).val();
            $("#certif .content #cont" + id).remove();
        } else
            return false;
    }
</script>