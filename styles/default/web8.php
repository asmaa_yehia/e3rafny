<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="<?php echo $cv[0]->title ; ?>" />
        <meta property="og:type" content="article" />
        <meta property="og:description" content="<?php echo $cv[0]->objective ; ?>" />
        <meta property="og:image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta property="og:url" content="<?php echo base_url(); ?><?php echo $user_name ; ?>/<?php echo $cv[0]->url_title ; ; ?>" />
        <meta property="og:site_name" content="e3rafny" />
        <meta name="twitter:card" value="summary" />
        <meta name="twitter:site" value="e3rafny" />
        <meta name="twitter:title" value="<?php echo $cv[0]->title ; ?>" />
        <meta name="twitter:description" value="<?php echo $cv[0]->objective ; ?>" />
        <meta name="twitter:image" value="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" />
        <meta name="twitter:creator" value="<?php echo $user_info[0]->user_cv_name ; ?>" />
        <meta itemprop="name" content="<?php echo $cv[0]->title ; ?>">
        <meta itemprop="description" content="<?php echo $cv[0]->objective ; ?>">
        <meta itemprop="image" content="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>">
    <title><?php echo $user_info[0]->user_cv_name ; ?></title>
    <link href="<?php echo base_url("styles/css/web8/bootstrap_".$cv[0]->lang.".css") ?>" rel="stylesheet">
<link href="<?php echo base_url(); ?>styles/css/web8/<?php echo $style_name."_".$cv[0]->lang.".css"; ?>" rel="stylesheet">
    
    
  
  </head>
  <body >
<div class="row line">
</div>
<div class="row name">
  <div class="container">
  <div class="row">
  <?php if( !(empty($user_info[0]->image)) || ($user_info[0]->image !=0)){ ?>  
    <div class="col-md-4 col-md-offset-3 pic">
      <div class="row">
       
        <div class="col-md-offset-6">
         
      <img class="personal img-responsive" src="<?php echo base_url(); ?>assets/users_img/<?php echo $user_info[0]->image ; ?>" alt="<?php echo $user_info[0]->user_cv_name ; ?>">
       
    </div>


  </div>
  
    </div>
<?php } ?>    
  </div>
  <div class="row">
   <div class=" col-md-offset-1">
    <div class="row">
    <h1 class="nam"><?php echo ucwords($user_info[0]->user_cv_name) ; ?></h1>
    <h1 class="nam"><?php echo $user_info[0]->title ; ?></h1>
</div>

        </div>
  </div>
  <?php foreach ($phones as $item) {?>    
  <div class="row">
   <div class="col-md-4 col-md-offset-5 margin">
   
   <div class="col-md-6 noo">
    <p><?php echo $item->code.$item->number ; ?></p>
   </div>

 </div>
</div>
  <?php } ?>
      
<div class="row">
   <div class="col-md-4 col-md-offset-5 " style="margin-top:5px;">
   
   <div class="col-md-6 env">
    <p><?php if(!empty($user_info[0]->alter_mail))echo $user_info[0]->alter_mail;
		else echo $user->mail; ?></p>
   </div>
   
 </div>
</div>
<?php if($user_info[0]->adress1 !=''){ ?>      
<div class="row">
   <div class="col-md-4 col-md-offset-5 " style="margin-top:5px;">
   
   <div class="col-md-6 addd">
    <p><?php echo $user_info[0]->adress1; ?></p>
   </div>
   
 </div>
</div>
<?php } ?> 
<?php if($user_info[0]->adress2 !=''){ ?>       
<div class="row">
   <div class="col-md-4 col-md-offset-5 " style="margin-top:5px;">
    
   <div class="col-md-6 addd">
    <p><?php echo $user_info[0]->adress2; ?> 
</p>
   </div>
   
 </div>
</div>
<?php } ?>      
<div class="row">
   <div class="col-md-4 col-md-offset-5 bh" style="margin-top:20px;">
    <ul class="social">
     <?php foreach ($social as $item) { ?>   
      <li class="<?php if($item->name=='facebook'){echo 'fb';}elseif($item->name=='twitter'){echo 'twet';}elseif($item->name=='linkledin'){echo 'inn';}elseif($item->name=='google'){echo 'google';}elseif($item->name=='behance'){echo 'bee';} ?>"><a href="<?php echo $item->link ; ?>"></a></li>
     <?php } ?>  
      
    </ul>
    <!--
    <div class="col-md-1">
    <a href="#"><img src="images/be.png" alt="behance icon"></a>
   </div>-->
   
   
 </div>
</div>
  </div>

</div>
<div class="clr">
</div>
<div class="row cont">
  <div class="container">
    <div class="row">
    <div class=" col-sm-offset-6 arrow-up">
    </div>
    </div>
    <div class="row">
     
    <div class="col-xs-5 bord">
       <div clas="row">
      <div class="row">
        <div class="col-xs-11 col-xs-offset-1">
          <h1 class="title"><?php echo lang("objective") ;?> </h1>
        </div>
        <div class="col-xs-1">
          <div class="ggg"></div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-9 col-sm-offset-3">
        <p class="text"><?php echo $cv[0]->objective ;?></p>
</div>
</div>
      </div>
<?php if( !(empty($skills)) || !(empty($qualifications)) ){ ?> 
<div clas="row">
      <div class="row">
        <div class="col-xs-11 col-xs-offset-1">
          <h1 class="skill"> <?php echo lang("skils") ; ?> </h1>
        </div>
        <div class="col-xs-1">
<div class="qqq"></div>
        </div>
                </div>
      
      <div class="row">
        <div class="col-xs-11">
            <?php foreach ($skills as $item) { ?>
                <h6 class="skill-name"><?php echo $item->name ; ?> ..<span class="grad"><?php if($item->grade=='i') echo lang('interemdate');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='w') echo lang('weak'); ?></span></h6>
            <?php } ?>    
            <?php foreach ($qualifications as $item) { ?>
                <h6 class="skill-name"><?php echo $item->name ; ?> 
                </h6>
            <?php } ?>  

</div>
</div>
      </div>
<?php } ?>
       <?php if( !(empty($experience)) || !(empty($projects)) ){ ?>  
      <div clas="row">
      <div class="row">
        <div class="col-xs-11 col-xs-offset-1">
          <h1 class="exp"> <?php if( !(empty($experience))) { echo lang("experience") ;} ?><?php if( !(empty($experience)) && !(empty($projects)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($projects))) echo lang("projects") ; ?> </h1>
        </div>
        <div class="col-xs-1">
          <div class="www">
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-11">
          <?php if (!(empty($user_info[0]->exp_no))){ ?>
            <p class="add"><?php echo lang('no_exp'); ?> : <?php echo $user_info[0]->exp_no ?></p>
   <?php } ?>    
          
        </div>
        <div class="row">
          <div class="col-xs-11">
              <?php foreach ($experience as $item) { ?>     
                <h6 class="skil" style="margin-top:12px;"><?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".$item->to_month."/".$item->to_year."<br>" ; ?>
        
	<?php	}elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
    
			<?php echo $item->from_month."/".$item->from_year."&nbsp;"."-"."&nbsp;".lang($item->to_month) ; ?>
       
			<?php } ?></h6>
                <h6 class="skil"><?php if( !(empty($item->company)))  echo $item->company ; ?></h6>
                <h6 class="skil"><?php if( !(empty($item->name)))  echo $item->name ; ?></h6>
                <?php  if(!empty($item->role)){ ?>  
                <?php $roles=unserialize($item->role); foreach ($roles as $r_item) { ?>
                <h6 class="skil"><?php echo $r_item ; ?></h6>
                <?php }} ?>
                <h6 class="skil"><?php if( !(empty($item->description)))  echo $item->description ; ?></h6>
              <?php } ?>
              <?php foreach ($projects as $item) { ?>     
                <h6 class="skil" style="margin-top:12px;"><?php if( !(empty($item->name)))  echo $item->name  ; ?></h6>
                <h6 class="skil"><?php if( !(empty($item->link)))?>  <a style="color:#FFF;font-size:12px;" href="<?php echo $item->link  ; ?>"><?php echo $item->link  ; ?></a></h6>
                
                <h6 class="skil"><?php if( !(empty($item->description)))  echo $item->description ; ?></h6>
              <?php } ?>  

   </div>           
</div>
</div>
</div>
       <?php } ?>
  <?php if( !(empty($courses)) || !(empty($certifcates)) ){ ?>       
 <div clas="row">
      <div class="row">
        <div class="col-xs-11 col-xs-offset-1">
          <h1 class="cer"> <?php if( !(empty($courses))) { echo lang("courses") ;} ?><?php if( !(empty($courses)) && !(empty($certifcates)) ){ echo "&nbsp;".lang("and")."&nbsp;" ; } if(!(empty($certifcates))) echo lang("certifcates") ; ?> </h1>
        </div>
        <div class="col-xs-1">
<div class="eee">
</div>
        </div>
      </div>
      <div class="row">
        
        <div class="col-xs-11">
        <?php foreach ($courses as $item) { ?>  
        <?php if( !(empty($item->name))){ ?>    
        <h6 class="skil" style="margin-top:20px;">-<?php  echo $item->name ; ?> <span class="white"></span></h6>
        <?php } ?>
        <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){ ?>
      <h6 class="skil" style="margin-top:20px;">-<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ;  ?><span class="white"></span></h6>
    <?php } ?>  
      <?php if( !(empty($item->institute))){ ?> 
        <h6 class="skil" style="margin-top:20px;">-<?php echo $item->institute ; ?> <span class="white"></span></h6>
      <?php } ?>
        <?php } ?>
        <?php foreach ($certifcates as $item) { ?>  
        <?php if( !(empty($item->name))){ ?>    
        <h6 class="skil" style="margin-top:20px;">-<?php  echo $item->name ; ?> <span class="white"></span></h6>
        <?php } ?>
        <?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year ))){ ?>
      <h6 class="skil" style="margin-top:20px;">-<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ;  ?><span class="white"></span></h6>
    <?php } ?>  
      <?php if( !(empty($item->institute))){ ?> 
        <h6 class="skil" style="margin-top:20px;">-<?php echo $item->institute ; ?> <span class="white"></span></h6>
      <?php } ?>
        <?php } ?>
</div>
</div>
</div>
  <?php } ?>        
      </div>






<div class="col-xs-5 bord-2">
  <div class="row">
      <div class="row">
        <div class="col-xs-1">
          <div class="rrr">
          </div>
        </div>
        <div class="col-xs-11 ">
          <h1 class="pers"> <?php echo lang("personalinfo") ; ?></h1>
        </div>
        
      </div>
      <div class="row">
        <div class="col-xs-11">
        <?php if($user_info[0]->nationality !=''){ ?>    
        <h6 class="nation"><?php echo lang("nationality");?>:  <span class="m"><?php echo ucfirst($user_info[0]->nationality); ?></span></h6>
        <?php } ?>
        <?php if($user_info[0]->day !='0' && $user_info[0]->month !='0' && $user_info[0]->year !='0'){ ?> 
        <h6 class="nation"><?php echo lang("birth_date") ; ?>:  <span class="m"><?php echo $user_info[0]->day."/".$user_info[0]->month."/".$user_info[0]->year; ?></span></h6>
        <?php } ?>
        <?php if($user_info[0]->marital_status !='n'){ 
		if($user_info[0]->marital_status =='s'){
			$m_status=lang("single");
		}elseif($user_info[0]->marital_status =='m'){
			$m_status=lang("married");
		}elseif($user_info[0]->marital_status =='d'){
			$m_status=lang("divercoed");
		}
?>    
        <h6 class="nation"><?php echo lang("marital_rstatus"); ?>:  <span class="m"><?php echo $m_status ; ?></span></h6>
        <?php } ?>
        <?php if($user_info[0]->gender !='n'){ 
			if($user_info[0]->gender =='m'){
				$gender=lang("male");
			}elseif($user_info[0]->gender =='f'){
				$gender=lang("female");
			}
	?>     
        <h6 class="nation"><?php echo lang("gender"); ?>:  <span class="m"><?php echo $gender ; ?></span></h6>
        <?php } ?>
        <?php if($user_info[0]->license !='no'){ 
		 if($user_info[0]->license =='y'){
			$License=lang("yes");
		}elseif($user_info[0]->license =='n'){
			$License=lang("no");
		}
?>   
        <h6 class="nation"><?php echo lang("License"); ?>:  <span class="m"><?php echo $License ; ?></span></h6>
        <?php } ?>
        <?php if($user_info[0]->military !='n'){ 
		if($user_info[0]->military =='e'){
			$military=lang("exemption");
		}elseif($user_info[0]->military =='c'){
			$military=lang("Complete_service");
		}elseif($user_info[0]->military =='p'){
			$military=lang("Postponed");
		}elseif($user_info[0]->military =='s'){
			$military=lang("Currently_serving");
		}elseif($user_info[0]->military =='d'){
			$military=lang("Doesnt_apply");
		}	 
	?>    
        <h6 class="nation"><?php echo lang("military"); ?>:  <span class="m"><?php echo $military; ?></span></h6>
        <?php } ?>

</div>
</div>
      </div>
    <?php if( !(empty($education))){ ?> 
      <div class="row">
      <div class="row">
        <div class="col-xs-1">
          <div class="ttt">
          </div>
        </div>
        <div class="col-xs-11 ">
          <h1 class="ed"><?php echo lang("education") ; ?></h1>
        </div>
        
      </div>
      <div class="row">
        <div class="col-xs-11">
        <?php foreach ($education as $item) { ?>     
        <h6 class="white">-<?php if( !(empty($item->type)))  echo $item->type ; ?>:  <span class="white"></span></h6>
        <h6 class="white">-<?php if( !(empty($item->from_month)) && !(empty($item->from_year)) && !(empty($item->to_month)) && !(empty($item->to_year )) 
		&& ($item->to_month !='now')){?>
        
			<?php echo $item->from_month."/".$item->from_year." - ".$item->to_month."/".$item->to_year ; ?>
          
		<?php }elseif(!(empty($item->from_month)) && !(empty($item->from_year)) && $item->to_month=='now'){ ?>
        
		<?php	echo $item->from_month."/".$item->from_year." - ".lang($item->to_month) ; ?>
        
	<?php	} ?>:  <span class="white"></span></h6>
        <h6 class="white">-<?php if( !(empty($item->name)))  echo $item->name ; ?>  <span class="white"></span></h6>
        <h6 class="white">-<?php if( !(empty($item->desc)))  echo $item->desc ; ?>  <span class="white"></span></h6>
        <?php } ?>
    
</div>
</div>
      </div>
    <?php } ?>
 <?php if( !(empty($langs))){ ?>     
 <div class="row">
      <div class="row">
        <div class="col-xs-1">
          <div class="yyy">
          </div>
        </div>
        <div class="col-xs-11 ">
          <h1 class="lang"><?php echo lang("langs") ; ?></h1>
        </div>
        
      </div>
      <div class="row">
        <div class="col-xs-11">
        <?php foreach ($langs as $item) {  ?>    
        <h6 class="white">-<?php echo lang($item->name); ?><span class="whit"><?php if($item->grade=='p') echo lang('poor');
										elseif($item->grade=='f') echo lang('fair');
										elseif($item->grade=='g') echo lang('good');
										elseif($item->grade=='v') echo lang('very_good');
										elseif($item->grade=='fl') echo lang('fluent');
										elseif($item->grade=='t') echo lang('tongue'); ?></span></h6>
        <?php } ?>
        
</div>
</div>
</div>
 <?php } ?> 
 <?php if( !(empty($references))){ ?>   
 <div class="row">
      <div class="row">
        <div class="col-xs-1">
          <div class="uuu">
          </div>
        </div>
        <div class="col-xs-11 ">
          <h1 class="ref"><?php echo lang("references") ; ?> </h1>
        </div>
        
      </div>
      <div class="row">
        <div class="col-xs-11">
        <?php foreach ($references as $item) { ?>      
        <h6 class="white" style="margin-top:23px !important;"><?php if( !(empty($item->name))){ echo" - " .$item->name." .. "; }?> </h6>
        <h6 class="white" style="margin-top:3px !important;"><?php echo $item->phone ; ?>   </h6>
        <h6 class="white" style="margin-top:3px !important;"><?php echo $item->mail ; ?>  </h6>
        <?php } ?>
</div>
</div>
</div>
 <?php } ?>




</div>
  </div>
  </div>
<div class="clr">
</div>
<div class="row rec">
  <div class="container">
    <div class="row">
      <div class="col-md-1 col-md-offset-5">
        <div class="iii">
        </div>
      </div>
      <div class="col-md-5">
        <h6><?php echo lang('recommendation'); ?> </h6>
         </div>
        
       <?php foreach ($recommends as $item) { ?> 
        <div class="col-md-5 col-md-offset-6">
        <p><?php echo date("d/m/Y" ,$item->date); ?></p>
        <p style="margin-top:3px;"><?php echo $item->name; ?></p>
        <p style="margin-top:3px;"><?php echo $item->job; ?></p>      
     
    </div>
    
     <div class="row">
      <div class="col-md-2 col-md-offset-5">
      <p style="margin-top:3px;text-align:center;" class="k"><?php echo $item->recommend; ?> </p>
</div>
</div>

       <?php } ?>     
<div class="row">
   <div class="btn-wrapper">
    <div class="col-md-4 col-md-offset-5">
                            <a class="btn" id="show-form"><?php echo strtoupper(lang('leave_recommendation')); ?> </a>
                        </div>
                      </div>
                      </div>
    <div id="form-wrapper"class="row">
          <div class="col-md-6 col-md-offset-4">
<span class="form-success" id="form-success" style="display:none"></span>
       <form class="recom-form">
       <input name="cv_lang" type="hidden" value="<?php echo $cv[0]->lang ; ?>">
       <input name="cv" type="hidden" value="<?php echo $cv[0]->id ; ?>">
       <input name="user_id" type="hidden" value="<?php echo $user_id ; ?>">
       <span class="error_message" id="error_message_name" style="display:none"></span>
       <input type="text" placeholder="<?php echo lang('name') ?>" name="name" class="recom_form" />
       <span class="error_message" id="error_message_job" style="display:none"></span>
       <input type="text" placeholder="<?php echo lang('job') ?>" name="job" class="recom_form" />
       <span class="error_message" id="error_message_recommend" style="display:none"></span>
       <textarea rows="4" placeholder="<?php echo lang('recommend') ?>" name="recommend" class="recom_form" ></textarea>
       <input class="btn-submit" type="button" value="<?php echo lang('recommend') ?>" id="save_btn">
       </form>
                        </div><!--form-wrapper-->
                      </div>
                     
  </div>
  </div>
 
<div class="clr">
</div>
<div class="row footer">
  </div>

     <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?php echo base_url(); ?>styles/js/bootstrap.min.js"></script>
    <script>
            $(document).ready(function() {
                $('#show-form').click(function() {
                    $('#form-wrapper').slideToggle("fast");
                                        $('#all').slideToggle("fast");

                });
            });
            $('#save_btn').click(function () {
				
				dataString = $(".recom-form").serialize();
				
				var request2 = $.ajax({
                     type: "POST",
                     url: "<?php echo base_url(); ?>profile/save",
                     data: dataString,
					 dataType : 'json',
                     success: function(result) {
					
						if(result=='<?php echo lang('done') ?>'){
							
							$('.error_message').hide();
							$('#form-success').show();
							$('#form-success').html("<?php echo lang('done') ?>");
							$('.recom_form').val("");
							//setTimeout(function() { $("#form-success").fadeOut(1500); }, 5000);
							//setTimeout(function() { $("#all").fadeIn(1500); }, 5000);
							 //$("#form-wrapper").slideToggle(1000);
							 //$("#all").slideToggle(1000);
							//$('#form-wrapper').slideToggle("slow");
                   			//$('#all').slideToggle("slow");
							
						}else{
							$('#form-success').hide();
							$('.error_message').hide();
							if(result['name']){
								$('#error_message_name').show();
								$('#error_message_name').html(result['name']);
							}
							if(result['job']){	
								$('#error_message_job').show();
								$('#error_message_job').html(result['job']);
							}
							if(result['recommend']){
								$('#error_message_recommend').show();
								$('#error_message_recommend').html(result['recommend']);
							}
							
						}
						
					},
											
				});
				
   			 });

        </script>
    </body>
</html>
  