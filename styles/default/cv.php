<form action="<?php  echo site_url(); ?>/add_cv/index" method="post">
<input type="hidden" value="<?php echo $percent; ?>" name="percent" />
                            <table class="wizard-form" cellpadding="0" cellspacing="20">
                                <tr>
                                    <td><label><?php echo lang('cv_lang'); ?>:</label></td>
                                    <td>
                                        <select  name="lang">
        
                                            <option value="ar" <?php echo set_select('lang', 'ar', TRUE); ?>><?php echo lang('arabic'); ?></option>
                                            <option value="en" <?php echo set_select('lang', 'en'); ?>><?php echo lang('english'); ?></option>
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label> <?php echo lang('cv_title'); ?>:</label></td>
                                    <td><input type="text" name="cv_tilte" value="<?php echo set_value('cv_tilte'); ?>"></td>
                                    <td><div class="error"><?php echo form_error('cv_tilte'); ?></div></td>
                                </tr>

                                <tr>
                                    <td><label><?php echo lang('objective'); ?>:</label></td>
                                    <td><textarea name="obj"><?php echo set_value('obj'); ?></textarea></td>
                                    <td><div class="erro"><?php echo form_error('obj'); ?></div></td>
                                </tr>
                            </table>

                            <div class="buttons">
                                <div class="save-next">
                                 <input type="submit"  name="save_button" class="save" value ="<?php echo lang('save'); ?>" />
   				 <input type="submit"  name="next_button" class="next-step"  value ="<?php echo lang('next'); ?>" />
                                   
                                </div>
                            </div>
                        </form>
                        

<script type="text/javascript">
    $('form').submit(function() {
        var str = $('[name="cv_tilte"]').val();
        if (str ==='') {
            $(".error").html("<?php echo lang('cv_empty'); ?>");
            return false;
        }
        else if (/^[a-zA-Z0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF]+[a-zA-Z0-9\u0600-\u065F\u066A-\u06EF\u06FA-\u06FF ]*$/.test(str) == false) {
            $(".error").html("<?php echo lang('validate_special'); ?>");
            return false;
            //alert('Your search string contains illegal characters.');
        }
    });
</script>

<!--//</body>
//</html>
-->