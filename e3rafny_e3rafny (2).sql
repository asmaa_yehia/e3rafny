-- phpMyAdmin SQL Dump
-- version 4.1.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 30, 2014 at 12:34 PM
-- Server version: 5.5.37-cll
-- PHP Version: 5.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `e3rafny_e3rafny`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE IF NOT EXISTS `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `activation` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `full_name`, `mail`, `password`, `activation`) VALUES
(8, 'asma', 'eng_asmaa27@yahoo.com', '202cb962ac59075b964b07152d234b70', ''),
(9, 'hoda', 'hoda.hussin@gmail.com', '202cb962ac59075b964b07152d234b70', '');

-- --------------------------------------------------------

--
-- Table structure for table `certifcates`
--

CREATE TABLE IF NOT EXISTS `certifcates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `from_year` varchar(255) NOT NULL,
  `to_year` varchar(255) NOT NULL,
  `from_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `to_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Dumping data for table `certifcates`
--

INSERT INTO `certifcates` (`id`, `cv_id`, `name`, `institute`, `from_year`, `to_year`, `from_month`, `to_month`, `percent`) VALUES
(12, 18, 'شسش', 'شسش', '1989', '1988', '09', '07', 10),
(13, 13, 'sda', 'dasd', '1975', '1975', '02', '02', 10),
(14, 20, 'iti', 'iti', '1985', '1990', '05', '07', 10),
(18, 21, 'لالبلابللابل', 'لابللابل', '1989', '1992', '12', '09', 10),
(19, 11, 'iti', 'iti', '1986', '1987', '08', '06', 10);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) CHARACTER SET utf8 NOT NULL,
  `phone` int(25) NOT NULL,
  `mail` varchar(255) CHARACTER SET utf8 NOT NULL,
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0',
  `admin_id` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `phone`, `mail`, `subject`, `message`, `parent`, `admin_id`, `date`) VALUES
(1, 'asmaa', 42534517, 'eng_asmaa27@yahoo.com', 'mans', 'helloooooooooo', 0, 0, '2014-06-15 13:40:25');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `institute` varchar(255) NOT NULL,
  `from_year` varchar(255) NOT NULL,
  `to_year` varchar(255) NOT NULL,
  `from_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `to_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=199 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `cv_id`, `name`, `institute`, `from_year`, `to_year`, `from_month`, `to_month`, `percent`) VALUES
(17, 14, 'sadasd', 'sdasd', '1978', '1979', '04', '05', 10),
(20, 16, 's\\dsad', 'sdacd', '1978', '1987', '06', '09', 10),
(22, 12, 'weqwr', 'asdaD', '', '', '', '', 10),
(23, 15, 'sdasd', 'sdsad', '1980', '1982', '03', '06', 10),
(66, 17, 'oracle', 'aa', '1977', '1986', '11', '07', 10),
(184, 18, 'oracle', 'egypt db', '1990', '1989', '08', '06', 10),
(185, 19, 'oracle', 'egypt db', '1989', '1988', '06', '05', 10),
(186, 13, '', 'ooo', '', '', '', '', 5),
(187, 20, 'oracle', 'egypt db', '1986', '1983', '05', '07', 10),
(193, 21, 'للابللابل', 'لابللالب', '1993', '2006', '01', '05', 10),
(194, 11, 'oracle', 'egypt db', '1982', '1983', '07', '07', 10),
(195, 11, 'oracle', '', '', '', '', '', 5),
(196, 22, 'oracle', 'egypt db', '1981', '1984', '06', '08', 10),
(197, 25, 'oracle', 'egypt db', '1991', '1989', '05', '08', 10),
(198, 27, 'oracle', 'egypt db', '1991', '1991', '05', '05', 10);

-- --------------------------------------------------------

--
-- Table structure for table `cv`
--

CREATE TABLE IF NOT EXISTS `cv` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url_title` varchar(255) NOT NULL,
  `objective` text NOT NULL,
  `lang` varchar(50) NOT NULL,
  `percent` double NOT NULL,
  `pdf_them` int(11) NOT NULL,
  `style_them` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `cv`
--

INSERT INTO `cv` (`id`, `user_id`, `title`, `url_title`, `objective`, `lang`, `percent`, `pdf_them`, `style_them`, `img`) VALUES
(11, 55, 'asmaa', 'asmaa', 'ana php developer hhhhhhhh hhhhhhhhhhhh  adjksjdksadj ksdjkasjdksajd kasjdkasjdkasjd ksajdkasjdkajsdk hhhhhhhhhhhhh hhhhhhhhhhhjsdhjsahd sakjdhajshd akshdakshdlk kshadkashdk kshdkashdl', 'en', 95, 6, 28, '40556018.jpg'),
(12, 55, 'اش', 'اش', 'ijkjLml''m.', 'en', 46, 4, 1, '10981.jpg'),
(13, 56, 'ff', 'ff', 'ss', 'ar', 52, 2, 0, '241257763.jpg'),
(14, 55, 'my resume', 'my_resume', 'ana developer', 'en', 45, 1, 0, '19033.jpg'),
(15, 55, 'my test cv', 'my_test_cv', 'ana dev', 'en', 42, 1, 1, '1.jpg'),
(16, 55, 'تيست', 'تيست', 'safasd', 'ar', 55, 1, 1, '1.jpg'),
(17, 55, 'منى', 'منى', 'الهدف', 'ar', 94, 1, 3, '839823800.jpg'),
(18, 55, 'اسماء', 'اسماء', 'الهدف', 'en', 55, 2, 1, '1416463906.jpg'),
(19, 55, 'مبرمجه', 'مبرمجه', 'سشيشسيشسي', 'ar', 52, 1, 1, '1.jpg'),
(20, 55, 'ديفلوبر', 'ديفلوبر', 'اانلاو', 'ar', 52, 1, 1, '1.jpg'),
(21, 58, 'bgbgf', 'bgbgf', 'bgfbfgbfgbfg', 'ar', 65, 6, 1, '692779587.jpg'),
(22, 55, 'testy', 'testy', 'hellooooooooo', 'en', 52, 1, 1, '1.jpg'),
(24, 55, 'مبرمج', 'مبرمج', 'سبؤسيبسيب', 'ar', 35, 1, 1, '1.jpg'),
(25, 55, 'ماي', 'ماي', 'لاتنلاتلاىز', 'ar', 42, 1, 1, '1.jpg'),
(26, 55, 'انا', 'انا', 'الاتلاو', 'ar', 35, 1, 1, '1.jpg'),
(27, 55, 'خخخخخ', 'خخخخخ', 'ءئبؤسيب', 'ar', 39, 1, 1, '1.jpg'),
(28, 55, 'ختخ', 'ختخ', 'انلانلاتو', 'ar', 19, 1, 1, '1314079669.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `cv_recommendation`
--

CREATE TABLE IF NOT EXISTS `cv_recommendation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `recomnd_id` int(11) NOT NULL,
  `cv` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `cv_recommendation`
--

INSERT INTO `cv_recommendation` (`id`, `recomnd_id`, `cv`) VALUES
(50, 29, 11),
(52, 30, 12),
(53, 30, 12),
(54, 38, 12),
(55, 42, 12);

-- --------------------------------------------------------

--
-- Table structure for table `education`
--

CREATE TABLE IF NOT EXISTS `education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `from_year` varchar(255) NOT NULL,
  `from_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `to_year` varchar(255) DEFAULT NULL,
  `to_month` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `desc` text NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=248 ;

--
-- Dumping data for table `education`
--

INSERT INTO `education` (`id`, `type_id`, `cv_id`, `name`, `from_year`, `from_month`, `to_year`, `to_month`, `desc`, `percent`) VALUES
(23, 1, 14, 'sadasd', '1981', '05', '1979', '03', 's\\dasd', 10),
(26, 1, 16, 'asdda', '1982', '06', '1982', '03', 'safaf', 10),
(28, 1, 12, 'secondry school', '1985', '10', '1985', '07', 'xsdasd', 10),
(29, 1, 15, 'sss', '1979', '04', '1978', '05', 'sdsad', 10),
(61, 1, 17, 'ااا', '1987', '06', '1987', '05', 'الوصف', 10),
(228, 1, 13, '5', '1993', '01', '', 'now', '222', 10),
(231, 3, 18, 'primary school', '1988', '05', '1990', '05', 'يسسشيشسي', 10),
(232, 1, 19, 'primary school', '1991', '06', '1990', '03', 'سشيشسي', 10),
(233, 3, 20, 'primary school', '1986', '04', '1986', '09', 'xczxv', 10),
(236, 3, 11, 'primary school', '1990', '11', '', 'now', 'zxsddsa;, sadmasldm ', 10),
(237, 1, 11, 'secondary school', '1986', '09', '', '10', 'zdcxc', 10),
(239, 3, 21, 'يبريبريب', '1993', '02', '2011', '01', 'ريبريبريبربي', 10),
(240, 1, 22, 'secondary school', '1979', '05', '1979', '04', 'xsaf', 10),
(242, 1, 24, 'primary school', '1991', '05', '1991', '04', 'ءئرءؤر', 10),
(244, 1, 25, 'primary school', '1992', '05', '1992', '05', 'نىنىو', 10),
(245, 1, 25, 'primary school', '1990', '09', '1992', '06', 'نىنىك', 10),
(246, 1, 26, 'primary school', '1991', '04', '1991', '04', 'ىنىزو', 10),
(247, 1, 27, 'primary school', '1991', '04', '1991', '03', 'ءئئءؤ', 10);

-- --------------------------------------------------------

--
-- Table structure for table `education_type`
--

CREATE TABLE IF NOT EXISTS `education_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_ar` varchar(255) NOT NULL,
  `type_en` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `education_type`
--

INSERT INTO `education_type` (`id`, `type_ar`, `type_en`) VALUES
(1, 'ثانوي', 'secondry'),
(3, '2ابتدائ', 'primary2');

-- --------------------------------------------------------

--
-- Table structure for table `experience`
--

CREATE TABLE IF NOT EXISTS `experience` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `company` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL,
  `from_year` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `to_year` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `from_month` varchar(255) NOT NULL,
  `to_month` varchar(255) NOT NULL,
  `exp_years` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=266 ;

--
-- Dumping data for table `experience`
--

INSERT INTO `experience` (`id`, `cv_id`, `name`, `company`, `role`, `from_year`, `to_year`, `from_month`, `to_month`, `exp_years`, `description`, `percent`) VALUES
(251, 19, 'ay 7aga', 'gt4', 'a:1:{i:0;s:6:"بلت";}', '1992', '1991', '04', '04', '', 'نىنى', 10),
(253, 13, 'oio', 'kj', 'a:1:{i:0;s:2:"oo";}', '1975', '', '01', 'now', '', 'oo', 10),
(260, 11, 'ay 7aga', '', 'a:1:{i:0;s:3:"gvb";}', '1983', '1980', '05', '05', '', 'kklhlkn', 5),
(261, 11, 'ay 7aga', 'gt4', 'a:2:{i:0;s:12:"second mnbaa";i:1;s:11:"first mnbaa";}', '1986', '1988', '06', '11', '', 'sdsfsdf', 10),
(264, 21, 'ريبريب', 'ربيربير', 'a:1:{i:0;s:22:"يبربيريبربي";}', '1975', '', '02', 'now', '', 'ربيريبريب', 10),
(265, 22, 'ay 7aga', 'gt4', 'a:1:{i:0;s:5:"sfsdf";}', '1981', '1979', '05', '04', '', 'xcdzc', 10);

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE IF NOT EXISTS `faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) CHARACTER SET utf8 NOT NULL,
  `answer` text CHARACTER SET utf8 NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `question_en` varchar(256) CHARACTER SET utf8 NOT NULL,
  `answer_en` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `question`, `answer`, `status`, `question_en`, `answer_en`) VALUES
(2, 'what is your name?', '  sss', 0, '', ''),
(3, 'waht is age?', 'hoad', 0, '', ''),
(4, 'what?', 'ko', 0, '', ''),
(5, 'hoda', 'hoda', 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `lang`
--

CREATE TABLE IF NOT EXISTS `lang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `grade` enum('p','f','g','v','fl','t') NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=81 ;

--
-- Dumping data for table `lang`
--

INSERT INTO `lang` (`id`, `cv_id`, `name`, `grade`, `percent`) VALUES
(72, 11, 'Chinese', 'p', 10),
(73, 11, 'Armenian', 'v', 10),
(74, 11, 'Czech', 'f', 10),
(79, 21, 'Arabic', 't', 10),
(80, 21, 'French', 'fl', 10);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `seen` tinyint(4) NOT NULL,
  `date` int(13) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `admin_id`, `user_id`, `title`, `message`, `seen`, `date`) VALUES
(1, 8, 55, 'اسماء', 'hhhhhhhhhhh', 1, 1402489563),
(2, 8, 55, 'اااا', 'is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.\r\n\r\nis simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', 1, 1402490810),
(3, 8, 55, 'helloo', 'asa', 1, 0),
(4, 8, 55, '222', 'sdasd', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pdf_thems`
--

CREATE TABLE IF NOT EXISTS `pdf_thems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `img_ar` varchar(255) NOT NULL,
  `img_en` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pdf_thems`
--

INSERT INTO `pdf_thems` (`id`, `file_name`, `img_ar`, `img_en`) VALUES
(1, 'them1', '1.jpg', '1.jpg'),
(2, 'them2', '1.jpg', '1.jpg'),
(3, 'them3', '1.jpg', '1.jpg'),
(4, 'them4', '1.jpg', '1.jpg'),
(5, 'them5', '1.jpg', '1.jpg'),
(6, 'them6', '1.jpg', '1.jpg'),
(7, 'them7', '1.jpg', '1.jpg'),
(8, 'them8', '1.jpg', '1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

CREATE TABLE IF NOT EXISTS `phones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cv_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=353 ;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `user_id`, `cv_id`, `code`, `number`) VALUES
(324, 56, 13, '040', '2565'),
(325, 56, 13, '24457', '2354'),
(333, 58, 21, '002', '101576700'),
(352, 55, 28, '002', '22222');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=297 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `cv_id`, `name`, `link`, `description`, `percent`) VALUES
(292, 19, 'mnbaa', 'http://www.google.com', 'عاتاكتم', 10),
(296, 11, 'mnbaa', 'http://www.google.com', 'sm.dnasm.cn.asc .saz c', 10);

-- --------------------------------------------------------

--
-- Table structure for table `qualifications`
--

CREATE TABLE IF NOT EXISTS `qualifications` (
  `name_en` varchar(255) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_ar` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `qualifications`
--

INSERT INTO `qualifications` (`name_en`, `id`, `name_ar`) VALUES
('ambious', 1, 'طموح'),
('good', 3, 'كويس'),
('aa', 4, 'dd3'),
('bb', 5, 'جوود'),
('cc', 6, 'cc'),
('dd', 7, 'dd');

-- --------------------------------------------------------

--
-- Table structure for table `qual_user`
--

CREATE TABLE IF NOT EXISTS `qual_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `qual_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `cv_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=326 ;

--
-- Dumping data for table `qual_user`
--

INSERT INTO `qual_user` (`id`, `qual_id`, `user_id`, `cv_id`) VALUES
(317, 3, 55, 11),
(318, 4, 55, 11),
(319, 5, 55, 11),
(324, 1, 58, 21),
(325, 3, 58, 21);

-- --------------------------------------------------------

--
-- Table structure for table `recommend`
--

CREATE TABLE IF NOT EXISTS `recommend` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `recommend` text NOT NULL,
  `job` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `seen` tinyint(4) NOT NULL,
  `date` int(13) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `recommend`
--

INSERT INTO `recommend` (`id`, `cv`, `user_id`, `name`, `recommend`, `job`, `status`, `seen`, `date`) VALUES
(1, 11, 0, 'testy', 'testy recomnd', 'testy job', 0, 1, 1402483807),
(2, 11, 0, 'testy11', 'testy recomnd11', 'testy job11', 1, 1, 1402489038),
(4, 11, 0, 'نتن', 'تن', 'متم', 0, 1, 1402491471),
(5, 11, 0, 'نتن', 'تن', 'متم', 1, 1, 1402491488),
(6, 11, 0, 'شس', 'نتكن', 'نى', 0, 1, 1402493258),
(7, 11, 0, 'شسئؤسشي', 'نتكن', 'نى', 0, 1, 1402493264),
(8, 11, 0, 'اسماء', 'برافوووو', 'لا', 0, 0, 1402729388),
(9, 11, 0, 'اسماءيل', 'برافوووويل', 'لابيل', 1, 1, 1402729523),
(10, 11, 0, 'dfsdf', 'zsczc', 'zcc', 0, 1, 1402739120),
(11, 11, 0, 'asmaa', 'nmn.', 'h', 0, 0, 1403431521),
(12, 21, 0, 'عمرو الشابوري', 'عامر ده فشيييييخ', 'مدير مشروعات', 1, 1, 1403452485);

-- --------------------------------------------------------

--
-- Table structure for table `references`
--

CREATE TABLE IF NOT EXISTS `references` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `references`
--

INSERT INTO `references` (`id`, `cv_id`, `name`, `phone`, `mail`, `percent`) VALUES
(52, 11, 'dr.a7med ali m7md', '', 'aa@hotmail.com', 5),
(53, 11, 'dr.a7med ali m7md', '87787878', '', 3),
(54, 11, '', '87787878', 'aa@hotmail.com', 3);

-- --------------------------------------------------------

--
-- Table structure for table `skills`
--

CREATE TABLE IF NOT EXISTS `skills` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `grade` enum('g','i','w') NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=69 ;

--
-- Dumping data for table `skills`
--

INSERT INTO `skills` (`id`, `cv_id`, `name`, `grade`, `percent`) VALUES
(63, 11, 'photoshop', 'g', 10),
(64, 11, 'c#', 'w', 10),
(65, 11, 'c++', 'i', 10),
(68, 21, 'شؤسيرلاب', 'i', 10);

-- --------------------------------------------------------

--
-- Table structure for table `social`
--

CREATE TABLE IF NOT EXISTS `social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `cv_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `percent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=801 ;

--
-- Dumping data for table `social`
--

INSERT INTO `social` (`id`, `user_id`, `cv_id`, `name`, `link`, `percent`) VALUES
(705, 55, 18, 'facebook', 'https://www.facebook.com/asmaa.yehia.182 ', 0),
(706, 55, 19, 'facebook', 'https://www.facebook.com/asmaa.yehia.182 ', 0),
(760, 56, 13, 'facebook', 'https://www.facebook.com/    ', 0),
(761, 56, 13, 'twitter', 'https://www.facebook.com/    ', 0),
(762, 56, 13, 'linkledin', 'https://www.facebook.com/    ', 0),
(763, 56, 13, 'Google+', 'https://www.facebook.com/    ', 0),
(764, 56, 13, 'behance', 'https://www.facebook.com/    ', 0),
(775, 58, 21, 'facebook', 'http://www.facebook.com/aolamer   ', 0),
(776, 58, 21, 'google', 'http://www.facebook.com/aolamer   ', 0),
(796, 55, 11, 'facebook', 'https://www.facebook.com/asmaa.yehia.182', 0),
(797, 55, 11, 'twitter', 'https://twitter.com/smileebaby', 0),
(798, 55, 11, 'google', 'https://twitter.com/smileebaby', 0),
(799, 55, 11, 'behance', 'http:www.facebook.com', 0),
(800, 55, 11, 'linkledin', 'https://www.facebook.com/asmaa.yehia.182', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `activation` varchar(255) NOT NULL,
  `fb_id` bigint(20) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `first_name`, `last_name`, `user_name`, `mail`, `password`, `activation`, `fb_id`, `status`) VALUES
(55, 'asmaa', 'yahia', 'asmaa_yahia', 'eng_asmaa272@yahoo.com', '202cb962ac59075b964b07152d234b70', '1e006043521a53c3f48b1abb97c3b7ee', 0, 1),
(56, 'hoda', 'hussin', 'hoda', 'hoda.hussin@gmail.com', '202cb962ac59075b964b07152d234b70', '509b2c140ac2161029d0ea8eb93a8884', 0, 1),
(57, 'Amer', 'Ibrahim', '873623716', 'jobs@mnbaa.com', '90d83feede8e96d11d146a5e80adc11a', '55b03c958a2d839de4b6509406f2b0b1', 0, 1),
(58, 'عامر', 'إبراهيم', 'amer', 'amer@mnbaa.com', '90d83feede8e96d11d146a5e80adc11a', '26dd8279752d1a6c4ec3a4d29af1ce3d', 0, 1),
(63, 'Asmaa1', 'Yehia', '562616658', 'eng_asmaa27@yahoo.com', '', '2ad8de73a5dd15aed69e7a0c814339fa', 10152321317239682, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cv_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_cv_name` varchar(255) NOT NULL,
  `marital_status` enum('n','s','m','d') NOT NULL,
  `alter_mail` varchar(255) NOT NULL,
  `adress1` varchar(255) NOT NULL,
  `adress2` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `day` int(11) NOT NULL,
  `month` int(11) NOT NULL,
  `year` varchar(255) NOT NULL,
  `nationality` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `gender` enum('m','f','n') NOT NULL,
  `military` enum('e','c','p','s','d','n') NOT NULL,
  `license` enum('y','no','n') NOT NULL,
  `percent` int(11) NOT NULL,
  `exp_no` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=415 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`id`, `cv_id`, `user_id`, `user_cv_name`, `marital_status`, `alter_mail`, `adress1`, `adress2`, `image`, `day`, `month`, `year`, `nationality`, `title`, `gender`, `military`, `license`, `percent`, `exp_no`) VALUES
(51, 14, 55, 'asas', 's', '', 'sfasdf', 'sdasd', 'Penguins.jpg', 0, 0, '0', 'dvsdg', 'xzd', 'n', 'n', 'n', 15, '0'),
(54, 16, 55, 'adas', 's', '', 'zxdsad', 'sasaff', '0', 0, 0, '0', 'scasd', 'sdasd', '', 'n', '', 12, '0'),
(59, 12, 55, 'aa sss sdasd', 'n', '', 'sdsa', 'sdsa', '0', 0, 0, '0', '', '', '', 'n', 'n', 6, '0'),
(60, 15, 55, 'asaS', 's', '', 'sdasd', 'sdasd', '0', 12, 4, '1988', 'ADSasd', 'sadsa', 'f', 'n', '', 12, '0'),
(123, 17, 55, 'اسماء يحيى', 's', '', 'bilqas', 'bilqas', 'Desert.jpg', 27, 9, '1995', 'مصريه', 'php developer', 'f', 'n', 'y', 15, '0'),
(381, 18, 55, 'asmaa_yahia', 's', 'emailsdasdhere@email.com', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', 'sfscsdfsdf', 'Lighthouse_2.jpg', 6, 10, '1982', 'egyptian', 'php developer', 'm', 'n', 'n', 15, ''),
(382, 19, 55, 'asmaa_yahia', 's', 'emailsdasdhere@email.com', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', '0', 15, 7, '1991', 'egyptian', 'php developer', 'm', 'n', 'n', 12, ''),
(383, 20, 55, 'asmaa yahia shehata', 's', '', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', 'Penguins_6.jpg', 5, 6, '1982', 'egyptian', 'pp developers', 'm', 'n', 'n', 12, ''),
(396, 13, 56, 'hoda hussin', 's', 'hoda.hussin@gmail.com', 'gg', 'gg', '0', 17, 11, '1993', 'egyption', 'php devloper', 'm', 'e', 'n', 12, ''),
(401, 21, 58, 'عامر إبراهيم', 'm', 'ph.amer89@gmail.com', '9 ش اللواء محمود الغازى مدينة مبارك', '', '3amer.png', 5, 7, '1989', 'مصرى', 'المدير التنفيذى', 'm', 'p', 'y', 15, '4'),
(404, 22, 55, 'asmaa', 's', '', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', 'Chrysanthemum_3.jpg', 5, 6, '1978', 'egyptian', 'pp developers', 'f', 'n', 'n', 12, '10 years'),
(407, 11, 55, 'اسماااااء', 's', 'emailsdasdhere@email.com', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', 'المنصوره', 'Lighthouse_3.jpg', 7, 4, '1979', 'egyptian', 'php developer', 'f', 'n', 'n', 15, ''),
(408, 23, 55, 'اسماااااء', 's', 'emailsdasdhere@email.com', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', 'Lighthouse_3.jpg', 8, 9, '1981', 'egyptian', 'php developer', 'f', 'n', 'n', 15, ''),
(410, 24, 55, 'monaaa', 's', 'emailsdasdhere@email.com', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', 'Koala_1.jpg', 10, 7, '1981', 'egyptian', 'php developer', 'm', 's', 'n', 15, ''),
(411, 25, 55, 'a_uuu', 'n', '', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', 'Lighthouse_3.jpg', 15, 9, '1993', 'egyptian', 'hello', 'm', 'p', 'n', 12, ''),
(412, 26, 55, 'انا', 's', 'emailsdasdhere@email.com', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', 'Chrysanthemum_3.jpg', 5, 6, '1980', 'egyptian', 'php developer', 'm', 'e', 'n', 15, ''),
(413, 27, 55, 'اسماااااء', 'n', '', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', '0', 3, 6, '1991', 'egyptian', 'php developer', 'm', 'n', 'n', 9, ''),
(414, 28, 55, 'gfhjvh.bn', 'n', '', 'شارع ساميه الجمل متفرع من احمد ماهر-المنصوره -دقهليه-مصر', '', '0', 15, 4, '1990', 'egyptian', 'php developer', 'm', 'n', 'n', 9, '');

-- --------------------------------------------------------

--
-- Table structure for table `web_thems`
--

CREATE TABLE IF NOT EXISTS `web_thems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `img_ar` varchar(255) NOT NULL,
  `img_en` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `style_name` varchar(255) NOT NULL,
  `parent` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `web_thems`
--

INSERT INTO `web_thems` (`id`, `file_name`, `img_ar`, `img_en`, `color`, `style_name`, `parent`) VALUES
(1, 'web1', '1.jpg', '1.jpg', '', 'web_style1', 0),
(2, 'web2', '1.jpg', '1.jpg', '', 'web_style2', 0),
(3, 'web3', '1.jpg', '1.jpg', '', 'web_style3', 0),
(4, 'web4', '1.jpg', '1.jpg', '', 'web_style4', 0),
(5, 'web5', '1.jpg', '1.jpg', '', 'web_style5', 0),
(6, 'web6', '1.jpg', '1.jpg', '', 'web_style6', 0),
(7, 'web7', '1.jpg', '1.jpg', '', 'web_style7', 0),
(8, 'web8', '1.jpg', '1.jpg', '', 'web_style8', 0),
(9, 'web1', 'bg.png', 'bg.png', '#363636', 'web_blackstyle1', 1),
(10, 'web1', 'circle.png', 'circle.png', '#F222A1', 'web_pinkkstyle1', 1),
(11, 'web1', '1.jpg', '1.jpg', '#00A99D', 'web_style1', 1),
(12, 'web3', 'bg.png', 'bg.png', '#252525', 'web_style3', 3),
(13, 'web3', 'bg.png', 'bg.png', '#448CCB', 'web_bluestyle3', 3),
(14, 'web3', 'bg.png', 'bg.png', '#A806A8', 'web_violetstyle3', 3),
(15, 'web7', '1.jpg', '1.jpg', '#00ccff', 'web_style7', 7),
(16, 'web7', 'bg.png', 'bg.png', '#12e9e1', 'web_greenstyle7', 7),
(17, 'web7', 'circle.png', 'circle.png', '#997aca', 'web_plumstyle7', 7),
(18, 'web2', '1.jpg', '1.jpg', '#404041', 'web_style2', 2),
(19, 'web2', '1.jpg', '1.jpg', '#2E3192', 'web_bluestyle2', 2),
(20, 'web2', '1.jpg', '1.jpg', '#D50078', 'web_foshyaqstyle2', 2),
(21, 'web2', '1.jpg', '1.jpg', '#FF4D4D', 'web_redstyle2', 2),
(22, 'web4', '1.jpg', '1.jpg', '#2DB0E7', 'web_style4', 4),
(23, 'web4', '1.jpg', '1.jpg', '#EF687E', 'web_pinckystyle4', 4),
(24, 'web4', '1.jpg', '1.jpg', '#28C0A1', 'web_greenstyle4', 4),
(25, 'web8', '1.jpg', '1.jpg', '#363636', 'web_style8', 8),
(26, 'web8', '1.jpg', '1.jpg', '#ff0066', 'web_mooovstyle8', 8),
(27, 'web8', '1.jpg', '1.jpg', '#0072ff', 'web_bluestyle8', 8),
(28, 'web5', '1.jpg', '1.jpg', '#f32b2c', 'web_style5', 5),
(29, 'web5', '1.jpg', '1.jpg', '#1f9a39', 'web_greenstyle5', 5),
(30, 'web5', '1.jpg', '1.jpg', '#3399ff', 'web_lbanystyle5', 5);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
