<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo lang("sign_up"); ?></title>
	<!-- The styles -->
	<link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='<?php echo base_url(); ?>assets/css/fullcalendar.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='<?php echo base_url(); ?>assets/css/chosen.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/uniform.default.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/colorbox.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/jquery.cleditor.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/opa-icons.css' rel='stylesheet'>
	<link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
</head>

<body>
		<div class="container-fluid">
		<div class="row-fluid">
		
			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2></h2>
				</div><!--/span-->
			</div><!--/row-->
			
			<div class="row-fluid">
				<div class="well span5 center login-box">
					<div class="alert alert-info">
						<?php echo lang('sign_up') ?>
					</div>
                    <form class="form-horizontal" action="<?php echo site_url(); ?>/backstage/login/add" method="post">
						<fieldset>
							<div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span>
                                <input placeholder="<?php echo lang('full_name') ?>" value="<?php echo set_value('full_name'); ?>" autofocus class="input-large span10" name="full_name" id="full_name" type="text"  />
							</div>
                            <div class="form-msg-error-text"><?php echo form_error('full_name'); ?></div>
							<div class="clearfix"></div>
                            <div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-user"></i></span>
                                <input placeholder="<?php echo lang('email') ?>" value="<?php echo set_value('email'); ?>" autofocus class="input-large span10" name="email" id="email" type="email"  />
							</div>
                            <div class="form-msg-error-text"><?php echo form_error('email'); ?></div>
							<div class="clearfix"></div>
                            <div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span>
                                <input placeholder="<?php echo lang('password') ?>" value="<?php echo set_value('password'); ?>" autofocus class="input-large span10" name="password" id="password" type="password"  />
							</div>
                            <div class="form-msg-error-text"><?php echo form_error('password'); ?></div>
							<div class="clearfix"></div>
                            <div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on"><i class="icon-lock"></i></span>
                                <input placeholder="<?php echo lang('re_password') ?>" value="<?php echo set_value('re_password'); ?>" autofocus class="input-large span10" name="re_password" id="re_password" type="password"  />
							</div>
                            <div class="form-msg-error-text"><?php echo form_error('re_password'); ?></div>
							<div class="clearfix"></div>
							<p class="center span5">
							<button type="submit" class="btn btn-primary"><?php echo lang('save'); ?></button>
							</p>
						</fieldset>
					</form>

				</div><!--/span-->
			</div><!--/row-->
				</div><!--/fluid-row-->
		
	</div><!--/.fluid-container-->

</body>
</html>
