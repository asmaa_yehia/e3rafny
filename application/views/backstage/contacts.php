<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> <?php echo lang("contact"); ?> </h2>
            <?php
            $result = $this->session->flashdata('result');
            if (!empty($result)) {
                ?>
                <h2 class="alert_success">
                <?php echo $result; ?>
                </h2>
<?php } ?>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <div id="DataTables_Table_0_filter" class="dataTables_filter">
            </div>

<?php if (!(count($contacts))) { ?><div class="alert alert-error"> <button class="close" data-dismiss="alert" type="button">×</button><?php echo lang("no_data_found"); ?></div><?php } else { ?>
                <table class="table table-striped table-bordered bootstrap-datatable datatable">
                    <thead>
                        <tr>
                            <th><?php echo lang("name"); ?></th>
                            <th><?php echo lang("address"); ?></th>
                            <th><?php echo lang("options"); ?></th>
                        </tr>
                    </thead>   
                    <tbody>
    <?php foreach ($contacts as $item) { ?>
                            <tr>

                                <td class="center"><?php echo $item->name; ?></td>
                                <td class="center"><?php echo $item->subject; ?></td>
                                <td class="center">

                                    <!--<a class="btn btn-info" href="<?php echo site_url(); ?>/backstage/index/reply/<?php echo $item->id; ?>">
                                        <i class="icon-edit icon-white"></i>  
        <?php echo lang('reply'); ?>                                        
                                    </a>-->
                                    <a class="btn btn-danger" href="<?php echo site_url(); ?>/backstage/index/delete_contact/<?php echo $item->id; ?>" onClick="if (!confirm('<?php echo lang("cofirm_delete_c"); ?>'))
                                                return false;" >
                                        <i class="icon-trash icon-white"></i> 
        <?php echo lang('delete'); ?>
                                    </a>
                                      <a class="btn btn-info" href="<?php echo site_url(); ?>/backstage/index/view_contact/<?php echo $item->id; ?>" >
                                        <i class="icon-edit icon-white"></i> 
        <?php echo lang('view'); ?>
                                    </a>

                                </td>
                            </tr>
    <?php } ?>
                    </tbody>
                </table>  

            <?php } ?>

<?php if (count($contacts)) { ?>
                <div class="box-bt-bar">  
                    <ul class="box-nav">        
    <?php echo $this->pagination->create_links(); ?>
                    </ul>   
                </div>    
<?php } ?>          
        </div>
    </div><!--/span-->

</div><!--/row-->	
