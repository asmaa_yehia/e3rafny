<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> <?php echo lang("viewuser_cvs"); ?> </h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
                    <div id="DataTables_Table_0_filter" class="dataTables_filter">
                    </div>
                   
						<?php if(!(count($resums))){ ?><div class="alert alert-error"> <button class="close" data-dismiss="alert" type="button">×</button><?php echo lang("no_data_found");?></div><?php } else{?>
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th><?php echo lang("cv_title"); ?></th>
								  <th><?php echo lang("user_name"); ?></th>
								  <th><?php echo lang("options"); ?></th>
							  </tr>
						  </thead>   
						  <tbody>
                          <?php foreach($resums as $item){?>
							<tr>
								<td><?php echo $item->title ; ?></td>
								<td class="center"><?php echo $user_cv->user_name ;?></td>
								<td class="center">
									
                                    <a class="btn btn-success" href="<?php echo base_url().$user_cv->user_name."/".$item->url_title ;?> ">
										<i class="icon-file icon-white"></i>  
										<?php echo lang('cv_link'); ?>                                        
									</a>
								</td>
							</tr>
							<?php } ?>
						  </tbody>
					  </table>  
                      
					<?php } ?>
                   
                      <?php if(count($resums)){ ?>
                             <div class="box-bt-bar">  
                             <ul class="box-nav">        
                               <?php echo $this->pagination->create_links(); ?>
                            </ul>   
                             </div>    
                            <?php } ?>          
					</div>
				</div><!--/span-->
</div>
