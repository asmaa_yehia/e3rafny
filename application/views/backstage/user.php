<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th><?php echo lang("name"); ?></th>
								  <th><?php echo lang("email"); ?></th>
                                  <th><?php echo lang("user_name"); ?></th>
                                  <th><?php echo lang("status"); ?></th>
								  <th><?php echo lang("options"); ?></th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td><?php echo $user->first_name ." ".$user->last_name;?></td>
								<td class="center"><?php echo $user->mail ;?></td>
                                <td class="center"><?php echo $user->user_name ;?></td>
                                <td class="center"><?php if($user->status==0){ ?> <span class="label label-important"><?php echo lang("not_active"); ?></span>
							<?php }elseif($user->status==1){ ?><span class="label label-success"><?php echo lang("active"); ?></span> <?php } ?>
                            </td>
								<td class="center">
									<!--<a class="btn btn-success" href="<?php //echo site_url();?>/backstage/index/show/<?php //echo $user->id;?>">
										<i class="icon-zoom-in icon-white"></i>  
										<?php //echo lang('view_details'); ?>                                        
									</a>-->
									<a class="btn btn-info" href="<?php  echo site_url();?>/backstage/index/send_message/<?php echo $user->id;?>">
										<i class="icon-envelope icon-white"></i>  
										<?php echo lang('send_message'); ?>                                         
									</a>
									<a class="btn btn-danger" href="<?php  echo site_url();?>/backstage/index/delete/<?php echo $user->id;?>" onClick="if(!confirm('<?php echo lang("cofirm_deleteuser"); ?>'))return false;" >
										<i class="icon-trash icon-white"></i> 
										<?php echo lang('delete'); ?>
									</a>
                                    <a class="btn btn-success" href="<?php  echo site_url();?>/backstage/index/show_resums/<?php echo $user->id;?>">
										<i class="icon-file icon-white"></i>  
										<?php echo lang('view_cvs'); ?>                                        
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>  
                     
