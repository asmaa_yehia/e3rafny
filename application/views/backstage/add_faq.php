<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i><?php echo lang("faq"); ?></h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <form action="<?php echo site_url(); ?>/backstage/index/add_faq" method="post" enctype="multipart/form-data">
                <fieldset>
                    <legend></legend>

                    <div class="control-group">
                        <label class="control-label" for="typeahead"><?php echo lang('ques'); ?></label>
                        <div class="controls">
                            <input id="disabledInput" name="question" class="input-xlarge" type="text" value="<?php echo set_value('question');?>">
                        </div>
                        <div class="form-msg-error-text"><?php echo form_error('question'); ?></div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="typeahead"><?php echo lang('answer'); ?></label>
                        <div class="controls">
                              <textarea class="cleditor" name="answer"  id="textarea2" rows="3"><?php echo  set_value('answer');?></textarea>
                          <!--  <input id="disabledInput" name="name_ar" class="input-xlarge" type="text"  value="">-->
                        </div>
                        <div class="form-msg-error-text"><?php echo form_error('answer'); ?></div>
                    </div>
                    
                    <!-- ===================== ENGLISH =================== -->
                    <div class="control-group">
                        <label class="control-label" for="typeahead"><?php echo lang('ques_en'); ?></label>
                        <div class="controls">
                            <input id="disabledInput" name="question_en" class="input-xlarge" type="text"  value="<?php echo set_value('question_en');?>">
                        </div>
                        <div class="form-msg-error-text"><?php echo form_error('question_en'); ?></div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="typeahead"><?php echo lang('answer_en'); ?></label>
                        <div class="controls">
                              <textarea class="cleditor" name="answer_en"  id="textarea2" rows="3"><?php set_value('answer_en');?></textarea>
                          <!--  <input id="disabledInput" name="name_ar" class="input-xlarge" type="text"  value="">-->
                        </div>
                        <div class="form-msg-error-text"><?php echo form_error('answer_en'); ?></div>
                    </div>
                    <div class="form-actions">

                        <input type="submit" name="button" id="button" class="btn btn-primary" value="<?php echo lang('save'); ?>" />
                    </div>
                </fieldset>
            </form>   

        </div>
    </div><!--/span-->

</div><!--/row-->
