  <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i><?php echo lang("add_qualification"); ?></h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action="<?php echo site_url(); ?>/backstage/index/add_qualification" method="post" enctype="multipart/form-data">
						  <fieldset>
							<legend></legend>
                            
							<div class="control-group">
							  <label class="control-label" for="typeahead"><?php echo lang('name_en'); ?></label>
							  <div class="controls">
                               <input id="disabledInput" name="name_en" class="input-xlarge" type="text"  value="">
							  </div>
                              <div class="form-msg-error-text"><?php echo form_error('name_en'); ?></div>
							</div>
                            
                            
							<div class="control-group">
							  <label class="control-label" for="typeahead"><?php echo lang('name_ar'); ?></label>
							  <div class="controls">
                               <input id="disabledInput" name="name_ar" class="input-xlarge" type="text"  value="">
							  </div>
                              <div class="form-msg-error-text"><?php echo form_error('name_ar'); ?></div>
							</div>
                            <div class="form-actions">
								
								<input type="submit" name="button" id="button" class="btn btn-primary" value="<?php echo lang('save'); ?>" />
							  </div>
							</fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->
