  <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i><?php echo lang("user_data"); ?></h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form class="form-horizontal">
						  <fieldset>
							<legend></legend>
                            <?php if(!empty($user->first_name)){ ?>
							<div class="control-group">
							  <label class="control-label" for="typeahead"><?php echo lang('first_name'); ?></label>
							  <div class="controls">
                               <input id="disabledInput" class="input-xlarge disabled" type="text" disabled="" value="<?php echo $user->first_name; ?>">
							  </div>
							</div>
                            <?php } ?>
                            <?php if(!empty($user->last_name)){ ?>
							<div class="control-group">
							  <label class="control-label" for="date01"><?php echo lang('last_name'); ?></label>
							  <div class="controls">
                            <input id="disabledInput" class="input-xlarge disabled" type="text" disabled="" value="<?php echo $user->last_name; ?>">
							  </div>
							</div>
							<?php } ?>
                            <?php if(!empty($user->mail)){ ?>
							<div class="control-group">
							  <label class="control-label" for="date01"><?php echo lang('email'); ?></label>
							  <div class="controls">
                             <input id="disabledInput" class="input-xlarge disabled" type="text" disabled="" value="<?php echo $user->mail; ?>">
							  </div>
							</div>
                            <?php } ?>        
							<?php if(!empty($user->user_name)){ ?>
							<div class="control-group">
							  <label class="control-label" for="date01"><?php echo lang('user_name'); ?></label>
							  <div class="controls">
                             <input id="disabledInput" class="input-xlarge disabled" type="text" disabled="" value="<?php echo $user->user_name; ?>">
							  </div>
							</div>
                            <?php } ?>   
							<div class="control-group">
							  <label class="control-label" for="date01"><?php echo lang('status'); ?></label>
							  <div class="controls">
                             <?php if($user->status==0){ ?> <span class="label label-important"><?php echo lang("not_active"); ?></span>
							<?php }elseif($user->status==1){ ?><span class="label label-success"><?php echo lang("active"); ?></span> <?php } ?>
							  </div>
							</div>        
                         </fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->