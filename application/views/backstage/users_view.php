<div class="row-fluid sortable">		
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-user"></i> <?php echo lang("users_view"); ?> </h2>
            <?php $result = $this->session->flashdata('result');
            if (!empty($result)) {
                ?>
                <h2 class="alert_success">
                <?php echo $result; ?>
                </h2>
<?php } ?>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
            <div id="DataTables_Table_0_filter" class="dataTables_filter">
                <label>
<?php echo lang("search") . ":"; ?>
                    <input name="name" id="id_input" placeholder="<?php echo lang("type_username"); ?>" type="text" onKeyUp="lookup(this.value)" autocomplete="off">
                    <div id="suggestions">
                        <div class="autoSuggestionsList_l" id="autoSuggestionsList">    
                        </div>
                    </div>

                </label>
            </div>
            <div id="send_msg">
                <a href="<?php echo site_url(); ?>/backstage/index/send_usersmessages"><?php echo lang("send_message_allusers"); ?></a>
            </div>
            <div id="result">
<?php if (!(count($users))) { ?><div class="alert alert-error"> <button class="close" data-dismiss="alert" type="button">×</button><?php echo lang("no_data_found"); ?></div><?php } else { ?>
                    <table class="table table-striped table-bordered bootstrap-datatable datatable">
                        <thead>
                            <tr>
                                <th><?php echo lang("name"); ?></th>
                                <th><?php echo lang("email"); ?></th>
                                <th><?php echo lang("user_name"); ?></th>
                                <th><?php echo lang("status"); ?></th>
                                <th><?php echo lang("options"); ?></th>
                            </tr>
                        </thead>   
                        <tbody>
    <?php foreach ($users as $item) { ?>
                                <tr>
                                    <td><?php echo $item->first_name . " " . $item->last_name; ?></td>
                                    <td class="center"><?php echo $item->mail; ?></td>
                                    <td class="center"><?php echo $item->user_name; ?></td>
                                    <td class="center"><?php if ($item->status == 0) { ?> <span class="label label-important"><?php echo lang("not_active"); ?></span>
        <?php } elseif ($item->status == 1) { ?><span class="label label-success"><?php echo lang("active"); ?></span> <?php } ?>
                                    </td>
                                    <td class="center">
                                            <!--<a class="btn btn-success" href="<?php //echo site_url(); ?>/backstage/index/show/<?php //echo $item->id; ?>">
                                                    <i class="icon-zoom-in icon-white"></i>  
        <?php //echo lang('view_details');  ?>                                        
                                            </a>-->
                                        <a class="btn btn-info" href="<?php echo site_url(); ?>/backstage/index/send_message/<?php echo $item->id; ?>">
                                            <i class="icon-envelope icon-white"></i>  
        <?php echo lang('send_message'); ?>                                         
                                        </a>
                                        <a class="btn btn-danger" href="<?php echo site_url(); ?>/backstage/index/delete/<?php echo $item->id; ?>" onClick="if (!confirm('<?php echo lang("cofirm_deleteuser"); ?>'))
                                                    return false;" >
                                            <i class="icon-trash icon-white"></i> 
        <?php echo lang('delete'); ?>
                                        </a>
                                        <a class="btn btn-success" href="<?php echo site_url(); ?>/backstage/index/show_resums/<?php echo $item->id; ?>">
                                            <i class="icon-file icon-white"></i>  
        <?php echo lang('view_cvs'); ?>                                        
                                        </a>
                                    </td>
                                </tr>
    <?php } ?>
                        </tbody>
                    </table>  

            <?php } ?>
            </div>
                    <?php if (count($users)) { ?>
                <div class="box-bt-bar">  
                    <ul class="box-nav">        
                <?php echo $this->pagination->create_links(); ?>
                    </ul>   
                </div>    
<?php } ?>          
        </div>
    </div><!--/span-->

</div><!--/row-->	
<script>
    function lookup(inputString) {
        if (inputString.length == 0) {
            $('#suggestions').hide();
        } else {
            $.post("<?php echo site_url(); ?>/backstage/index/autocomplete/", {queryString: "" + inputString + ""}, function(data) {
                if (data.length > 0) {
                    $('#suggestions').show();
                    $('#autoSuggestionsList').html(data);
                }
            });
        }
    }

    function fill(id) {
        //$('#id_input').val(f_name+" "+l_name);
        var request = $.ajax({
            type: "POST",
            url: "<?php echo site_url("backstage/index/get_username/"); ?>",
            data: "data=" + id,
            cache: false,
            async: false,
            dataType: "json",
            success: function(result) {
                $('#id_input').val(result);

            },
        });
        setTimeout("$('#suggestions').hide();", 200);
        $("#result").load("<?php echo site_url(); ?>/backstage/index/get_user/" + id);
    }
</script>
