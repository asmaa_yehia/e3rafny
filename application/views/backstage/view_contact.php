<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i><?php echo lang("contact"); ?></h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        <div class="box-content">
       
                    <div class="control-group">
                        <label class="control-label" for="typeahead"><?php echo lang('address'); ?></label>
                        <div class="controls">
                            <input id="disabledInput" name="subject" class="input-xlarge" type="text"  value="<?php echo $contact->subject;?>" readonly="true">
                        </div>
                        <div class="form-msg-error-text"><?php echo form_error('subject'); ?></div>
                    </div>


                    <div class="control-group">
                        <label class="control-label" for="typeahead"><?php echo lang('message'); ?></label>
                        <div class="controls">
                            <textarea  name="message"  id="textarea2" rows="3" disabled><?php echo $contact->message;?></textarea>
                          <!--  <input id="disabledInput" name="name_ar" class="input-xlarge" type="text"  value="">-->
                        </div>
                        <div class="form-msg-error-text"><?php echo form_error('message'); ?></div>
                    </div>
                    <div class="form-actions">

                        <a href="<?php echo site_url(); ?>/backstage/index/reply/<?php echo $contact->id; ?>" type="button" name="button" id="button" class="btn btn-primary" ><?php echo lang('reply'); ?></a>
                    </div>
                </fieldset>
          

    </div>
    </div><!--/span-->

</div><!--/row-->
