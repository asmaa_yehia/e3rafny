  <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i><?php echo lang("edit_education_type"); ?></h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action="<?php echo site_url(); ?>/backstage/index/edit_education_type/<?php echo $types->id ?>" method="post" enctype="multipart/form-data">
						  <fieldset>
							<legend></legend>
                            
							<div class="control-group">
							  <label class="control-label" for="typeahead"><?php echo lang('type_en'); ?></label>
							  <div class="controls">
                               <input id="disabledInput" name="type_en" class="input-xlarge" type="text"  value="<?php echo $types->type_en ?>">
							  </div>
                              <div class="form-msg-error-text"><?php echo form_error('type_en'); ?></div>
							</div>
                            
                            
							<div class="control-group">
							  <label class="control-label" for="typeahead"><?php echo lang('type_ar'); ?></label>
							  <div class="controls">
                               <input id="disabledInput" name="type_ar" class="input-xlarge" type="text"  value="<?php echo $types->type_ar ?>">
							  </div>
                              <div class="form-msg-error-text"><?php echo form_error('type_ar'); ?></div>
							</div>
                            <div class="form-actions">
								
								<input type="submit" name="button" id="button" class="btn btn-primary" value="<?php echo lang('save'); ?>" />
							  </div>
							</fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->
