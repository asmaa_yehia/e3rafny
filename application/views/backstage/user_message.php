  <div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i><?php echo lang("senduser_msg"); ?></h2>
						<div class="box-icon">
							<a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
							<a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
							<a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<form action="<?php echo site_url(); ?>/backstage/index/send_message/<?php echo $user_id ; ?>" method="post" enctype="multipart/form-data">
						  <fieldset>
							<legend></legend>
                            <input type="hidden" value="<?php echo $user_info->id ; ?>" id="user_id" name="user_id"  />
							<div class="control-group">
							  <label class="control-label" for="typeahead"><?php echo lang('title'); ?></label>
							  <div class="controls">
                               <input id="disabledInput" name="title" class="input-xlarge" type="text"  value="">
							  </div>
                              <div class="form-msg-error-text"><?php echo form_error('title'); ?></div>
							</div>
                            
                            
							<div class="control-group">
							  <label class="control-label" for="date01"><?php echo lang('message'); ?></label>
							  <div class="controls">
                            <textarea class="cleditor" name="message"  id="textarea2" rows="3"></textarea>
							  </div>
                              <div class="form-msg-error-text"><?php echo form_error('message'); ?></div>
							</div>
                            <div class="form-actions">
								
								<input type="submit" name="button" id="button" class="btn btn-primary" value="<?php echo lang('send'); ?>" />
							  </div>
							</fieldset>
						</form>   

					</div>
				</div><!--/span-->

			</div><!--/row-->
