<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo lang("admin_panel"); ?></title>
        <!-- The styles -->
        <link id="bs-css" href="<?php echo base_url(); ?>assets/css/bootstrap-cerulean.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="<?php echo base_url(); ?>assets/css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/charisma-app.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='<?php echo base_url(); ?>assets/css/fullcalendar.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='<?php echo base_url(); ?>assets/css/chosen.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/uniform.default.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/colorbox.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.cleditor.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/noty_theme_default.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/elfinder.min.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/elfinder.theme.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/opa-icons.css' rel='stylesheet'>
        <link href='<?php echo base_url(); ?>assets/css/uploadify.css' rel='stylesheet'>
    </head>

    <body>
        <!-- topbar starts -->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="<?php echo site_url("backstage/index/index/") ?>"> <span>Mnbaa</span></a>
                    <!-- user dropdown starts -->
                    <div class="btn-group pull-right" >
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user"></i><span class="hidden-phone"><?php echo lang("admin"); ?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li><a href="<?php echo site_url("backstage/login/logout/") ?>"><?php echo lang("logout"); ?></a></li>

                        </ul>
                    </div>
                    <!-- user dropdown ends -->

                    <!-- user dropdown starts -->
                    <div class="btn-group pull-right" >
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-tint"></i><span class="hidden-phone"><?php echo lang("change_lang"); ?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">

                            <li><a href="<?php
                                if (LANG == 'en')
                                    echo base_url() . 'ar/backstage/index';
                                else
                                    echo base_url() . 'en/backstage/index';
                                ?>"><?php
                                       if (LANG == 'en')
                                           echo lang("arabic");
                                       else
                                           echo lang("english");
                                       ?></a></li>
                        </ul>
                    </div>
                    <!-- user dropdown ends -->

                    <div class="top-nav nav-collapse">
                        <ul class="nav">
                            <li></li>
                            <li>

                            </li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <!-- topbar ends -->
        <div class="container-fluid">
            <div class="row-fluid">

                <!-- left menu starts -->
                <div class="span2 main-menu-span">
                    <div class="well nav-collapse sidebar-nav">
                        <ul class="nav nav-tabs nav-stacked main-menu">
                            <li class="nav-header hidden-tablet"></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/index/") ?>"><i class="icon-home"></i><span class="hidden-tablet"> <?php echo lang('home'); ?></span></a></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/show_admins/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('admins_view'); ?></span></a></li>
                            <li class="nav-header hidden-tablet"><?php echo lang('qualifications'); ?></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/add_qualification/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('add_qualification'); ?></span></a></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/show_qualifications/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('show_qualifications'); ?></span></a></li>
                            <li class="nav-header hidden-tablet"><?php echo lang('education_type'); ?></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/add_education_type/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('add_education_type'); ?></span></a></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/show_education_type/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('show_education_type'); ?></span></a></li>
                             <li class="nav-header hidden-tablet"><?php echo lang('pages'); ?></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/show_contact/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('contact'); ?></span></a></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/show_faq/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('faq'); ?></span></a></li>
                            <li><a class="ajax-link" href="<?php echo site_url("backstage/index/add_faq/") ?>"><i class="icon-align-justify"></i><span class="hidden-tablet"> <?php echo lang('add_faq'); ?></span></a></li>
                            
                        </ul>
                        <label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"><?php echo lang('menu_ajax'); ?></label>
                    </div><!--/.well -->
                </div><!--/span-->
                <!-- left menu ends -->

                <noscript>
                <div class="alert alert-block span10">
                    <h4 class="alert-heading">Warning!</h4>
                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                </div>
                </noscript>

                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <a href="<?php echo site_url("backstage/index/index/") ?>"><?php echo lang('home'); ?></a> <span class="divider">/</span>
                            </li>
                            <li>
                                <a href="<?php echo site_url(); ?>/backstage/<?php echo $menu_link; ?>"><?php echo $menu_item; ?></a>
                            </li>
                        </ul>
                    </div>
                    {yield}
                    <!-- content ends -->
                </div><!--/#content.span10-->
            </div><!--/fluid-row-->

            <hr>

            <div class="modal hide fade" id="myModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary">Save changes</a>
                </div>
            </div>

            <footer>
                <p class="pull-left"></p>
                <p class="pull-right"></p>
            </footer>

        </div><!--/.fluid-container-->

        <!-- external javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- jQuery -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery-1.7.2.min.js"></script>
        <!-- jQuery UI -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery-ui-1.8.21.custom.min.js"></script>
        <!-- transition / effect library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-transition.js"></script>
        <!-- alert enhancer library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-alert.js"></script>
        <!-- modal / dialog library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-modal.js"></script>
        <!-- custom dropdown library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-dropdown.js"></script>
        <!-- scrolspy library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-scrollspy.js"></script>
        <!-- library for creating tabs -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-tab.js"></script>
        <!-- library for advanced tooltip -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-tooltip.js"></script>
        <!-- popover effect library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-popover.js"></script>
        <!-- button enhancer library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-button.js"></script>
        <!-- accordion library (optional, not used in demo) -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-collapse.js"></script>
        <!-- carousel slideshow library (optional, not used in demo) -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-carousel.js"></script>
        <!-- autocomplete library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-typeahead.js"></script>
        <!-- tour library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/bootstrap-tour.js"></script>
        <!-- library for cookie management -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.cookie.js"></script>
        <script src='<?php echo base_url(); ?>assets/js_folder/fullcalendar.min.js'></script>

        <!-- data table plugin -->
        <script src='<?php echo base_url(); ?>assets/js_folder/jquery.dataTables.min.js'></script>

        <!-- select or dropdown enhancer -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.chosen.min.js"></script>
        <!-- checkbox, radio, and file input styler -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.uniform.min.js"></script>
        <!-- plugin for gallery image view -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.colorbox.min.js"></script>
        <!-- rich text editor library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.cleditor.min.js"></script>
        <!-- file manager library -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.elfinder.min.js"></script>
        <!-- star rating plugin -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.raty.min.js"></script>
        <!-- for iOS style toggle switch -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.iphone.toggle.js"></script>
        <!-- autogrowing textarea plugin -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.autogrow-textarea.js"></script>
        <!-- multiple file upload plugin -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.uploadify-3.1.min.js"></script>
        <!-- history.js for cross-browser state change on ajax -->
        <script src="<?php echo base_url(); ?>assets/js_folder/jquery.history.js"></script>
        <!-- application script for Charisma demo -->
        <script src="<?php echo base_url(); ?>assets/js_folder/charisma.js"></script>


        <script>
            (function(i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function() {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-43924505-1', 'templatehat.com');
            ga('send', 'pageview');

        </script>
    </body>
</html>
