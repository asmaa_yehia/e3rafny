<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Yield_content {

    function doYield() {

        $CI = & get_instance();
        $output = $CI->output->get_output();

        /* Set the default layout file */

        if (!isset($CI->layout))
            $CI->layout = 'default';

        if (!preg_match('/(.+).php$/', $CI->layout)) {
            $CI->layout .= '.php';
        }


        if (LOCATION == "BACKSTAGE")
            $requested = 'application/views/backstage/layout/' . $CI->layout;
        else {
            $requested = 'styles/' . STYLE . '/layout/' . $CI->layout;
            // cache_page();
            }
         
        // If theres been a request for layout and the file exists
        // replace the dafault layout file
        if (file_exists($requested)) {
            $layoutFile = $requested;
        }


        $layout = $CI->load->file($layoutFile, true);
        $view = str_replace("{yield}", $output, $layout);
        
        $CI->output->_display($view);
    }

}

/* End of file Yield.php */
/* Location: ./application/hooks/Yield.php */