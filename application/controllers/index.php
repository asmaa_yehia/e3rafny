<?php
class index extends CI_Controller {

    function __construct() {
        parent:: __construct();
        if (!$this->user->login())
            redirect("auth/show_home");
           // redirect("auth/index");
        $this->load->model('recommend');
        $this->load->model('cv_recommendation');
        $this->load->model('messages');
        $this->load->model('cv');
        $this->load->model('admins');
        $this->load->model('pdf_thems');
        $this->load->model('contact');
        $this->load->model('faq');
        $this->load->helper('share');
        $this->load->helper('text');
        $this->lang->load("user");
        $this->lang->load("cv");
        $this->current_lang = LANG;

        $user_id = $this->session->userdata('userid');
        $this->user->id = $user_id;
        $this->data['user_status'] = $this->user->get();
        $this->user->clear();
        if (empty($this->data['user_status'])) {
            showx_404();
        }
        $this->layout = "cv";
    }

    function index() {

        $user_id = $this->session->userdata('userid');
        $this->cv->user_id = $user_id;
        $this->data['cv'] = $this->cv->get();
        $this->cv->clear();
        if (!$this->data['cv'])
            redirect("add_cv/index");
        else
            redirect("index/home");
          // redirect("index/show_home");
    }

    
    function home() {

        $id = $this->session->userdata('userid');
        $this->user->id = $id;
        $this->load->library("form_validation");

        $this->form_validation->set_rules('first_name', lang('first_name'), 'required');
        $this->form_validation->set_rules('last_name', lang('lang_name'), 'required');
        $this->form_validation->set_rules('user_name', lang('user_name'), 'required|callback_username_check|trim|is_unique[user.user_name.id.' . $this->input->post("id") . ']');
        $this->form_validation->set_rules('password', lang('password'), 'matches[re_password]');
        $this->form_validation->set_rules('re_password', lang('re_password'), 'matches[password]');

        if ($this->form_validation->run() == TRUE) {

            if ($this->input->post("password") != '') {
                $this->user->password = md5($this->input->post("password"));
                $pass = md5($this->input->post("password"));
            } else {
                $this->user->id = $this->input->post("id");
                $this->data['users'] = $this->user->get();

                $this->user->password = $this->data['users']->password;
                $pass = $this->data['users']->password;
                $this->user->clear();
            }
            $this->user->id = $id;
            $this->user->first_name = $this->input->post("first_name");
            $this->user->last_name = $this->input->post("last_name");
            $this->user->user_name = $this->input->post("user_name");
            $this->user->password = $pass;

            $this->user->save();
            $this->user->clear();
        }
        $this->user->id = $id;
        $this->data['user'] = $this->user->get();
        $this->user->clear();

        $this->user->id = $id;
        $this->data['users'] = $this->user->get();
        $this->cv->user_id = $id;
        $this->cv->order_by = array("id" => "DESC");
        $this->data['items'] = $this->cv->get();
        $this->cv->clear();
        $this->user->clear();
        $folder_name = 'u_' . $id;
        $this->data['folder_name'] = $folder_name;
        foreach ($this->data['items'] as $item) {
            $this->pdf_thems->order_by = array("id" => "ASC");
            $this->pdf_thems->limit = "1";
            $this->pdf_thems->select = array("pdf_thems.*", "pdf_thems.img_" . $item->lang . " as img");
            $this->data['pdf_themsdefault'] = $this->pdf_thems->get();
            $this->pdf_thems->clear();
        }

        $this->load->view("home", $this->data);
    }

    function notifcations() {
        $this->layout = "ajax";
        $user_id = $this->session->userdata('userid');
        //get count of none seen refrences
        $this->recommend->user_id = $user_id;
        $this->recommend->seen = '0';
        $this->recommend->order_by = array("id" => "DESC");
        $this->data['recommend'] = $this->recommend->get();
        $this->data['count'] = count($this->data['recommend']);
        $this->recommend->clear();
        $this->recommend->user_id = $user_id;
        $this->data['all_recommends'] = $this->recommend->get();
        $this->data['all_count'] = count($this->data['all_recommends']);
        $this->recommend->clear();

        $this->recommend->user_id = $user_id;
        $this->recommend->limit = notifcations_limit;
        $this->recommend->order_by = array("seen" => "ASC", "id" => "DESC");
        $this->data['all_recommend'] = $this->recommend->get();
        //$this->data['all_count'] = count($this->data['all_recommend']);
        $this->recommend->clear();


        $this->load->view("notifcations", $this->data);
    }

    function all_recommendation() {

        $user_id = $this->session->userdata('userid');
        $this->recommend->user_id = $user_id;
        $this->recommend->order_by = array("seen" => "ASC", "id" => "DESC");
        $this->data['recommends'] = $this->recommend->get();
        $this->recommend->clear();

        $this->load->view("all_recommends", $this->data);
    }

    function view_recommendation($id = false) {
        $user_id = $this->session->userdata('userid');
        if (!$id)
            showx_404();
        $this->recommend->user_id = $user_id;
        $this->recommend->id = $id;
        $this->data['isrecommend'] = $this->recommend->get();
        if (!$this->data['isrecommend'])
            showx_404();
        $this->recommend->clear();

        $this->recommend->id = $id;
        $this->recommend->seen = "1";
        $this->recommend->save();
        $this->recommend->clear();

        //$this->recommend->user_id = $user_id;
        $this->recommend->id = $id;
        $this->recommend->select = array("recommend.*", "cv.title as cv_name", "cv.url_title as url_title");
        $this->recommend->join = array("cv" => "cv.id = recommend.cv");
        $this->data['recommend_row'] = $this->recommend->get();

        $this->recommend->clear();
        //echo $this->db->last_query();

        $this->user->id = $user_id;
        $this->data['user_data'] = $this->user->get();
        $this->user->clear();

        $this->load->view("view_recommend", $this->data);
    }

    function publish($id = false) {
        if (!$id)
            showx_404();
        $this->recommend->id = $id;
        $this->data['isrecommend'] = $this->recommend->get();
        if (!$this->data['isrecommend'])
            showx_404();
        $this->recommend->clear();

        $this->recommend->id = $id;
        $this->recommend->status = "1";
        $this->recommend->save();
        $this->recommend->clear();
        $this->session->set_flashdata('result', lang('done'));
        redirect("index/all_recommendation");
    }

    function delete($id = false) {
        if (!$id)
            showx_404();
        $this->recommend->id = $id;
        $this->data['isrecommend'] = $this->recommend->get();
        if (!$this->data['isrecommend'])
            showx_404();
        $this->recommend->clear();

        $this->recommend->id = $id;
        $this->recommend->delete();
        $this->recommend->clear();
        $this->session->set_flashdata('result', lang('done'));
        redirect("index/all_recommendation");
    }

    function messages() {
        $this->layout = "ajax";
        $user_id = $this->session->userdata('userid');

        $this->messages->user_id = $user_id;
        $this->messages->seen = '0';
        $this->messages->order_by = array("id" => "DESC");
        $this->data['messages'] = $this->messages->get();
        $this->data['count_msg'] = count($this->data['messages']);
        $this->messages->clear();

        $this->messages->user_id = $user_id;
        $this->data['all_messagess'] = $this->messages->get();
        $this->data['all_count'] = count($this->data['all_messagess']);
        $this->messages->clear();

        $this->messages->user_id = $user_id;
        $this->messages->limit = notifcations_limit;
        $this->messages->order_by = array("seen" => "ASC", "id" => "DESC");
        $this->data['all_messages'] = $this->messages->get();
        //$this->data['all_count'] = count($this->data['all_messages']);
        $this->messages->clear();

        $this->load->view("messages", $this->data);
    }

    function all_messages() {

        $user_id = $this->session->userdata('userid');
        $this->messages->user_id = $user_id;
        $this->messages->seen = '1';
        $this->messages->order_by = array("id" => "DESC");
        $this->data['all_seen_msgs'] = $this->messages->get();
        $this->messages->clear();
        //echo $this->db->last_query();

        $user_id = $this->session->userdata('userid');
        $this->messages->user_id = $user_id;
        $this->messages->seen = '0';
        $this->messages->order_by = array("id" => "DESC");
        $this->data['all_nonseen_msgs'] = $this->messages->get();
        $this->messages->clear();
        //echo $this->db->last_query();

        $this->load->view("all_messages", $this->data);
    }

    function view_message($id = false) {
        $user_id = $this->session->userdata('userid');
        if (!$id)
            showx_404();
        $this->messages->user_id = $user_id;
        $this->messages->id = $id;
        $this->data['ismessages'] = $this->messages->get();
        if (!$this->data['ismessages'])
            showx_404();
        $this->messages->clear();

        $this->messages->id = $id;
        $this->messages->seen = "1";
        $this->messages->save();
        $this->messages->clear();

        $this->messages->user_id = $user_id;
        $this->messages->id = $id;
        $this->messages->select = array("messages.*", "admins.*");
        $this->messages->join = array("admins" => "admins.id = messages.admin_id");
        $this->data['message_row'] = $this->messages->get();
        $this->messages->clear();

        $this->load->view("view_message", $this->data);
    }

    function get_cvs($id) {

        $this->layout = "ajax";
        $user_id = $this->session->userdata('userid');
        $this->cv_recommendation->recomnd_id = $id;
        $this->data['cvs_recomnnd'] = $this->cv_recommendation->get();
        $this->cv_recommendation->clear();
        /* if(count($this->data['cvs_recomnnd'])==0){
          $this->cv->select = array("cv.*","cv.id as cv_id");
          $this->cv->user_id = $user_id;
          $this->cv->order_by = array("id" => "DESC");
          $this->data['cvs'] = $this->cv->get();

          }else{ */
        /* $this->cv->select = array("cv.*", "cv_recommendation.*","cv.id as cv_id");
          $this->cv->join_type="left";
          $this->cv->join = array("cv_recommendation" => "cv_recommendation.cv = cv.id");
          $this->cv->user_id = $user_id;
          $this->cv->where =array("cv_recommendation.recomnd_id" => $id);
          $this->cv->or_where =array("cv_recommendation.recomnd_id" => NULL);
          $this->cv->group =array("cv");
          $this->cv->order_by = array("cv.id" => "DESC");
          $this->data['cvs'] = $this->cv->get(); */
        $this->cv->select = array("cv.*", "cv.id as cv_id");
        $this->cv->user_id = $user_id;
        $this->cv->order_by = array("id" => "DESC");
        $this->data['cvs'] = $this->cv->get();


        $newsendlogs = array();

        foreach ($this->data['cvs'] as $item) {
            $newdata = array();
            $newdata['sendlogs'] = $item;

            $this->cv_recommendation->cv = $item->id;
            $this->cv_recommendation->recomnd_id = $id;
            $newdata['cv_recom'] = $this->cv_recommendation->get();
            $newsendlogs[] = $newdata;
        }
        $this->data['query'] = $newsendlogs;
        $this->cv->clear();

        $this->data['id'] = $id;
        $this->load->view("all_cvs", $this->data);
    }

    function approve() {
        $cvs = $this->input->post('checkbox');
        $id = $this->input->post('recomnd_id');
        $this->cv_recommendation->recomnd_id = $id;
        $this->data['recommended'] = $this->cv_recommendation->get();
        $this->cv_recommendation->clear();
        if (count($this->data['recommended']) != 0) {
            $this->cv_recommendation->recomnd_id = $id;
            $this->cv_recommendation->delete();
        }
        $this->recommend->id = $id;
        $this->recommend->status = "1";
        $this->recommend->save();
        $this->recommend->clear();
        foreach ($cvs as $item) {

            $this->cv_recommendation->cv = $item;
            $this->cv_recommendation->recomnd_id = $id;
            $this->cv_recommendation->save();
            //echo $this->db->last_query();
        }
        $this->session->set_flashdata('result', lang('done'));
        redirect("index/all_recommendation");
    }

    function username_check($str) {

        $allowed = array(".", "-", "_");
        if ((ctype_alnum(str_replace($allowed, '', $str))) && ctype_alpha($str[0])) {
            return TRUE;
        } else {
            $this->form_validation->set_message('username_check', lang("user_nameerror"));
            return FALSE;
        }
    }

    function delete_cv($id = false) {
        if (!$id)
            showx_404();
        $this->cv->id = $id;
        $this->data['iscv'] = $this->cv->get();
        if (!$this->data['iscv'])
            showx_404();
        $this->cv->clear();
        $this->cv->id = $id;
        $this->cv->del_all($id);
        $this->cv->delete();
        $this->cv->clear();
        $this->session->set_flashdata('result', lang('done'));
        redirect("index/home");
    }

    function test() {
        $this->layout = "ajax";
        $path = 'assets/u_27/file.pdf';
        $path2 = 'assets/u_27/page';
        $this->layout = "ajax";
        exec("convert -density 300 " . $path . "[0] " . $path2 . ".jpg");
    }

    function about() {
        $this->load->view('about', $this->data);
    }

    //
    function contact($msg = false) {
        $this->load->library("form_validation");
        $this->load->helper('set_value');
        if ($_POST) {
            $this->form_validation->set_rules('name', lang('name'), 'trim|required|xss_clean');
            $this->form_validation->set_rules('mail', lang('mail'), 'trim|required|valid_email');
            //$this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
            $this->form_validation->set_rules('message', lang('message'), 'trim|required|xss_clean');
            if ($this->form_validation->run() == TRUE) {
                $this->contact->name = $this->input->post('name');
                $this->contact->phone = $this->input->post('phone');
                $this->contact->mail = $this->input->post('mail');
                $this->contact->subject = $this->input->post('subject');
                $this->contact->message = $this->input->post('message');
                $this->contact->save();
                //  $this->load->view('contact');
                redirect('index/contact/sucess');
            } else {
                $this->load->view('contact', $this->data);
            }
        } else {
            if ($msg) {
                $this->data['msg'] = "sucess";
            }
            $this->load->view('contact', $this->data);
        }
    }

    function faq($offset = 0) {

        $search = $this->input->post('search');
        if (isset($search)) {
            $this->faq->or_like = array('answer' => $search, 'question' => $search);
        }
        //$this->data['faq_result'] = $this->faq->get();
        $this->faq->where = array("status !=" => 0);
        $this->session->set_flashdata('offset', $offset);
        $this->faq->order_by = array("id" => "desc");
        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("index/faq"),
            "total_rows" => $this->faq->get(true),
            "per_page" => 5,
            "uri_segment" => 4,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->faq->limit = $config['per_page'];
        $this->faq->offset = $offset;
        //
        $this->data['faq'] = $this->faq->get();
        //echo $this->db->last_query();
        $this->faq->clear();
        $this->load->view('faq', $this->data);
    }

    function search_faq($pure_search = '', $offset = 0) {

        $this->faq->clear();
        $search = (urldecode(trim($pure_search)) == 1 && $pure_search == 1) ? '' : urldecode(trim($pure_search));
        $this->layout = "ajax";
        if (strlen($search) > 0) {
            $this->faq->or_like = (LANG == 'ar') ? array('answer' => $search, 'question' => $search) : array('answer_en' => $search, 'question_en' => $search);
            $this->faq->where = array("status !=" => 0);
            $this->faq->order_by = array("id" => "desc");
            $this->load->library("pagination");
            $config = array(
                "base_url" => site_url("index/search_faq/" . $search),
                "total_rows" => $this->faq->get(true),
                "per_page" => 5,
                "uri_segment" => 5,
                "num_links" => 2,
            );
            $this->pagination->initialize($config);
            $this->faq->limit = $config['per_page'];
            $this->faq->offset = $offset;
            $this->data['faq'] = $this->faq->get();
            $this->data['search'] = $search;

            $this->faq->clear();
            if (empty($this->data['faq'])) {
                echo "لايوجد نتائج بحث حول " . " <span class='highlighted'> - " . $search . " - </span>";
            } else {
                $this->load->view('faq_result', $this->data);
            }
        } else {
            $this->faq->where = array("status !=" => 0);
            $this->faq->order_by = array("id" => "desc");
            $this->load->library("pagination");
            $config = array(
                "base_url" => site_url("index/search_faq/" . $search),
                "total_rows" => $this->faq->get(true),
                "per_page" => 5,
                "uri_segment" => 5,
                "num_links" => 2,
            );
            $this->pagination->initialize($config);
            $this->faq->limit = $config['per_page'];
            $this->faq->offset = $offset;
            $this->data['faq'] = $this->faq->get();
            $this->data['search'] = $search;
            $this->faq->clear();
            $this->load->view('faq_result', $this->data);
        }
    }

}
