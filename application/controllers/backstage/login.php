<?php

class login extends CI_Controller {
	var $layout = 'ajax';
    function __construct() {
        parent:: __construct();
		$this->load->helper('url');
        $this->load->helper('form');
		$this->load->model('admin_model');
		$this->lang->load("user");
		$this->current_lang=LANG;
        
    }
	
	function index() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("mail", lang('email'), "trim|required|valid_email");
        $this->form_validation->set_rules("password", lang('password'), "trim|required");

        if ($this->form_validation->run()) {
            $this->admins->mail = $this->input->post('mail');
			$this->admins->or_where =  array("id" => $this->input->post('mail'));
            $this->admins->password = md5($this->input->post('password'));
            if ($this->admins->login($this->input->post('remember'))){
                redirect('backstage/index/index');
			}else{
				 $this->session->set_flashdata('msg_error', lang('login_error'));
				 redirect('backstage/login/index');
			}
        }
        $this->load->view('login');
    }

   function logout() {
        $this->session->unset_userdata(array('adminid' => '', 'mail' => ''));
		$cookie = array( 'name' => 'adminid', 'value' => '', 'expire' => 0 ); 
		$this->input->set_cookie($cookie);
		$cookie2 = array( 'name' => 'mail', 'value' => '', 'expire' => 0 ); 
		$this->input->set_cookie($cookie2);
        redirect('backstage/login/index');
   }
   
   function register() {
        $this->load->view("register");
   }
   
   //insert data in db	 
    function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('full_name', lang('full_name'), 'required');
        $this->form_validation->set_rules('email', lang('email'), 'required|valid_email|is_unique[admins.mail]');
        $this->form_validation->set_rules('password', lang('password'), 'required');
        $this->form_validation->set_rules('re_password', lang('re_password'), 'required|matches[password]');
        

        if ($this->form_validation->run() == FALSE) {
            $this->register();
        } else {
			$this->admins->full_name = $this->input->post("full_name");
			$this->admins->mail = $this->input->post("email");
			$this->admins->password = md5($this->input->post("password"));
			$this->admins->status = 0;
			$this->admins->super_admin = 0;
			$this->admins->save();
			$this->admins->clear() ;
			redirect("backstage/index/index");
        }
    }
	
	//generate activation key forget password
	function generate_key(){
		// Create a unique  activation code:
		$activation = md5(uniqid(rand(), true));
		$this->admins->activation = $activation;
        $this->data['activation'] = $this->admins->get();
		if(count($this->data['activation']) !=0){
			$this->generate_key();
		}else{
			return $activation ;
		}
		$this->admins->clear();
	}
	
	function fgpass() {
		
        $this->load->library("form_validation");
        $this->form_validation->set_rules("email", lang('email'), "trim|required|valid_email");

        if ($this->form_validation->run()) {
			$activation = $this->generate_key();
           // Send the email:
             $message = lang('forget_pass_message'). "\n\n";
			 $message .= site_url()."/backstage/login/fg_pass/". urlencode($this->input->post("email")) . "/".$activation;				
			 $headers = 'From: '.email. "\r\n" .
			'Reply-To: '. email. "\r\n" .
			'X-Mailer: PHP/' . phpversion();
             mail($this->input->post("email"), lang('forget_pass_title'), $message, $headers);
			 $this->admin_model->activation = $activation;
			 $this->admin_model->mail = $this->input->post("email");
		     $this->admin_model->save();
			// $this->session->set_flashdata('fg_pass_done', lang('fg_pass_done'));
			$this->data['fg_pass_done']=lang('fg_pass_done');
			$this->load->view('fgpass',$this->data);
			
        }else{
			$this->data['fg_pass_done']='';
			$this->load->view('fgpass',$this->data);
		}
        
    }
	
	function fg_pass($email,$key) {
		$this->admins->activation = $key;
		$data['admin_data'] = $this->admins->get();
		$this->admins->clear();
		if(count($data['admin_data'])==0){
			$this->load->view('404');
		}else{
			$this->data['key']=$key ;
			$this->data['email']=$email ;
			
			$this->load->library("form_validation");
		  
			$this->form_validation->set_rules('password', lang('password'), 'required');
			$this->form_validation->set_rules('re_password', lang('re_password'), 'required|matches[password]');
	
			if ($this->form_validation->run()) {
				$this->admins->activation = $key;
				$data['admin'] = $this->admins->get();
				$this->admins->clear() ;
				$this->admins->id = $data['admin']['0']->id;
				$this->admins->password=md5($this->input->post("password"));
				$this->admins->save();
				
				redirect('backstage/login/index');
			}
		
			$this->load->view('fg_pass',$this->data);
		}
		$this->admins->clear() ;
    }

}
