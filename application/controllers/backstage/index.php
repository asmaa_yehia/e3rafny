<?php

class index extends CI_Controller {

    function __construct() {
        parent:: __construct();
        if (!$this->admins->login())
            redirect("backstage/login/index");
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('phones');
        $this->load->model('contact');
        $this->load->model('faq');
        $this->load->model('cv');
        $this->load->model('messages');
        $this->load->model('qualifications');
        $this->load->model('education_type');
        $this->lang->load("user");
        $this->lang->load("message");
        $this->current_lang = LANG;
    }

    function index($offset = 0) {
        $this->session->set_flashdata('offset', $offset);

        $this->user->order_by = array("id" => "desc");

        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("backstage/index/index/"),
            "total_rows" => $this->user->get(true),
            "per_page" => list_limit,
            "uri_segment" => 5,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->user->limit = $config['per_page'];
        $this->user->offset = $offset;

        $this->data['users'] = $this->user->get();
        $this->user->clear();
        $this->data['menu_item'] = lang("users_view");
        $this->data['menu_link'] = "index/index";
        $this->load->view("users_view", $this->data);
    }

    function delete($id = false) {
        if (!$id)
            showx_404();
        $this->user->id = $id;
        $this->data['isusers'] = $this->user->get();
        if (!$this->data['isusers'])
            showx_404();
        $this->user->clear();
        $this->user->id = $id;
        $this->user->del_all($id);
        $this->user->delete();
        $this->user->clear();
        $this->session->set_flashdata('result', lang('done'));
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }

        redirect("backstage/index/index/" . $offset);
    }

    function show_resums($id = false, $offset = 0) {
        if (!$id)
            showx_404();
        $this->session->set_flashdata('offset', $offset);

        $this->cv->order_by = array("id" => "desc");
        $this->cv->user_id = $id;

        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("backstage/index/show_resums/" . $id . "/"),
            "total_rows" => $this->cv->get(true),
            "per_page" => list_limit,
            "uri_segment" => 6,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->cv->limit = $config['per_page'];
        $this->cv->offset = $offset;

        $this->data['resums'] = $this->cv->get();
        $this->cv->clear();
        $this->user->id = $id;
        $this->data['user_cv'] = $this->user->get();
        $this->user->clear();
        $this->data['menu_item'] = lang("viewuser_cvs");
        $this->data['menu_link'] = "index/show_resums/" . $id . "/" . $offset;
        $this->load->view("cvs_view", $this->data);
    }

    function send_message($user_id = false) {
        if (!$user_id)
            showx_404();
        $this->user->id = $user_id;
        $this->data['isusers'] = $this->user->get();
        if (!$this->data['isusers'])
            showx_404();
        $this->user->clear();
        $this->data['user_id'] = $user_id;
        $this->user->id = $user_id;
        $this->data['user_info'] = $this->user->get();
        $email = $this->data['user_info']->mail;
        $this->user->clear();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('title', lang('title'), 'required');
        $this->form_validation->set_rules('message', lang('message'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['menu_item'] = lang("senduser_msg");
            $this->data['menu_link'] = "index/send_message/" . $user_id;
            $this->load->view("user_message", $this->data);
        } else {
            $this->messages->title = $this->input->post("title");
            $this->messages->message = $this->input->post("message");
            $this->messages->user_id = $this->input->post("user_id");
            $this->messages->admin_id = $this->session->userdata('adminid');
            $this->messages->date = time();
            $this->messages->seen = '0';
            $this->messages->save();
            // Send the email:
            $message = $this->input->post("message");
            $headers = 'From: ' . email . "\r\n" .
                    'Reply-To: ' . email . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            mail($email, $this->input->post("title"), $message, $headers);
            $this->session->set_flashdata('result', lang('done'));
            $offset = $this->session->flashdata('offset');
            if ($offset == '') {
                $offset = 0;
            }

            redirect("backstage/index/index/" . $offset);
        }
    }

    function send_usersmessages() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('title', lang('title'), 'required');
        $this->form_validation->set_rules('message', lang('message'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['menu_item'] = lang("sendusers_msg");
            $this->data['menu_link'] = "index/send_usersmessages";
            $this->load->view("users_messages", $this->data);
        } else {
            $this->data['user_info'] = $this->user->get();
            $this->messages->title = $this->input->post("title");
            $this->messages->message = $this->input->post("message");
            $this->messages->admin_id = $this->session->userdata('adminid');
            $this->messages->date = time();
            $this->messages->seen = '0';
            $message = $this->input->post("message");
            $headers = 'From: ' . email . "\r\n" .
                    'Reply-To: ' . email . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            foreach ($this->data['user_info'] as $item) {
                $this->messages->user_id = $item->id;
                $this->messages->save();
                // Send the email:
                $email = $item->mail;
                mail($email, $this->input->post("title"), $message, $headers);
            }
            $this->user->clear();
            $this->session->set_flashdata('result', lang('done'));
            $offset = $this->session->flashdata('offset');
            if ($offset == '') {
                $offset = 0;
            }

            redirect("backstage/index/index/" . $offset);
        }
    }

    function autocomplete() {
        $this->layout = "ajax";
        $query = $this->user->get_autocomplete();
        foreach ($query->result() as $row):
            echo "<li id=" . "'" . $row->id . "'" . " onclick='fill(" . $row->id . ")'>" . $row->first_name . " " . $row->last_name . "</li>";
        endforeach;
    }

    function get_user($user_id) {
        $this->layout = "ajax";
        $this->user->id = $user_id;
        $this->data['user'] = $this->user->get();
        $this->user->clear();
        $this->load->view("user", $this->data);
    }

    function get_username() {
        $this->layout = "ajax";
        $user_id = $this->input->post("data");
        $this->user->id = $user_id;
        $this->data['user_name'] = $this->user->get();
        $this->user->clear();
        $outputdata = $this->data['user_name']->first_name . " " . $this->data['user_name']->last_name;
        echo json_encode($outputdata);
    }

    function add_qualification() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_en', lang('name_en'), 'required');
        $this->form_validation->set_rules('name_ar', lang('name_ar'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['menu_item'] = lang("add_qualification");
            $this->data['menu_link'] = "index/add_qualification";
            $this->load->view("add_qualification", $this->data);
        } else {
            $this->qualifications->name_en = $this->input->post("name_en");
            $this->qualifications->name_ar = $this->input->post("name_ar");
            $this->qualifications->save();

            $this->session->set_flashdata('result', lang('done'));
            $offset = $this->session->flashdata('offset');
            if ($offset == '') {
                $offset = 0;
            }

            redirect("backstage/index/show_qualifications/" . $offset);
        }
    }

    function show_qualifications($offset = 0) {
        $this->session->set_flashdata('offset', $offset);

        $this->qualifications->order_by = array("id" => "desc");

        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("backstage/index/show_qualifications/"),
            "total_rows" => $this->qualifications->get(true),
            "per_page" => list_limit,
            "uri_segment" => 5,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->qualifications->limit = $config['per_page'];
        $this->qualifications->offset = $offset;

        $this->data['qualifications'] = $this->qualifications->get();
        $this->qualifications->clear();
        $this->data['menu_item'] = lang("show_qualifications");
        $this->data['menu_link'] = "index/show_qualifications";
        $this->load->view("show_qualifications", $this->data);
    }

    function delete_qualification($id = false) {
        if (!$id)
            showx_404();
        $this->qualifications->id = $id;
        $this->data['isqalify'] = $this->qualifications->get();
        if (!$this->data['isqalify'])
            showx_404();
        $this->qualifications->clear();
        $this->qualifications->id = $id;
        $this->qualifications->del_all($id);
        $this->qualifications->delete();
        $this->qualifications->clear();
        $this->session->set_flashdata('result', lang('done'));
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }

        redirect("backstage/index/show_qualifications/" . $offset);
    }

    function edit_qualification($id = false) {
        if (!$id)
            showx_404();
        $this->qualifications->id = $id;
        $this->data['isqalify'] = $this->qualifications->get();
        if (!$this->data['isqalify'])
            showx_404();
        $this->qualifications->clear();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('name_en', lang('name_en'), 'required');
        $this->form_validation->set_rules('name_ar', lang('name_ar'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->qualifications->id = $id;
            $this->data['qualifications'] = $this->qualifications->get();
            $this->qualifications->clear();
            $this->data['menu_item'] = lang("edit_qualification");
            $this->data['menu_link'] = "index/edit_qualification/" . $id;
            $this->load->view("edit_qualification", $this->data);
        } else {
            $this->qualifications->id = $id;
            $this->qualifications->name_en = $this->input->post("name_en");
            $this->qualifications->name_ar = $this->input->post("name_ar");
            $this->qualifications->save();

            $this->session->set_flashdata('result', lang('done'));
            $offset = $this->session->flashdata('offset');
            if ($offset == '') {
                $offset = 0;
            }

            redirect("backstage/index/show_qualifications/" . $offset);
        }
    }

    function add_education_type() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('type_en', lang('type_en'), 'required');
        $this->form_validation->set_rules('type_ar', lang('type_ar'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->data['menu_item'] = lang("add_education_type");
            $this->data['menu_link'] = "index/add_education_type";
            $this->load->view("add_education_type", $this->data);
        } else {
            $this->education_type->type_en = $this->input->post("type_en");
            $this->education_type->type_ar = $this->input->post("type_ar");
            $this->education_type->save();

            $this->session->set_flashdata('result', lang('done'));
            $offset = $this->session->flashdata('offset');
            if ($offset == '') {
                $offset = 0;
            }

            redirect("backstage/index/show_education_type/" . $offset);
        }
    }

    function show_education_type($offset = 0) {
        $this->session->set_flashdata('offset', $offset);

        $this->education_type->order_by = array("id" => "desc");

        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("backstage/index/show_education_type/"),
            "total_rows" => $this->education_type->get(true),
            "per_page" => list_limit,
            "uri_segment" => 5,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->education_type->limit = $config['per_page'];
        $this->education_type->offset = $offset;

        $this->data['types'] = $this->education_type->get();
        $this->education_type->clear();
        $this->data['menu_item'] = lang("show_education_type");
        $this->data['menu_link'] = "index/show_education_type";
        $this->load->view("show_education_type", $this->data);
    }

    function delete_education_type($id = false) {
        if (!$id)
            showx_404();
        $this->education_type->id = $id;
        $this->data['isedu_type'] = $this->education_type->get();
        if (!$this->data['isedu_type'])
            showx_404();
        $this->education_type->clear();
        $this->education_type->id = $id;
        $this->education_type->del_all($id);
        $this->education_type->delete();
        $this->education_type->clear();
        $this->session->set_flashdata('result', lang('done'));
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }

        redirect("backstage/index/show_education_type/" . $offset);
    }

    function edit_education_type($id = false) {
        if (!$id)
            showx_404();
        $this->education_type->id = $id;
        $this->data['isedu_type'] = $this->education_type->get();
        if (!$this->data['isedu_type'])
            showx_404();
        $this->education_type->clear();
        $this->load->library("form_validation");
        $this->form_validation->set_rules('type_en', lang('type_en'), 'required');
        $this->form_validation->set_rules('type_ar', lang('type_ar'), 'required');
        if ($this->form_validation->run() == FALSE) {
            $this->education_type->id = $id;
            $this->data['types'] = $this->education_type->get();
            $this->education_type->clear();
            $this->data['menu_item'] = lang("edit_education_type");
            $this->data['menu_link'] = "index/edit_education_type/" . $id;
            $this->load->view("edit_education_type", $this->data);
        } else {
            $this->education_type->id = $id;
            $this->education_type->type_en = $this->input->post("type_en");
            $this->education_type->type_ar = $this->input->post("type_ar");
            $this->education_type->save();

            $this->session->set_flashdata('result', lang('done'));
            $offset = $this->session->flashdata('offset');
            if ($offset == '') {
                $offset = 0;
            }

            redirect("backstage/index/show_education_type/" . $offset);
        }
    }

    function show_contact($offset = 0) {
        $this->session->set_flashdata('offset', $offset);
        $this->contact->parent = "0";
        $this->contact->order_by = array("id" => "desc");
        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("backstage/index/show_contact/"),
            "total_rows" => $this->contact->get(true),
            "per_page" => list_limit,
            "uri_segment" => 5,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->contact->limit = $config['per_page'];
        $this->contact->offset = $offset;
        $this->data['contacts'] = $this->contact->get();
        //echo $this->db->last_query();
        $this->contact->clear();
        $this->data['menu_item'] = lang("contact");
        $this->data['menu_link'] = "index/show_contact";
        $this->load->view('contacts', $this->data);
    }

    function delete_contact($id = false) {
        if (!$id)
            showx_404();
        $this->contact->id = $id;
        $this->data['isqalify'] = $this->contact->get();
        if (!$this->data['isqalify'])
            showx_404();

        $this->del_all_contact($id);
        $this->contact->clear();
        $this->contact->id = $id;
        $this->contact->delete();
        $this->contact->clear();
        $this->session->set_flashdata('result', lang('done'));
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }

        redirect("backstage/index/show_contact/" . $offset);
    }

    function del_all_contact($id = false) {
        $this->contact->clear();
        $this->contact->parent = $id;
        $this->contact->delete();
    }

    function reply($id = false) {
        if (!$id)
            showx_404();
        $this->contact->id = $id;
        $this->data['iscontact'] = $this->contact->get();
        if (!$this->data['iscontact'])
            showx_404();
        if ($_POST) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('subject', lang('address'), 'required');
            $this->form_validation->set_rules('message', lang('message'), 'required');
            if ($this->form_validation->run() == true) {
                $this->contact->clear();
                $this->contact->name = 'Mnbaa';
                $this->contact->subject = $this->input->post('subject');
                $this->contact->message = $this->input->post('message');
                $this->contact->parent = $id;
                $this->contact->admin_id = $this->session->userdata('adminid');
                $this->contact->save();
                //
                $this->contact->clear();
                $this->contact->id = $id;
                $contacts = $this->contact->get();
                // var_dump($contacts);
                $email = $contacts->mail;
                //echo $email;
                // Send the email:
                $message = $this->input->post("message");
                $headers = 'From: ' . email . "\r\n" .
                        'Reply-To: ' . email . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                mail($email, $this->input->post("subject"), $message, $headers);
                $this->session->set_flashdata('result', lang('done'));
                $offset = $this->session->flashdata('offset');
                if ($offset == '') {
                    $offset = 0;
                }

                redirect("backstage/index/show_contact/" . $offset);
                // echo "sucess";
            } else {
                $this->data['menu_item'] = lang("contact");
                $this->data['menu_link'] = "index/show_contact";
                $this->data['id'] = $id;
                // $this->load->view('contacts', $this->data);
                $this->load->view('reply', $this->data);
            }

            /* $this->contact->clear();

              $this->contact->order_by = array("id" => "desc");

              $this->data['contacts'] = $this->contact->get();
              echo $this->db->last_query();
              $this->contact->clear();
              echo $this->sendemail(); */
        } else {

            $this->data['menu_item'] = lang("contact");
            $this->data['menu_link'] = "index/show_contact";
            $this->data['id'] = $id;
            // $this->load->view('contacts', $this->data);
            $this->load->view('reply', $this->data);
        }
    }

    function view_contact($id = false) {
        $this->contact->id = $id;
        $this->data['contact'] = $this->contact->get();
        $this->data['menu_item'] = lang("contact");
        $this->data['menu_link'] = "index/show_contact";
        $this->load->view('view_contact', $this->data);
    }

    //FAQ
    function show_faq($offset = 0) {
        $this->session->set_flashdata('offset', $offset);
        $this->faq->order_by = array("id" => "desc");
        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("backstage/index/show_faq/"),
            "total_rows" => $this->faq->get(true),
            "per_page" => list_limit,
            "uri_segment" => 5,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->faq->limit = $config['per_page'];
        $this->faq->offset = $offset;
        $this->data['faq'] = $this->faq->get();
        //echo $this->db->last_query();
        $this->faq->clear();
        $this->data['menu_item'] = lang("faq");
        $this->data['menu_link'] = "index/show_faq";
        $this->load->view('faq', $this->data);
    }

    //
    function delete_faq($id = false) {
        if (!$id)
            showx_404();
        $this->faq->id = $id;
        $this->data['isqalify'] = $this->faq->get();
        if (!$this->data['isqalify'])
            showx_404();

        $this->del_all_contact($id);
        $this->faq->clear();
        $this->faq->id = $id;
        $this->faq->delete();
        $this->faq->clear();
        $this->session->set_flashdata('result', lang('done'));
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }

        redirect("backstage/index/show_faq/" . $offset);
    }

    //
    function add_faq() {
        if ($_POST) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('question', lang('ques'), 'required');
            $this->form_validation->set_rules('answer', lang('answer'), 'required');
            $this->form_validation->set_rules('question_en', lang('ques_en'), 'required');
            $this->form_validation->set_rules('answer_en', lang('answer_en'), 'required');
            if ($this->form_validation->run() == true) {
                $this->faq->clear();
                $this->faq->question = $this->input->post("question");
                $this->faq->answer = $this->input->post("answer");
                $this->faq->question_en = $this->input->post("question_en");
                $this->faq->answer_en = $this->input->post("answer_en");
                $this->faq->save();
                $this->session->set_flashdata('result', lang('done'));
                $offset = $this->session->flashdata('offset');
                if ($offset == '') {
                    $offset = 0;
                }

                redirect("backstage/index/show_faq/" . $offset);
                // echo "sucess";
            } else {
                $this->data['menu_item'] = lang("faq");
                $this->data['menu_link'] = "index/show_faq";
                $this->load->view('add_faq', $this->data);
            }
        } else {
            $this->data['menu_item'] = lang("faq");
            $this->data['menu_link'] = "index/add_faq";
            $this->load->view('add_faq', $this->data);
        }
    }

    function edit_faq($id = false) {
        if ($_POST) {
            $this->load->library("form_validation");
            $this->form_validation->set_rules('question', lang('ques'), 'required');
            $this->form_validation->set_rules('answer', lang('answer'), 'required');
            $this->form_validation->set_rules('question_en', lang('ques_en'), 'required');
            $this->form_validation->set_rules('answer_en', lang('answer_en'), 'required');
            if ($this->form_validation->run() == true) {
                $this->faq->clear();
                $this->faq->id = $id;
                $this->faq->question = $this->input->post("question");
                $this->faq->answer = $this->input->post("answer");
                $this->faq->question_en = $this->input->post("question_en");
                $this->faq->answer_en = $this->input->post("answer_en");
                echo $this->db->last_query();
                $this->faq->save();
                $this->session->set_flashdata('result', lang('done'));
                $offset = $this->session->flashdata('offset');
                if ($offset == '') {
                    $offset = 0;
                }

                redirect("backstage/index/show_faq/" . $offset);
            } else {
                $this->data['menu_item'] = lang("faq");
                $this->data['menu_link'] = "index/show_faq";
                $this->load->view('add_faq', $this->data);
            }
        } else {
            $this->data['menu_item'] = lang("faq");
            $this->data['menu_link'] = "index/add_faq";
            $this->faq->clear();
            $this->faq->id = $id;
            $this->data['faq'] = $this->faq->get();
            $this->load->view('edit_faq', $this->data);
        }
    }

    function active_faq($id = false, $offset = 0) {
        $this->faq->id = $id;
        $faqs = $this->faq->get();
        if ($faqs->status == 0)
            $this->faq->status = 1;
        else
            $this->faq->status = 0;
        $this->faq->save();
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }
        redirect("backstage/index/show_faq/" . $offset);
    }
	
	function show_admins($offset = 0) {
		$this->admins->super_admin = '0';
        $this->session->set_flashdata('offset', $offset);

        $this->admins->order_by = array("id" => "desc");

        $this->load->library("pagination");
        $config = array(
            "base_url" => site_url("backstage/index/show_admins/"),
            "total_rows" => $this->admins->get(true),
            "per_page" => list_limit,
            "uri_segment" => 5,
            "num_links" => 2,
        );
        $this->pagination->initialize($config);
        $this->admins->limit = $config['per_page'];
        $this->admins->offset = $offset;

        $this->data['admins'] = $this->admins->get();
        $this->admins->clear();
        $this->data['menu_item'] = lang("admins_view");
        $this->data['menu_link'] = "index/show_admins";
		//echo $this->db->last_query();
        $this->load->view("admins_view", $this->data);
    }
	function delete_admin($id = false) {
        if (!$id)
            showx_404();
        $this->admins->id = $id;
        $this->data['isusers'] = $this->admins->get();
        if (!$this->data['isusers'])
            showx_404();
        $this->admins->clear();
        $this->admins->id = $id;
		$this->admins->delete();
        $this->admins->clear();
        $this->session->set_flashdata('result', lang('done'));
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }

        redirect("backstage/index/show_admins/" . $offset);
    }
	
	//check status and change it

    function edit_status($id) {

        $this->admins->id = $id;
        $this->data['admins'] = $this->admins->get();

        if ($this->data['admins']->status == '0') {
            $this->admins->status = '1';
        } else {
            $this->admins->status = '0';
        }

        $this->admins->id = $id;
        $this->admins->save();
       $this->session->set_flashdata('result', lang('done'));
        $offset = $this->session->flashdata('offset');
        if ($offset == '') {
            $offset = 0;
        }

        redirect("backstage/index/show_admins/" . $offset);
    }

}
