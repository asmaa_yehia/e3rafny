<?php

class finish extends CI_Controller {

    function __construct() {
        parent:: __construct();
        if (!$this->user->login())
            redirect("auth/index");
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->helper('share');
        $this->load->helper('download');
        $this->load->model('cv');
        $this->load->model('phones');
        $this->load->model('education');
        $this->load->model('education_type');
        $this->load->model('courses');
        $this->load->model('experience');
        $this->load->model('languages');
        $this->load->model('skills');
        $this->load->model('projects');
        $this->load->model('references');
        $this->load->model('social');
        $this->load->model('recommend');
        $this->load->model('pdf_thems');
        $this->load->model('web_thems');
        $this->load->model('projects');
        $this->load->model('qualifications');
        $this->load->model('certifcates');
        $this->load->model('qual_user');
        $this->load->model('projects');
        $this->load->model("user_info");
        $this->lang->load("user");
        $this->lang->load("cv");
        $user_id = $this->session->userdata('userid');
        $this->user->id = $user_id;
        $this->data['user_status'] = $this->user->get();
        $this->user->clear();
		if(empty($this->data['user_status'])){
			showx_404();
		}
        $this->layout = "cv";
    }

    function index($cv_id = false) {
        
        $user_id = $this->session->userdata('userid');
        if (!$cv_id)
            showx_404();
        $this->cv->id = $cv_id;
        $this->cv->user_id = $user_id;
        $this->data['iscv'] = $this->cv->get();
        if (!$this->data['iscv'])
            showx_404();
        $this->cv->clear();
        $this->cv->id = $cv_id;
        $this->data['cv'] = $this->cv->get();
        $this->cv->clear();
//echo $cv_id;
        $this->user->id = $user_id;
        $this->data['user'] = $this->user->get();
        $this->user->clear();

        $percent = $this->data['cv']->percent;
        $status = $this->data['user']->status;
        if ($status == 0 || $percent < 35) {
            $this->session->set_flashdata('result', lang('publish_error'));
            redirect("thems/web_thems/" . $cv_id);
        } else {

            $this->user_info->cv_id = $cv_id;
            $user_info = $this->user_info->get();
            $this->data['userinfo'] = $user_info;
            //
            $this->data['cv_id'] = $cv_id;
            $percent = $this->data['cv']->percent;
            $this->data['percent'] = $percent;
            $this->data['step'] = 9;
            $this->load->view('steps', $this->data);
            $this->load->view('progress_bar', $this->data);
            $this->load->view("finish", $this->data);
        }
    }

    function pdf_file($cv_id = false) {
        $user_id = $this->session->userdata('userid');
        if (!$cv_id)
            showx_404();
        $this->cv->id = $cv_id;
        $this->cv->user_id = $user_id;
        $this->data['iscv'] = $this->cv->get();
        if (!$this->data['iscv'])
            showx_404();
        if ($this->data['iscv']->percent < 35)
            redirect("finish/publish_error");
        $this->cv->clear();

        $this->user->id = $user_id;
        $this->data['user'] = $this->user->get();
        $this->user->clear();
        if ($this->data['user']->status == 0)
            redirect("finish/publish_error");

        $folder_name = 'u_' . $user_id;

        $this->load->library('pdf_library');
        //$pdf = $this->pdf_library->load();
        //$pdf->SetAutoFont(AUTOFONT_RTL);

        $this->user_info->cv_id = $cv_id;
        $this->data['user_info'] = $this->user_info->get();
        $this->user_info->clear();
        //var_dump($this->data['user_info']);
        //get cv data
        $this->cv->user_id = $user_id;
        $this->cv->id = $cv_id;
        $this->data['cv'] = $this->cv->get();
        $file_name = $this->data['cv']->title;
        $path = 'assets/' . $folder_name . '/' . 'cv.pdf';
        $this->cv->clear();

        //get phones
        $this->phones->cv_id = $cv_id;
        $this->data['phones'] = $this->phones->get();
        $this->phones->clear();

        //get education
        $this->education->cv_id = $cv_id;
        $this->education->select = array("education.*", "education_type.type_" . $this->data['cv']->lang . " as type");
        $this->education->join = array("education_type" => "education_type.id = education.type_id");
        $this->data['education'] = $this->education->get();
        $this->education->clear();

        //get courses
        $this->courses->cv_id = $cv_id;
        $this->data['courses'] = $this->courses->get();
        $this->courses->clear();

        //get certifcates
        $this->certifcates->cv_id = $cv_id;
        $this->data['certifcates'] = $this->certifcates->get();
        $this->certifcates->clear();

        //get experience
        $this->experience->cv_id = $cv_id;
        $this->data['experience'] = $this->experience->get();
        $this->experience->clear();

        //get projects
        $this->projects->cv_id = $cv_id;
        $this->data['projects'] = $this->projects->get();
        $this->projects->clear();

        //get langs
        $this->languages->cv_id = $cv_id;
        $this->data['langs'] = $this->languages->get();
        $this->languages->clear();

        //get skills
        $this->skills->cv_id = $cv_id;
        $this->data['skills'] = $this->skills->get();
        $this->skills->clear();

        //get qualifications
        $this->qual_user->cv_id = $cv_id;
        $this->qual_user->select = array("qual_user.*", "qualifications.name_" . $this->data['cv']->lang . " as name");
        $this->qual_user->join = array("qualifications" => "qualifications.id =qual_user.qual_id");
        $this->data['qualifications'] = $this->qual_user->get();
        $this->qual_user->clear();

        //get refrences
        $this->references->cv_id = $cv_id;
        $this->data['references'] = $this->references->get();
        $this->references->clear();

        //get social links
        $this->social->cv_id = $cv_id;
        $this->data['social'] = $this->social->get();
        $this->social->clear();

        if ($this->data['cv']->lang == 'en') {
            $this->lang->load('cvdata', 'english');
        } elseif ($this->data['cv']->lang == 'ar') {
            $this->lang->load('cvdata', 'arabic');
        }

        //get pdf_them
        $pdf_them = $this->data['cv']->pdf_them;
        $this->pdf_thems->id = $pdf_them;
        $this->data['pdf_thempage'] = $this->pdf_thems->get();
        $this->pdf_thems->clear();

        if (empty($this->data['pdf_thempage'])) {
            $this->pdf_thems->order_by = array("id" => "ASC");
            $this->pdf_thems->limit = "1";
            $this->data['pdf_themsdefault'] = $this->pdf_thems->get();
            $this->pdf_thems->clear();
            $them = $this->data['pdf_themsdefault'][0]->file_name;
        } else
            $them = $this->data['pdf_thempage']->file_name;

        $html = $this->load->view($them, $this->data, true);
        if ($them == 'them4' || $them == 'them5') {
            $pdf = $this->pdf_library->load('utf-8', 'A4', "", "", 0, 0, 0, 0, 0, 0, 'P');


            $pdf->AddPage('p', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 'side-bar');
        } elseif (($them != 'them1') && ($them != 'them2')) {
            $pdf = $this->pdf_library->load();
            $pdf->AddPage('P', // L - landscape, P - portrait
                    '', '', '', '', 0, // margin_left
                    0, // margin right
                    0, // margin top
                    0, // margin bottom
                    0, // margin header
                    0); // margin footer
        } else {
            $pdf = $this->pdf_library->load();
        }

        $pdf->SetDisplayMode('fullpage');
        $pdf->SetTitle($this->data['user_info'][0]->user_cv_name);
        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output($path, "F"); // save to file because we can
        //$pdf->Output(); 
        //redirect("finish/download/".$folder_name);
        $path = 'assets/' . $folder_name . '/' . 'cv.pdf';
         header("Content-type: application/pdf");
          header("Content-disposition: attachment; filename=cv.pdf");
          readfile($path); 

        /*$pth = file_get_contents($path);
        $nme = "cv.pdf";
        force_download($nme, $pth);*/

       // unlink($path);
    }

    function publish_error() {
        $this->load->view("publish_error", $this->data);
    }

//    function edit($cv_id=false) {
//        if (isset($_POST['save'])) {
//            $cv_id = $this->input->post("cv_id");
//            $id = $this->session->userdata('userid');
//            $this->user->id = $id;
//            $this->load->library("form_validation");
//            $this->form_validation->set_rules('user_name', lang('user_name'), 'required|callback_username_check|trim|is_unique[user.user_name.id.' . $this->input->post("id") . ']');
//
//            if ($this->form_validation->run() == TRUE) {
//
//                $this->user->id = $id;
//                $this->user->user_name = $this->input->post("user_name");
//                $this->user->save();
//                $this->user->clear();
//                redirect('finish/index/'.$cv_id);
//                
//            }else{
//                $this->index($cv_id);
//            }
//        }else{
//           $this->index($cv_id);
//        }    
//       // $this->session->set_flashdata('cv_id', $cv_id);
////        $this->cv->id = $cv_id;
////        $this->data['cv'] = $this->cv->get();
////        $this->cv->clear();
////
////        $user_id = $this->session->userdata('userid');
////        $this->user->id = $user_id;
////        $this->data['user'] = $this->user->get();
////        $this->user->clear();
////        $this->data['cv_id'] = $cv_id;
////        //
////        $percent = $this->data['cv']->percent;
////        $this->data['percent'] = $percent;
////        $this->data['step'] = 9;
////        $this->load->view('steps', $this->data);
////        $this->load->view('progress_bar', $this->data);
////        //
////        $this->load->view("finish", $this->data);
//    }
    
    function edit($cv_id = false) {
        //$cv_id = $this->input->post("cv_id");
        $id = $this->session->userdata('userid');
        $this->user->id = $id;
        $this->load->library("form_validation");
        $this->form_validation->set_rules('user_name', lang('user_name'), 'required|callback_username_check|trim|is_unique[user.user_name.id.' . $id . ']');

        if ($this->form_validation->run() == TRUE) {

            $this->user->id = $id;
            $this->user->user_name = $this->input->post("user_name");
            $this->user->save();
            $this->user->clear();
        }
        $this->cv->id = $cv_id;
        $this->data['cv'] = $this->cv->get();
        $this->cv->clear();

        $user_id = $this->session->userdata('userid');
        $this->user->id = $user_id;
        $this->data['user'] = $this->user->get();
        $this->user->clear();
        $this->data['cv_id'] = $cv_id;
        //
        $percent = $this->data['cv']->percent;
        $this->data['percent'] = $percent;
        $this->data['step'] = 9;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        //
        $this->load->view("finish", $this->data);
    }

    function username_check($str) {

        $allowed = array(".", "-", "_");
        if ((ctype_alnum(str_replace($allowed, '', $str))) && ctype_alpha($str[0])) {
            return TRUE;
        } else {
            $this->form_validation->set_message('username_check', lang("user_nameerror"));
            return FALSE;
        }
    }

}
