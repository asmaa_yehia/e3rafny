<?php

class add_cv extends CI_Controller {

    function __construct() {
        parent:: __construct();
        if (!$this->user->login())
            redirect("auth/index");

        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('cv');
        $this->load->model('courses');
        $this->load->model('education');
        $this->load->model('education_type');
        $this->load->model('experience');
        $this->load->model('projects');
        $this->load->model('recommend');
        $this->load->model('references');
        $this->load->model('phones');
        $this->load->model('skills');
        $this->load->model('qualifications');
        $this->load->model('qual_user');
        $this->load->model('certifcates');
        $this->load->model('social');
        $this->load->model('user_info');
        $this->load->model('languages');
        $this->load->model("pdf_thems");
        $this->lang->load('cv');
        $this->lang->load('basic');
        $this->lang->load("user");
        $this->load->library('session');
        //
        $user_id = $this->session->userdata('userid');
        $this->user->id = $user_id;
        $this->data['user_status'] = $this->user->get();
        $this->user->clear();
        if (empty($this->data['user_status'])) {
            showx_404();
        }
        $this->layout = "cv";
    }

    public function index() {

        $percent = 0;
        if ($_POST) {
            $this->insert();
        } else {
            $this->data['percent'] = $percent;
            $this->data['start'] = 0;
            $this->data['step'] = 1;
            $this->load->view('steps', $this->data);
            $this->load->view('progress_bar', $this->data);
            $this->load->view('cv');
        }
    }

    //insert in cv table
    public function insert() {
        //echo "in post";

        $user_id = $this->session->userdata('userid');
        $percent = $this->input->post("percent");
        //check cv title is unique
        $this->load->library("form_validation");
        $this->form_validation->set_rules('cv_tilte', lang('title'), 'required|callback_cv_title_check');
        $this->form_validation->set_rules('obj', lang('objective'), 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->cv->user_id = $user_id;

            $this->cv->title = $this->input->post("cv_tilte");
            $name = str_replace(' ', '_', $this->cv->title);
            $this->cv->url_title = $name;
            //
            $this->cv->pdf_them = 1;
            $this->cv->style_them = 1;
            //get  image of defalut theme
            $this->pdf_thems->id = 1;
            $this->data['pdf'] = $this->pdf_thems->get();
            $cv_lang = $this->input->post("lang");
            if ($cv_lang == 'ar')
                $this->cv->img = $this->data['pdf']->img_ar;
            else
                $this->cv->img = $this->data['pdf']->img_en;

            $folder_name = 'u_' . $user_id;
            $path = 'assets/' . $folder_name . '/' . $this->cv->img;

            $source = 'assets/images' . '/' . $this->cv->img;
            copy($source, $path);
            //
            $this->cv->objective = $this->input->post("obj");
            $this->cv->lang = $cv_lang;
            //get id of inserted cv
            $this->cv->save();
            $cv_id = $this->db->insert_id();

            //get  last percent
            if ($this->cv->title != '')
                $percent+=5;
            if ($this->cv->objective != '')
                $percent+=5;
            $this->data['percent'] = $percent;
            //echo $percent;
            $this->update_cv_percent($cv_id, $percent);
            $this->data['cv_id'] = $cv_id;

            //
            $this->session->set_userdata('percent', $percent);
            if (isset($_POST['next_button'])) {
                redirect('add_cv/view_user/' . $cv_id);
            } else {
                //echo "in save";
                //get all cv data 
                $this->cv->id = $cv_id;
                $this->data['items'] = $this->cv->get();
                $this->data['step'] = 1;
                $this->load->view('steps', $this->data);
                $this->load->view('progress_bar', $this->data);
                $this->load->view('update_cv', $this->data);
            }
        }
        //if validation desn't run
        else {
            $this->data['step'] = 1;
            $this->load->view('steps', $this->data);
            $this->data['percent'] = $percent;
            $this->load->view('progress_bar', $this->data);
            $this->load->view('cv', $this->data);
        }
    }

    //
    //claaback function
    function cv_title_check($str) {
        //echo $str;
        $user_id = $this->session->userdata('userid');
        $cv_title = $this->input->post("cv_tilte");
        $query = $this->db->get_where('cv', array('user_id' => $user_id, "title" => $cv_title));
        if ($query->num_rows() != 0) {
            $this->form_validation->set_message('cv_title_check', lang('exist_title'));
            return false;
        }
    }

    //function that update cv percent
    function update_cv_percent($cv_id, $percent) {
        $this->cv->id = $cv_id;
        $this->cv->percent = $percent;
        $this->cv->save();
    }

    function check_userinfo($cv_id) {
        $this->user_info->cv_id = $cv_id;
        $user_info = $this->user_info->get();
        return $user_info;
    }

    //update cv by user_id
    public function update($cv_id = false) {
        //echo $cv_id;
        //check if cv_id is exist
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        //var_dump($cv);
        if (empty($cv))
            showx_404();
        if (!$cv_id)
            showx_404();

        //validation on cv_title
        $this->load->library("form_validation");
        $user_id = $this->session->userdata('userid');

        //get percent value from user_page
        $percent = $this->session->userdata('percent');
        //echo $percent;
        $percent = $this->input->post("percent");
        //$cv_id=$this->input->post('cv_id');
        if ($cv->title != '')
            $percent = ($percent - 5);
        if ($cv->objective != '')
            $percent = ($percent - 5);
        //echo $percent;
        //update  cv data
        $this->form_validation->set_rules('cv_tilte', lang('title'), 'required');
        $this->form_validation->set_rules('obj', lang('objective'), 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->cv->id = $this->input->post('cv_id');
            $this->cv->title = $this->input->post("cv_tilte");
            $name = str_replace(' ', '_', $this->cv->title);
            $this->cv->url_title = $name;
            $this->cv->objective = $this->input->post("obj");
            $cv_lang = $this->input->post("lang");
            $this->cv->lang = $cv_lang;
            $this->cv->save();
            $this->cv->clear();
            //get new values
            if ($this->input->post("cv_tilte") != '')
                $percent+=5;
            if ($this->input->post("obj") != '')
                $percent+=5;
            $this->data['percent'] = $percent;
            //update cv with last values
            $this->update_cv_percent($cv_id, $percent);
            $this->data['cv_id'] = $cv_id;

            if (isset($_POST['next_button'])) {

                //view data of education next step
                //.'/'.$percent
                redirect('add_cv/view_user/' . $cv_id);
                //$this->view_user($percent,$cv_id);
            } else {
                $this->cv->id = $cv_id;
                //
                $userinfo = $this->check_userinfo($cv_id);

                $this->data['userinfo'] = $userinfo;
                //
                $this->data['step'] = 1;
                $this->load->view('steps', $this->data);
                $this->data['items'] = $this->cv->get();
                $this->load->view('progress_bar', $this->data);
                $this->load->view('update_cv', $this->data);
            }
        }
        //validation  isn't write
        else {
            $this->back_cv($cv_id);
        }
    }

    //function go to update_user page
    public function view_user($cv_id = false) {
        $this->load->helper('set_value');

        //check if cv_id is exist
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        //var_dump($cv);
        if (empty($cv))
            showx_404();
        if (!$cv_id)
            showx_404();
        $this->data['value'] = value_field('military', 'n');
        $this->data['status'] = value_field('marital_status', 'n');
        $percent = $this->session->userdata('percent');
        $user_id = $this->session->userdata('userid');
        $this->data['user'] = $this->user->get();
        // get all pones for this user
        $this->phones->cv_id = $cv_id;
        $this->phones->order_by = array("id" => "asc");
        $this->data['phones'] = $this->phones->get();
        $this->data['num_row'] = count($this->data['phones']);
        //
        $this->social->cv_id = $cv_id;
        $this->data['items'] = $this->social->get();

        $soc_num = count($this->data['items']);
        $this->data['soc_num'] = $soc_num;
        //
        $this->data['cv_id'] = $cv_id;

        //check if user data is inserted bdfore
        $this->user_info->user_id = $user_id;
        $this->user_info->cv_id = $cv_id;
        $user_info = $this->user_info->get();
        $this->data['user'] = '';
        foreach ($user_info as $item) {
            $user_info_id = $item->id;
            $this->user_info->id = $user_info_id;
            $this->data['user'] = $this->user_info->get();
            $this->data['info_id'] = $this->data['user']->id;
            $this->data['img'] = $this->data['user']->image;
        }
        $this->data['percent'] = $percent;
        //
        if (!empty($this->data['user'])) {
            //echo "in edit";
            $this->data['percent'] = $percent;
            //
            $userinfo = $this->check_userinfo($cv_id);
            $this->data['userinfo'] = $userinfo;
            //
            $this->data['step'] = 2;
            $this->load->view('steps', $this->data);
            $this->load->view('progress_bar', $this->data);
            $this->load->view('user_edit', $this->data);
        } else {
            $this->data['step'] = 2;
            $this->load->view('steps', $this->data);
            $this->load->view('progress_bar', $this->data);
            $this->load->view('add_user', $this->data);
        }
    }

    //view cv data on back button
    public function back_cv($cv_id = false) {
        if (!$cv_id)
            showx_404();
        //echo $cv_id;
        $this->cv->clear();
        $this->cv->id = $cv_id;
        $this->data['items'] = $this->cv->get();
        if (!$this->data['items'])
            showx_404();
        $this->data['percent'] = $this->data['items']->percent;
        $this->data['cv_id'] = $this->data['items']->id;
        //
        $userinfo = $this->check_userinfo($cv_id);
        $this->data['userinfo'] = $userinfo;
        //
        $this->data['step'] = 1;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('update_cv', $this->data);
    }

    public function view_step($frist_model, $second_model, $cv_id = false) {

        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();
        //
        $this->$frist_model->cv_id = $cv_id;
        $this->data['f_items'] = $this->$frist_model->get();
        //
        $this->$second_model->cv_id = $cv_id;
        $this->data['s_items'] = $this->$second_model->get();
        //
        $this->data['cv_id'] = $cv_id;
        $percent = $this->session->userdata('percent');
        //$percent=$this->input->post('percent');
        $this->data['percent'] = $percent;
        $this->load->view($frist_model, $this->data);
    }

    //view education data on back button
    public function back_edu($cv_id = false) {

        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();
        //get eduicaation types
        $this->data['types'] = $this->education_type->get();

        //
        $this->education->cv_id = $cv_id;
        $this->data['items'] = $this->education->get();
        //
        $this->certifcates->cv_id = $cv_id;
        $this->data['certifcates'] = $this->certifcates->get();
        //
        $this->data['cv_id'] = $cv_id;
        $percent = $this->session->userdata('percent');
        //
        $userinfo = $this->check_userinfo($cv_id);
        $this->data['userinfo'] = $userinfo;
        //
        $this->data['step'] = 3;
        $this->load->view('steps', $this->data);
        $this->data['percent'] = $percent;
        $this->load->view('progress_bar', $this->data);
        $this->load->view('education', $this->data);
    }

    //
    //view education data on back button
    public function back_course($cv_id = false) {
        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();
        $this->certifcates->cv_id = $cv_id;
        $this->data['certifcates'] = $this->certifcates->get();
        //
        $this->courses->cv_id = $cv_id;
        $this->data['items'] = $this->courses->get();
        $this->data['cv_id'] = $cv_id;
        $percent = $this->session->userdata('percent');
        $this->data['percent'] = $percent;
        //
        $userinfo = $this->check_userinfo($cv_id);
        $this->data['userinfo'] = $userinfo;
        //
        $this->data['step'] = 4;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('courses', $this->data);
    }

    //view education data on back button
    public function back_exp($cv_id = false) {
        //  echo "hoda";
        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();
        $this->user_info->clear();
        $this->user_info->cv_id = $cv_id;

        $info = $this->user_info->get();

        foreach ($info as $item) {
            $this->data['exp_no'] = $item->exp_no;
        }
        //   var_dump($this->data['exp_no'] );
        //
        $this->experience->cv_id = $cv_id;
        $this->data['items'] = $this->experience->get();
        //
        $this->projects->cv_id = $cv_id;
        $this->data['projects'] = $this->projects->get();
        //
        $this->data['cv_id'] = $cv_id;
        $percent = $this->session->userdata('percent');
        $this->data['percent'] = $percent;
        //
        $userinfo = $this->check_userinfo($cv_id);
        $this->data['userinfo'] = $userinfo;
        //
        $this->data['step'] = 5;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('experience', $this->data);
    }

    public function back_lang($cv_id = false) {

        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();

        //
        $user_id = $this->session->userdata('userid');
        //get qualification data
        $data['quals'] = $this->qualifications->get();
        $newsendlogs = array();
        foreach ($data['quals'] as $item) {
            $newdata = array();
            $newdata['qual'] = $item;
            //
            $this->qual_user->qual_id = $item->id;
            $this->qual_user->cv_id = $cv_id;
            $this->qual_user->user_id = $user_id;
            $newdata['user_qual'] = $this->qual_user->get();
            //var_dump($newdata['user_qual']);
            $newsendlogs[] = $newdata;
            //
        }
        $this->data['query'] = $newsendlogs;

        //
        $array = array('cv_id' => $cv_id, 'grade !=' => 't');
        $this->db->where($array);
        $this->data['items'] = $this->db->get('lang');
        //
        $this->languages->grade = 't';
        $this->languages->cv_id = $cv_id;
        $this->data['item_tong'] = $this->languages->get();
        //
        $this->skills->cv_id = $cv_id;
        $this->data['skills'] = $this->skills->get();
        //
        $this->data['cv_id'] = $cv_id;
        $percent = $this->session->userdata('percent');
        $this->data['percent'] = $percent;
        //
        $userinfo = $this->check_userinfo($cv_id);
        $this->data['userinfo'] = $userinfo;
        //
        $this->data['step'] = 6;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('lang', $this->data);
    }

    //
    public function back_ref($cv_id = false) {
        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();


        $this->references->cv_id = $cv_id;
        $this->data['items'] = $this->references->get();
        //echo $this->db->last_query();
        $this->data['cv_id'] = $cv_id;
        $percent = $this->session->userdata('percent');
        $this->data['percent'] = $percent;
        //
        $userinfo = $this->check_userinfo($cv_id);
        $this->data['userinfo'] = $userinfo;
        //
        $this->data['step'] = 7;
        //
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('refrences', $this->data);
    }

    public function update_edu($cv_id = false) {


        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //check if cv_id isn't given
        if (!$cv_id)
            showx_404();
        if ($_POST) {
            $percent = $this->session->userdata('percent');
            //echo $percent;
            //echo $percent;
            $this->education->cv_id = $cv_id;
            $edu = $this->education->get();
            $this->education->clear();
            $old_percent = 0;
            if (!empty($edu)) {
                $old_percent = $edu[0]->percent;
            }

            $percent = $percent - $old_percent;
            //echo $percent;
            //delete all education items to this cv
            $this->db->where('cv_id', $cv_id)->delete('education');
            $name = $this->input->post("name");
            $count = count($name);
            //get varibles values
            $type = $this->input->post("type");
            $from_month = $this->input->post('from_month');
            $from_year = $this->input->post('from_year');
            $to_month = $this->input->post('to_month');
            $to_year = $this->input->post('to_year');
            $desc = $this->input->post("desc");
            for ($i = 0; $i < $count; $i++) {
                if (!empty($type[$i]) && !empty($name[$i]) && !empty($desc[$i])) {
                    $this_percent = 0;
                    if ($name[$i] != '') {
                        $this_percent+=(5);
                    }
                    if ($desc[$i] != '') {
                        $this_percent+=(5);
                    }
                    $data = array(
                        "type_id" => $type[$i],
                        "name" => $name[$i],
                        "cv_id" => $cv_id,
                        "from_month" => $from_month[$i],
                        "from_year" => $from_year[$i],
                        "to_month" => $to_month[$i],
                        "to_year" => $to_year[$i],
                        "desc" => $desc[$i],
                        "percent" => $this_percent);
                    $this->db->insert('education', $data);
                }
            }//end of for
            $this->education->cv_id = $cv_id;
            $new_edu = $this->education->get();
            $this->education->clear();
            $old_percent = 0;
            if (!empty($new_edu))
                $old_percent = $new_edu[0]->percent;

            //update complete cv pecent	
            $percent = $old_percent + $percent;
            $this->update_cv_percent($cv_id, $percent);
            $this->data['percent'] = $percent;
            $this->data['cv_id'] = $cv_id;
            $this->session->set_userdata('percent', $percent);
            if (isset($_POST['next_button'])) {
                redirect('add_cv/update_course/' . $cv_id);
                //$this->load->view('courses',$this->data);
            } else {

                $this->back_edu($cv_id);
            }
        } else {
            $this->back_edu($cv_id);
        }
    }

// end of function update _edu
    //view course page as next step
    public function update_course($cv_id = false) {

        //echo  $percent;
        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();
        if ($_POST) {
            $percent = $this->session->userdata('percent');
            //echo  $percent;
            $cors_percent = 0;
            $this->courses->cv_id = $cv_id;
            $items = $this->courses->get();
            $this->courses->clear();
            //echo $this->db->last_query();
            //var_dump($items);
            if (!empty($items)) {
                $cors_percent = ($items[0]->percent);
            }
            //discard  from old percent
            $percent = $percent - $cors_percent;
            //
            //echo  $percent;
            $this->db->where('cv_id', $cv_id)->delete('courses');
            //$id=$this->input->post("id");
            $name = $this->input->post("course");
            $count = count($name);
            $institute = $this->input->post("institute");
            $from_month = $this->input->post('from_month');
            $from_year = $this->input->post('from_year');
            $to_month = $this->input->post('to_month');
            $to_year = $this->input->post('to_year');
            for ($i = 0; $i < $count; $i++) {
                if (!empty($institute[$i]) || !empty($name[$i])) {
                    $this_percent = 0;
                    if ($name[$i] != '') {
                        $this_percent+=(5);
                    }
                    if ($institute[$i] != '') {
                        $this_percent+=(5);
                    }
                    $data = array(
                        "name" => $name[$i],
                        "cv_id" => $cv_id,
                        "institute" => $institute[$i],
                        "from_month" => $from_month[$i],
                        "from_year" => $from_year[$i],
                        "to_month" => $to_month[$i],
                        "to_year" => $to_year[$i],
                        //"description"=>$desc[$i],
                        "percent" => $this_percent);
                    //$this->db->where('id', $id[$i]);
                    $this->db->insert('courses', $data);
                }
            }
            $this->courses->cv_id = $cv_id;
            $cors = $this->courses->get();
            //var_dump($cors);
            if (!empty($cors)) {
                $course_percnt = $cors[0]->percent;
                //echo $course_percnt;
                $percent = $course_percnt + $percent;
            }


            //insert certification data
            $this->certifcates->cv_id = $cv_id;

            $certfs = $this->certifcates->get();
            $this->certifcates->clear();
            //get old percent for courses
            $old_percent = 0;
            if (!empty($certfs))
                $old_percent = ($certfs[0]->percent);
            //	echo $old_percent;
            //discard  from old percent
            $percent = $percent - $old_percent;
            //echo $percent;
            $this->db->where('cv_id', $cv_id)->delete('certifcates');
            //
            //$id=$this->input->post("certf_id");
            $certificate = $this->input->post("certf_course");
            $count_certf = count($certificate);
            $institute = $this->input->post("certf_institute");
            $from_month = $this->input->post('certf_from_month');
            $from_year = $this->input->post('certf_from_year');
            $to_month = $this->input->post('certf_to_month');
            $to_year = $this->input->post('certf_to_year');
            //$desc=$this->input->post("certf_desc");
            for ($i = 0; $i < $count_certf; $i++) {
                if (!empty($institute[$i]) || !empty($certificate[$i])) {
                    $this_percent = 0;
                    if ($certificate[$i] != '') {
                        $this_percent+=(5);
                    }
                    if ($institute[$i] != '') {
                        $this_percent+=(5);
                    }
                    $data = array(
                        "name" => $certificate[$i],
                        "cv_id" => $cv_id,
                        "institute" => $institute[$i],
                        "from_month" => $from_month[$i],
                        "from_year" => $from_year[$i],
                        "to_month" => $to_month[$i],
                        "to_year" => $to_year[$i],
                        "percent" => $this_percent);

                    //$this->db->where('id', $id[$i]);
                    $this->db->insert('certifcates', $data);
                }
                // 
            }
            //
            $this->certifcates->cv_id = $cv_id;
            $certfs = $this->certifcates->get();
            $cer_percent = 0;
            if (!empty($certfs)) {
                $cer_percent = $certfs[0]->percent;
                $percent = $cer_percent + $percent;
            }
            //echo $percent;
            //$data['percent']=$percent;
            //
		//$data['cv_id']=$cv_id;
            $this->update_cv_percent($cv_id, $percent);
            $this->session->set_userdata('percent', $percent);
            //if next button //got to experience page
            if (isset($_POST['next_button'])) {
                redirect('add_cv/update_exp/' . $cv_id);
            }
            //if save button
            else {
                $this->back_course($cv_id);
            }
        } else
            $this->back_course($cv_id);;
        //
    }

    //
    public function update_exp($cv_id = false) {
        //check if cv_id parameter isn't exist
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();

        //check if cv_id isn't sent
        if (!$cv_id)
            showx_404();

        if ($_POST) {
            //echo "post";
            $old_percent = 0;
            $this->experience->cv_id = $cv_id;
            $items = $this->experience->get();
            $this->experience->clear();

            $percent = $this->session->userdata('percent');
            //discard  from old percent
            if (!empty($items)) {
                $old_percent = ($items[0]->percent);
            }
            $percent = $percent - $old_percent;
            //echo $percent;
            //
		$this->db->where('cv_id', $cv_id)->delete('experience');
            //
            // insert exp yearsin user_info
            $exp_no = $this->input->post('exp_no');
            $this->user_info->cv_id = $cv_id;
            $users = $this->user_info->get();
            $this->user_info->clear();
            //var_dump($users);
            foreach ($users as $user) {
                $user_id = $user->id;
                // echo $user_id."<br>";
                $this->user_info->id = $user_id;
                $this->user_info->exp_no = $exp_no;
                $this->user_info->save();
                //echo $this->db->last_query();
            }

            //$id=$this->input->post("id");
            //$role = $this->input->post("role");
            $name = $this->input->post("exp_name");
            $count = count($name);
            $comp = $this->input->post("exp_comp");
            $from_month = $this->input->post('from_month');
            $from_year = $this->input->post('from_year');
            $till_now = $this->input->post('till_now');
            $to_month = $this->input->post('to_month');
            $to_year = $this->input->post('to_year');
            // $years = $this->input->post("years");
            $desc = $this->input->post("desc");
            //var_dump($till_now);
            for ($i = 0; $i < $count; $i++) {
                if (!empty($name[$i]) || !empty($comp[$i])) {
                    $this_percent = 0;
                    if ($name[$i] != '') {
                        $this_percent+=(5);
                    }
                    if ($comp[$i] != '') {
                        $this_percent+=(5);
                    }


                    $data = array(
                        "name" => $name[$i],
                        "company" => $comp[$i],
                        "cv_id" => $cv_id,
                        "from_month" => $from_month[$i],
                        "from_year" => $from_year[$i],
                        "to_month" => $to_month[$i],
                        "to_year" => $to_year[$i],
                        "description" => $desc[$i],
                        "percent" => $this_percent
                    );
                    $role = $this->input->post("role" . $i);
                    $roles = count($role);

                    $ser_roles = array();
                    unset($ser_roles);
                    for ($r = 0; $r < $roles; $r++) {
                        if ($role[$r] != '') {
                            $ser_roles[] = ($role[$r]);
                        }
                    }
                    if (!empty($ser_roles)) {
                        $arr_roles = array("role" => serialize($ser_roles));
                        $data = array_merge($data, $arr_roles);
                    }

                    //$this->db->where('id', $id[$i]);
                    $this->db->insert('experience', $data);
                }
            }
            //
            $this->experience->cv_id = $cv_id;
            $exp = $this->experience->get();
            $new_percent = 0;
            if (!empty($exp)) {
                $new_percent = $exp[0]->percent;
                $percent = $new_percent + $percent;
                //echo $percent;
            }
            //echo $percent;
            //update_projects
            $old_percent = 0;
            $this->projects->cv_id = $cv_id;
            $items = $this->projects->get();
            $count = count($items);
            //echo $count;
            //discard  from old percent
            if (!empty($items[0]))
                $old_percent = ($items[0]->percent);
            //echo $old_percent."</br>";
            $percent = $percent - $old_percent;
            //echo $old_percent;
            //
		$this->db->where('cv_id', $cv_id)->delete('projects');
            //$id=$this->input->post("proj_id");
            //
            $this->load->library("form_validation");
            // $this->form_validation->set_rules('proj_name[]', lang('name'), 'required');
            // if ($this->form_validation->run() == TRUE) {
            $name = $this->input->post("proj_name");
            $count = count($name);
            $link = $this->input->post("proj_link");
            $desc = $this->input->post("proj_desc");

            for ($i = 0; $i < $count; $i++) {
                if (!empty($name[$i]) || !empty($link[$i])) {
                    $this_percent = 0;
                    if ($name[$i] != '') {
                        $this_percent+=(5);
                    }
                    if ($link[$i] != '') {
                        $this_percent+=(5);
                    }
                    $data = array(
                        "name" => $name[$i],
                        "link" => $link[$i],
                        "cv_id" => $cv_id,
                        "description" => $desc[$i],
                        "percent" => $this_percent);
                    //$this->db->where('id', $id[$i]);
                    $this->db->insert('projects', $data);
                }
            }
            //
            //get percent of updated data
            $this->projects->cv_id = $cv_id;
            $all_items = $this->projects->get();
            $proj_percent = 0;
            if (!empty($all_items))
                $proj_percent = $all_items[0]->percent;
            $percent = $percent + $proj_percent;

            //$data['cv_id']=$cv_id;
            //$data['percent']=$percent;
            $this->session->set_userdata('percent', $percent);
            $this->update_cv_percent($cv_id, $percent);

            //if next button just save data
            if (isset($_POST['next_button'])) {
                redirect('add_cv/update_lang/' . $cv_id);
            }
            //if save button
            else {
                //echo $cv_id;
                $this->back_exp($cv_id);
            }
            //} //else //echo "yy";
            // $this->back_exp($cv_id);
        }// end of $post
        else
            $this->back_exp($cv_id);
    }

    public function update_lang($cv_id = false) {

        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();

        if ($_POST) {
            $user_id = $this->session->userdata('userid');
            $percent = $this->session->userdata('percent');
            $old_percent = 0;
            //
            $this->db->where("cv_id", $cv_id)->delete("qual_user");
            //echo $this->db->last_query();
            //get count of qualifications
            $qual = $this->input->post('qual');
            //$qual=$this->qualifications->get();

            $count_qual = sizeof($qual);
            //echo $count_qual;

            for ($q = 0; $q < $count_qual; $q++) {
                if (isset($qual[$q]) != '') {
                    $data = array(
                        "qual_id" => $qual[$q],
                        "cv_id" => $cv_id,
                        "user_id" => $user_id);
                    $this->db->insert('qual_user', $data);
                }
            }


            /* $array = array('cv_id' => $cv_id, 'grade !=' => 't');
              $this->db->where($array);
              $items = $this->db->get('lang'); */
            // get tngu value form db
            $this->languages->cv_id = $cv_id;
            // $this->languages->grade = 't';
            $items = $this->languages->get();

            //var_dump($item_tong);

            $old_percent = 0;
            if (!empty($items)) {
                $old_percent = $items[0]->percent;
            }

            // 
            $percent = $percent - $old_percent;
            //
            //echo $percent;
            //inset language data
            $this->db->where("cv_id", $cv_id)->delete("lang");
            $name = $this->input->post("name");
            $grade = $this->input->post("grade");
            $tongu = $this->input->post("name_tong");
            if ($tongu != '') {
                $data = array(
                    "name" => $tongu,
                    "grade" => 't',
                    "cv_id" => $cv_id,
                    "percent" => 10);
                $this->db->insert('lang', $data);
            }

            $count = count($name);
            for ($i = 0; $i < $count; $i++) {

                if (!empty($name[$i]) && !empty($grade[$i])) {
                    // echo $i;
                    $this_percent = 0;
                    if ($name[$i] != '') {
                        $this_percent+=5;
                    }
                    if ($grade[$i] != '') {
                        $this_percent+=5;
                    }
                    $data = array(
                        "name" => $name[$i],
                        "grade" => $grade[$i],
                        "cv_id" => $cv_id,
                        "percent" => $this_percent);

                    $this->db->insert('lang', $data);
                }
            }
            //
            //get percent of updated data
            $this->languages->cv_id = $cv_id;
            $all_items = $this->languages->get();
            $proj_percent = 0;
            if (!empty($all_items))
                $proj_percent = $all_items[0]->percent;
            //echo $proj_percent;
            $percent = $percent + $proj_percent;
            //
            //echo $percent;
            //
		//insert  skills data
            $this->skills->cv_id = $cv_id;
            $skill_items = $this->skills->get();

            //discard old percent of skills befor delete them
            $old_percent = 0;
            if (!empty($skill_items))
                $old_percent = ($skill_items[0]->percent);
            $percent = $percent - $old_percent;

            $this->db->where("cv_id", $cv_id)->delete("skills");

            //$id=$this->input->post("skill_id");
            $skill_name = $this->input->post("skill_name");
            $grade = $this->input->post("skill_grade");
            //
            $count = count($skill_name);
            for ($s = 0; $s < $count; $s++) {
                if (!empty($skill_name[$s]) && !empty($grade[$s])) {
                    $this_percent = 0;
                    if ($skill_name[$s] != '') {
                        $this_percent+=5;
                    }
                    if ($grade[$s] != '') {
                        $this_percent+=5;
                    }
                    $data = array(
                        "name" => $skill_name[$s],
                        "grade" => $grade[$s],
                        "cv_id" => $cv_id,
                        "percent" => $this_percent);
                    //$this->db->where('id', $id[$i]);
                    $this->db->insert('skills', $data);
                }
            }
            //
            $this->skills->cv_id = $cv_id;
            $all_items = $this->skills->get();
            $proj_percent = 0;
            if (!empty($all_items))
                $proj_percent = $all_items[0]->percent;
            $percent = $percent + $proj_percent;
            //
            $this->session->set_userdata('percent', $percent);
            $this->update_cv_percent($cv_id, $percent);
            //if next button just save data//get data of social next page 
            if (isset($_POST['next_button'])) {
                redirect('add_cv/update_ref/' . $cv_id);
            }
            //if save button
            else {
                $this->back_lang($cv_id);
            }
        } else
            $this->back_lang($cv_id);
    }

    //
    public function update_ref($cv_id = false) {

        //check if this cv_id desn't exist in  db
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        if (empty($cv))
            showx_404();
        //
        if (!$cv_id)
            showx_404();
        if ($_POST) {
            $old_percent = 0;
            $this->references->cv_id = $cv_id;
            $items = $this->references->get();

            //
            $percent = $this->session->userdata('percent');
            //	echo $percent;
            if (!empty($items))
                $old_percent = ($items[0]->percent);
            //echo $old_percent."</br>";
            $percent = $percent - $old_percent;
            //
            $this->db->where("cv_id", $cv_id)->delete("references");
            //
            //$id=$this->input->post("id");
            $name = $this->input->post("ref_name");
            $phone = $this->input->post("ref_phone");
            $mail = $this->input->post("ref_mail");
            $count = count($name);

            for ($i = 0; $i < $count; $i++) {
                if (!empty($name[$i]) || !empty($mail[$i])) {
                    $this_percent = 0;
                    if ($name[$i] != '') {
                        $this_percent+=(2.5);
                    }
                    if ($mail[$i] != '') {
                        $this_percent+=(2.5);
                    }
                    $data = array(
                        "name" => $name[$i],
                        "phone" => $phone[$i],
                        "cv_id" => $cv_id,
                        "mail" => $mail[$i],
                        "percent" => $this_percent);
                    //$this->db->where('id', $id[$i]);
                    $this->db->insert('references', $data);
                }
            }
            //
            $this->references->cv_id = $cv_id;
            $all_items = $this->references->get();
            $proj_percent = 0;
            if (!empty($all_items))
                $proj_percent = $all_items[0]->percent;
            $percent = $percent + $proj_percent;
            //


            $this->update_cv_percent($cv_id, $percent);
            $this->session->set_userdata('percent', $percent);
            //$this->data['percent']=$percent;


            if (isset($_POST['next_button'])) {
                //$this->references->cv_id=$cv_id;
                //$data['items']=$this->references->get();	
                /* $this->data['cv_id']=$cv_id;	
                  $this->data['percent']=$percent;
                  $this->load->view('load_pdf',$this->data); */
                redirect("thems/index/" . $cv_id);
            }
            //if save button
            else {
                $this->back_ref($cv_id);
            }
        } else
            $this->back_ref($cv_id);
    }

    //
    function username_check($str) {

        $allowed = array(".", "-", "_");
        if ((ctype_alnum(str_replace($allowed, '', $str))) && ctype_alpha($str[0])) {
            return TRUE;
        } else {
            $this->form_validation->set_message('username_check', lang("user_nameerror"));
            return FALSE;
        }
    }

}

?>