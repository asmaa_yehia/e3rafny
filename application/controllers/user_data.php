<?php

class user_data extends CI_Controller {

    function __construct() {
        parent:: __construct();
        if (!$this->user->login())
            redirect("auth/index");
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('phones');
        $this->load->model('cv');
        $this->load->model('social');
        $this->load->model('auth_model');
        $this->lang->load("user");
        $this->load->model("user_info");
        $this->load->model("education");
        $this->load->model("education_type");
        $this->lang->load("cv");
        $this->current_lang = LANG;
        $this->user->clear();
        $user_id = $this->session->userdata('userid');
        $this->user->id = $user_id;
        $this->data['user_status'] = $this->user->get();
        $this->user->clear();
        if (empty($this->data['user_status'])) {
            showx_404();
        }
        $this->layout = "cv";
    }

    function upload_img() {

        $this->layout = "ajax";

        $config['upload_path'] = 'assets/temp';
        // $config['allowed_types'] = 'jpg|png|jpeg|gif|bmp';
        // $config['max_size']	= '10';

        $this->load->library('uploader', $config);

        if ($this->uploader->do_upload('ajaxuploader')) {
            $datafile = $this->uploader->data();
            $this->output->set_output(json_encode(
                            array(
                                'status' => TRUE,
                                'full_url' => $datafile['full_url'],
                                'client_name' => $datafile['client_name']
            )));
        } else {
            $this->output->set_output(json_encode(array('status' => FALSE,
                'error' => $this->uploader->errors)));
        }
    }

    function delete_img() {
        $this->layout = "ajax";
        unlink($this->input->post('img_name'));
    }

    //insert user data in db	 
    function add($cv_id) {

        $this->load->helper('set_value');
        $percent = $this->input->post('percent');
        $id = $this->session->userdata('userid');

        //get old user percent
        $this->user_info->user_id = $id;
        $this->user_info->cv_id = $cv_id;
        $users = $this->user_info->get();
        $this->user->clear();
        //
        $old_percent = 0;
        if (!empty($users)) {
            foreach ($users as $user)
                $old_percent = $user->percent;
        }
        // validation
        $this->load->library("form_validation");
        $this->form_validation->set_rules('user_name', lang('user_name'), 'required');
        $this->form_validation->set_rules('adress1', lang('address'), 'required');
        $this->form_validation->set_rules('nationality', lang('nationality'), 'required');
        $this->form_validation->set_rules('day', lang('day'), 'required');
        $this->form_validation->set_rules('month', lang('month'), 'required');
        $this->form_validation->set_rules('year', lang('year'), 'required');
        $this->form_validation->set_rules('title', lang('job'), 'required');
        $this->form_validation->set_message('check_default', 'You need to select something other than the default');
        $phone_vaild = $this->add_phones($id, $cv_id);
        if ($this->form_validation->run() == TRUE && $phone_vaild == TRUE) {
            $percent = $percent - $old_percent;
            //echo $percent;
            $this->db->where('cv_id', $cv_id)->delete('user_info');
            //
            //$cv_id=$this->input->post("cv_id");	
            $this->user_info->user_id = $id;
            $this->user_info->cv_id = $cv_id;
            $this->user_info->user_cv_name = $this->input->post("user_name");
            $this->user_info->marital_status = $this->input->post("marital_status");
            $this->user_info->alter_mail = $this->input->post("alter_mail");
            $this->user_info->adress1 = $this->input->post("adress1");
            $this->user_info->adress2 = $this->input->post("adress2");
            $this->user_info->image = $this->input->post("ajaxupload");
            $this->user_info->day = $this->input->post("day");
            $this->user_info->month = $this->input->post("month");
            $this->user_info->year = $this->input->post("year");
            $this->user_info->nationality = $this->input->post("nationality");
            $this->user_info->title = $this->input->post("title");
            $this->user_info->gender = $this->input->post("gender");
            $this->user_info->license = $this->input->post("licence");
            $this->user_info->military = $this->input->post("military");
            //update percent
            $this_percent = 0;
            if ($this->input->post("user_name") != '')
                $this_percent+=3;
            if ($this->input->post("nationality") != '')
                $this_percent+=3;
            if ($this->input->post("title") != '')
                $this_percent+=3;
            if ($this->input->post("address1") != '')
                $this_percent+=3;
            if ($this->input->post("alter_mail") != '')
                $this_percent+=3;
            if ($this->input->post("ajaxupload") != '' && $this->input->post("ajaxupload") != '0')
                $this_percent+=3;
            //
            //echo $this_percent;
            $percent = $this_percent + $percent;
            //echo $percent;
            $this->user_info->percent = $this_percent;
            $this->user_info->save();
            // $info_id = $this->db->insert_id();
            //insert phones user
            //update social info
            $this->update_social($id, $cv_id);
            $img = $this->input->post('ajaxupload');
            //echo $img;
            if (is_file("assets/temp/$img")) {
                copy("assets/temp/$img", "assets/users_img/$img");
                unlink("assets/temp/$img");
            }

            //if(!empty($img)){
            //			copy("assets/temp/$img", "assets/users_img/$img");
            //			unlink("assets/temp/$img");
            //		}
            $this->user_info->clear();
            $this->update_cv_percent($cv_id, $percent);
            $this->session->set_userdata('percent', $percent);

            //var_dump($data['items']);
            if (isset($_POST['next_button'])) {
                redirect('add_cv/update_edu/' . $cv_id);
            } else {
                redirect('add_cv/view_user/' . $cv_id);
            }

            //validation desn't run
        } else {

            if (isset($users) && (!empty($users))) {
                $this->view_user($cv_id);
            } else {
                $this->view_user_validiation($cv_id);
            }
        }
    }

    //
    public function view_user($cv_id = false) {
        $percent = $this->session->userdata('percent');
        //keep value when validtion run
        $this->load->helper('set_value');
        //$this->data['value'] = value_field('military', 'n');
        //check if cv_id is exist
        $this->cv->id = $cv_id;
        $cv = $this->cv->get();
        //var_dump($cv);
        if (empty($cv))
            showx_404();
        if (!$cv_id)
            showx_404();
        $this->data['value'] = value_field('military', 'n');
        $this->data['status'] = value_field('marital_status', 'n');
        $this->data['gender'] = value_field('gender', 'm');
        // $percent = $this->session->userdata('percent');
        $user_id = $this->session->userdata('userid');
        $this->data['user'] = $this->user->get();
        // get all pones for this user
        $this->phones->cv_id = $cv_id;
        $this->phones->order_by = array("id" => "asc");
        $this->data['phones'] = $this->phones->get();
        $this->data['num_row'] = count($this->data['phones']);
        //
        $this->social->cv_id = $cv_id;
        $this->data['items'] = $this->social->get();

        $soc_num = count($this->data['items']);
        $this->data['soc_num'] = $soc_num;
        //
        $this->data['cv_id'] = $cv_id;

        //check if user data is inserted bdfore
        $this->user_info->user_id = $user_id;
        $this->user_info->cv_id = $cv_id;
        $user_info = $this->user_info->get();
        $this->data['user'] = '';
        foreach ($user_info as $item) {
            $user_info_id = $item->id;
            $this->user_info->id = $user_info_id;
            $this->data['user'] = $this->user_info->get();
            $this->data['info_id'] = $this->data['user']->id;
            $this->data['img'] = $this->data['user']->image;
        }
        $this->data['percent'] = $percent;
        //
        $this->user_info->clear();
        $this->user_info->cv_id = $cv_id;
        $user_info = $this->user_info->get();
        // echo $this->db->last_query();
        // echo  $user_info;
        $this->data['userinfo'] = $user_info;
        //
        $this->data['step'] = 2;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('user_edit', $this->data);
    }

    //
    function add_phones($id, $cv_id) {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('code[0]', lang('phone'), 'required');
        $this->form_validation->set_rules('number[0]', lang('phone'), 'required');
        if ($this->form_validation->run() == TRUE) {
            $this->db->where("user_id", $id)->delete("phones");
            $codes = $this->input->post("code");
            $numbers = $this->input->post("number");
            $result = sizeof($numbers);

            for ($i = 0; $i < $result; $i++) {

                $data = array(
                    "code" => $codes[$i],
                    "number" => $numbers[$i],
                    "user_id" => $id,
                    "cv_id" => $cv_id
                );

                if ($data['code'] != '' || $data['number'] != '') {

                    $this->db->insert("phones", $data);
                }
            }
            return TRUE;
        } else {
            return FALSE;
        }
    }

    //


    function get_user_percent($percent) {
        //echo $percent;
        //get user data  to view progress bar percent
        $user_id = $this->session->userdata('userid');
        $this->user->id = $user_id;
        $this->data['user'] = $this->user->get();
        $users = $this->data['user'];

        //echo $users->nationality;
        if ($users->nationality != '')
            $percent+=3;

        if ($users->title != '')
            $percent+=3;

        if ($users->address1 != '')
            $percent+=3;

        if ($users->alter_mail != '')
            $percent+=3;

        if ($users->image != '')
            $percent+=3;
        //echo $percent;
        return $percent;
    }

    function update_cv_percent($cv_id, $percent) {
        $this->cv->id = $cv_id;
        $this->cv->percent = $percent;
        $this->cv->save();
    }

    //
    function view_user_validiation($cv_id) {
        $percent = $this->session->userdata('percent');
        $this->load->helper('set_value');
        $this->data['value'] = value_field('military', 'n');
        $this->data['status'] = value_field('marital_status', 'n');
        $this->data['gender'] = value_field('gender', 'm');
        //
        $this->data['percent'] = $percent;
        $this->data['cv_id'] = $cv_id;
        //
        $this->phones->cv_id = $cv_id;
        $this->phones->order_by = array("id" => "asc");
        $this->data['phones'] = $this->phones->get();
        //
        $this->data['step'] = 2;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('add_user', $this->data);
    }

    function show_userinfo($cv_id = false, $percent) {
        $this->data['percent'] = $percent;
        $id = $this->session->userdata('userid');
        $this->user->id = $id;
        $this->data['user'] = $this->user->get();


        $this->phones->user_id = $id;
        $this->phones->order_by = array("id" => "asc");
        $this->data['phones'] = $this->phones->get();
        $this->data['num_row'] = count($this->data['phones']);
        $this->data['img'] = $this->data['user']->image;
        $this->phones->clear();
        $this->user->clear();
        //get all social data
        $this->social->user_id = $id;
        $this->data['items'] = $this->social->get();
        $soc_num = count($this->data['items']);
        //echo $soc_num;
        $this->data['soc_num'] = $soc_num;

        //back cv inserted before
        if ($cv_id) {
            $this->data['cv_id'] = $cv_id;
            $this->cv->id = $cv_id;
            $cvs = $this->cv->get();
            //echo $cvs->percent;
            $this->data['percent'] = $cvs->percent;
        } else {
            $user = $this->data['user'];
            $percent = 0;
            if ($user->nationality != '' && $user->nationality != '0')
                $percent+=3;
            if ($user->title != '' && $user->title != '0')
                $percent+=3;
            if ($user->address1 != '' && $user->address1 != '0')
                $percent+=3;
            if ($user->alter_mail != '' && $user->alter_mail != '0')
                $percent+=3;
            if ($user->image != '' && $user->image != '0')
                $percent+=3;
            $this->data['percent'] = $percent;
        }
        $this->user_info->cv_id = $cv_id;
        $user_info = $this->user_info->get();
        $this->data['userinfo'] = $userinfo;
        //
        $this->data['step'] = 2;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view('user_edit', $this->data);
    }

    function edit_user() {
        $id = $this->session->userdata('userid');
        $this->user->id = $id;
        $this->load->library("form_validation");

        $this->form_validation->set_rules('first_name', lang('first_name'), 'required');
        $this->form_validation->set_rules('last_name', lang('lang_name'), 'required');
        $this->form_validation->set_rules('user_name', lang('user_name'), 'required|is_unique[user.user_name]|alpha_dash|trim');
        $this->form_validation->set_rules('password', lang('password'), 'matches[re_password]');
        $this->form_validation->set_rules('re_password', lang('re_password'), 'matches[password]');

        if ($this->form_validation->run() == FALSE) {
            //if(!$id) showx_404();
            $this->user->id = $id;
            $this->data['user'] = $this->user->get();
            $this->user->clear();

            // if(!$this->data['user']) showx_404();
            $this->load->view("userprofile_edit", $this->data);
        } else {

            if ($this->input->post("password") != '') {
                $this->user->password = md5($this->input->post("password"));
                $pass = md5($this->input->post("password"));
            } else {
                $this->user->id = $this->input->post("id");
                $this->data['users'] = $this->user->get();

                $this->user->password = $this->data['users']->password;
                $pass = $this->data['users']->password;
                $this->user->clear();
            }
            $this->user->id = $id;
            $this->user->first_name = $this->input->post("first_name");
            $this->user->last_name = $this->input->post("last_name");
            $this->user->user_name = $this->input->post("user_name");
            $this->user->password = $pass;

            $this->user->save();
            $this->user->clear();
        }
    }

    public function update_social($user_id, $cv_id) {
        //echo $cv_id;
        $this->db->where("cv_id", $cv_id)->delete("social");
        $name_soc = $this->input->post("name");
        //var_dump($name_soc);
        $link = $this->input->post("link");
        $social_result = sizeof($link);
        //echo $name_soc[0];
        for ($s = 0; $s < $social_result; $s++) {
            $data = array(
                "user_id" => $user_id,
                "cv_id" => $cv_id,
                "name" => $name_soc[$s],
                "link" => $link[$s]
            );
            if ($data['name'] != '' && $data['link'] != '') {
                $this->db->insert("social", $data);
            }
        }
    }

}
