<?php

class edu_type extends CI_Controller {

    function __construct() {
        //echo "in constructor";
        parent:: __construct();
        if (!$this->user->login())
            redirect("auth/index");
            $this->load->model('education_type');
    }

    public function index($lang) {
        //echo $lang;
        $types = $this->education_type->get();
        foreach ($types as $type) {
            echo $type->{"type_" . $lang} . '*' . $type->id . '#';
        }
    }

}
