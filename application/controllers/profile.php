<?php
class Profile extends CI_Controller {
    public function __construct() {
    parent::__construct();
	$this->load->helper('url');
	$this->load->helper('form');
	$this->load->model('cv');
	$this->load->model('phones');
	$this->load->model('education');
	$this->load->model('education_type');
	$this->load->model('courses');
	$this->load->model('experience');
	$this->load->model('languages');
	$this->load->model('skills');
	$this->load->model('projects');
	$this->load->model('references');
	$this->load->model('social');
	$this->load->model('recommend');
	$this->load->model('pdf_thems');
	$this->load->model('web_thems');
	$this->load->model('projects');
	$this->load->model('cv_recommendation');
	$this->load->model('qualifications');
	$this->load->model('certifcates');
	$this->load->model('qual_user');
	$this->load->model('projects');
	$this->load->model("user_info");
    }

    public function index()
    {
       $user_name = $this->uri->segment(1);
	   $cv_title = $this->uri->segment(2);
	   $cv_title= urldecode($cv_title);
        if ((empty($user_name)) || ($this->uri->segment(2)==FALSE)) {
            $this->displayPageNotFound();
        } else{
			$this->user->user_name = $user_name;
			$this->data['user_num'] = $this->user->get();
			$this->user->clear();
			
			$this->cv->url_title = $cv_title;
			$this->data['cv_num'] = $this->cv->get();
			$this->cv->clear();
			
			if((count($this->data['user_num'])==0) ||(count($this->data['cv_num'])==0)){
				$this->displayPageNotFound();
			}
			else {
				$this->data['user_name']=$user_name;
				$this->user->user_name = $user_name;
				$this->data['user_data'] = $this->user->get();
				$this->user->clear();
				if($this->data['user_data'][0]->status==0) showx_404();
				$user_id=$this->data['user_data'][0]->id ;
				$this->data['user_id']=$user_id;
				$this->user->id = $user_id;
				$this->data['user'] = $this->user->get();
				$this->user->clear();
				//get cv data
				$this->cv->user_id = $user_id;
				$this->cv->url_title = $cv_title;
				$this->data['cv'] = $this->cv->get();
				$file_name=$this->data['cv'][0]->title;
				$this->cv->clear();
				$cv_id=$this->data['cv'][0]->id;
				if($this->data['cv'][0]->percent<35) showx_404();
				
				$this->user_info->cv_id = $cv_id;
        		$this->data['user_info'] = $this->user_info->get();
				$this->user_info->clear();
			
				
				//get phones
				$this->phones->cv_id = $cv_id;
				$this->data['phones'] = $this->phones->get();
				$this->phones->clear();
				
				//get education
				$this->education->cv_id = $cv_id;
				$this->education->select = array("education.*", "education_type.type_".$this->data['cv'][0]->lang." as type");
				$this->education->join = array("education_type" => "education_type.id = education.type_id");
				$this->data['education'] = $this->education->get();
				$this->education->clear();
				
				//get courses
				$this->courses->cv_id = $cv_id;
				$this->data['courses'] = $this->courses->get();
				$this->courses->clear();
				
				//get certifcates
				$this->certifcates->cv_id = $cv_id;
				$this->data['certifcates'] = $this->certifcates->get();
				$this->certifcates->clear();
				
				//get experience
				$this->experience->cv_id = $cv_id;
				$this->data['experience'] = $this->experience->get();
				$this->experience->clear();
				
				//get projects
				$this->projects->cv_id = $cv_id;
				$this->data['projects'] = $this->projects->get();
				$this->projects->clear();
				
				//get langs
				$this->languages->cv_id = $cv_id;
				$this->data['langs'] = $this->languages->get();
				$this->languages->clear();
				
				//get skills
				$this->skills->cv_id = $cv_id;
				$this->data['skills'] = $this->skills->get();
				$this->skills->clear();
				
				//get qualifications
				$this->qual_user->cv_id = $cv_id;
				$this->qual_user->select = array("qual_user.*", "qualifications.name_".$this->data['cv'][0]->lang." as name");
				$this->qual_user->join = array("qualifications" => "qualifications.id =qual_user.qual_id");
				$this->data['qualifications'] = $this->qual_user->get();
				$this->qual_user->clear();
				
				//get refrences
				$this->references->cv_id = $cv_id;
				$this->data['references'] = $this->references->get();
				$this->references->clear();
				
				//get social links
				$this->social->cv_id = $cv_id;
				$this->data['social'] = $this->social->get();
				$this->social->clear();
				
				if($this->data['cv'][0]->lang=='en'){
					$this->lang->load('cvdata', 'english');
				}elseif($this->data['cv'][0]=='ar'){
					$this->lang->load('cvdata', 'arabic');
				}
				
				
				//get recomnds
//				$this->cv_recommendation->cv = $cv_id;
//				$this->cv_recommendation->select = array("cv_recommendation.*", "recommend.name as name", "recommend.date as date", "recommend.job as job", "recommend.recommend as recommend", "recommend.job as job", "recommend.id as id");
//				$this->cv_recommendation->join = array("recommend" => "recommend.id = cv_recommendation.recomnd_id");
//				$this->data['recommends'] = $this->cv_recommendation->get();
//				$this->cv_recommendation->clear();
                                
                                $this->recommend->cv = $cv_id;
                                $this->recommend->status = '1';
                                $this->data['recommends'] = $this->recommend->get();
                                $this->recommend->clear();
				
				
				if($this->data['cv'][0]->lang=='en'){
					$this->lang->load('cvdata', 'english');
					$this->lang->load('form_validation', 'english');
				}elseif($this->data['cv'][0]->lang=='ar'){
					$this->lang->load('cvdata', 'arabic');
					$this->lang->load('form_validation', 'arabic');
				}
				
				//get web_them
				$web_them=$this->data['cv'][0]->style_them;
				
				$this->web_thems->id = $web_them;
				$this->data['web_thempage'] = $this->web_thems->get();
				
				$this->web_thems->clear();
				if(empty($this->data['web_thempage'])) {
					$this->web_thems->order_by = array("id" => "ASC");
					$this->web_thems->limit="1";
					$this->data['web_themsdefault'] = $this->web_thems->get();
					$this->web_thems->clear();
					$them=$this->data['web_themsdefault'][0]->file_name;
				
                                        $style_name= $this->data['web_themsdefault'][0]->style_name;
                                } else{
                                    $them = $this->data['web_thempage']->file_name;
                                    $style_name= $this->data['web_thempage']->style_name;
                                }    
                                $this->web_thems->clear();
                               $this->data['style_name']=$style_name;
				
				$this->load->view($them, $this->data);
				
				/*$this->load->library("form_validation");
				$this->form_validation->set_rules("name", lang('name'), "trim|required");
				$this->form_validation->set_rules("job", lang('job'), "trim|required");
				$this->form_validation->set_rules("recommend", lang('recommend'), "trim|required");
		
				if ($this->form_validation->run()) {
					$this->recommend->user_id = $user_id;
					$this->recommend->name = $this->input->post('name');
					$this->recommend->job = $this->input->post('job');
					$this->recommend->recommend = $this->input->post('recommend');
					$this->recommend->date = time();
					$this->recommend->status = 0;
					$this->recommend->save();	
					$this->session->set_flashdata('result', lang('done'));
					$this->data['result']=lang('done');
					$this->load->view($them, $this->data);
					
				}else{
					echo validation_errors();
					$this->load->view($them, $this->data);
				}*/
			}
		}
    }

    protected function displayPageNotFound() {
        $this->layout="ajax";
        $this->output->set_status_header('404');
        $this->load->view('404');
    }
	public function save() {
				$cv_lang=$this->input->post('cv_lang');
				if($cv_lang=='en'){
					$this->lang->load('cvdata', 'english');
					$this->lang->load('form_validation', 'english');
				}elseif($cv_lang=='ar'){
					$this->lang->load('cvdata', 'arabic');
					$this->lang->load('form_validation', 'arabic');
				}
        		$this->load->library("form_validation");
				$this->form_validation->set_rules("name", lang('name'), "trim|required");
				$this->form_validation->set_rules("job", lang('job'), "trim|required");
				$this->form_validation->set_rules("recommend", lang('recommend'), "trim|required");
		
				if ($this->form_validation->run()) {
					$this->recommend->cv = $this->input->post('cv');
                                        $this->recommend->user_id = $this->input->post('user_id');
					$this->recommend->name = $this->input->post('name');
					$this->recommend->job = $this->input->post('job');
					$this->recommend->recommend = $this->input->post('recommend');
					$this->recommend->date = time();
					$this->recommend->status = 0;
					$this->recommend->save();	
					
					echo json_encode(lang('done'));
					
				}else{
					echo json_encode($this->form_validation->error_array()); 
					
				}
    }
	
}
