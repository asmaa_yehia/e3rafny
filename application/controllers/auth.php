<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class auth extends CI_Controller {
	
    var $layout = 'ajax';
    public $user;
	private $connection;

    function __construct() {
        parent:: __construct();
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('auth_model');
        $this->load->library('facebook', array("appId" => '259037567621500', "secret" => '7e75fab1b7208b45eeb94fbfa4896e3a'));
        $user = $this->facebook->getUser();
        $this->lang->load("user");
        $this->current_lang = LANG;
		
		$this->load->library('twitteroauth');
		// Loading twitter configuration.
		$this->config->load('twitter');
		
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// If user already logged in
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('access_token'),  $this->session->userdata('access_token_secret'));
		}
		elseif($this->session->userdata('request_token') && $this->session->userdata('request_token_secret'))
		{
			// If user in process of authentication
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'), $this->session->userdata('request_token'), $this->session->userdata('request_token_secret'));
		}
		else
		{
			// Unknown user
			$this->connection = $this->twitteroauth->create($this->config->item('twitter_consumer_token'), $this->config->item('twitter_consumer_secret'));
		}
		
    }
	
function show_home(){
	//session_destroy();
		$this->load->view('main_home');
    }
    
    function index() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules("mail", lang('email'), "trim|required|valid_email");
        $this->form_validation->set_rules("password", lang('password'), "trim|required");

        if ($this->form_validation->run()) {
            $this->user->mail = $this->input->post('mail');
            $this->user->or_where = array("id" => $this->input->post('mail'));
            $this->user->password = md5($this->input->post('password'));
            //echo $this->input->post('remember');
            if ($this->user->login($this->input->post('remember'))) {
                redirect('index/index');
            } else {
                $this->session->set_flashdata('msg', lang('login_error'));
                redirect('auth/index');
            }
        }
        //echo $this->db->last_query();
        $this->load->view('login');
    }

    function register() {
		
        $this->load->view("register");
    }

    //generate activation key for register and forget password
    function generate_key() {
        // Create a unique  activation code:
        $activation = md5(uniqid(rand(), true));
        $this->user->activation = $activation;
        $this->data['activation'] = $this->user->get();
        if (count($this->data['activation']) != 0) {
            $this->generate_key();
        } else {
            return $activation;
        }
        $this->user->clear();
    }

    //generate user name for register 
    function generate_username() {
        // Create a unique  username:
		
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    	$randomString = '';
    	for ($i = 0; $i < 2; $i++) {
        	$randomString .= $characters[rand(0, strlen($characters) - 1)];
   		 }
		
        $user_name = rand();
		$final_user_name=$randomString.$user_name;
        $this->user->user_name = $final_user_name;
        $this->data['user_name'] = $this->user->get();
        if (count($this->data['user_name']) != 0) {
            $this->generate_username();
        } else {
            return $final_user_name;
        }
        $this->user->clear();
    }

    //insert data in db	 

    function add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('first_name', lang('first_name'), 'required');
        $this->form_validation->set_rules('last_name', lang('last_name'), 'required');
        $this->form_validation->set_rules('email', lang('email'), 'required|valid_email|is_unique[user.mail]');
        $this->form_validation->set_rules('password', lang('password'), 'required');
        $this->form_validation->set_rules('re_password', lang('re_password'), 'required|matches[password]');


        if ($this->form_validation->run() == FALSE) {
            $this->register();
        } else {
            $activation = $this->generate_key();
            $user_name = $this->generate_username();
            $this->user->first_name = $this->input->post("first_name");
            $this->user->last_name = $this->input->post("last_name");
            $this->user->mail = $this->input->post("email");
            $this->user->password = md5($this->input->post("password"));
            $this->user->activation = $activation;
            $this->user->user_name = $user_name;

            // Send the email:
            $message = lang("reg_confirmation_msg") . "\n\n";
            $message .= site_url() . "/auth/activate/" . urlencode($this->input->post("email")) . "/" . $activation;
            $headers = 'From: ' . email . "\r\n" .
                    'Reply-To: ' . email . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            mail($this->input->post("email"), lang("reg_confirmation"), $message, $headers);
            $this->user->save();
            $insert_id = $this->db->insert_id();
            $folder_name = 'u_' . $insert_id;
            mkdir("assets/" . $folder_name);
            $this->user->clear();
			//echo $message;
            redirect("auth/index");
        }
    }

    public function facebook_login() {
        if ($this->user) {
            try {
                $user_profile = $this->facebook->api('/me');

                //print_r($user_profile);
                //echo $user_profile['email'];
            } catch (FacebookApiException $e) {

                $this->user = null;

                //$this->data['error_msg']=lang('fb_login_error');
                //$this->load->view('login',$this->data);
            }
        }
        //print_r ($this->user);
        if ($this->user) {
            $logout = $this->facebook->getLogoutUrl(array("next" => base_url() . 'auth/f_logout/'));
            //echo "<a href='$logout'>logout</a>";
            //print_r($user_profile);
            $this->user->fb_id = $user_profile['id'];
            $this->data['users'] = $this->user->get();
            $this->user->clear();

            if (count($this->data['users']) == '0') {
                $this->data['user_profile'] = $user_profile;
                $this->load->view("f_register", $this->data);
            } else {
                $this->session->set_userdata(array('userid' => $this->data['users'][0]->id, 'mail' => $this->data['users'][0]->mail));
                redirect("index/index");
            }
        } else {
            $login = $this->facebook->getLoginUrl(array("scope" => 'email'));
            //echo "<a href='$login'>login</a>";
			redirect($login);
            //$this->data['error_msg']=lang('fb_login_error');
            //$this->load->view('login',$this->data);
        }
    }

    public function f_logout() {
        //redirect($this->facebook->getLogoutUrl(array("next"=>base_url().'auth/f_logout/'))); 
        session_destroy();
        //redirect("auth/f_login");
    }

    function facebook_add() {

        $this->load->library("form_validation");
        $this->form_validation->set_rules('first_name', lang('first_name'), 'required');
        $this->form_validation->set_rules('last_name', lang('last_name'), 'required');
        $this->form_validation->set_rules('email', lang('email'), 'required|valid_email|is_unique[user.mail]');



        if ($this->form_validation->run() == FALSE) {
            $this->facebook_login();
        } else {
            $activation = $this->generate_key();
            $user_name = $this->generate_username();
            $this->user->first_name = $this->input->post("first_name");
            $this->user->last_name = $this->input->post("last_name");
            $this->user->mail = $this->input->post("email");
            $this->user->fb_id = $this->input->post("fb_id");
            $this->user->activation = $activation;
            $this->user->user_name = $user_name;

            // Send the email:
            $message = lang("reg_confirmation_msg") . "\n\n";
            $message .= site_url() . "/auth/activate/" . urlencode($this->input->post("email")) . "/" . $activation;
            $headers = 'From: ' . email . "\r\n" .
                    'Reply-To: ' . email . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            mail($this->input->post("email"), lang("reg_confirmation"), $message, $headers);
            $this->user->save();
            $insert_id = $this->db->insert_id();
            $folder_name = 'u_' . $insert_id;
            mkdir("assets/" . $folder_name);
            $this->user->clear();
            $this->session->set_userdata(array('userid' => $insert_id, 'mail' => $this->input->post("email")));
            redirect("index/index");
        }
    }
	
	function tw_add() {
        $this->load->library("form_validation");
        $this->form_validation->set_rules('first_name', lang('first_name'), 'required');
        $this->form_validation->set_rules('last_name', lang('last_name'), 'required');
        $this->form_validation->set_rules('email', lang('email'), 'required|valid_email|is_unique[user.mail]');



        if ($this->form_validation->run() == FALSE) {
            $this->twitter_login();
		  // redirect("twitter/auth");
        } else {
            $activation = $this->generate_key();
            $user_name = $this->generate_username();
            $this->user->first_name = $this->input->post("first_name");
            $this->user->last_name = $this->input->post("last_name");
            $this->user->mail = $this->input->post("email");
            $this->user->twitter_id = $this->input->post("twitter_id");
            $this->user->activation = $activation;
            $this->user->user_name = $user_name;

            // Send the email:
            $message = lang("reg_confirmation_msg") . "\n\n";
            $message .= site_url() . "/auth/activate/" . urlencode($this->input->post("email")) . "/" . $activation;
            $headers = 'From: ' . email . "\r\n" .
                    'Reply-To: ' . email . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            mail($this->input->post("email"), lang("reg_confirmation"), $message, $headers);
            $this->user->save();
            $insert_id = $this->db->insert_id();
            $folder_name = 'u_' . $insert_id;
            mkdir("assets/" . $folder_name);
            $this->user->clear();
            $this->session->set_userdata(array('userid' => $insert_id, 'mail' => $this->input->post("email")));
            redirect("index/index");
        }
    }

    //change user status to be active
    function activate($email, $key) {
        $this->user->activation = $key;
        $data['user_info'] = $this->user->get();
        $this->user->clear();
        if (count($data['user_info']) == 0) {

            $this->data['email'] = urldecode($email);
            $this->load->view('error', $this->data['email']);
        } else {
            $this->user->activation = $key;
            $data['user'] = $this->user->get();
            $this->user->id = $data['user']['0']->id;
            $this->user->status = 1;
            $this->user->save();
            $this->user->clear();
            $this->session->set_flashdata('msg', lang('activation_done'));
            redirect("auth/index");
        }
    }

	function done(){
		$this->load->view('done');
	}

    function new_activation($email) {
		
        $this->user->mail = urldecode($email);
        $data['users'] = $this->user->get();
        $this->user->clear();
        $this->user->id = $data['users']['0']->id;
        $activation = $this->generate_key();
        $this->user->activation = $activation;
        // Send the email:
        $message = lang('reg_confirmation_msg') . "\n\n";
        $message .= site_url() . "/auth/activate/" . urlencode($email) . "/" . $activation;
        $headers = 'From: ' . email . "\r\n" .
                'Reply-To: ' . email . "\r\n" .
                'X-Mailer: PHP/' . phpversion();
        mail(urldecode($email), lang('reg_confirmation'), $message, $headers);
        $this->user->save();
        $this->user->clear();
        $this->session->set_flashdata('msg', lang('newactivation_mailsent'));
		redirect("auth/done");
		//echo $message;
       // $this->load->view('done');
    }

    public function logout() {
        $this->session->unset_userdata(array('userid' => '', 'mail' => ''));
        $cookie = array('name' => 'userid', 'value' => '', 'expire' => 0);
        $this->input->set_cookie($cookie);
        $cookie2 = array('name' => 'mail', 'value' => '', 'expire' => 0);
        $this->input->set_cookie($cookie2);
        $this->f_logout();
		
		//$this->session->sess_destroy();
		redirect('auth/index');
    }

    function fgpass() {

        $this->load->library("form_validation");
        $this->form_validation->set_error_delimiters("<div class='form-msg-error-advanced'>", "</div>");

        $this->form_validation->set_rules("email", lang('email'), "trim|required|valid_email");

        if ($this->form_validation->run()) {
            $activation = $this->generate_key();
            // Send the email:
            $message = lang('forget_pass_message') . "\n\n";
            $message .= site_url() . "/auth/fg_pass/" . urlencode($this->input->post("email")) . "/" . $activation;
            $headers = 'From: ' . email . "\r\n" .
                    'Reply-To: ' . email . "\r\n" .
                    'X-Mailer: PHP/' . phpversion();
            mail($this->input->post("email"), lang('forget_pass_title'), $message, $headers);
            $this->auth_model->activation = $activation;
            $this->auth_model->mail = $this->input->post("email");
            $this->auth_model->save();
            $this->data["msg"] = lang('fgpass_mailsent');
            $this->load->view('fgpass', $this->data);
        } else {
            $this->data["msg"] = "";
            $this->load->view('fgpass', $this->data);
        }
    }

    function fg_pass($email, $key) {
        $this->user->activation = $key;
        $data['user_data'] = $this->user->get();
        $this->user->clear();
        if (count($data['user_data']) == 0) {
            $this->load->view('404');
        } else {
            $this->data['key'] = $key;
            $this->data['email'] = $email;

            $this->load->library("form_validation");

            $this->form_validation->set_rules('password', lang('password'), 'required');
            $this->form_validation->set_rules('re_password', lang('re_password'), 'required|matches[password]');

            if ($this->form_validation->run()) {
                $this->user->activation = $key;
                $data['user'] = $this->user->get();
                $this->user->clear();
                $this->user->id = $data['user']['0']->id;
                $this->user->password = md5($this->input->post("password"));
                $this->user->save();

                redirect('auth/index');
            }

            $this->load->view('fg_pass', $this->data);
        }
        $this->user->clear();
    }
	
	//twitter
	
	public function twitter_login()
	{
		if($this->session->userdata('access_token') && $this->session->userdata('access_token_secret'))
		{
			// User is already authenticated. Add your user notification code here.
			//redirect(base_url('/'));
			$content = $this->connection->get('account/verify_credentials');
			
			//$this->user->twitter_id = $content->id;
			$this->user->twitter_id = $this->session->userdata('twitter_user_id');
            $this->data['users'] = $this->user->get();
            $this->user->clear();

            if (count($this->data['users']) == '0') {
                $this->data['user_profile'] = $content;
                $this->load->view("tw_register", $this->data);
            } else {
				//echo "hii";
                $this->session->set_userdata(array('userid' => $this->data['users'][0]->id, 'mail' => $this->data['users'][0]->mail));
                redirect("index/index");
            }
			
			//print_r($content);
			//echo "hiiii";
		}
		else
		{
			// Making a request for request_token
			$request_token = $this->connection->getRequestToken(site_url('auth/callback'));

			$this->session->set_userdata('request_token', $request_token['oauth_token']);
			$this->session->set_userdata('request_token_secret', $request_token['oauth_token_secret']);
			
			if($this->connection->http_code == 200)
			{
				$url = $this->connection->getAuthorizeURL($request_token);
				
				redirect($url);
				//$content = $this->connection->get('account/verify_credentials');
				//print_r ($content);
			}
			else
			{
				// An error occured. Make sure to put your error notification code here.
				redirect(site_url('/'));
				//echo "error";
			}
		}
	}
	
	/**
	 * Callback function, landing page for twitter.
	 * @access	public
	 * @return	void
	 */
	public function callback()
	{
	if($this->input->get('denied')){
			//session_destroy();
			$this->session->sess_destroy();
			redirect ("auth/show_home");
			}else{
		if($this->input->get('oauth_token') && $this->session->userdata('request_token') !== $this->input->get('oauth_token'))
		{
			$this->reset_session();
			//echo "hi";
			redirect(site_url('/auth/twitter_login'));
		}
		else
		{
			$access_token = $this->connection->getAccessToken($this->input->get('oauth_verifier'));
			
		
			if ($this->connection->http_code == 200)
			{
				$this->session->set_userdata('access_token', $access_token['oauth_token']);
				$this->session->set_userdata('access_token_secret', $access_token['oauth_token_secret']);
				$this->session->set_userdata('twitter_user_id', $access_token['user_id']);
				$this->session->set_userdata('twitter_screen_name', $access_token['screen_name']);

				$this->session->unset_userdata('request_token');
				$this->session->unset_userdata('request_token_secret');
				
				
				//redirect(base_url('/'));
				redirect("auth/twitter_login");
			}
			else
			{
				// An error occured. Add your notification code here.
				redirect(site_url('/'));
				//echo "erroor";
				
			}
		}
			}
	}
	
	
	/**
	 * Reset session data
	 * @access	private
	 * @return	void
	 */
	private function reset_session()
	{
		$this->session->unset_userdata('access_token');
		$this->session->unset_userdata('access_token_secret');
		$this->session->unset_userdata('request_token');
		$this->session->unset_userdata('request_token_secret');
		$this->session->unset_userdata('twitter_user_id');
		$this->session->unset_userdata('twitter_screen_name');
	}

}
