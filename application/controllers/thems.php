<?php

class thems extends CI_Controller {

    function __construct() {
        parent:: __construct();
        if (!$this->user->login())
            redirect("auth/index");
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->model('cv');
        $this->load->model('phones');
        $this->load->model('education');
        $this->load->model('education_type');
        $this->load->model('courses');
        $this->load->model('experience');
        $this->load->model('languages');
        $this->load->model('skills');
        $this->load->model('projects');
        $this->load->model('references');
        $this->load->model('social');
        $this->load->model('recommend');
        $this->load->model('pdf_thems');
        $this->load->model('web_thems');
        $this->load->model('projects');
        $this->load->model('qualifications');
        $this->load->model('certifcates');
        $this->load->model('qual_user');
        $this->load->model('projects');
        $this->load->model("user_info");
        $this->load->model('cv_recommendation');
        $this->lang->load("user");
        $this->lang->load("cv");
        $user_id = $this->session->userdata('userid');
        $this->user->id = $user_id;
        $this->data['user_status'] = $this->user->get();
        $this->user->clear();
		if(empty($this->data['user_status'])){
			showx_404();
		}
        $this->layout = "cv";
    }

    function index($cv_id = false) {

        $this->cv->id = $cv_id;
        $this->data['cv'] = $this->cv->get();
        $this->cv->clear();

        $this->pdf_thems->order_by = array("id" => "ASC");
        $this->pdf_thems->select = array("pdf_thems.*", "pdf_thems.img_" . $this->data['cv']->lang . " as img");
        $this->data['pdf_thems'] = $this->pdf_thems->get();
        $this->pdf_thems->clear();

        $this->pdf_thems->order_by = array("id" => "ASC");
        $this->pdf_thems->limit = "1";
        $this->data['pdf_themsdefault'] = $this->pdf_thems->get();
        $this->pdf_thems->clear();
        //
        $this->data['cv_id'] = $cv_id;
        $this->user_info->cv_id = $cv_id;
        $user_info = $this->user_info->get();
        $this->data['userinfo'] = $user_info;
        //
        $percent = $this->data['cv']->percent;
        $this->data['percent'] = $percent;
        $this->data['step'] = 8;
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        //
        $this->load->view("pdf_thems", $this->data);
    }

    //generate cv image
    function generate_imgname() {
        // Create a unique  username:
        $img_name = rand();
        $this->cv->img = $img_name;
        $this->data['img_name'] = $this->cv->get();
        if (count($this->data['img_name']) != 0) {
            $this->generate_imgname();
        } else {
            return $img_name;
        }
        $this->cv->clear();
    }

    function save_pdfthem() {
        $user_id = $this->session->userdata('userid');
        $pdf_id = $this->input->post("id");
        $cv_id = $this->input->post("cv_id");
        if (isset($_POST['button_next']) || isset($_POST['button_save'])) {
            $this->cv->id = $cv_id;
            $this->cv->pdf_them = $pdf_id;
            $this->cv->save();
            $this->cv->clear();
            $folder_name = 'u_' . $user_id;


            $this->load->library('pdf_library');
            //$pdf = $this->pdf_library->load();
            //$pdf->SetAutoFont(AUTOFONT_RTL);

            $this->user->id = $user_id;
            $this->data['user'] = $this->user->get();
            $this->user->clear();

            $this->user_info->cv_id = $cv_id;
            $this->data['user_info'] = $this->user_info->get();
            $this->user_info->clear();
            //var_dump($this->data['user_info']);
            //get cv data
            $this->cv->user_id = $user_id;
            $this->cv->id = $cv_id;
            $this->data['cv'] = $this->cv->get();
            $file_name = $this->data['cv']->title;
            $path = 'assets/' . $folder_name . '/' . 'cv.pdf';
            $this->cv->clear();

            //get phones
            $this->phones->cv_id = $cv_id;
            $this->data['phones'] = $this->phones->get();
            $this->phones->clear();

            //get education
            $this->education->cv_id = $cv_id;
            $this->education->select = array("education.*", "education_type.type_" . $this->data['cv']->lang . " as type");
            $this->education->join = array("education_type" => "education_type.id = education.type_id");
            $this->data['education'] = $this->education->get();
            $this->education->clear();

            //get courses
            $this->courses->cv_id = $cv_id;
            $this->data['courses'] = $this->courses->get();
            $this->courses->clear();

            //get certifcates
            $this->certifcates->cv_id = $cv_id;
            $this->data['certifcates'] = $this->certifcates->get();
            $this->certifcates->clear();

            //get experience
            $this->experience->cv_id = $cv_id;
            $this->data['experience'] = $this->experience->get();
            $this->experience->clear();

            //get projects
            $this->projects->cv_id = $cv_id;
            $this->data['projects'] = $this->projects->get();
            $this->projects->clear();

            //get langs
            $this->languages->cv_id = $cv_id;
            $this->data['langs'] = $this->languages->get();
            $this->languages->clear();

            //get skills
            $this->skills->cv_id = $cv_id;
            $this->data['skills'] = $this->skills->get();
            $this->skills->clear();

            //get qualifications
            $this->qual_user->cv_id = $cv_id;
            $this->qual_user->select = array("qual_user.*", "qualifications.name_" . $this->data['cv']->lang . " as name");
            $this->qual_user->join = array("qualifications" => "qualifications.id =qual_user.qual_id");
            $this->data['qualifications'] = $this->qual_user->get();
            $this->qual_user->clear();

            //get refrences
            $this->references->cv_id = $cv_id;
            $this->data['references'] = $this->references->get();
            $this->references->clear();

            //get social links
            $this->social->cv_id = $cv_id;
            $this->data['social'] = $this->social->get();
            $this->social->clear();

            if ($this->data['cv']->lang == 'en') {
                $this->lang->load('cvdata', 'english');
            } elseif ($this->data['cv']->lang == 'ar') {
                $this->lang->load('cvdata', 'arabic');
            }

            //get pdf_them
            $pdf_them = $this->data['cv']->pdf_them;
            $this->pdf_thems->id = $pdf_them;
            $this->data['pdf_thempage'] = $this->pdf_thems->get();
            $this->pdf_thems->clear();
            if (empty($this->data['pdf_thempage'])) {
                $this->pdf_thems->order_by = array("id" => "ASC");
                $this->pdf_thems->limit = "1";
                $this->data['pdf_themsdefault'] = $this->pdf_thems->get();
                $this->pdf_thems->clear();
                $them = $this->data['pdf_themsdefault'][0]->file_name;
            } else
                $them = $this->data['pdf_thempage']->file_name;

            $html = $this->load->view($them, $this->data, true);
            if ($them == 'them4' || $them == 'them5') {
                $pdf = $this->pdf_library->load('utf-8', 'A4', "", "", 0, 0, 0, 0, 0, 0, 'P');

                $pdf->AddPage('p', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 'side-bar');
            } elseif (($them != 'them1') && ($them != 'them2')) {
                $pdf = $this->pdf_library->load();
                $pdf->AddPage('P', // L - landscape, P - portrait
                        '', '', '', '', 0, // margin_left
                        0, // margin right
                        0, // margin top
                        0, // margin bottom
                        0, // margin header
                        0); // margin footer
            } else {
                $pdf = $this->pdf_library->load();
            }

            $pdf->SetDisplayMode('fullpage');
            $pdf->WriteHTML($html); // write the HTML into the PDF
            $filename = $_SERVER['DOCUMENT_ROOT'] . 'assets/' . $folder_name . '/' . 'cv.pdf';
            $pdf->Output($path, "F"); // save to file because we can
            //$pdf->Output($path); 
            $img_name = $this->generate_imgname();
            $img_path = 'assets/' . $folder_name . '/' . $img_name;
            $this->cv->id = $cv_id;
            $this->cv->img = $img_name . ".jpg";
            $this->cv->save();
            $this->cv->clear();

            exec("convert -verbose -density 150 -trim -quality 100 " . $path . "[0] -resize 100% " . $img_path . ".jpg");
            //unlink($path);

            if (isset($_POST['button_next'])) {
                redirect("thems/web_thems/" . $cv_id);
            } else {
                redirect("thems/index/" . $cv_id);
            }
        } elseif (isset($_POST['back'])) {
            redirect("add_cv/update_ref/" . $cv_id);
        }
    }

    function web_thems($cv_id = false) {

        $this->cv->id = $cv_id;
        $this->data['cv'] = $this->cv->get();
        $this->cv->clear();

        $this->web_thems->parent = "0";
        $this->web_thems->order_by = array("id" => "ASC");
        $this->web_thems->select = array("web_thems.*", "web_thems.img_" . $this->data['cv']->lang . " as img");

        // $this->web_thems->join = array("web_thems w2" => "w2.parent = web_thems.id");
        $this->data['web_thems'] = $this->web_thems->get();
        $this->web_thems->clear();


        //echo $this->db->last_query();

        $newsendlogs = array();

        foreach ($this->data['web_thems'] as $item) {
            $newdata = array();
            $newdata['sendlogs'] = $item;

            $this->web_thems->parent = $item->id;
            $newdata['web_thems_child'] = $this->web_thems->get();
            $newsendlogs[] = $newdata;
        }
        $this->data['query'] = $newsendlogs;

        $this->web_thems->order_by = array("id" => "ASC");
        $this->web_thems->limit = "1";
        $this->data['web_themsdefault'] = $this->web_thems->get();
        $this->web_thems->clear();
        //
        $this->data['cv_id'] = $cv_id;
        $this->user_info->cv_id = $cv_id;
        $user_info = $this->user_info->get();
        $this->data['userinfo'] = $user_info;
        
        $this->data['step'] = 8;
        $percent = $this->data['cv']->percent;
        $this->data['percent'] = $percent;
        
        $this->load->view('steps', $this->data);
        $this->load->view('progress_bar', $this->data);
        $this->load->view("web_thems", $this->data);
    }

    function save_webthem() {
        $style_id = $this->input->post("id");
        $cv_id = $this->input->post("cv_id");

        if (isset($_POST['button_next'])) {

            $this->cv->id = $cv_id;
            $this->cv->style_them = $style_id;
            $this->cv->save();
            $this->cv->clear();
            redirect("finish/index/" . $cv_id);
        } elseif (isset($_POST['button_save'])) {
            $this->cv->id = $cv_id;
            $this->cv->style_them = $style_id;
            $this->cv->save();
            $this->cv->clear();
            redirect("thems/web_thems/" . $cv_id);
        } else {
            redirect("thems/index/" . $cv_id);
        }
    }

    /* function finish($cv_id){
      $this->cv->id=$cv_id;
      $this->data['cv']=$this->cv->get();
      $this->cv->clear();

      $user_id=$this->session->userdata('userid') ;
      $this->user->id = $user_id;
      $this->data['user'] = $this->user->get();
      $this->user->clear();

      $percent=$this->data['cv']->percent;
      $status=$this->data['user']->status;
      if($status==0 || $percent<35) {
      $this->session->set_flashdata('result', lang('publish_error'));
      redirect("thems/web_thems");
      }else{
      $this->load->view("finish");
      }


      } */

    function preview($cv_id = false, $style_id = false) {
        $this->layout = "ajax";
        $user_id = $this->session->userdata('userid');
        if (!$cv_id)
            showx_404();
        //if(!$style_id) showx_404();
        $this->cv->id = $cv_id;
        $this->cv->user_id = $user_id;
        $this->data['iscv'] = $this->cv->get();
        if (!$this->data['iscv'])
            showx_404();
        $this->cv->clear();

        $this->user->id = $user_id;
        $this->data['user'] = $this->user->get();
        $this->user->clear();

        $folder_name = 'u_' . $user_id;

        $this->load->library('pdf_library');

        //$pdf->SetAutoFont(AUTOFONT_RTL);

        $this->user_info->cv_id = $cv_id;
        $this->data['user_info'] = $this->user_info->get();
        $this->user_info->clear();
        //var_dump($this->data['user_info']);
        //get cv data
        $this->cv->user_id = $user_id;
        $this->cv->id = $cv_id;
        $this->data['cv'] = $this->cv->get();
        $file_name = $this->data['cv']->title;
        $path = 'assets/' . $folder_name . '/' . 'cv.pdf';
        $this->cv->clear();

        //get phones
        $this->phones->cv_id = $cv_id;
        $this->data['phones'] = $this->phones->get();
        $this->phones->clear();

        //get education
        $this->education->cv_id = $cv_id;
        $this->education->select = array("education.*", "education_type.type_" . $this->data['cv']->lang . " as type");
        $this->education->join = array("education_type" => "education_type.id = education.type_id");
        $this->data['education'] = $this->education->get();
        $this->education->clear();

        //get courses
        $this->courses->cv_id = $cv_id;
        $this->data['courses'] = $this->courses->get();
        $this->courses->clear();

        //get certifcates
        $this->certifcates->cv_id = $cv_id;
        $this->data['certifcates'] = $this->certifcates->get();
        $this->certifcates->clear();

        //get experience
        $this->experience->cv_id = $cv_id;
        $this->data['experience'] = $this->experience->get();
        $this->experience->clear();

        //get projects
        $this->projects->cv_id = $cv_id;
        $this->data['projects'] = $this->projects->get();
        $this->projects->clear();

        //get langs
        $this->languages->cv_id = $cv_id;
        $this->data['langs'] = $this->languages->get();
        $this->languages->clear();

        //get skills
        $this->skills->cv_id = $cv_id;
        $this->data['skills'] = $this->skills->get();
        $this->skills->clear();

        //get qualifications
        $this->qual_user->cv_id = $cv_id;
        $this->qual_user->select = array("qual_user.*", "qualifications.name_" . $this->data['cv']->lang . " as name");
        $this->qual_user->join = array("qualifications" => "qualifications.id =qual_user.qual_id");
        $this->data['qualifications'] = $this->qual_user->get();
        $this->qual_user->clear();

        //get refrences
        $this->references->cv_id = $cv_id;
        $this->data['references'] = $this->references->get();
        $this->references->clear();

        //get social links
        $this->social->cv_id = $cv_id;
        $this->data['social'] = $this->social->get();
        $this->social->clear();

        if ($this->data['cv']->lang == 'en') {
            $this->lang->load('cvdata', 'english');
        } elseif ($this->data['cv']->lang == 'ar') {
            $this->lang->load('cvdata', 'arabic');
        }


        //get pdf_them
        $this->pdf_thems->id = $style_id;
        $this->data['pdf_thempage'] = $this->pdf_thems->get();
        $this->pdf_thems->clear();

        if (empty($this->data['pdf_thempage'])) {
            $this->pdf_thems->order_by = array("id" => "ASC");
            $this->pdf_thems->limit = "1";
            $this->data['pdf_themsdefault'] = $this->pdf_thems->get();
            $this->pdf_thems->clear();
            $them = $this->data['pdf_themsdefault'][0]->file_name;
        } else
            $them = $this->data['pdf_thempage']->file_name;

        $html = $this->load->view($them, $this->data, true);
        if ($them == 'them4' || $them == 'them5') {
            $pdf = $this->pdf_library->load('utf-8', 'A4', "", "", 0, 0, 0, 0, 0, 0, 'P');

            $pdf->AddPage('p', '', '', '', '', 0, 0, 0, 0, 0, 0, '', '', '', '', 0, 0, 0, 0, 'side-bar');
        } elseif (($them != 'them1') && ($them != 'them2')) {
            $pdf = $this->pdf_library->load();
            $pdf->AddPage('P', // L - landscape, P - portrait
                    '', '', '', '', 0, // margin_left
                    0, // margin right
                    0, // margin top
                    0, // margin bottom
                    0, // margin header
                    0); // margin footer
        } else {
            $pdf = $this->pdf_library->load();
        }

        $pdf->SetDisplayMode('fullpage');
        $pdf->SetTitle($this->data['user_info'][0]->user_cv_name);
        $pdf->WriteHTML($html); // write the HTML into the PDF
        $pdf->Output("cv", "I");
    }

    public function preview_webthem($cv_id = false, $web_them = false) {
        $this->layout = "ajax";
        $user_id = $this->session->userdata('userid');
        if (!$cv_id)
            showx_404();
        //if(!$style_id) showx_404();
        $this->cv->id = $cv_id;
        $this->cv->user_id = $user_id;
        $this->data['iscv'] = $this->cv->get();
        if (!$this->data['iscv'])
            showx_404();
        $this->cv->clear();

        $this->user->id = $user_id;
        $this->data['user'] = $this->user->get();
        $this->user->clear();
        $this->data['user_id'] = $user_id;

        $this->data['user_name'] = $this->data['user']->user_name;

        //get cv data
        $this->cv->id = $cv_id;
        $this->data['cv_data'] = $this->cv->get();
        $this->cv->clear();

        $this->cv->user_id = $user_id;
        $this->cv->url_title = $this->data['cv_data']->url_title;
        $this->data['cv'] = $this->cv->get();
        $this->cv->clear();

        $this->user_info->cv_id = $cv_id;
        $this->data['user_info'] = $this->user_info->get();
        $this->user_info->clear();


        //get phones
        $this->phones->cv_id = $cv_id;
        $this->data['phones'] = $this->phones->get();
        $this->phones->clear();

        //get education
        $this->education->cv_id = $cv_id;
        $this->education->select = array("education.*", "education_type.type_" . $this->data['cv'][0]->lang . " as type");
        $this->education->join = array("education_type" => "education_type.id = education.type_id");
        $this->data['education'] = $this->education->get();
        $this->education->clear();

        //get courses
        $this->courses->cv_id = $cv_id;
        $this->data['courses'] = $this->courses->get();
        $this->courses->clear();

        //get certifcates
        $this->certifcates->cv_id = $cv_id;
        $this->data['certifcates'] = $this->certifcates->get();
        $this->certifcates->clear();

        //get experience
        $this->experience->cv_id = $cv_id;
        $this->data['experience'] = $this->experience->get();
        $this->experience->clear();

        //get projects
        $this->projects->cv_id = $cv_id;
        $this->data['projects'] = $this->projects->get();
        $this->projects->clear();

        //get langs
        $this->languages->cv_id = $cv_id;
        $this->data['langs'] = $this->languages->get();
        $this->languages->clear();

        //get skills
        $this->skills->cv_id = $cv_id;
        $this->data['skills'] = $this->skills->get();
        $this->skills->clear();

        //get qualifications
        $this->qual_user->cv_id = $cv_id;
        $this->qual_user->select = array("qual_user.*", "qualifications.name_" . $this->data['cv'][0]->lang . " as name");
        $this->qual_user->join = array("qualifications" => "qualifications.id =qual_user.qual_id");
        $this->data['qualifications'] = $this->qual_user->get();
        $this->qual_user->clear();

        //get refrences
        $this->references->cv_id = $cv_id;
        $this->data['references'] = $this->references->get();
        $this->references->clear();

        //get social links
        $this->social->cv_id = $cv_id;
        $this->data['social'] = $this->social->get();
        $this->social->clear();

        if ($this->data['cv'][0]->lang == 'en') {
            $this->lang->load('cvdata', 'english');
        } elseif ($this->data['cv'][0] == 'ar') {
            $this->lang->load('cvdata', 'arabic');
        }


        if ($this->data['cv'][0]->lang == 'en') {
            $this->lang->load('cvdata', 'english');
        } elseif ($this->data['cv'][0]->lang == 'ar') {
            $this->lang->load('cvdata', 'arabic');
        }

        //get recomnds
//        $this->cv_recommendation->cv = $cv_id;
//        $this->cv_recommendation->select = array("cv_recommendation.*", "recommend.name as name", "recommend.date as date", "recommend.job as job", "recommend.recommend as recommend", "recommend.job as job", "recommend.id as id");
//        $this->cv_recommendation->join = array("recommend" => "recommend.id = cv_recommendation.recomnd_id");
//        $this->data['recommends'] = $this->cv_recommendation->get();
//        $this->cv_recommendation->clear();

        $this->recommend->cv = $cv_id;
        $this->recommend->status = '1';
        $this->data['recommends'] = $this->recommend->get();
        $this->recommend->clear();

        //get web_them
        $this->web_thems->id = $web_them;
        $this->data['web_thempage'] = $this->web_thems->get();
        $this->web_thems->clear();
        if (empty($this->data['web_thempage'])) {
            $this->web_thems->order_by = array("id" => "ASC");
            $this->web_thems->limit = "1";
            $this->data['web_themsdefault'] = $this->web_thems->get();
            $this->web_thems->clear();
            $them = $this->data['web_themsdefault'][0]->file_name;
            $style_name = $this->data['web_themsdefault'][0]->style_name;
        } else {
            $them = $this->data['web_thempage']->file_name;
            $style_name = $this->data['web_thempage']->style_name;
        }
        $this->web_thems->clear();
        $this->data['style_name'] = $style_name;
        $this->load->view($them, $this->data);
    }

    function get_them() {
        $this->layout = "ajax";
        $them = $this->input->post("data");
        $this->web_thems->id = $them;
        $data = $this->web_thems->get();
        $this->web_thems->clear();

        $cv_id = $this->input->post("cv_id");
        $this->cv->id = $cv_id;
        $this->data['cv'] = $this->cv->get();
        $this->cv->clear();

        echo $data->id . '*' . $data->parent . "*" . $data->{"img_" . $this->data['cv']->lang};
    }

}
