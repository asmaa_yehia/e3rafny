<?php

class uploader {

    public $ctrl; // codeigniter controller object
    // params for upload library
    public $upload_path = false;
    public $allowed_types = false;
    public $max_size = false;
    public $overwrite = false;
    public $encrypt_name = false;
    public $max_width = false;
    public $max_height = false;
    public $image_info = false;
    // post-defined class properties
    public $relative_path;
    public $file_info;
    public $file_path = false;
    public $local_file;
    public $file_name = "";
    public $file_size;
    public $file_type;
    public $errors = array();

    public function error($error) {
        $this->errors[] = $error;
    }

    public function __construct($params) {

        $this->ctrl = & get_instance();
        $this->ctrl->load->helper("url");



        foreach ($params as $name => $value) {
            $this->$name = $value;
        }

        $this->check_upload_path();
        $this->relative_path = $this->upload_path;
        $this->upload_path = realpath($this->upload_path);
    }

    public function do_upload($field) {

        if (count($this->errors)) {
            $this->errors = array();
        }

        if (is_array($field)) {
            $file_info = $field;
        } else {
            $file_info = $_FILES[$field];
        }

        if ($file_info) {

            //$file_info = $_FILES[$field];
            $this->file_info = $file_info;
            $this->file_temp = $file_info["tmp_name"];
            $this->local_file = $file_info["name"];
            if ($this->file_name == "") {
                $this->file_name = $this->get_name($file_info["name"]);
            }
            $this->file_type = $this->get_extension($file_info["name"]);
            $this->file_size = round($file_info["size"] / 1024, 1);

            if ($this->encrypt_name) {
                $this->file_name = md5($this->file_name);
            }

            if ($this->max_size) {
                $this->check_max_size();
            }

            if ($this->allowed_types) {
                $this->check_file_type();
            }

            if (!$this->overwrite) {
                $this->check_same_name();
            }

            $this->file_path = rtrim($this->upload_path) . DIRECTORY_SEPARATOR . $this->file_name . "." . $this->file_type;



            $this->check_image_dimensions();


            if (empty($this->errors)) {
                if (!move_uploaded_file($this->file_info['tmp_name'], $this->file_path)) {
                    $this->error('File could not be uploaded');
                    return false;
                } else {
                    return true;
                }
            }
        }
    }

    public function check_upload_path() {
        if (!$this->upload_path) {
            $this->error("upload path is not specified");
        } else {

            if (!is_dir($this->upload_path)) {
                $this->error("upload path is not valid: " . $this->upload_path);
                return false;
            }

            if (!is_writable($this->upload_path)) {
                $this->error("upload path is not writable: " . $this->upload_path);
                return false;
            }

            return true;
        }
    }

    public function is_image() {
        $allowed_image_types = array("jpg", "jpeg", "png", "gif", "bmp");
        if (in_array($this->file_type, $allowed_image_types)) {
            return true;
        } else {
            return false;
        }
    }

    public function check_image_dimensions() {

        if (!$this->is_image()) {
            return;
        }
        $image_info = getimagesize($this->file_info['tmp_name']);
        $this->image_info = $image_info;
        if ($image_info) {
            if ($this->max_width > 0 AND $image_info[0] > $this->max_width) {
                $this->error("image width is too large");
            }

            if ($this->max_height > 0 AND $image_info[1] > $this->max_height) {
                $this->error("image height is too large");
            }
        } else {
            $this->error("cannot get dimensions of image : " . $this->file_info['tmp_name']);
        }
    }

    public function get_extension($filename) {
        $x = explode('.', $filename);
        return end($x);
    }

    public function get_name($filename) {

        $array = explode(".", $filename);
        $type = strtolower($array[count($array) - 1]);
        unset($array[count($array) - 1]);
        return implode(".", $array);
    }

    function check_same_name() {
        $path = rtrim($this->upload_path) . DIRECTORY_SEPARATOR;
        $filename = $this->file_name;

        if (!file_exists($path . $filename . "." . $this->file_type)) {
            return;
        }
        $i = 1;
        while ($i >= 1) {
            if (!file_exists($path . $filename . "_" . $i . "." . $this->file_type)) {
                $new_filename = $filename . "_" . $i;
                break;
            }
            $i++;
        }

        $this->file_name = $new_filename;
    }

    function check_max_size() {
        if ($this->file_size > $this->max_size) {
            $this->error('large file size selected');
            return false;
        }
    }

    function check_file_type() {
        $allowed_types = explode("|", $this->allowed_types);
        $allowed_types = $this->clean_array($allowed_types);
        if (!in_array($this->file_type, $allowed_types)) {
            $this->error('Un-allowed file type was selected');
            return false;
        }
    }

    function clean_array($array) {
        $modified_array = array();
        foreach ($array as $value) {
            $modified_array[] = strtolower(trim($value));
        }
        return $modified_array;
    }

    public function data() {

        $attributes = array();
        if (file_exists($this->file_path)) {
            $attributes["full_path"] = $this->file_path;
            $attributes["full_url"] = $this->cleanPath($this->relative_path . "/" . $this->file_name . "." . $this->file_type);
        }
        $attributes["file_type"] = $this->file_type;
        $attributes["file_size"] = $this->file_size;

        $attributes["orig_name"] = $this->file_info['name'];


        if (file_exists($this->file_path)) {
            $attributes["raw_name"] = $this->file_name;
            $attributes["client_name"] = $this->file_name . "." . $this->file_type;
        }

        if ($this->is_image($this->file_path)) {
            $attributes["is_image"] = 1;
            $attributes["image_width"] = $this->image_info[0];
            $attributes["image_height"] = $this->image_info[1];
            $attributes["image_size_str"] = $this->image_info[3];
        } else {
            $attributes["is_image"] = 0;
        }

        return $attributes;
    }

    function cleanPath($path) {
        $result = array();
        // $pathA = preg_split('/[\/\\\]/', $path);
        $pathA = explode('/', $path);
        if (!$pathA[0])
            $result[] = '';
        foreach ($pathA AS $key => $dir) {
            if ($dir == '..') {
                if (end($result) == '..') {
                    $result[] = '..';
                } elseif (!array_pop($result)) {
                    $result[] = '..';
                }
            } elseif ($dir && $dir != '.') {
                $result[] = $dir;
            }
        }
        if (!end($pathA))
            $result[] = '';
        return implode('/', $result);
    }

    public function http_file_path() {
        return APPURL . "uploads/" . $this->upload_dir . $this->file_name;
    }

    function delete($file_name) {
        $this->file_path = $this->file_dir . $fileName;
        if (is_file($this->file_path)) {
            unlink($this->file_path);
            return 'Your file was successfully deleted';
        } else {
            return 'No such file exists: ' . $this->file_path;
        }
    }

}

?>