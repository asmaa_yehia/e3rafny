<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('site_url'))
{
    function site_url($uri = '')
    {
        $CI = &get_instance();
        if (defined('LANG'))
            return $CI->config->site_url(LANG . '/' . $uri);
        else
            return $CI->config->site_url($uri);
    }
}