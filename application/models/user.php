<?php
class user extends MY_Model{
	
	function __construct(){
		$this->table = "user";
		parent::start();
	}
	
	 public function login($remember=false) {
		
        if ($this->session->userdata('userid') || $this->input->cookie('userid', true) == true){
			if($this->input->cookie('userid', true) == true){
				$id=$this->input->cookie('userid',TRUE);
				$mail=$this->input->cookie('mail',TRUE);
				$this->session->set_userdata(array('userid' => $id, 'mail' => $mail));
				return TRUE;
			}else{
				return TRUE;
			}
		}
		
        elseif ($this->mail && $this->password) {
            

            $allusers = $this->get();

            $user = array_shift($allusers);

            if (isset($user) && $user->password == $this->password) {

                $this->session->set_userdata(array('userid' => $user->id, 'mail' => $this->mail));
				
				if($remember)
   				{
					$cookie = array( 'name' => 'userid', 'value' => $user->id, 'expire' => 86500 );
					$this->input->set_cookie($cookie); 
					
					$cookie2 = array( 'name' => 'mail', 'value' => $this->mail, 'expire' => 86500 );
					$this->input->set_cookie($cookie2); 
					
    			}
   				
				
                return TRUE;
            }
        }

        return FALSE;
    }

	function del_all($id){
		$this->db->where('user_id',$id);
		$query = $this->db->get('cv');

		foreach ($query->result() as $row)
		{
			$cv_id=$row->id;
			$this->db->where('cv_id',$cv_id)->delete('courses');
			$this->db->where('cv_id',$cv_id)->delete('education');
			$this->db->where('cv_id',$cv_id)->delete('experience');
			$this->db->where('cv_id',$cv_id)->delete('lang');
			$this->db->where('cv_id',$cv_id)->delete('projects');
			$this->db->where('cv_id',$cv_id)->delete('references');
			$this->db->where('cv_id',$cv_id)->delete('skills');
                        $this->db->where('cv',$id)->delete('recommend');
			
		}
		$this->db->where('user_id',$id)->delete('social');
		$this->db->where('user_id',$id)->delete('cv');
		$this->db->where('user_id',$id)->delete('phones');
//		$this->db->where('user_id',$id)->delete('recommend');
		$this->db->where('user_id',$id)->delete('user_info');

	}
	function get_autocomplete(){
	$this->db->select('*');
	$this->db->like('first_name',$this->input->post('queryString'));
	return $this->db->get('user');    
	}
	
		
}