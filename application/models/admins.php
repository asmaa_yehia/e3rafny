<?php
class admins extends MY_Model{
	
	function __construct(){
		$this->table = "admins";
		parent::start();
	}
	
	public function login($remember=false) {
		
        if ($this->session->userdata('adminid') || $this->input->cookie('adminid', true) == true){
			if($this->input->cookie('adminid', true) == true){
				$id=$this->input->cookie('adminid',TRUE);
				$mail=$this->input->cookie('mail',TRUE);
				$this->session->set_userdata(array('adminid' => $id, 'mail' => $mail));
				return TRUE;
			}else{
				return TRUE;
			}
		}	
		
        elseif ($this->mail && $this->password) {
            
			 $this->status = '1';
            $allusers = $this->get();

            $user = array_shift($allusers);

            if (isset($user) && $user->password == $this->password) {

                $this->session->set_userdata(array('adminid' => $user->id, 'mail' => $this->mail));
				
				if($remember)
   				{
        			$cookie = array( 'name' => 'adminid', 'value' => $user->id, 'expire' => 86500 );
					$this->input->set_cookie($cookie); 
					
					$cookie2 = array( 'name' => 'mail', 'value' => $this->mail, 'expire' => 86500 );
					$this->input->set_cookie($cookie2); 
    			}
   				
				
                return TRUE;
            }
        }

        return FALSE;
    }
	
		
}