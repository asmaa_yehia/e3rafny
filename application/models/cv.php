<?php
class cv extends MY_Model{
	
	function __construct(){
		$this->table = "cv";
		$this->key = 'id';
		parent::start();
	}
	
	function del_all($cv_id){
		
		$this->db->where('cv_id',$cv_id)->delete('courses');
		$this->db->where('cv_id',$cv_id)->delete('education');
		$this->db->where('cv_id',$cv_id)->delete('experience');
		$this->db->where('cv_id',$cv_id)->delete('lang');
		$this->db->where('cv_id',$cv_id)->delete('projects');
		$this->db->where('cv_id',$cv_id)->delete('references');
		$this->db->where('cv_id',$cv_id)->delete('skills');
		$this->db->where('cv',$cv_id)->delete('cv_recommendation');
	}
	
	
		
}