<?php

class setting extends CI_Model {

    function __construct() 
    {
        parent::__construct();
        
        if (!defined('STYLE')) define ('STYLE','default');
        if (!in_array(strtolower($this->uri->segment(1)), array('ar', 'en'))){
			if (!defined('LANG'))
            define("LANG", "ar");
		}else{
		   if (!defined('LANG'))
           define("LANG", strtolower($this->uri->segment(1)));
		}
        $CI = &get_instance();
        $autoloadlang = array('basic', 'error');
        if (LANG === "en") {
            $CI->config->set_item('language', 'english');
            foreach ($autoloadlang as $langload)
                $this->lang->load($langload);
        } else {
            $CI->config->set_item('language', 'arabic');
            foreach ($autoloadlang as $langload)
                $this->lang->load($langload);
        }
    }
}
