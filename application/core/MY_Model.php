<?php

(defined('BASEPATH')) or exit('No direct script access allowed');

/**
 *
 * Super Model
 *
 * @uses CI_Model
 * @package CRUD for Codeigniter 
 *
 */
class MY_Model extends CI_Model
{

    /**
     * Table Name 
     * 
     * @var string 
     * 
     */
    var $table;

    /**
     * Primary Key 
     * 
     * @var string 
     * 
     */
    var $key = false;

    /**
     * Selected Colomns 
     * 
     * @var array 
     * 
     */
    var $select = array();
    var $distinct;

    /**
     * Query limit 
     * 
     * @var num 
     * 
     */
    var $limit;

    /**
     * Query offset 
     * 
     * @var num 
     * 
     */
    var $offset = 0;

    /**
     * Query Where 
     * 
     * @var num 
     * 
     */
    var $where = array();
    // array('date > ' => 'Value');
    var $like = array();
    var $like_wildcard = 'both';
    var $where_in = array();
    var $or_like = array();
    var $or_where = array();


    /**
     * Query order 
     * 
     * @var array 
     * 
     */

    var $sp_order_by = array();
    var $order_by = array();

    /**
     * Query join 
     * 
     * @var array 
     * 
     */
    var $join_type = false;

    var $join = array();


    var $group = array();
    /**
     * start 
     *
     * This function load class with table information
     *
     * @access  public
     *
     */
    public function start()
    {
        parent::__construct();
        $this->fields = $this->db->list_fields($this->table);
        foreach ($this->fields as $field)
        {
            $this->$field = null;
        }
        if (!$this->key)
        {
            $this->key = $this->fields[0];
        }
    }

    /**
     * get 
     *
     * This function get entries from database
     *
     * @access  public
     * @return  array
     */
    public function get($count = false)
    {

        $fields = $this->fields;

        if (count($this->select))
        {
            foreach ($this->select as $field)
            {

                $this->db->select($field);

            }
        }

        foreach ($fields as $field)
        {
            if ($this->$field != null)
            {
                $this->db->where($this->table . '.' . $field, $this->$field);
            }
        }

        if (count($this->group) > 0)
        {
            foreach ($this->group as $field)
                $this->db->group_by($field);
        }
        ////////////

        if (count($this->where) > 0)
        {
            //foreach ($this->where as $field=>$value)
            $this->db->where($this->where);
        }

        if (count($this->where_in) > 0)
        {
            foreach ($this->where_in as $field => $value)
                $this->db->where_in($field, $value);
        }

        if (count($this->or_where) > 0)
        {
            foreach ($this->or_where as $field => $value)
                $this->db->or_where($field, $value);
        }

        if (count($this->like) > 0)
        {
            foreach ($this->like as $field => $value)
            {
                $this->db->like($field, $value, $this->like_wildcard);
            }
        }

        if (count($this->or_like) > 0)
        {
            $or_count = 1;
            $orlike = "";
            foreach ($this->or_like as $field => $value)
            {
                if ($or_count === 1)
                {
                    $orlike = "$field like '%$value%'";
                } else
                    $orlike .= " OR $field like '%$value%'";
                $or_count++;
            }
            $this->db->where("($orlike)");
        }
        ////////////

        if ($this->distinct)
            $this->db->distinct();
        /////////////

        if (count($this->join))
        {
            foreach ($this->join as $table => $condition)
            {
                if ($this->db->table_exists($table))
                {
                    if ($this->join_type)
                    {
                        $this->db->join($table, $condition, $this->join_type);
                    } else
                    {
                        $this->db->join($table, $condition);
                    }
                }
            }
        }

        if ($count == true)
        {
            return $this->db->count_all_results($this->table);
        }

        if (count($this->order_by))
        {
            foreach ($this->order_by as $field => $order)
            {
                if (in_array(strtoupper($order), array(
                    "DESC",
                    "ASC",
                    "RANDOM")))
                {
                    $this->db->order_by($field, $order);
                }
            }
        }

        if (count($this->sp_order_by))
        {
            foreach ($this->sp_order_by as $field => $order)
            {
                if (in_array(strtoupper($order), array(
                    "DESC",
                    "ASC",
                    "RANDOM")))
                {
                    $this->db->order_by($field, $order);
                }
            }
        }

        $result = $this->db->get($this->table, $this->limit, $this->offset);

        $key = $this->key;
        if ($this->$key)
        {
            return $result->row();
        } else
        {
            return $result->result();
        }
    }

    /**
     * save 
     *
     * This function update an entry based on primary key and also insert a new entry
     *
     * @access  public
     * @return  boolean  True if successful / False if not
     */
    public function save($update = FALSE)
    {
        $fields = $this->fields;

        foreach ($fields as $field)
        {
            if ($this->$field !== null)
            {
                $this->db->set($this->table . '.' . $field, $this->$field);
            }
        }

        $key = $this->key;
        if ($this->$key || $update == true)
        {
            if ($this->$key)
                $operation = $this->db->where($key, $this->$key)->update($this->table);
            else
                $operation = $this->db->where($this->where)->update($this->table);
        } else
        {
            $operation = $this->db->insert($this->table);
        }

        if ($operation)
        {
            return true;
        } else
        {
            return false;
        }
    }

    /**
     * delete 
     *
     * This function delete an entry based on primary key
     *
     * @access  public
     * @return  boolean  True if successful / False if not
     */
    public function delete()
    {
        $del = false;
        $fields = $this->fields;
        foreach ($fields as $field)
        {
            if ($this->$field != null)
            {
                $this->db->where($this->table . '.' . $field, $this->$field);
                $del = true;
            }
        }

        if ($del)
        {
            if ($this->db->delete($this->table))
            {
                return true;
            } else
            {
                return false;
            }
        }
    }

    public function clear()
    {
        foreach ($this->fields as $field)
        {
            $this->$field = null;
        }
    }
    function clean()
    {
        $this->clear();
        $this->select = array();
        $this->limit = false;
        $this->offset = 0;
        $this->where = array();
        $this->order_by = array('id' => 'desc');
        $this->join = array();
    }

}
