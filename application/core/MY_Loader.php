<?php
class MY_Loader extends CI_Loader {
    
    var $_ci_view_paths = array();
    
    function __construct() {
        parent::__construct();
    }
    
    function view($view, $vars = array(), $return = FALSE) {
        $ext = pathinfo($view , PATHINFO_EXTENSION);
        $view = ($ext == '')? $view.".php" : $view;
        
        if(LOCATION == "FRONT-END")
            $this->_ci_view_paths = array("styles/".STYLE."/"=> true);
        elseif(LOCATION == "BACKSTAGE")
            $this->_ci_view_paths = array(APPPATH."views/backstage/"=> true);
         
        return parent::view($view, $vars, $return);
    }
}