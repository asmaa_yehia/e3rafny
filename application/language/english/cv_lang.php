<?php
$lang['single'] = 'Single';
$lang['married'] = 'Married';
$lang['divercoed'] = 'Divorced';
//grades
$lang['fluent'] = 'Fluent';
$lang['interemdate'] = 'Intermediate';
$lang['weak'] = 'weak';
//
$lang['title']="Title";
$lang['language']="Language";
$lang['name']="Name";
$lang['dept']="Department";
$lang['desc']="Description";
$lang['next']="Next";
$lang['prev']="Last";
$lang['edu']="Education";
$lang['courses']="Courses";
$lang['exp']="Experience";
$lang['projects']="Projects";
$lang['skills']="Technical Skills";
$lang['refrences']="Refrences";
$lang['type']="Type";
$lang['phone']="Phone";
$lang['mail']="Email";
$lang['link']="Link";
$lang['grade']="Grade";
$lang['objective']="Objective";
$lang['alter']="Another E-mail";
//
$lang['exemption']="Exemption";
$lang['Complete_service']="Complete service";
$lang['Postponed']="Postponed";
$lang['Currently_serving']="Currently serving";
$lang['Doesnt_apply']="Doesnot apply";
//

$lang['cofirm_delete'] = 'Are you sure you want to delete this cv?';
$lang['done'] = 'Your action is done';

$lang['exist_title']="This title is already used";
$lang['social']="Social Links";
$lang['female']="female";
$lang['male']="male";
$lang['gender']="Gender";
$lang['licence']="Licence";
$lang['chooce_social']="Choose Site";
$lang['chooce_year']="Chooce Year";
$lang['chooce_month']="Chooce Month";
$lang['chooce_type']="Chooce Type";
$lang['exp_year']="Experience Years";
//
//lang
$lang['poor']="Poor";
$lang['fair']="Fair";
$lang['good']="Good";
$lang['very_good']="very Good";
$lang['tongue']="Tongue";

$lang['choose_grade']="Choose Grade";
$lang['choose_pdf']="Choose PDF Theme";
$lang['choose_web']="Choose Web Theme";

//languages
$lang['Afrikaans']="Afrikaans";
$lang['Albanian']="Albanian";
$lang['Arabic']="Arabic";
$lang['Armenian']="Armenian";
$lang['Basque']="Basque";
$lang['Bengali']="Bengali";
$lang['Bulgarian']="Bulgarian";
$lang['Catalan']="Catalan";
$lang['Chinese']="Chinese";
$lang['Cambodian']="Cambodian";
$lang['Croatian']="Croatian";
$lang['Czech']="Czech";
$lang['Danish']="Danish";
$lang['Dutch']="Dutch";
$lang['English']="English";
$lang['Estonian']="Estonian";
$lang['Fiji']="Fiji";
$lang['Finnish']="Finnish";
$lang['French']="French";
$lang['Georgian']="Georgian";
$lang['German']="German";
$lang['Greek']="Greek";
$lang['Gujarati']="Gujarati";
$lang['Hebrew']="Hebrew";
$lang['Hindi']="Hindi";
$lang['Hungarian']="Hungarian";
$lang['Icelandic']="Icelandic";
$lang['Indonesian']="Indonesian";
$lang['Irish']="Irish";
$lang['Italian']="Italian";
$lang['Japanese']="Japanese";
$lang['Korean']="Korean";
$lang['Latin']="Latin";
$lang['Mongolian']="Mongolian";
$lang['Nepali']="Nepali";
$lang['Norwegian']="Norwegian";
$lang['Persian']="Persian";
$lang['Polish']="Polish";
$lang['French']="Portuguese";
$lang['Russian']="Russian";
$lang['Irish']="Irish";
$lang['Spanish']="Spanish";
$lang['Swahili']="Swahili";
$lang['Swedish']="Swedish";
$lang['Turkish']="Turkish";
$lang['Ukrainian']="Ukrainian";
$lang['Urdu']="Urdu";
$lang['Uzbek']="Uzbek";
$lang['Vietnamese']="Vietnamese";
//
$lang['choose_gender']="Choose Gender";
$lang['military']="Military";
$lang['yes']="Yes";
$lang['no']="No";
$lang['choose_licence']="Licence";
$lang['education']="Education";
$lang['company']="company";
$Lang['role']="Role";
$lang['now']="Till now";
$lang['personal_info']="Personal Info";
$lang['image_setting']="Image Setting";
$lang['last']="Pervious step";
$lang['validate_special']=" Enter Correct Data";
$lang['cv_empty']="User name is required";
$lang['cv_finish']="you completed your cv sucessfully";
$lang['download']="PDf ready to print";
$lang['download_edit']="*You can edit and save your CV from";
$lang['link_edit']=" *This link generated automatically, You can modify it by change username";
$lang['cv_sahre']="Publich your CV";
$lang['here']="Here";
//
$lang['no_exp']="No of experience";
$lang['years']="years";
$lang['contacts'] = 'Contacts';
$lang['and'] = ' & ';
$lang['emaill'] = 'Email';
$lang['personalinfo']="Personal info";
$lang['skils'] = 'Skills';
//
$lang['prev_cv']="Pervious CV";
$lang['home_title']="You can view any cv,edit or republish";
$lang['comp_precent']="*Percent is completed by percent ";
$lang['can_publish']=" % and you can publish and print";
$lang['cv_publish']="Publish You cv";
$lang['pdf']="PDF version";
$lang['edit_cv']="Modify your cv data";
$lang['or_delete']="or delete";
$lang['cv_url']="Your CV Link";